<?php
/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different URLs to chosen controllers and their actions (functions).
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

use Cake\Core\Plugin;
use Cake\Routing\RouteBuilder;
use Cake\Routing\Router;
use Cake\Routing\Route\DashedRoute;
use App\Model\Table\UsersTable;

/**
 * The UUID Pattern used by CakePHP;
 */
const PATTERN_UUID = '[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}';

/**
 * The integer ID pattern (SQL DB)
 */
const PATTERN_INTEGER_ID = '[0-9]{1,11}';

Router::defaultRouteClass(DashedRoute::class);

Router::scope('/', function (RouteBuilder $routes) {

    $routes->connect('/', ['controller' => 'Announces', 'action' => 'index'],
        ['_name' => 'home']);

    $routes->connect('/login',
        ['controller' => 'Users', 'action' => 'login'],
        ['_name' => 'login']
    );

    $routes->connect('/register',
        ['controller' => 'Users', 'action' => 'register'],
        ['_name' => 'register']
    );

    $routes->connect('/logout',
        ['controller' => 'Users', 'action' => 'logout'],
        ['_name' => 'logout']
    );


    Router::scope('/messages', ['controller' => 'Messages'], function ($routes) {
        $routes->connect('/',
            ['action' => 'index'],
            [
                '_name' => 'messagesOfUser',
            ]
        );

        $routes->connect('/inbox',
            ['action' => 'indexDetailed'],
            [
                '_name' => 'messagesOfUserDetailed',
            ]
        );

        $routes->connect('/view/:id',
            ['action' => 'view'],
            [
                '_name' => 'viewMessage',
                'pass' => ['id'],
                'id' => PATTERN_UUID,
            ]
        );

        $routes->connect('/edit/:id',
            ['action' => 'edit'],
            [
                '_name' => 'editMessage',
                'pass' => ['id'],
                'id' => PATTERN_UUID,
            ]
        );

        $routes->connect('/delete/:id',
            ['action' => 'delete'],
            [
                '_name' => 'deleteMessage',
                'pass' => ['id'],
                'id' => PATTERN_UUID,
            ]
        );

        $routes->connect('/send/:recipient_id',
            ['action' => 'send'],
            [
                '_name' => 'sendMessage',
                'pass' => ['recipient_id'],
                'recipient_id' => PATTERN_UUID,
            ]
        );

        $routes->scope('/conversations',
            function ($routes) {
                $routes->connect('/:recipient_id',
                    ['action' => 'conversation'],
                    [
                        '_name' => 'conversation',
                        'pass' => ['recipient_id'],
                        'recipient_id' => PATTERN_UUID,
                    ]
                );

                $routes->connect('/:recipient_id/:direction/:datetime',
                    ['action' => 'navigateConversation'],
                    [
                        '_name' => 'navigateConversation',
                        'pass' => ['recipient_id', 'direction', 'datetime'],
                        'recipient_id' => PATTERN_UUID,
                        'datetime' => '[0-9]{4}-[0-9]{2}-[0-9]{2}_[0-9]{2}:[0-9]{2}:[0-9]{2}',
                        'direction' => 'next|prev',
                    ]
                );

                $routes->connect('/mark-seen/:recipient_id',
                    ['action' => 'markSeen'],
                    [
                        '_name' => 'markSeenConversation',
                        'pass' => ['recipient_id'],
                        'recipient_id' => PATTERN_UUID,
                    ]
                );
            }
        );
    });

    Router::scope('/files', function ($routes) {
        $routes->connect('/view/:id',
            ['controller' => 'Files', 'action' => 'view'],
            [
                '_name' => 'viewFile',
                'pass' => ['id'],
                'id' => PATTERN_UUID,
            ]
        );
    });

    Router::scope('/users', function ($routes) {

        $routes->connect('/delete-my-account',
            ['controller' => 'Validations', 'action' => 'deleteAccount'],
            ['_name' => 'requestDeleteAccount']
        );

        $routes->connect('/delete-my-account/:code',
            ['controller' => 'Validations', 'action' => 'confirmDeleteAccount'],
            [
                '_name' => 'validateDeleteAccount',
                'pass' => ['code'],
                'code' => PATTERN_UUID
            ]
        );

        $routes->connect('/validate',
            ['controller' => 'Validations', 'action' => 'validateEmail'],
            ['_name' => 'requestValidationEmail']
        );

        $routes->connect('/validate/:email/:code',
            ['controller' => 'Validations', 'action' => 'confirmEmail'],
            [
                '_name' => 'validateEmail',
                'pass' => ['email', 'code'],
                'code' => PATTERN_UUID
            ]
        );

        $routes->connect('/retrieve-password',
            ['controller' => 'Validations', 'action' => 'retrievePassword'],
            ['_name' => 'requestNewPassword']
        );

        $routes->connect('/retrieve-password/:email/:code',
            ['controller' => 'Validations', 'action' => 'confirmPassword'],
            [
                '_name' => 'validateNewPassword',
                'pass' => ['email', 'code'],
                'code' => PATTERN_UUID
            ]
        );

        $routes->connect('/profile/:id',
            ['controller' => 'Users', 'action' => 'profile'],
            [
                '_name' => 'profile',
                'pass' => ['id'],
                'id' => PATTERN_UUID,
            ]
        );

        $routes->connect('/search',
            ['controller' => 'Users', 'action' => 'search'],
            [
                '_name' => 'searchUser',
            ]
        );
    });

    Router::scope('/profile', function ($routes) {
        $routes->connect('/update',
            ['controller' => 'Users', 'action' => 'update'],
            [
                '_name' => 'updateProfile',
            ]
        );

        $routes->connect('/skills',
            [
                'controller' => 'Users',
                'action' => 'skills',
                'rolesRequired' => [UsersTable::roles['student'], UsersTable::roles['professor'], UsersTable::roles['admin']],
            ],
            [
                '_name' => 'updateSkills',
            ]
        );
    });

    Router::scope('/announces', ['_namePrefix' => 'announces:', 'controller' => 'Announces'], function ($routes) {

        $routes->scope('/candidacies', ['_namePrefix' => 'candidacies:', 'controller' => 'Candidacies'],
            function ($routes) {

                $routes->connect('/postulate/:id',
                    [
                        'action' => 'postulate',
                        'rolesRequired' => [UsersTable::roles['student']]
                    ],
                    [
                        '_name' => 'postulate',
                        'pass' => ['id'],
                        'id' => PATTERN_INTEGER_ID,
                    ]
                );

                $routes->connect('/manage',
                    [
                        'action' => 'manage',
                        'rolesRequired' => [UsersTable::roles['student']]
                    ],
                    [
                        '_name' => 'manage'
                    ]
                );

                $routes->connect('/review/:announce_id',
                    [
                        'action' => 'review',
                        'rolesRequired' => [UsersTable::roles['company']]
                    ],
                    [
                        '_name' => 'review',
                        'pass' => ['announce_id'],
                        'announce_id' => PATTERN_INTEGER_ID,
                    ]
                );

                $routes->connect('/cancel/:announce_id/:student_id',
                    [
                        'action' => 'delete',
                        'rolesRequired' => [UsersTable::roles['student'], UsersTable::roles['company']]
                    ],
                    [
                        '_name' => 'cancel',
                        'pass' => ['student_id', 'announce_id'],
                        'announce_id' => PATTERN_INTEGER_ID,
                        'student_id' => PATTERN_UUID,
                    ]
                );

                $routes->connect('/confirm/:announce_id',
                    [
                        'action' => 'confirm',
                        'rolesRequired' => [UsersTable::roles['student']]
                    ],
                    [
                        '_name' => 'confirm',
                        'pass' => ['announce_id'],
                        'announce_id' => PATTERN_INTEGER_ID,
                    ]
                );

                $routes->connect('/accept/:announce_id/:student_id',
                    [
                        'action' => 'accept',
                        'rolesRequired' => [UsersTable::roles['company']]
                    ],
                    [
                        '_name' => 'accept',
                        'pass' => ['student_id', 'announce_id'],
                        'announce_id' => PATTERN_INTEGER_ID,
                        'student_id' => PATTERN_UUID,
                    ]
                );
            }
        );

        $routes->connect('/:id',
            [
                'action' => 'view',
            ],
            [
                '_name' => 'view',
                'pass' => ['id'],
                'id' => PATTERN_INTEGER_ID,
            ]);

        $routes->connect('/match',
            [
                'action' => 'match',
                'rolesRequired' => [UsersTable::roles['student']]
            ],
            ['_name' => 'match']
        );

        $routes->connect('/post',
            [
                'action' => 'post',
                'rolesRequired' => [UsersTable::roles['company']]
            ],
            ['_name' => 'post']
        );

        $routes->connect('/manage',
            [
                'action' => 'manage',
                'rolesRequired' => [UsersTable::roles['company']]
            ],
            ['_name' => 'manage']
        );

        $routes->connect('/edit/:id',
            ['action' => 'edit'],
            [
                '_name' => 'edit',
                'pass' => ['id'],
                'rolesRequired' => [UsersTable::roles['company']],
                'id' => PATTERN_INTEGER_ID,
            ]
        );

        $routes->connect('/archive/:id',
            ['action' => 'archive'],
            [
                '_name' => 'archive',
                'pass' => ['id'],
                'rolesRequired' => [UsersTable::roles['company']],
                'id' => PATTERN_INTEGER_ID,
            ]
        );

        $routes->connect('/delete/:id',
            ['action' => 'delete'],
            [
                '_name' => 'delete',
                'pass' => ['id'],
                'rolesRequired' => [UsersTable::roles['company']],
                'id' => PATTERN_INTEGER_ID,
            ]
        );
    });

    Router::scope('/traineeships', ['_namePrefix' => 'traineeships:', 'controller' => 'Traineeships'], function ($routes) {
        $routes->connect('/index',
            [
                'action' => 'index',
            ],
            ['_name' => 'index']
        );

        $routes->connect('/view/:id',
            [
                'action' => 'view',
            ],
            [
                '_name' => 'view',
                'pass' => ['id'],
                'id' => PATTERN_INTEGER_ID,
            ]
        );

        $routes->scope('/appreciations', ['_namePrefix' => 'appreciations:', 'controller' => 'Appreciations'], function ($routes) {

            $routes->connect('/add/:traineeship_id',
                [
                    'action' => 'add',
                ],
                [
                    '_name' => 'add',
                    'pass' => ['traineeship_id'],
                    'traineeship_id' => PATTERN_INTEGER_ID,
                ]
            );
        });
    });

    Router::scope('/api', ['_namePrefix' => 'api:'], function ($routes) {
        $routes->extensions(['json']);

        $routes->scope('/users', ['_namePrefix' => 'users:', 'controller' => 'Users'],
            function ($routes) {
                $routes->connect('/search',
                    ['action' => 'search'],
                    [
                        '_name' => 'search',
                    ]
                );
            }
        );

        $routes->scope('/messages', ['_namePrefix' => 'messages:', 'controller' => 'Messages'],
            function ($routes) {

                $routes->scope('/stats', ['_namePrefix' => 'stats:'],
                    function ($routes) {
                        $routes->connect('/notSeenConversations',
                            ['action' => 'notSeenConversations'],
                            [
                                '_name' => 'notSeenConversations',
                            ]
                        );
                    }
                );

                $routes->scope('/conversations', ['_namePrefix' => 'conversation:'],
                    function ($routes) {

                        $routes->connect('/:recipient_id/:direction/:datetime',
                            ['action' => 'navigateConversation'],
                            [
                                '_name' => 'navigate',
                                'pass' => ['recipient_id', 'direction', 'datetime'],
                                'recipient_id' => PATTERN_UUID,
                                'datetime' => '[0-9]{4}-[0-9]{2}-[0-9]{2}_[0-9]{2}:[0-9]{2}:[0-9]{2}',
                                'direction' => 'next|prev',
                            ]
                        );

                        $routes->connect('/:recipient_id/updated/:datetime',
                            ['action' => 'updated'],
                            [
                                '_name' => 'updated',
                                'pass' => ['recipient_id', 'datetime'],
                                'recipient_id' => PATTERN_UUID,
                                'datetime' => '[0-9]{4}-[0-9]{2}-[0-9]{2}_[0-9]{2}:[0-9]{2}:[0-9]{2}',
                            ]
                        );
                    }
                );

                $routes->connect('/last/:limit',
                    ['action' => 'index'],
                    [
                        '_name' => 'last',
                        'pass' => ['limit'],
                        'limit' => '[1-7]',
                    ]
                );

                $routes->connect('/send/:recipient_id',
                    ['action' => 'send'],
                    [
                        '_name' => 'send',
                        'pass' => ['recipient_id'],
                        'limit' => PATTERN_UUID,
                    ]
                );
            }
        );
    });

    Router::scope('/rss', ['_namePrefix' => 'rss:'], function ($routes) {
        $routes->extensions(['rss']);

        $routes->connect('/announces', ['controller' => 'Announces', 'action' => 'indexRSS'],
            ['_name' => 'announces:last']);
    });

    Router::prefix('admin', ['_namePrefix' => 'Admin:'], function (RouteBuilder $routes) {
        $routes->connect('/', ['controller' => 'Pages', 'action' => 'admin', 'admin'],
            ['_name' => 'index']);

        $routes->scope('/users', function ($routes) {

            $routes->connect('/edit/:id',
                ['controller' => 'Users', 'action' => 'edit'],
                [
                    'pass' => ['id'],
                    'id' => PATTERN_UUID,
                    '_name' => 'editUser',
                ]
            );

            $routes->connect('/delete/:id',
                ['controller' => 'Users', 'action' => 'delete'],
                [
                    'pass' => ['id'],
                    'id' => PATTERN_UUID,
                    '_name' => 'deleteUser',
                ]
            );

            $routes->connect('/index',
                ['controller' => 'Users', 'action' => 'index'],
                [
                    '_name' => 'indexUsers',
                ]
            );
        });

        $routes->scope('/skills', function ($routes) {

            $routes->connect('/add',
                ['controller' => 'Skills', 'action' => 'add'],
                [
                    '_name' => 'addSkill',
                ]
            );

            $routes->connect('/delete/:id',
                ['controller' => 'Skills', 'action' => 'delete'],
                [
                    'pass' => ['id'],
                    'id' => PATTERN_INTEGER_ID,
                    '_name' => 'deleteSkill',
                ]
            );

            $routes->connect('/index',
                ['controller' => 'Skills', 'action' => 'index'],
                [
                    '_name' => 'indexSkills',
                ]
            );
        });

        $routes->scope('/files', function ($routes) {

            $routes->connect('/index',
                ['controller' => 'Files', 'action' => 'index'],
                [
                    '_name' => 'indexFiles',
                ]
            );

            $routes->connect('/delete',
                ['controller' => 'Files', 'action' => 'delete'],
                [
                    '_name' => 'deleteFile',
                    'id' => PATTERN_UUID,
                ]
            );
        });

        $routes->scope('/announces', ['_namePrefix' => 'announces:', 'controller' => 'Announces'], function ($routes) {

            $routes->connect('/index',
                ['action' => 'index'],
                ['_name' => 'index',]
            );

            $routes->connect('/waiting',
                ['action' => 'waiting'],
                ['_name' => 'waiting',]
            );

            $routes->connect('/view/:id',
                ['action' => 'view'],
                [
                    '_name' => 'view',
                    'pass' => ['id'],
                    'id' => PATTERN_INTEGER_ID,
                ]
            );

            $routes->connect('/validate/:id',
                ['action' => 'validate'],
                [
                    '_name' => 'validate',
                    'pass' => ['id'],
                    'id' => PATTERN_INTEGER_ID,
                ]
            );

            $routes->connect('/delete/:id',
                ['action' => 'delete'],
                [
                    '_name' => 'delete',
                    'pass' => ['id'],
                    'id' => PATTERN_INTEGER_ID,
                ]
            );
        });

        $routes->scope('/traineeships', ['_namePrefix' => 'traineeships:', 'controller' => 'Traineeships'], function ($routes) {

            $routes->connect('/index',
                ['action' => 'index'],
                ['_name' => 'index',]
            );

            $routes->connect('/configure',
                ['action' => 'configure'],
                ['_name' => 'configure',]
            );

            $routes->connect('/edit/:id',
                ['action' => 'edit'],
                [
                    'pass' => ['id'],
                    'id' => PATTERN_INTEGER_ID,
                    '_name' => 'edit',
                ]
            );

            $routes->connect('/delete/:id',
                ['action' => 'delete'],
                [
                    'pass' => ['id'],
                    'id' => PATTERN_INTEGER_ID,
                    '_name' => 'delete',
                ]
            );

        });

        $routes->scope('/newsletters', ['_namePrefix' => 'newsletters:', 'controller' => 'Newsletters'], function ($routes) {
            $routes->connect('/index',
                ['action' => 'index'],
                ['_name' => 'index',]
            );

            $routes->connect('/add',
                ['action' => 'add'],
                ['_name' => 'add',]
            );
        });
    });
});
/**
 * Load all plugin routes.  See the Plugin documentation on
 * how to customize the loading of plugin routes.
 */
Plugin::routes();
