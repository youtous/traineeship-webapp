<?php
use Migrations\AbstractMigration;

class CreateTraineeships extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('traineeships');
        $table->addColumn('beginning', 'date', [
            'default' => null,
            'null' => false,
        ]);
        $table->addColumn('ending', 'date', [
            'default' => null,
            'null' => false,
        ]);
        $table->addColumn('created', 'datetime', [
            'default' => null,
            'null' => false,
        ]);


        $table
            ->addColumn('announce_id', 'integer',
                [
                    'default' => null,
                    'null' => true,
                ])
            ->addForeignKey('announce_id', 'announces', 'id',
                ['delete' => 'SET_NULL', 'update' => 'CASCADE']
            );

        $table
            ->addColumn('student_id', 'uuid',
                [
                    'default' => null,
                    'null' => true,
                ])
            ->addForeignKey('student_id', 'users', 'id',
                ['delete' => 'SET_NULL', 'update' => 'CASCADE']
            );

        $table
            ->addColumn('professor_id', 'uuid',
                [
                    'default' => null,
                    'null' => true,
                ])
            ->addForeignKey('professor_id', 'users', 'id',
                ['delete' => 'SET_NULL', 'update' => 'CASCADE']
            );


        $table->create();
    }
}
