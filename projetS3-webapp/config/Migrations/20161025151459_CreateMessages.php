<?php
use Migrations\AbstractMigration;

class CreateMessages extends AbstractMigration
{

    public $autoId = false;

    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('messages');
        $table
            ->addColumn('id', 'uuid', [
                'default' => null,
                'limit' => 255,
                'null' => false,
            ])
            ->addPrimaryKey('id');
        $table->addColumn('seen', 'datetime', [
            'default' => null,
            'null' => true,
        ]);
        $table->addColumn('content', 'text', [
            'default' => null,
            'null' => false,
        ]);
        $table->addColumn('created', 'datetime', [
            'default' => null,
            'null' => false,
        ]);
        $table->addColumn('modified', 'datetime', [
            'default' => null,
            'null' => false,
        ]);
        $table
            ->addColumn('recipient_id', 'uuid', [
                'default' => null,
                'null' => true,
            ])
            ->addForeignKey('recipient_id', 'users', 'id',
                ['delete' => 'SET_NULL', 'update' => 'CASCADE']
            );

        $table
            ->addColumn('author_id', 'uuid',
                [
                    'default' => null,
                    'null' => true,
                ])
            ->addForeignKey('author_id', 'users', 'id',
                ['delete' => 'SET_NULL', 'update' => 'CASCADE']
            );

        $table->create();
    }
}
