<?php
use Migrations\AbstractMigration;

class CreateAnnounces extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('announces');
        $table->addColumn('title', 'string', [
            'default' => null,
            'limit' => 100,
            'null' => false,
        ]);
        $table->addColumn('description', 'text', [
            'default' => null,
            'null' => false,
        ]);
        $table
            ->addColumn('user_id', 'uuid')
            ->addForeignKey('user_id', 'users', 'id',
                ['delete' => 'RESTRICT', 'update' => 'NO_ACTION']
            );;
        $table->addColumn('created', 'datetime', [
            'default' => null,
            'null' => false,
        ]);
        $table->addColumn('modified', 'datetime', [
            'default' => null,
            'null' => false,
        ]);
        $table->addColumn('status', 'char', [
            'default' => 'W',
            'limit' => 1,
            'null' => false,
        ]);
        $table->addIndex(['status'], ['unique' => false, 'name' => 'idx_announces_status']);
        $table->create();
    }
}
