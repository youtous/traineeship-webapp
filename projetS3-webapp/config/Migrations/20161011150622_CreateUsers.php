<?php
use Migrations\AbstractMigration;

class CreateUsers extends AbstractMigration
{
    public $autoId = false;

    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('users');
        $table
            ->addColumn('id', 'uuid', [
                'default' => null,
                'limit' => 255,
                'null' => false,
            ])
            ->addPrimaryKey('id');
        $table->addColumn('firstname', 'string', [
            'default' => null,
            'limit' => 100,
            'null' => false,
        ]);
        $table->addColumn('title', 'string', [
            'default' => null,
            'limit' => 100,
            'null' => true,
        ]);
        $table->addColumn('lastname', 'string', [
            'default' => null,
            'limit' => 100,
            'null' => false,
        ]);
        $table->addColumn('street', 'string', [
            'default' => null,
            'limit' => 255,
            'null' => false,
        ]);
        $table->addColumn('birthday', 'date', [
            'default' => null,
            'null' => false,
        ]);
        $table->addColumn('sex', 'char', [
            'default' => null,
            'null' => false,
            'limit' => 1,
        ]);
        $table->addColumn('street_number', 'integer', [
            'default' => null,
            'limit' => 11,
            'null' => false,
        ]);
        $table->addColumn('street_number_complement', 'char', [
            'default' => null,
            'limit' => 1,
            'null' => true,
        ]);
        $table->addColumn('city', 'string', [
            'default' => null,
            'limit' => 100,
            'null' => false,
        ]);
        $table->addColumn('postal_code', 'integer', [
            'default' => null,
            'limit' => 5,
            'null' => false,
        ]);
        $table->addColumn('phone', 'char', [
            'default' => null,
            'limit' => 10,
            'null' => true,
        ]);
        $table->addColumn('website', 'string', [
            'default' => null,
            'limit' => 255,
            'null' => true,
        ]);
        $table->addColumn('email', 'string', [
            'default' => null,
            'limit' => 255,
            'null' => false,
        ]);
        $table->addIndex(['email'], ['unique' => true, 'name' => 'idx_users_email']);

        $table->addColumn('password', 'string', [
            'default' => null,
            'limit' => 255,
            'null' => false,
        ]);
        $table->addColumn('created', 'datetime', [
            'default' => null,
            'null' => false,
        ]);
        $table->addColumn('modified', 'datetime', [
            'default' => null,
            'null' => false,
        ]);
        $table->addColumn('last_login', 'datetime', [
            'default' => null,
            'null' => true,
        ]);
        $table->addColumn('last_ip', 'string', [
            'default' => null,
            'null' => false,
            'limit' => 64,
        ]);
        $table->addColumn('subscribed', 'boolean', [
            'default' => true,
            'null' => false,
        ]);
        $table->addColumn('accept_notifications', 'boolean', [
            'default' => true,
            'null' => false,
        ]);
        $table->addColumn('role', 'char', [
            'default' => 'C',
            'limit' => 1,
            'null' => false,
        ]);
        $table->create();
    }
}
