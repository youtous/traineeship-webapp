<?php
use Migrations\AbstractMigration;

class CreateNotifications extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('notifications');

        $table->addColumn('type', 'string', [
            'default' => null,
            'limit' => 25,
            'null' => false,
        ]);

        $table
            ->addColumn('user_id', 'uuid', [
                'default' => null,
                'null' => false,
            ])
            ->addForeignKey('user_id', 'users', 'id',
                ['delete' => 'RESTRICT', 'update' => 'CASCADE']
            );

        $table->addColumn('created', 'datetime', [
            'default' => null,
            'null' => false,
        ]);

        $table->create();
    }
}
