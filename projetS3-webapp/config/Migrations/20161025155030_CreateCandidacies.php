<?php
use App\Model\Table\CandidaciesTable;
use Migrations\AbstractMigration;

class CreateCandidacies extends AbstractMigration
{
    public $autoId = false;

    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('candidacies');
        $table->addColumn('content', 'text', [
            'default' => null,
            'null' => false,
        ]);

        $table->addColumn('status', 'char', [
            'default' => CandidaciesTable::status['pending'],
            'limit' => 1,
            'null' => false,
        ]);
        $table->addIndex(['status'], ['unique' => false, 'name' => 'idx_candidacies_status']);

        $table->addColumn('created', 'datetime', [
            'default' => null,
            'null' => false,
        ]);

        $table->addColumn('modified', 'datetime', [
            'default' => null,
            'null' => false,
        ]);

        $table
            ->addColumn('student_id', 'uuid')
            ->addForeignKey('student_id', 'users', 'id',
                ['delete' => 'CASCADE', 'update' => 'CASCADE']
            );

        $table
            ->addColumn('announce_id', 'integer')
            ->addForeignKey('announce_id', 'announces', 'id',
                ['delete' => 'CASCADE', 'update' => 'CASCADE']
            );

        $table->addPrimaryKey(['student_id', 'announce_id']);

        $table->create();
    }
}
