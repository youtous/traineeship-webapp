<?php
use Migrations\AbstractMigration;

class CreateValidations extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('validations');

        $table->addColumn('type', 'string', [
            'default' => null,
            'limit' => 100,
            'null' => false,
        ]);
        $table->addColumn('code', 'uuid', [
            'default' => null,
            'limit' => 255,
            'null' => false,
        ]);
        $table->addIndex(['code'], ['unique' => true, 'name' => 'idx_validations_code']);
        $table
            ->addColumn('user_id', 'uuid')
            ->addForeignKey('user_id', 'users', 'id',
                ['delete' => 'CASCADE', 'update' => 'CASCADE']
            );
        $table->addColumn('created', 'datetime', [
            'default' => null,
            'null' => false,
        ]);
        $table->create();
    }
}
