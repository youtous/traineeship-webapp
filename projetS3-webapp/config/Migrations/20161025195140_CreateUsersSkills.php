<?php
use Migrations\AbstractMigration;

class CreateUsersSkills extends AbstractMigration
{
    public $autoId = false;

    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('users_skills');

        $table
            ->addColumn('skill_id', 'integer')
            ->addForeignKey('skill_id', 'skills', 'id',
                ['delete' => 'CASCADE', 'update' => 'CASCADE']
            );

        $table
            ->addColumn('user_id', 'uuid')
            ->addForeignKey('user_id', 'users', 'id',
                ['delete' => 'CASCADE', 'update' => 'CASCADE']
            );

        $table->addPrimaryKey(['skill_id', 'user_id']);
        $table->create();
    }
}
