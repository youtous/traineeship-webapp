<?php
use Migrations\AbstractMigration;

class CreateAppreciations extends AbstractMigration
{
    public $autoId = false;

    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('appreciations');
        $table->addColumn('content', 'text', [
            'default' => null,
            'null' => false,
        ]);
        $table->addColumn('created', 'datetime', [
            'default' => null,
            'null' => false,
        ]);

        $table
            ->addColumn('user_id', 'uuid',
                [
                    'default' => null,
                    'null' => true,
                ])
            ->addForeignKey('user_id', 'users', 'id',
                ['delete' => 'SET_NULL', 'update' => 'CASCADE']
            );

        $table
            ->addColumn('traineeship_id', 'integer')
            ->addForeignKey('traineeship_id', 'traineeships', 'id',
                ['delete' => 'CASCADE', 'update' => 'CASCADE']
            );

        $table->addColumn('id', 'integer');
        $table->addPrimaryKey(['id', 'traineeship_id']);
        $table->create();
    }
}
