<?php
use Migrations\AbstractMigration;

class CreateFiles extends AbstractMigration
{

    public $autoId = false;

    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('files');
        $table
            ->addColumn('id', 'uuid', [
                'default' => null,
                'null' => false,
            ])
            ->addPrimaryKey('id');
        $table->addColumn('mime', 'string', [
            'default' => null,
            'null' => false,
            'limit' => 255,
        ]);
        $table->addColumn('name', 'string', [
            'default' => null,
            'limit' => 255,
            'null' => false,
        ]);


        // newsletter link
        $table
            ->addColumn('newsletter_id', 'integer', [
                'default' => null,
                'null' => true,
            ])
            ->addForeignKey('newsletter_id', 'newsletters', 'id',
                ['delete' => 'RESTRICT', 'update' => 'CASCADE']
            );

        // candidacy link
        $table
            ->addColumn('announce_id', 'integer', [
                'default' => null,
                'null' => true,
            ])
            ->addForeignKey('announce_id', 'announces', 'id',
                ['delete' => 'RESTRICT', 'update' => 'CASCADE']
            );
        $table
            ->addColumn('student_id', 'uuid', [
                'default' => null,
                'null' => true,
            ])
            ->addForeignKey('student_id', 'users', 'id',
                ['delete' => 'RESTRICT', 'update' => 'CASCADE']
            );

        //messages link
        $table
            ->addColumn('message_id', 'uuid', [
                'default' => null,
                'null' => true,
            ])
            ->addForeignKey('message_id', 'messages', 'id',
                ['delete' => 'RESTRICT', 'update' => 'CASCADE']
            );

        $table->create();
    }
}
