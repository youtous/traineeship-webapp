<?php
use Migrations\AbstractMigration;

class CreateAnnouncesSkills extends AbstractMigration
{
    public $autoId = false;

    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('announces_skills');

        $table
            ->addColumn('announce_id', 'integer')
            ->addForeignKey('announce_id', 'announces', 'id',
                ['delete' => 'CASCADE', 'update' => 'CASCADE']
            );

        $table
            ->addColumn('skill_id', 'integer')
            ->addForeignKey('skill_id', 'skills', 'id',
                ['delete' => 'CASCADE', 'update' => 'CASCADE']
            );

        $table->addPrimaryKey(['announce_id', 'skill_id']);


        $table->create();
    }
}
