<?php
use Cake\ORM\TableRegistry;
use Faker\Factory;
use Migrations\AbstractSeed;

/**
 * Messages seed.
 */
class MessagesSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $data = [];
        $usersTable = TableRegistry::get('Users');
        $users = $usersTable->find()->select(['id'])->toArray();
        $generator = Factory::create();

        $pos = 0;
        foreach ($users as $user) {
            for ($i = 0; $i < rand(0, 20); $i++) {
                $recipient = $users[rand(0, (sizeof($users) - 1))]->id;


                for ($j = 0; $j < rand(0, 20); $j++) {
                    if (rand(0, 1) == 1) {
                        $authorId = $recipient;
                        $recipientId = $user->id;
                    } else {
                        $authorId = $user->id;
                        $recipientId = $recipient;
                    }

                    $created = $generator->dateTimeBetween('- 3 years', 'now')->format('Y-m-d H:i:s');
                    $data[$pos] = [
                        'id' => $generator->uuid,
                        'seen' => $generator->dateTimeBetween('- 3 years', 'now')->format('Y-m-d H:i:s'),
                        'content' => $generator->paragraph,
                        'created' => $created,
                        'modified' => $created,
                        'recipient_id' => $recipientId,
                        'author_id' => $authorId,
                    ];
                    $pos++;
                }
            }
        }

        $table = $this->table('messages');

        $table->insert($data)->save();
    }
}
