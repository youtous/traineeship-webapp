<?php
use App\Model\Table\AnnouncesTable;
use App\Model\Table\CandidaciesTable;
use App\Model\Table\UsersTable;
use Cake\ORM\TableRegistry;
use Faker\Factory;
use Migrations\AbstractSeed;

/**
 * Candidacies seed.
 */
class CandidaciesSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $data = [];
        $assos = [];
        $usersTable = TableRegistry::get('Users');
        $announcesTable = TableRegistry::get('Announces');
        $students = $usersTable->findByRole(UsersTable::roles['student'])->select(['id']);
        $announces = $announcesTable->find('all')->where(['status IN' => [AnnouncesTable::status['expired'], AnnouncesTable::status['online']]])->select(['id']);

        $generator = Factory::create();

        $candidaciesStatus = array_values(CandidaciesTable::status);

        $index = 0;
        foreach ($announces as $announce) {
            foreach ($students as $student) {
                $data[$index] = [
                    'content' => $generator->paragraphs(3, true),
                    'status' => $candidaciesStatus[rand(0, 2)],
                    'created' => $generator->dateTimeBetween('-3 months')->format('Y-m-d H:i:s'),
                    'modified' => $generator->dateTimeBetween('-3 months')->format('Y-m-d H:i:s'),
                    'student_id' => $student->id,
                    'announce_id' => $announce->id,
                ];
                $index += 1;
            }
        }

        $table = $this->table('candidacies');
        $table->insert($data)->save();
    }
}
