<?php
use Migrations\AbstractSeed;

/**
 * Skills seed.
 */
class SkillsSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id' => '1',
                'name' => 'C',
            ],
            [
                'id' => '2',
                'name' => 'C#',
            ],
            [
                'id' => '3',
                'name' => 'C++',
            ],
            [
                'id' => '4',
                'name' => 'CakePHP',
            ],
            [
                'id' => '12',
                'name' => 'Cisco Packet Tracer',
            ],
            [
                'id' => '18',
                'name' => 'CSS3',
            ],
            [
                'id' => '17',
                'name' => 'HTML5',
            ],
            [
                'id' => '20',
                'name' => 'Java',
            ],
            [
                'id' => '21',
                'name' => 'JavaFx',
            ],
            [
                'id' => '19',
                'name' => 'JavaScript',
            ],
            [
                'id' => '14',
                'name' => 'Linux',
            ],
            [
                'id' => '15',
                'name' => 'mac os',
            ],
            [
                'id' => '11',
                'name' => 'ORACLE',
            ],
            [
                'id' => '6',
                'name' => 'php',
            ],
            [
                'id' => '8',
                'name' => 'Python',
            ],
            [
                'id' => '9',
                'name' => 'R Language',
            ],
            [
                'id' => '7',
                'name' => 'Ruby',
            ],
            [
                'id' => '13',
                'name' => 'Shell bash',
            ],
            [
                'id' => '10',
                'name' => 'SQL',
            ],
            [
                'id' => '5',
                'name' => 'Symphony',
            ],
            [
                'id' => '16',
                'name' => 'Windows',
            ],
        ];

        $table = $this->table('skills');
        $table->insert($data)->save();
    }
}
