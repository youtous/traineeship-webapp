<?php
use App\Model\Table\UsersTable;
use Faker\Factory;
use Migrations\AbstractSeed;

/**
 * Database seed.
 *
 * Used for seeding the database.
 */
class DatabaseSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $this->call('SkillsSeed');
        $this->call('UsersSeed');
        $this->call('MessagesSeed');
        $this->call('AnnouncesSeed');
        $this->call('CandidaciesSeed');
    }
}
