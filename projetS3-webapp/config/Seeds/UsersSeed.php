<?php
use App\Model\Table\UsersTable;
use Faker\Factory;
use Migrations\AbstractSeed;

/**
 * Users seed.
 */
class UsersSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     *
     * @return void
     */
    public function run()
    {

        $generator = Factory::create('fr_FR');

        $data = [];
        for ($i = 0; $i < 300; $i++) {
            $role = array_values(UsersTable::roles)[rand(0, sizeof(UsersTable::roles) - 1)];
            $sexe = ['F', 'M', 'I'][rand(0, 2)];

            $title = null;
            if($role == UsersTable::roles['company']) {
                $title = $generator->company;
            }


            $data[$i] = [
                'id' => $generator->uuid,
                'role' => $role,
                'firstname' => $generator->firstName,
                'lastname' => $generator->lastName,
                'title' => $title,
                'street' => $generator->streetName,
                'birthday' => $generator->date('Y-m-d', '- 16 years'),
                'sex' => $sexe,
                'street_number' => $generator->numberBetween(),
                'street_number_complement' => $generator->optional()->numberBetween('A','Z'),
                'city' => $generator->city,
                'postal_code' => $generator->numberBetween(10000, 99999),
                'email' => $generator->email,
                'password' => sha1($generator->password),
                'modified' => $generator->dateTimeBetween('- 3 years', 'now')->format('Y-m-d H:i:s'),
                'created' => $generator->dateTimeBetween('- 3 years', 'now')->format('Y-m-d H:i:s'),
                'last_login' => $generator->dateTimeBetween('- 9 months', 'now')->format('Y-m-d H:i:s'),
                'last_ip' => ((boolean)rand(0, 1)) ? $generator->ipv4 : $generator->ipv6,
                'subscribed' => rand(0, 1),
                'phone' => $generator->optional()->numberBetween(100000000, 9999999999),
                'website' => $generator->optional()->url,
            ];
        }

        $table = $this->table('users');
        $table->insert($data)->save();
    }
}
