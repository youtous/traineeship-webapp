<?php
use App\Model\Table\AnnouncesTable;
use App\Model\Table\UsersTable;
use Cake\ORM\TableRegistry;
use Faker\Factory;
use Migrations\AbstractSeed;

/**
 * Announces seed.
 */
class AnnouncesSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $data = [];
        $assos = [];
        $usersTable = TableRegistry::get('Users');
        $skillsTable = TableRegistry::get('Skills');
        $companies = $usersTable->findByRole(UsersTable::roles['company'])->select(['id'])->toArray();
        $skills = $skillsTable->find()->select(['id'])->toArray();

        $generator = Factory::create();

        $pos = 0;
        foreach ($companies as $company) {
            for ($j = 0; $j < rand(0, 3); $j++) {
                $created = $generator->dateTimeBetween('- 3 years', 'now')->format('Y-m-d H:i:s');
                $id = $generator->numberBetween();
                $data[$pos] = [
                    'id' => $id,
                    'title' => $generator->sentence(),
                    'description' => $generator->paragraph,
                    'user_id' => $company->id,
                    'created' => $created,
                    'modified' => $created,
                    'status' => AnnouncesTable::status['online'],
                ];

                $generatorSkill = Factory::create();

                for ($i = 0; $i < rand(1, 4); $i++) {
                    array_push($assos, [
                        'announce_id' => $id,
                        'skill_id' => $skills[$generatorSkill->unique()->numberBetween(0, (sizeof($skills) - 1))]->id,
                    ]);
                }

                $pos++;
            }
        }

        $table = $this->table('announces');
        $table->insert($data)->save();

        $this->table('announces_skills')->insert($assos)->save();

    }
}
