<?php

/**
 * ucwords for UTF-8 strings
 *
 * @param $str
 * @return mixed|string
 */
function mb_ucwords($str)
{
    $str = mb_convert_case($str, MB_CASE_TITLE, "UTF-8");
    return ($str);
}


/**
 * ucfirst for UTF-8 strings
 *
 * @param $str
 * @return string
 */
function mb_ucfirst($str) {
    $aParts = explode(" ",$str);
    $firstWord = mb_convert_case($aParts[0],MB_CASE_TITLE,"UTF-8");
    unset($aParts[0]);

    return $firstWord." ".implode(" ",$aParts);
}

/**
 * @see https://secure.php.net/manual/en/function.ucfirst.php#86902
 *
 * @param $string
 * @return string
 */
function sentence_case($string) {
    $sentences = preg_split('/([.?!]+)/', $string, -1, PREG_SPLIT_NO_EMPTY|PREG_SPLIT_DELIM_CAPTURE);
    $new_string = '';
    foreach ($sentences as $key => $sentence) {
        $new_string .= ($key & 1) == 0?
            mb_ucfirst(mb_strtolower(trim($sentence))) :
            $sentence.' ';
    }
    return trim($new_string);
}