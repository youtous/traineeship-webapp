<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Appreciations Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Users
 * @property \Cake\ORM\Association\BelongsTo $Traineeships
 *
 * @method \App\Model\Entity\Appreciation get($primaryKey, $options = [])
 * @method \App\Model\Entity\Appreciation newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Appreciation[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Appreciation|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Appreciation patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Appreciation[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Appreciation findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class AppreciationsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('appreciations');
        $this->displayField('id');
        $this->primaryKey(['id', 'traineeship_id']);

        $this->addBehavior('Timestamp');
        $this->addBehavior('CompositeIncrement', [
            'where_fields' => ['traineeship_id']
        ]);

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id'
        ]);
        $this->belongsTo('Traineeships', [
            'foreignKey' => 'traineeship_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->requirePresence('content', 'create', __('Un message est attendu.'))
            ->notEmpty('content');

        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'));
        $rules->add($rules->existsIn(['traineeship_id'], 'Traineeships'));

        return $rules;
    }
}
