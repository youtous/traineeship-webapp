<?php
namespace App\Model\Table;

use App\Model\Entity\User;
use ArrayObject;
use Cake\Event\Event;
use Cake\Mailer\MailerAwareTrait;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validation;
use Cake\Localized\Validation\FrValidation;
use Cake\Validation\Validator;

/**
 * Users Model
 *
 * @property \Cake\ORM\Association\HasMany $Announces
 * @property \Cake\ORM\Association\HasMany $Appreciations
 * @property \Cake\ORM\Association\BelongsToMany $Skills
 *
 * @method \App\Model\Entity\User get($primaryKey, $options = [])
 * @method \App\Model\Entity\User newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\User[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\User|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\User patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\User[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\User findOrCreate($search, callable $callback = null)
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class UsersTable extends Table
{
    use MailerAwareTrait;
    use SearchTrait;

    /**
     * Roles of the Users.
     */
    const roles = [
        'waiting' => 'W',
        'student' => 'S',
        'company' => 'C',
        'professor' => 'P',
        'admin' => 'A',
    ];

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('users');
        $this->displayField('title');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->hasMany('Announces', [
            'foreignKey' => 'user_id',
            'dependent' => true,
        ]);
        $this->hasMany('TraineeshipsStudent', [
            'className' => 'Traineeships',
            'foreignKey' => 'student_id',
            'dependent' => true,
        ]);
        $this->hasMany('TraineeshipsProfessor', [
            'className' => 'Traineeships',
            'foreignKey' => 'professor_id',
            'dependent' => true,
        ]);
        $this->hasMany('MessagesReceived', [
            'className' => 'Messages',
            'foreignKey' => 'recipient_id',
            'dependent' => true,
        ]);
        $this->hasMany('MessagesSent', [
            'className' => 'Messages',
            'foreignKey' => 'author_id',
            'dependent' => true,
        ]);
        $this->hasMany('Appreciations', [
            'foreignKey' => 'user_id',
            'dependent' => true,
        ]);
        $this->hasMany('Validations', [
            'foreignKey' => 'user_id',
            'dependent' => true,
        ]);
        // candidacies files for students
        $this->hasMany('Files', [
            'foreignKey' => 'student_id',
            'dependent' => true,
        ]);
        $this->hasMany('Appreciations', [
            'foreignKey' => 'user_id',
            'dependent' => true,
        ]);
        $this->belongsToMany('Skills', [
            'foreignKey' => 'user_id',
            'targetForeignKey' => 'skill_id',
            'joinTable' => 'users_skills'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator->provider('fr', FrValidation::class);
        $validator
            ->uuid('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('firstname', 'create', __('Veuillez entrer votre prénom.'))
            ->notEmpty('firstname');

        $that = $this;
        $validator
            ->requirePresence('title', 'create')
            ->allowEmpty('title', function ($context) use ($that) {
                if (isset($context['data']['role']) && $context['data']['role'] !== $that::roles['waiting']) {
                    return ($context['data']['role'] !== $that::roles['company']);
                } else if (isset($context['data']['email'])) {
                    if ($that->computeRole($context['data']['email']) === $that::roles['company']) {
                        return !empty($context['data']['title']);
                    }
                    {
                        return true;
                    }
                } else {
                    return false;
                }
            }, __('Veuillez entrer le nom de votre entreprise'));

        $validator
            ->requirePresence('lastname', 'create', __('Veuillez entrer votre nom.'))
            ->notEmpty('lastname');

        $validator
            ->requirePresence('street', 'create', __('Votre adresse doit comporter une rue.'))
            ->notEmpty('street');

        $validator
            ->requirePresence('birthday', 'create', __('Veuillez spéficier votre date de naissance.'))
            ->notEmpty('birthday')
            ->date('birthday', __('Une date valide est attendue.'));

        $validator
            ->requirePresence('sex', 'create', __('Veuillez spéficier votre sexe.'))
            ->notEmpty('sex')
            ->inList('street_number_complement', ['M', 'F', 'I'], __('Sexes disponibles : M, F, I'));

        $validator
            ->requirePresence('street_number', 'create', __('Veuillez spéficier votre numéro de rue.'))
            ->notEmpty('street_number')
            ->naturalNumber('street_number', __('Un entier naturel est attendu.'));

        $validator
            ->inList('street_number_complement', ['B', 'T', 'Q', 'A'], __('Compléments disponibles : B, T, Q, A'))
            ->allowEmpty('street_number_complement');

        $validator
            ->requirePresence('city', 'create', __('Veuillez indiquer votre ville de résidence.'))
            ->notEmpty('city');

        $validator
            ->requirePresence('postal_code', 'create', __('Veuillez indiquer le code postal de la ville.'))
            ->notEmpty('postal_code')
            ->add('postal_code', 'postalValid', [
                'rule' => 'postal',
                'provider' => 'fr',
                'message' => __('Un code postal valide est attendu.'),
            ]);

        $validator
            ->add('phone', 'phoneValid', [
                'rule' => 'phone',
                'provider' => 'fr',
                'message' => __('Numéro de téléphone valide attendu.'),
            ])
            ->allowEmpty('phone');

        $validator
            ->urlWithProtocol('website', __('Une URL valide est attendue pour votre site. (http(s):// requis)'))
            ->allowEmpty('website');

        $validator
            ->requirePresence('email', 'create', __('Veuillez spéficier votre adresse email.'))
            ->notEmpty('email')
            ->email('email', true, __('Une adresse email valide est attendue.'))
            ->add('email', 'unique', ['rule' => 'validateUnique', 'provider' => 'table', 'message' => __('Cet email est déjà utilisée.')]);

        $validator
            ->requirePresence('password', 'create', __('Veuillez choisir un mot de passe.'))
            ->notEmpty('password')
            ->lengthBetween('password', [6, 100], __('La taille du mot de passse est comprise entre 6 et 100 caractères.'));

        $validator
            ->dateTime('last_login')
            ->allowEmpty('last_login');

        $validator
            ->requirePresence('last_ip', 'create')
            ->notEmpty('last_ip')
            ->ip('last_ip', __('Une adresse IP valide est attendue.'));

        $validator
            ->requirePresence('subscribed', 'create', __('Veuillez indiquer votre choix.'))
            ->notEmpty('subscribed')
            ->boolean('subscribed', __('Booléen attendu.'));

        $validator
            ->allowEmpty('accept_notifications')
            ->boolean('accept_notifications', __('Booléen attendu.'));

        $validator
            ->requirePresence('cgu', 'create', __('Veuillez accepter les CGU.'))
            ->notEmpty('cgu')
            ->boolean('cgu')
            ->equals('cgu', true, __('Veuillez accepter les CGU.'));

        $validator
            ->requirePresence('role', 'create')
            ->notEmpty('role')
            ->inList('role', array_values(self::roles), __('Un rôle valide est requis.'));

        return $validator;
    }


    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['email']));
        $that = $this;
        $rules->add(function ($user) use ($that) {
            if (isset($user->title)) {
                if (!empty($user->title)) {
                    if ($user->role === $that::roles['waiting']) {
                        return (boolean)($that->computeRole($user->email) === $that::roles['company']);
                    } else {
                        return (boolean)($user->role === $that::roles['company']);
                    }
                }
                return (boolean)($user->role !== $that::roles['company']);
            }
            return true;
        }, 'titleForCompany', [
            'errorField' => 'title',
            'message' => __('Les entreprises doivent indiquer leur titre.'),
        ]);

        return $rules;
    }

    /**
     * {@inheritDoc}
     */
    public function afterSave(Event $event, User $user, \ArrayObject $options)
    {
        $this->onRegistration($user);
    }

    /**
     * {@inheritDoc}
     */
    public function beforeMarshal(Event $event, ArrayObject $data, \ArrayObject $options)
    {
        $this->normalize($data);
    }

    /**
     * {@inheritDoc}
     */
    public function afterDelete(Event $event, User $user, \ArrayObject $options)
    {
        if ($user->email != null) {
            return $this->getMailer('User')->send('goodbye', [$user]);
        }
    }

    /**
     * After create, generate a new validation for the account.
     *
     * @param User $user
     * @return bool|\Cake\Datasource\EntityInterface|mixed
     */
    public function onRegistration(User $user)
    {
        if ($user->isNew()) {
            $validations = TableRegistry::get('Validations');
            $validation = $validations->newEntity();
            $validation = $validations->patchEntity($validation, [
                'user_id' => $user->id,
                'type' => 'activation',
            ]);
            return $validations->save($validation);
        }
    }

    /**
     * Normalize the data set.
     *
     * @param ArrayObject $data
     */
    public function normalize(ArrayObject $data)
    {
        if (isset($data['firstname'])) {
            $data['firstname'] = mb_ucwords(mb_strtolower($data['firstname']));
        }
        if (isset($data['title'])) {
            $data['title'] = mb_ucwords(mb_strtolower($data['title']));
        }
        if (isset($data['lastname'])) {
            $data['lastname'] = mb_ucwords(mb_strtolower($data['lastname']));
        }
        if (isset($data['street'])) {
            $data['street'] = mb_ucwords(mb_strtolower($data['street']));
        }
        if (isset($data['city'])) {
            $data['city'] = mb_ucwords(mb_strtolower($data['city']));
        }
        if (isset($data['website'])) {
            $data['website'] = mb_strtolower($data['website']);
        }
        if (isset($data['email'])) {
            $data['email'] = mb_strtolower($data['email']);
        }
    }

    /**
     * Determine what is the role according to the email value
     *
     * @param $email
     * @return String - role self::roles | null if undefined
     */
    public function computeRole($email)
    {
        $email = strtolower($email);
        $professorEmailPattern = '/^[\p{L}0-9!#$%&\'*+\/=?^_`{|}~-]+(?:\.[\p{L}0-9!#$%&\'*+\/=?^_`{|}~-]+)*@(univ-reims\.fr)$/ui';
        $studentEmailPattern = '/^[\p{L}0-9!#$%&\'*+\/=?^_`{|}~-]+(?:\.[\p{L}0-9!#$%&\'*+\/=?^_`{|}~-]+)*@(etudiant\.univ-reims\.fr)$/ui';

        if (Validation::custom($email, $studentEmailPattern)) {
            return self::roles['student'];
        } else if (Validation::custom($email, $professorEmailPattern)) {
            return self::roles['professor'];
        } else if (Validation::email($email)) {
            return self::roles['company'];
        }
        return null;
    }

    /*
     * Return the stored char associated for a role.
     * @return char|null
     */
    public function getStoredRole($role)
    {
        $roles = [
            'étudiant' => 'student',
            'étudiants' => 'student',
            'professeur' => 'professor',
            'professeurs' => 'professor',
            'administrateur' => 'admin',
            'administrateurs' => 'admin',
            'entreprise' => 'company',
            'entreprises' => 'company',
            'attente' => 'waiting',
        ];
        if (array_key_exists($role, $roles)) {
            return self::roles[$roles[$role]];
        } else {
            return '';
        }
    }

    /**
     * Search the Users corresponding to the search value.
     * Search by : title, firstname, name, lastname, email, year of creation,
     * last_ip, city, id, role.
     *
     * @param Query $query - the query that will be used for the search
     * @param $search - the search term
     * @return Query
     */
    protected function _searchValue(Query $query, $search)
    {
        $search = strtolower($search);

        $titleQuery = $query->func()->levenshtein([$search, 'LOWER(title)' => 'literal']);
        $firstnameQuery = $query->func()->levenshtein([$search, 'LOWER(firstname)' => 'literal']);
        $lastnameQuery = $query->func()->levenshtein([$search, 'LOWER(lastname)' => 'literal']);
        $emailQuery = $query->func()->levenshtein([$search, 'LOWER(email)' => 'literal']);
        $yearQuery = $query->func()->year(['created' => 'identifier']);
        $nameQuery = $query->func()->levenshtein([$search, "LOWER(CONCAT(firstname, ' ',lastname))" => 'literal']);

        // tolerance is the distance of the string accepted
        $tolerance = 2;
        $query
            ->where(['role' => $this->getStoredRole($search)])
            ->orWhere(['id' => $search])
            ->orWhere(['last_ip' => $search])
            ->orWhere(['city' => $search])
            ->orWhere(function ($exp) use ($titleQuery, $tolerance) {
                return $exp->between($titleQuery, 0, $tolerance)->isNotNull('title');
            })
            ->orWhere(function ($exp) use ($firstnameQuery, $tolerance) {
                return $exp->between($firstnameQuery, 0, $tolerance);
            })
            ->orWhere(function ($exp) use ($lastnameQuery, $tolerance) {
                return $exp->between($lastnameQuery, 0, $tolerance);
            })
            ->orWhere(function ($exp) use ($emailQuery, $tolerance) {
                return $exp->between($emailQuery, 0, $tolerance);
            })
            ->orWhere([function ($exp) use ($yearQuery, $search) {
                return $exp->eq($yearQuery, $search);
            }])
            ->orWhere([function ($exp) use ($nameQuery, $tolerance) {
                return $exp->between($nameQuery, 0, $tolerance);
            }])
            ->distinct();

        return $query;
    }

    /**
     * Search the Users corresponding to the search value.
     * Search by : firstname, name, lastname.
     * Users.role must not be "waiting".
     * If the User is a company, the name should be not divulged.
     *
     * @param Query $query
     * @param $search
     * @return Query
     */
    public function searchContact(Query $query, $search)
    {
        $search = strtolower($search);

        $titleQuery = $query->func()->levenshtein([$search, 'LOWER(title)' => 'literal']);
        $firstnameQuery = $query->func()->levenshtein([$search, 'LOWER(firstname)' => 'literal']);
        $lastnameQuery = $query->func()->levenshtein([$search, 'LOWER(lastname)' => 'literal']);
        $nameQuery = $query->func()->levenshtein([$search, "LOWER(CONCAT(firstname, ' ',lastname))" => 'literal']);

        // tolerance is the distance of the string accepted
        $tolerance = 1;
        $waiting = self::roles['waiting'];
        $query
            ->where(
                function ($exp) use ($waiting) {
                    return $exp->notEq('role', $waiting);
                })
            ->andWhere([
                'OR' => [
                    function ($exp) use ($titleQuery, $tolerance) {
                        return $exp->between($titleQuery, 0, $tolerance)->isNotNull('title');
                    },
                    function ($exp) use ($firstnameQuery, $tolerance) {
                        return $exp->between($firstnameQuery, 0, $tolerance);
                    },
                    function ($exp) use ($lastnameQuery, $tolerance) {
                        return $exp->between($lastnameQuery, 0, $tolerance);
                    },
                    function ($exp) use ($nameQuery, $tolerance) {
                        return $exp->between($nameQuery, 0, $tolerance);
                    }
                ]
            ])
            ->distinct();
        return $query;
    }

    public function findProfile(Query $query)
    {
        return $query
            ->select(
                [
                    'id',
                    'firstname',
                    'lastname',
                    'title',
                    'city',
                    'phone',
                    'role',
                    'website',
                    'created',
                    'last_login',
                    'email',
                ]);
    }

    public function findComplete(Query $query)
    {
        return $query
            ->select(
                [
                    'id',
                    'firstname',
                    'lastname',
                    'email',
                    'title',
                    'city',
                    'street',
                    'street_number',
                    'street_number_complement',
                    'postal_code',
                    'birthday',
                    'phone',
                    'website',
                    'created',
                    'modified',
                    'last_login',
                    'last_ip',
                    'subscribed',
                    'role',
                    'sex',
                    'accept_notifications',
                ]);
    }

    public function findName(Query $query)
    {
        return $query
            ->select(
                [
                    'id',
                    'firstname',
                    'lastname',
                    'title',
                ]);
    }

    public function findTable(Query $query)
    {
        return $this->findName($query)
            ->select([
                'email',
                'role',
            ]);
    }

    /**
     * Selector used for indexing companies.
     *
     * @param Query $query
     * @return $this
     */
    public function findCompany(Query $query)
    {
        return $query
            ->select([
                'id',
                'email',
                'title',
                'city',
                'postal_code'
            ]);
    }

    /**
     * Selector used for companies
     * with the city and postal code.
     *
     * @param Query $query
     * @return $this
     */
    public function findNameAddress(Query $query)
    {
        return
            $query->select([
                'id',
                'title',
                'city',
                'postal_code',
            ]);
    }

    /**
     * Selector used for companies
     * with the full address.
     *
     * @param Query $query
     * @return mixed
     */
    public function findNameFullAddress(Query $query)
    {
        return $this->findName($query)
            ->select([
                'email',
                'phone',
                'city',
                'postal_code',
                'street',
                'street_number',
                'street_number_complement',
            ]);
    }
}
