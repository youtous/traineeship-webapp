<?php
namespace App\Model\Table;

use App\Event\NotificationManager;
use App\Model\Entity\Candidacy;
use Cake\Event\Event;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Candidacies Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Users
 * @property \Cake\ORM\Association\BelongsTo $Announces
 *
 * @method \App\Model\Entity\Candidacy get($primaryKey, $options = [])
 * @method \App\Model\Entity\Candidacy newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Candidacy[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Candidacy|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Candidacy patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Candidacy[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Candidacy findOrCreate($search, callable $callback = null)
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class CandidaciesTable extends Table
{

    /**
     * Status of the Announce
     */
    const status = [
        'pending' => 'W',
        /*
         * When the candidacy has been accepted by the company.
         */
        'accepted' => 'A',
        /**
         * When the candidacy has been accepted by the company and accepted by the student.
         */
        'validated' => 'V',
    ];

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('candidacies');
        $this->displayField('created');
        $this->primaryKey(['student_id', 'announce_id']);

        $this->addBehavior('Timestamp');

        $this->belongsTo('Users', [
            'foreignKey' => 'student_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Announces', [
            'foreignKey' => 'announce_id',
            'joinType' => 'INNER'
        ]);

        $this->hasMany('Files', [
            'dependent' => true,
            'foreignKey' => [
                'student_id',
                'announce_id'
            ]
        ]);

        $this->hasMany('Traineeships', [
            'foreignKey' => ['student_id', 'announce_id'],
            'dependent' => false,
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->requirePresence('content', 'create')
            ->notEmpty('content', __('Le contenu de la candidature ne peut pas être nul.'));

        $validator
            ->requirePresence('status', 'create')
            ->inList('status', array_values(self::status), __('Ce statut n\'est pas valide.'))
            ->notEmpty('status');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['student_id'], 'Users'));
        $rules->add($rules->existsIn(['announce_id'], 'Announces'));

        $rules->add([$this, 'isValidStatus'], 'validStatus', [
            'errorField' => 'status',
            'message' => 'Ce statut n\'est pas valide.'
        ]);

        $rules->addCreate($rules->validCount('files', 2, '<=', 'Vous devez avoir au plus 2 fichiers associées.'));

        return $rules;
    }

    /**
     * Check the status of a Candidacy.
     *
     * @param Candidacy $candidacy
     * @return bool
     */
    public function isValidStatus(Candidacy $candidacy)
    {
        return in_array($candidacy->status, array_values(self::status));
    }

    /**
     * {@inheritDoc}
     */
    public function afterSave(Event $event, Candidacy $candidacy)
    {
        if ($candidacy->isNew()) {
            $this->eventManager()->on(new NotificationManager());

            $eventAfterCreate = new Event('Model.Candidacies.afterCreate', $this, [
                'candidacy' => $candidacy
            ]);
            $this->eventManager()->dispatch($eventAfterCreate);
        }
    }

    /**
     * Candidacies linked to a Traineeship should not be deleted.
     *
     * @param Event $event
     * @param Candidacy $candidacy
     * @param \ArrayObject $options
     * @return bool
     */
    public function beforeDelete(Event $event, Candidacy $candidacy, \ArrayObject $options)
    {
        return (bool)$this->Traineeships->findByAnnounceIdAndStudentId($candidacy->announce_id, $candidacy->student_id)->count() == 0;
    }

    /**
     * Check if a student has already postulate to a candidacy.
     *
     * @param $student_id
     * @param $announce_id
     * @return bool
     */
    public function hasPostulate($student_id, $announce_id)
    {
        return (bool)$this->findByStudentIdAndAnnounceId($student_id, $announce_id)->count() > 0;
    }

    /**
     * Selector used for index (manage).
     *
     * @param Query $query
     * @return $this
     */
    public function findTable(Query $query)
    {
        return $query
            ->select([
                'announce_id',
                'student_id',
                'modified',
                'created',
            ]);
    }

    /**
     * Selector used for reviewing (companies).
     *
     * @param Query $query
     * @return $this
     */
    public function findReview(Query $query) {
        return $query
            ->select([
                'announce_id',
                'student_id',
                'content',
                'created',
            ]);
    }
}
