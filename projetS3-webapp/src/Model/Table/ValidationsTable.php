<?php
namespace App\Model\Table;

use App\Model\Entity\Validation;
use Cake\Event\Event;
use Cake\Mailer\MailerAwareTrait;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Utility\Text;
use Cake\Validation\Validator;

/**
 * Validations Model
 *
 */
class ValidationsTable extends Table
{
    use MailerAwareTrait;

    /**
     * {@inheritDoc}
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('validations');
        $this->displayField('type');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Users');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id', 'create')
            ->naturalNumber('id');

        $validator
            ->requirePresence('type')
            ->notEmpty('type')
            ->inList('type', ['activation', 'password', 'deleteAccount']);

        $validator
            ->allowEmpty('code')
            ->lengthBetween('code', [4, 36]);

        return $validator;
    }

    /**
     * Validator used for Validations requiring
     * Email reverse search.
     *
     * @param Validator $validator
     * @return Validator
     */
    public function validationRetrieve(Validator $validator)
    {
        $validator
            ->requirePresence('email', __('Veuillez indiquer un email'))
            ->email('email', false, __('Email valide requise.'));
        $validator
            ->add('email', 'emailExists', [
                'rule' => function ($value) {
                    $users = TableRegistry::get('Users');
                    return (bool) $users->findByEmail($value)->select(1)->count();
                },
                'message' => __('Cet email est inconnu.'),
            ]);

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['code']));
        $rules->add($rules->existsIn(['user_id'], 'Users'));

        return $rules;
    }


    /**
     * Generate the code if not set.
     *
     * @param Event $event
     * @param Validation $validation
     */
    public function beforeRules(Event $event, Validation $validation)
    {
        if ($validation->isNew()) {
            // generate the code if not set
            if ($validation->code === null) {
                $validation->code = Text::uuid();
            }
        }
    }

    /**
     * Remove all Confirmations of the same type.
     *
     * @param Event $event
     * @param Validation $validation
     */
    public function beforeSave(Event $event, Validation $validation)
    {
        $this->deleteAll(['user_id' => $validation->user_id, 'type' => $validation->type]);
    }

    /**
     * Send the confirmation email after save.
     *
     * @param Event $event
     * @param Validation $validation
     * @return array
     */
    public function afterSave(Event $event, Validation $validation)
    {
        if ($validation->isNew()) {
            // send the mail
            $users = TableRegistry::get('Users');
            return $this->getMailer('Validation')->send(
                $validation->type,
                [$users->findNameById($validation->user_id)->select(['email'])->firstOrFail(), $validation->code]
            );
        }
    }

    /**
     * Validate a submitted Confirmation.
     * Delete the confirmation after check.
     *
     * @param $code
     * @param $user_id
     * @param $type
     * @return bool
     */
    public function validate($code, $user_id, $type)
    {
        $validation = $this->findByCodeAndUserIdAndType($code, $user_id, $type)->firstOrFail();

        $res = !($validation->expired);
        $this->delete($validation);

        return $res;
    }
}
