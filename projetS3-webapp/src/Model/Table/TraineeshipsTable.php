<?php
namespace App\Model\Table;

use App\Model\Entity\Traineeship;
use App\Model\Entity\User;
use Cake\Event\Event;
use Cake\Mailer\MailerAwareTrait;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Traineeships Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Announces
 * @property \Cake\ORM\Association\BelongsTo $Students
 * @property \Cake\ORM\Association\BelongsTo $Professors
 * @property \Cake\ORM\Association\HasMany $Appreciations
 *
 * @method \App\Model\Entity\Traineeship get($primaryKey, $options = [])
 * @method \App\Model\Entity\Traineeship newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Traineeship[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Traineeship|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Traineeship patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Traineeship[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Traineeship findOrCreate($search, callable $callback = null)
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class TraineeshipsTable extends Table
{
    use MailerAwareTrait;

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('traineeships');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Announces', [
            'foreignKey' => 'announce_id'
        ]);
        $this->belongsTo('Students', [
            'className' => 'Users',
            'foreignKey' => 'student_id'
        ]);
        $this->belongsTo('Professors', [
            'className' => 'Users',
            'foreignKey' => 'professor_id'
        ]);
        $this->hasMany('Appreciations', [
            'foreignKey' => 'traineeship_id',
            'dependent' => true,
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->date('beginning', ['ymd'], __('Une date de début est requise.'))
            ->requirePresence('beginning', 'create')
            ->notEmpty('beginning');

        $validator
            ->date('ending', ['ymd'], __('Une date de fin est requise.'))
            ->requirePresence('ending', 'create')
            ->notEmpty('ending');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['announce_id'], 'Announces'));
        $rules->add($rules->existsIn(['student_id'], 'Students'));
        $rules->add($rules->existsIn(['professor_id'], 'Professors'));

        return $rules;
    }

    /**
     * {@inheritDoc}
     */
    public function afterDelete(Event $event, Traineeship $traineeship, \ArrayObject $options)
    {
        $this->getMailer('Traineeship')->send('traineeshipCanceled', [$traineeship]);
    }

    /**
     * Check if the User takes part of the Traineeship.
     *
     * @param $user_id
     * @param $user_role
     * @param $traineeship_id
     * @return bool
     */
    public function isPartOfTraineeship($user_id, $user_role, $traineeship_id)
    {
        $traineeship = $this->findById($traineeship_id);

        switch ($user_role) {
            case UsersTable::roles['student'] :
                $traineeship->where(['student_id' => $user_id]);
                break;
            case UsersTable::roles['company'] :
                $announces = $this->Announces->findByUserId($user_id)->select(['id']);
                $traineeship->where(['announce_id IN' => $announces]);
                break;
            case UsersTable::roles['professor'] :
                $traineeship->where(['professor_id' => $user_id]);
                break;
        }

        return (bool)$traineeship->count() > 0;
    }
}
