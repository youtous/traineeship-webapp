<?php
namespace App\Model\Table;

use App\Model\Entity\Skill;
use Cake\Event\Event;
use Cake\Filesystem\File;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Intervention\Image\ImageManagerStatic;

/**
 * Skills Model
 *
 * @property \Cake\ORM\Association\BelongsToMany $Users
 *
 * @method \App\Model\Entity\Skill get($primaryKey, $options = [])
 * @method \App\Model\Entity\Skill newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Skill[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Skill|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Skill patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Skill[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Skill findOrCreate($search, callable $callback = null)
 */
class SkillsTable extends Table
{
    const SKILLS_IMAGES_WEBPATH = 'skills' . DS;
    const SKILLS_IMAGES_PATH = WWW_ROOT . DS . 'img' . DS . self::SKILLS_IMAGES_WEBPATH;

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('skills');
        $this->displayField('name');
        $this->primaryKey('id');

        $this->belongsToMany('Students', [
            'className' => 'Users',
            'foreignKey' => 'skill_id',
            'conditions' => ['Users.role' => UsersTable::roles['student']],
            'targetForeignKey' => 'user_id',
            'joinTable' => 'users_skills'
        ]);

        $this->belongsToMany('Professors', [
            'className' => 'Users',
            'foreignKey' => 'skill_id',
            'conditions' => ['Users.role IN' => [UsersTable::roles['professor'], UsersTable::roles['admin']]],
            'targetForeignKey' => 'user_id',
            'joinTable' => 'users_skills'
        ]);

        $this->belongsToMany('Announces', [
            'foreignKey' => 'skill_id',
            'targetForeignKey' => 'announce_id',
            'joinTable' => 'announces_skills'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('name', 'create', __('Veuillez spéficier le nom de la compétence.'))
            ->notEmpty('name')
            ->add('name', 'unique', [
                'rule' => 'validateUnique',
                'provider' => 'table',
                'message' => __('Ce nom de compétence est déjà utilisé.'),
            ]);

        $validator
            ->requirePresence('icon', 'create', __('Une icone d\'illustration est requise.'))
            ->add('icon', [
                'uploadError' => [
                    'rule' => 'uploadError',
                    'message' => __('L\'envois du fichier a échoué.'),
                    'last' => true
                ],
                'mimeType' => [
                    'rule' => ['mimeType', ['image/jpeg', 'image/png']],
                    'message' => __("Le mimeType n'est pas autorisé (jpeg ou png)."),
                    'alllowEmpty' => true,
                ],
                'fileExtension' => [
                    'rule' => ['extension', ['jpg', 'jpeg', 'png']],
                    'message' => __("Les extensions autorisées sont : {0}.", '.jpg, .jpeg and 1.png'),
                ],
                'fileSize' => [
                    'rule' => ['fileSize', '<', '4MB'],
                    'message' => __("Le poids maximum du fichier est de {0}", '4MB'),
                ],
            ]);

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['name']));

        return $rules;
    }

    /**
     * {@inheritDoc}
     */
    public function beforeDelete(Event $event, Skill $skill, \ArrayObject $options)
    {
        $file = new File(self::SKILLS_IMAGES_PATH . $skill->id . '.png');
        if (!$file->delete()) {
            $skill->errors('icon', [__('Impossible de supprimer l\'image sur du serveur.')]);
        }
    }

    /**
     * {@inheritDoc}
     */
    public function afterSave(Event $event, Skill $skill, \ArrayObject $options)
    {
        //image logic
        $img = ImageManagerStatic::make($event->data['path']);

        $img->encode('png');

        $file = new File(self::SKILLS_IMAGES_PATH . $skill->id . '.png');

        // file exists and not writeable
        if ($file->exists() && !$file->writable()) {
            $skill->errors('icon', [__('Image non éditable déjà existante.')]);
            return false;
        }

        if (!$file->write($img, 'w', true)) {
            $skill->errors('icon', [__('Erreur lors de la sauvegarde de l\'image sur le serveur.')]);
            return false;
        }

        $file->close();
        return true;
    }

    /**
     * {@inheritDoc}
     */
    public function beforeMarshal(Event $event, \ArrayObject $data, \ArrayObject $options) {
        return $this->defineTempPath($event, $data, $options);
    }

    /**
     * Attach to the event the tmp path of the uploaded file.
     *
     * @param Event $event
     * @param \ArrayObject $data
     * @param \ArrayObject $options
     */
    public function defineTempPath(Event $event, \ArrayObject $data, \ArrayObject $options)
    {
        if (isset($data['icon']['tmp_name'])) {
            $this->eventManager()->on('Model.afterSave', ['priority' => 9], function ($event) use ($data) {
                $event->data['path'] = $data['icon']['tmp_name'];
            });
        }
    }
}
