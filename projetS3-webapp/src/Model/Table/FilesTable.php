<?php
namespace App\Model\Table;

use App\Model\Entity\Candidacy;
use App\Model\Entity\File;
use App\Model\Entity\Message;
use Cake\Database\Query;
use Cake\Event\Event;
use Cake\Filesystem\File as FileCake;
use Cake\Network\Exception\InternalErrorException;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;
use Intervention\Image\ImageManagerStatic;

/**
 * Files Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Newsletters
 * @property \Cake\ORM\Association\BelongsTo $Announces
 * @property \Cake\ORM\Association\BelongsTo $Users
 * @property \Cake\ORM\Association\BelongsTo $Messages
 *
 * @method \App\Model\Entity\File get($primaryKey, $options = [])
 * @method \App\Model\Entity\File newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\File[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\File|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\File patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\File[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\File findOrCreate($search, callable $callback = null)
 */
class FilesTable extends Table
{
    const UPLOAD_PATH = ROOT . DS . 'uploads' . DS . 'files' . DS;

    public static $mimeTypes = [
        'image/jpeg' => 'jpeg',
        'image/jpg' => 'jpg',
        'image/png' => 'png',
        'image/gif' => 'gif',
        'application/pdf' => 'pdf',
        'text/plain' => 'txt',
    ];

    public static $maxSize = '4MB';

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('files');
        $this->displayField('name');
        $this->primaryKey('id');

        $this->belongsTo('Newsletters', [
            'joinType' => 'LEFT'
        ]);
        $this->belongsTo('Candidacies', [
            'className' => 'Candidacies',
            'foreignKey' => ['student_id', 'announce_id'],
            'bindingKey' => ['student_id', 'announce_id'],
            'joinType' => 'LEFT'
        ]);
        $this->belongsTo('Messages');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->uuid('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('name', true, __('Un nom de fichier est requis.'))
            ->notEmpty('name');

        $validator
            ->requirePresence('mime', true, __('Un type(mime) de fichier est requis.'))
            ->notEmpty('mime');

        $validator
            ->requirePresence('tmp_name', true, __('Un fichier est requis.'))
            ->add('tmp_name', [
                'uploadError' => [
                    'rule' => 'uploadError',
                    'message' => __('L\'envois du fichier a échoué.'),
                    'last' => true
                ],
                'mimeType' => [
                    'rule' => ['mimeType', array_keys(self::$mimeTypes)],
                    'message' => __("Le mimeType n'est pas autorisé ({0}).", implode(', ', array_keys(self::$mimeTypes))),
                ],
                'fileSize' => [
                    'rule' => ['fileSize', '<', self::$maxSize],
                    'message' => __("Le poids maximum du fichier est de {0}", self::$maxSize),
                ],
            ]);

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['newsletter_id'], 'Newsletters'));
        $rules->add($rules->existsIn(['announce_id', 'student_id'], 'Candidacies'));
        $rules->add($rules->existsIn(['message_id'], 'Messages'));

        return $rules;
    }

    /**
     * {@inheritDoc}
     */
    public function afterSave(Event $event, File $file, \ArrayObject $options)
    {
        return $this->saveFile($event, $file, $options);
    }

    /**
     * {@inheritDoc}
     */
    public function beforeDelete(Event $event, File $file, \ArrayObject $options)
    {
        return $this->deleteFile($event, $file, $options);
    }

    /**
     * {@inheritDoc}
     */
    public function beforeMarshal(Event $event, \ArrayObject $data, \ArrayObject $options)
    {
        if ($data['type'] !== null) {
            $data['mime'] = $data['type'];
        }
    }

    /**
     * Save the submitted File to the self::UPLOAD_PATH
     * The id of the File is used to identify the file.
     *
     * @param Event $event
     * @param File $file
     * @param \ArrayObject $options
     * @return bool
     */
    public function saveFile(Event $event, File $file, \ArrayObject $options)
    {
        if (!(new FileCake(self::UPLOAD_PATH))->writable()) {
            throw new InternalErrorException("The upload folder : " . self::UPLOAD_PATH . " is not writeable !");
        }

        $fileTemp = new FileCake($file->tmp_name);
        $fullPath = self::UPLOAD_PATH . $file->id . '.' . self::$mimeTypes[$fileTemp->mime()];

        if ($fileTemp->mime() === 'image/jpeg' || $fileTemp->mime() === 'image/jpg') {
            $image = ImageManagerStatic::make($fileTemp->pwd());
            $core = $image->getCore();
            if ($core instanceof \Imagick) {
                $core->stripImage();
                // save with Imagick
                $core->writeImage($fullPath);
                $core->clear();
                $core->destroy();
                $fileTemp->close();
                return true;
            } else {
                $im = imagecreatefromjpeg($fileTemp->pwd());
                // save with GD
                imagejpeg($im, $fullPath, 100);
                $fileTemp->close();
                return true;
            }
        }
        // else do simple copy of the file
        $fileTemp->copy($fullPath);
        $fileTemp->close();
        return true;
    }

    /**
     * Try to delete the associated FileSystem of a File.
     *
     * @param Event $event
     * @param File $file
     * @param \ArrayObject $options
     * @return bool - success of the operation
     */
    public function deleteFile(Event $event, File $file, \ArrayObject $options)
    {
        $file = new FileCake(self::UPLOAD_PATH . $file->id . '.' . $file->extension);
        if (!$file->delete()) {
            $file->errors('file', [__('Impossible de supprimer le fichier du serveur.')]);
            return false;
        } else {
            return true;
        }
    }

    public function findView(Query $query)
    {
        return $query
            ->select(
                [
                    'id',
                    'mime',
                ]);
    }

    /**
     * Get the File specified by $id and check if the User ($user_id)
     * can access to that file.
     *
     * @param $id - the File id
     * @param $user_id - the User.id that request the file
     * @param null $user_role - the User.role (used for admins)
     * @return File|false - false if not accessible
     */
    public function getRestricted($id, $user_id, $user_role)
    {
        $file = $this->findViewById($id)
            ->contain([
                'Messages' => function ($q) {
                    return $q->select(['author_id', 'recipient_id']);
                },
                'Candidacies' => [
                    'queryBuilder' => function ($q) {
                        return $q->select(['student_id', 'announce_id']);
                    },
                    'Announces' => [
                        'joinType' => 'LEFT',
                        'queryBuilder' => function ($q) {
                            return $q->select(['user_id']);
                        },
                    ]
                ]
            ])
            ->firstOrFail();

        if ($file->public || ($user_role === UsersTable::roles['admin'])) {
            return $file;
        } else {
            // check if the specified user can access
            if ($file->message instanceof Message && in_array($user_id, [
                    $file->message->recipient_id,
                    $file->message->author_id])
            ) {
                return $file;
            }
            if ($file->candidacy instanceof Candidacy) {
                if ($file->candidacy->student_id === $user_id) {
                    return $file;
                } else if ($file->candidacy->announce->user_id === $user_id) {
                    return $file;
                }
            }
        }
        return false;
    }
}
