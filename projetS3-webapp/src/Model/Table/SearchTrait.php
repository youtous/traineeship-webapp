<?php
/**
 * Created by PhpStorm.
 * User: youtous
 * Date: 11/12/2016
 * Time: 00:14
 */

namespace App\Model\Table;


use Cake\ORM\Query;

trait SearchTrait
{

    /**
     * For each search terms proceed a _searchValue, then union all the queries.
     * Using "" or '' in a terms will not split the sub-terms.
     *
     * @param $search - the search terms
     * @param $method - the method called for the search (must return a Query)
     * @param Query $query - the query that will be used for the search
     * @param array $options - array of options for the query (['find' => 'all'])
     * @param int $limit - the maximum terms
     * @return Query - the result of this search
     */
    public function search(Query $query, $search, $method, array $options = [], $limit = 10)
    {
        $searchParameters = preg_split("/[\s,]*\\\"([^\\\"]+)\\\"[\s,]*|" . "[\s,]*'([^']+)'[\s,]*|" . "[\s,]+/",
            $search, $limit, PREG_SPLIT_NO_EMPTY | PREG_SPLIT_DELIM_CAPTURE);

        $_methodName = '_search' . $method;

        $result = null;
        for ($i = 0; $i < sizeof($searchParameters); $i++) {
            if ($i == 0) {
                $result = $this->$_methodName($query->cleanCopy(), $searchParameters[$i], $options);
            } else {
                $result->union($this->$_methodName($query->cleanCopy(), $searchParameters[$i], $options));
            }
        }
        return $result;
    }
}