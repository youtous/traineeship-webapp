<?php
namespace App\Model\Table;

use App\Model\Entity\Announce;
use Cake\Event\Event;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
use Cake\Validation\Validator;

/**
 * Announces Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Users
 * @property \Cake\ORM\Association\HasMany $Candidacies
 * @property \Cake\ORM\Association\HasMany $Files
 * @property \Cake\ORM\Association\HasMany $Traineeships
 * @property \Cake\ORM\Association\BelongsToMany $Tags
 * @property \Cake\ORM\Association\BelongsToMany $Skills
 *
 * @method \App\Model\Entity\Announce get($primaryKey, $options = [])
 * @method \App\Model\Entity\Announce newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Announce[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Announce|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Announce patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Announce[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Announce findOrCreate($search, callable $callback = null)
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class AnnouncesTable extends Table
{
    /**
     * Status of the Announces.
     */
    const status = [
        'waiting' => 'W',
        'online' => 'O',
        'expired' => 'E',
    ];

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('announces');
        $this->displayField('title');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('Candidacies', [
            'foreignKey' => 'announce_id',
            'dependent' => true,
        ]);
        $this->hasMany('Traineeships', [
            'foreignKey' => 'announce_id'
        ]);
        $this->belongsToMany('Skills', [
            'foreignKey' => 'announce_id',
            'targetForeignKey' => 'skill_id',
            'joinTable' => 'announces_skills',
        ]);

        // required for the assoc cascade (deleting)
        $this->hasMany('Files', [
            'dependent' => true,
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('title', 'create')
            ->notEmpty('title')
            ->lengthBetween('title', [6, 100], __('Le titre d\'annonce doit être compris entre 6 et 100 caractères.'));

        $validator
            ->requirePresence('description', 'create')
            ->notEmpty('description', __('Le contenu descriptif ne peut pas être nul.'));

        $validator
            ->requirePresence('status', 'create')
            ->notEmpty('status');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'));

        $rules->add([$this, 'isValidStatus'], 'validStatus', [
            'errorField' => 'status',
            'message' => 'Ce statut n\'est pas valide.'
        ]);

        $rules->addCreate([$this, 'userAllowedPost'], 'allowedPost', [
            'errorField' => 'user_id',
            'message' => 'Cet utilisateur n\'est pas autorisé à poster.'
        ]);

        $rules->add($rules->validCount('skills', 1, '>=', 'Vous devez avoir au moins une compétence associée.'));
        $rules->add($rules->validCount('skills', 4, '<=', 'Vous devez avoir au plus 4 compétences associées.'));

        return $rules;
    }

    /**
     * Check the status of an Announce.
     *
     * @param Announce $announce
     * @return bool
     */
    public function isValidStatus(Announce $announce)
    {
        return in_array($announce->status, array_values(self::status));
    }

    /**
     * Check the associated User the Announce is allowed to post an Announce.
     *
     * @param Announce $announce
     * @return bool
     */
    public function userAllowedPost(Announce $announce)
    {
        $usersTable = TableRegistry::get('Users');
        $user = $usersTable->findById($announce->user_id)->select(['role'])->firstOrFail();

        return $user->role === UsersTable::roles['company'];
    }

    /**
     * {@inheritDoc}
     */
    public function beforeMarshal(Event $event, \ArrayObject $data, \ArrayObject $options)
    {
        $this->normalize($data);
    }


    /**
     * If the Announce is related to Traineeships,
     * will be not deleted.
     *
     * @param Event $event
     * @param Announce $announce
     * @param \ArrayObject $options
     * @return bool
     */
    public function beforeDelete(Event $event, Announce $announce, \ArrayObject $options)
    {
        // check if the announce is associated to Traineeships
        if (!$this->Traineeships->findByAnnounceId($announce->id)->select(['announce_id'])->limit(1)->isEmpty()) {
            $announce->errors('Traineeships', [__('L\'annonce est associée à des stages. Elle ne peut pas être supprimée.')]);
            return false;
        }

    }

    /**
     * Normalize the data set.
     *
     * @param \ArrayObject $data
     */
    public function normalize(\ArrayObject $data)
    {
        if (isset($data['title'])) {
            $data['title'] = mb_ucwords(mb_strtolower($data['title']));
        }
    }

    /**
     * Search the Announces corresponding to the search value.
     * Search by date, company, city.
     *
     * @param Query $query - the query that will be used for the search
     * @param $search
     * @return Query
     */
    public function searchIndex(Query $query, $search)
    {
        $search = strtolower($search);
        $datePattern = '/(\d{1,2})(?:-|\/|\.)(\d{1,2})(?:-|\/|\.)(\d{4})/';
        $formattedDate = [];
        preg_match($datePattern, $search, $formattedDate);

        $userTitleQuery = $query->func()->levenshtein([$search, 'LOWER(Users.title)' => 'literal']);

        // tolerance is the distance of the string accepted
        $tolerance = 2;
        if (sizeof($formattedDate) > 0) {
            $query->where([function ($exp) use ($formattedDate) {
                return $exp->between('Announces.created', $formattedDate[3] . '-' . $formattedDate[2] . '-' . $formattedDate[1], $formattedDate[3] . '-' . $formattedDate[2] . '-' . $formattedDate[1] . ' 23:59:59');
            }]);
        } else {
            $query
                ->where(function ($exp) use ($userTitleQuery, $tolerance) {
                    return $exp->between($userTitleQuery, 0, $tolerance);
                })
                ->orWhere(['Announces.title LIKE' => '%' . $search . '%'])
                ->orWhere(['Users.city' => $search]);
        }

        $query->distinct();

        return $query;
    }

    /**
     * Search extends searchIndex plus postal_code and id.
     * Used by admin.
     *
     * @param Query $query
     * @param $search
     * @return Query
     */
    public function searchIndexAdmin(Query $query, $search)
    {
        $regx = '/^[0-9]{2}$/';
        if (preg_match($regx, $search) === 1) {
            $query = $this->findUserDepartment($query, $search);
        } else {
            $query = $this->searchIndex($query, $search);
        }

        $query->orWhere(['Announces.id' => $search]);

        return $query;
    }

    /**
     * Find the Announces of the department.
     *
     * @param Query $query
     * @param $department
     * @return Query
     */
    public function findUserDepartment(Query $query, $department)
    {
        return $query->where(function ($exp) use ($department) {
            return $exp->between('Users.postal_code', $department . '000', $department . '999');
        });
    }

    /**
     * Find validated announces.
     *
     * @param Query $query
     * @return Query
     */
    public function findValidated(Query $query)
    {
        return $query->where(['status IN' => [
            self::status['online'],
            self::status['expired']
        ]]);
    }

    /**
     * Selector of the attributes required for
     * individual view.
     *
     * @param Query $query
     * @return $this
     */
    public function findView(Query $query)
    {
        return $query
            ->select(
                [
                    'id',
                    'title',
                    'description',
                    'created',
                    'modified',
                    'status'
                ]);
    }

    /**
     * Selector used for indexing the announces.
     * Announces status must be online.
     *
     * @param Query $query
     * @return $this
     */
    public function findIndex(Query $query)
    {
        return $query
            ->where(['status' => self::status['online']])
            ->select([
                'id',
                'title',
                'created'
            ]);
    }

    /**
     * Selector used for index (manage and Admin).
     *
     * @param Query $query
     * @return $this
     */
    public function findTable(Query $query)
    {
        return $query
            ->select([
                'id',
                'title',
                'modified',
                'created',
                'status'
            ]);
    }
}
