<?php
namespace App\Model\Table;

use App\Event\NotificationManager;
use App\Model\Entity\Message;
use Cake\Collection\Collection;
use Cake\Event\Event;
use Cake\I18n\Time;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Messages Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Users
 * @property \Cake\ORM\Association\BelongsTo $Author
 * @property \Cake\ORM\Association\HasMany $Files
 *
 * @method \App\Model\Entity\Message get($primaryKey, $options = [])
 * @method \App\Model\Entity\Message newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Message[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Message|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Message patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Message[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Message findOrCreate($search, callable $callback = null)
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class MessagesTable extends Table
{
    use SearchTrait;

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('messages');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Recipients', [
            'className' => 'Users'
        ]);
        $this->belongsTo('Authors', [
            'className' => 'Users'
        ]);
        $this->hasMany('Files', [
            'dependent' => true,
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->uuid('id')
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('content', 'create', __('Un message est attendu.'))
            ->notEmpty('content');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['recipient_id'], 'Recipients'));
        $rules->add($rules->existsIn(['author_id'], 'Authors'));

        $rules->addCreate(function ($message) {
            return $message->recipient_id !== $message->author_id;
        }, 'recipientIsNotAuthor', [
            'errorField' => 'recipient_id',
            'message' => __('Le destinataire doit être différent de l\'auteur.'),
        ]);

        $rules->addCreate($rules->validCount('files', 4, '<=', 'Vous devez avoir au plus 4 fichiers associées.'));

        return $rules;
    }

    /**
     * {@inheritDoc}
     */
    public function afterSave(Event $event, Message $message)
    {
        if ($message->isNew()) {
            $this->eventManager()->on(new NotificationManager());

            $eventAfterCreate = new Event('Model.Messages.afterCreate', $this, [
                'message' => $message
            ]);
            $this->eventManager()->dispatch($eventAfterCreate);
        }
    }

    public function findBasic(Query $query)
    {
        return $query
            ->select(
                [
                    'content',
                    'created',
                ]);
    }

    /**
     * Select used for the view of a message.
     *
     * @param Query $query
     * @return $this
     */
    public function findView(Query $query)
    {
        return $this->findBasic($query)
            ->select([
                'id',
                'author_id',
                'modified',
                'seen',
            ]);
    }

    /**
     * Return the last message for each Conversation with the others Users.
     * An User can only have one Conversation (all the messages exchanged).
     *
     * @param $recipient_id - the recipient of the messages
     * @return $this - the messages containing the Authors
     */
    public function findLastMessages($recipient_id)
    {
        $messagesReceived = $this->findByRecipientId($recipient_id);
        $messagesReceived
            ->select(['Messages__created' => $messagesReceived->func()->max('Messages.created')])
            ->contain(['Authors' => function ($q) {
                return $q->select('id');
            }])
            ->group('Authors__id');

        $messagesSent = $this->findByAuthorId($recipient_id);
        $messagesSent
            ->select([
                'Messages__created' => $messagesReceived->func()->max('Messages.created'),
                'Authors__id' => 'Messages.recipient_id'
            ])
            ->group('Authors__id');

        $maxMessagesCreated = $this->find();
        $maxMessagesCreated
            ->select(['Messages__created' => $maxMessagesCreated->func()->max('Messages__created'), 'Authors__id' => 'Authors__id'])
            ->from(['sub' => $messagesReceived->union($messagesSent)])
            ->group('Authors__id');

        $messages = $this->find('basic');
        $messages
            ->innerJoin(['maxMessagesCreated' => $maxMessagesCreated], ['Messages.created = maxMessagesCreated.Messages__created'])
            ->select([
                'Authors__id' => 'Authors__id',
                'Messages__created' => 'Messages__created'
            ])
            ->contain(['Authors' => [
                'foreignKey' => false,
                'queryBuilder' => function ($q) {
                    return $q
                        ->find('name')
                        ->select('email')
                        ->where(['Authors.id = (Authors__id)']);
                }
            ]])
            ->order(['Messages__created' => 'DESC']);

        return $messages;
    }

    /**
     * Return the conversation (a Collection of Messages) between
     * two users.
     *
     * @param Query $query
     * @param array $options - array of the users id
     * @return Query
     */
    public function findConversation(Query $query, array $options)
    {
        $query
            ->where(['recipient_id' => $options[0], 'author_id' => $options[1]])
            ->orWhere(['recipient_id' => $options[1], 'author_id' => $options[0]]);

        return $query;
    }

    /**
     * Search the Conversations corresponding to the search value.
     * Search by date, author.
     *
     * @param Query $query - the query that will be used for the search
     * @param $search
     * @return Query
     */
    protected function _searchConversations(Query $query, $search)
    {
        $search = strtolower($search);
        $datePattern = '/(\d{1,2})(?:-|\/|\.)(\d{1,2})(?:-|\/|\.)(\d{4})/';
        $formattedDate = [];
        preg_match($datePattern, $search, $formattedDate);

        $titleQuery = $query->func()->levenshtein([$search, 'LOWER(Authors.title)' => 'literal']);
        $firstnameQuery = $query->func()->levenshtein([$search, 'LOWER(Authors.firstname)' => 'literal']);
        $lastnameQuery = $query->func()->levenshtein([$search, 'LOWER(Authors.lastname)' => 'literal']);
        $nameQuery = $query->func()->levenshtein([$search, "LOWER(CONCAT(Authors.firstname, ' ',Authors.lastname))" => 'literal']);

        // tolerance is the distance of the string accepted
        $tolerance = 2;
        if (sizeof($formattedDate) > 0) {
            $query->where([function ($exp) use ($formattedDate) {
                return $exp->between('Messages.created', $formattedDate[3] . '-' . $formattedDate[2] . '-' . $formattedDate[1], $formattedDate[3] . '-' . $formattedDate[2] . '-' . $formattedDate[1] . ' 23:59:59');
            }]);
        } else {
            $query
                ->where(function ($exp) use ($titleQuery, $tolerance) {
                    return $exp->between($titleQuery, 0, $tolerance)->isNotNull('Authors.title');
                })
                ->orWhere(function ($exp) use ($firstnameQuery, $tolerance) {
                    return $exp->between($firstnameQuery, 0, $tolerance);
                })
                ->orWhere(function ($exp) use ($lastnameQuery, $tolerance) {
                    return $exp->between($lastnameQuery, 0, $tolerance);
                })
                ->orWhere([function ($exp) use ($nameQuery, $tolerance) {
                    return $exp->between($nameQuery, 0, $tolerance);
                }]);
        }

        $query->distinct();


        return $query;
    }

    /**
     * Count the Conversations not seen (Users messages exchanges that the
     * author_id have not seen yet).
     *
     * @param $author_id
     * @return integer - number of not seen conversations
     */
    public function countNotSeenConversations($author_id)
    {
        return $this->findByRecipientId($author_id)
            ->select('author_id')
            ->where(function ($exp) {
                return $exp->isNull('seen');
            })
            ->group('author_id')
            ->count();
    }

    /**
     * Count the Messages not seen of a Conversation (messages exchanged
     * between 2 distinct users).
     *
     * @param $sender_id - the sender of the messages not seen
     * @param $recipient_id - the recipient of the not seen messages
     * @return mixed
     */
    public function countNotSeenMessages($sender_id, $recipient_id)
    {
        return $this->findByRecipientIdAndAuthorId($recipient_id, $sender_id)
            ->where(function ($exp) {
                return $exp->isNull('seen');
            })
            ->count();
    }

    /**
     * Update the seen state of messages. Set the seen Datetime at NOW.
     *
     * @param Collection|Query $messages - the messages to mark as seen
     * @param $author_id - the author_id of the messages
     * @return int - number of messages seen
     */
    public function markSeen($messages, $author_id)
    {
        if ($messages->isEmpty()) {
            return 0;
        }
        return $this->updateAll([
            'seen' => new Time()
        ], [
            function ($exp) {
                return $exp->isNull('seen');
            },
            'author_id' => $author_id,
            'id IN' => $messages->extract('id')->toArray()
        ]);
    }
}
