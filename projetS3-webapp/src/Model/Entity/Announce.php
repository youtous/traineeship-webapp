<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Announce Entity
 *
 * @property int $id
 * @property string $title
 * @property string $description
 * @property string $user_id
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Candidacy[] $candidacies
 * @property \App\Model\Entity\File[] $files
 * @property \App\Model\Entity\Traineeship[] $traineeships
 * @property \App\Model\Entity\Tag[] $tags
 */
class Announce extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => false,
        'title' => true,
        'description' => true,
        'status' => true,
        'user_id' => true,
        'created' => true,
        'modified' => true,
    ];
}
