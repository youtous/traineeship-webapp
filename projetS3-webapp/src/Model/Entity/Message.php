<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Message Entity
 *
 * @property string $id
 * @property string $object
 * @property string $content
 * @property \Cake\I18n\Time $created
 * @property string $recipient_id
 * @property string $author_id
 */
class Message extends Entity
{

    /**
     * Check if the Message is editable (10 min)
     *
     * @return bool
     */
    protected function _getEditable()
    {
        return $this->created->wasWithinLast('10 minutes');
    }

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => false,
        'seen' => true,
        'content' => true,
        'created' => true,
        'modified' => true,
        'recipient_id' => true,
        'author_id' => true,
    ];

    protected $_virtual = [
        'editable',
    ];
}
