<?php
namespace App\Model\Entity;

use App\Model\Table\FilesTable;
use Cake\ORM\Entity;

/**
 * File Entity
 *
 * @property string $id
 * @property string $mime
 * @property string $name
 * @property int $newsletter_id
 * @property int $announce_id
 * @property string $student_id
 * @property string $message_id
 *
 * @property \App\Model\Entity\Newsletter $newsletter
 * @property \App\Model\Entity\Announce $announce
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Message $message
 */
class File extends Entity
{
    /**
     * Return the visibility of the File
     * Public if belongsTo a newsletter.
     *
     * @return bool
     */
    protected function _getPublic()
    {
        return !empty($this->newsletter_id);
    }

    /**
     * Return the extension of the File
     *
     * @return the extension
     */
    protected function _getExtension()
    {
        return in_array($this->mime, array_keys(FilesTable::$mimeTypes)) ? FilesTable::$mimeTypes[$this->mime] : 'txt';
    }

    /**
     * Return the type of the File
     *
     * @return the type
     */
    protected function _getType()
    {
        return explode('/', $this->mime)[0];
    }

    /**
     * Return the absolute path of the File
     *
     * @return the path
     */
    protected function _getPath()
    {
        return FilesTable::UPLOAD_PATH . $this->id . '.' . $this->extension;
    }

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => false,
        'mime' => true,
        'name' => true,
        'newsletter_id' => true,
        'announce_id' => true,
        'student_id' => true,
        'message_id' => true,
    ];

    protected $_virtual = [
        'public',
        'extension',
        'path',
        'type'
    ];
}
