<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

class Validation extends Entity
{
    /**
     * Check if the Confirmation has expired (2 days)
     *
     * @return bool
     */
    protected function _getExpired()
    {
        return !($this->created->wasWithinLast('2 days'));
    }

    protected $_accessible = [
        '*' => false,
        'user_id' => true,
        'type' => true,
        'code' => true,
        'created' => true,
    ];

    protected $_virtual = [
        'expired',
    ];
}
