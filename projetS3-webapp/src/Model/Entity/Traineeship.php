<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Traineeship Entity
 *
 * @property int $id
 * @property \Cake\I18n\Time $beginning
 * @property \Cake\I18n\Time $ending
 * @property \Cake\I18n\Time $created
 * @property int $announce_id
 * @property string $student_id
 * @property string $professor_id
 *
 * @property \App\Model\Entity\Announce $announce
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Appreciation[] $appreciations
 */
class Traineeship extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
