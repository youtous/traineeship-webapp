<?php
namespace App\Model\Entity;

use App\Model\Table\NotificationsTable;
use Cake\ORM\Entity;

/**
 * Notification Entity
 *
 * @property int $id
 * @property string $type
 * @property string $user_id
 * @property \Cake\I18n\Time $created
 *
 * @property \App\Model\Entity\User $user
 */
class Notification extends Entity
{

    /**
     * Return the class attribute frequency related to the type of this Notification.
     *
     * @return Time frequency (string)
     */
    protected function _getFrequency()
    {
        return NotificationsTable::types[$this->type]['frequency'];
    }

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => false,
        'type' => true,
        'user_id' => true,
        'created' => 'true',
    ];

    protected $_virtual = [
        'frequency',
    ];
}
