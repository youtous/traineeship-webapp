<?php
namespace App\Model\Entity;

use App\Model\Table\SkillsTable;
use Cake\ORM\Entity;

/**
 * Skill Entity
 *
 * @property int $id
 * @property string $name
 *
 * @property \App\Model\Entity\User[] $users
 */
class Skill extends Entity
{
    /**
     * Return the public webpath of the Skill
     *
     * @return the path
     */
    protected function _getWebpath()
    {
        return SkillsTable::SKILLS_IMAGES_WEBPATH . $this->id . '.png';
    }

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => false,
        'name' => true
    ];

    protected $_virtual = [
        'webpath',
    ];
}
