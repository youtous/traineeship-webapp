<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;
use Cake\Auth\DefaultPasswordHasher;

/**
 * User Entity
 *
 * @property string $id
 * @property string $firstname
 * @property string $title
 * @property string $lastname
 * @property string $street
 * @property \Cake\I18n\Time $birthday
 * @property string $sex
 * @property int $street_number
 * @property string $street_number_complement
 * @property string $city
 * @property int $postal_code
 * @property int $phone
 * @property string $email
 * @property string $password
 * @property \Cake\I18n\Time $created
 * @property \Cake\I18n\Time $modified
 * @property \Cake\I18n\Time $last_login
 * @property string $last_ip
 * @property bool $subscribed
 * @property string $role
 *
 * @property \App\Model\Entity\Announce[] $announces
 * @property \App\Model\Entity\Appreciation[] $appreciations
 * @property \App\Model\Entity\Skill[] $skills
 */
class User extends Entity
{

    /**
     * Format with 5 digits the postal code of the User.
     *
     * @return string the 5 digits
     */
    protected function _getPostalCodeFormatted()
    {
        return str_pad($this->postal_code, 5, '0', STR_PAD_LEFT);
    }

    /**
     * Concatenate the firstname and the lastname.
     * If the User is a company, return the title.
     *
     * @return string
     */
    protected function _getName()
    {
        if ($this->title != null) {
            return $this->title;
        } else {
            return $this->firstname . ' ' . $this->lastname;
        }
    }


    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => false,
        'firstname' => true,
        'title' => true,
        'lastname' => true,
        'street' => true,
        'birthday' => true,
        'sex' => true,
        'street_number' => true,
        'street_number_complement' => true,
        'city' => true,
        'postal_code' => true,
        'phone' => true,
        'website' => true,
        'email' => true,
        'password' => true,
        'created' => true,
        'modified' => true,
        'last_login' => true,
        'last_ip' => true,
        'subscribed' => true,
        'role' => true,
    ];

    protected $_virtual = [
        'name',
        'postal_code_formatted',
    ];

    /**
     * Fields that are excluded from JSON versions of the entity.
     *
     * @var array
     */
    protected $_hidden = [
        'password',
    ];

    protected function _setPassword($password)
    {
        return (new DefaultPasswordHasher)->hash($password);
    }
}
