<?php
namespace App\Model\Behavior;

use Cake\Datasource\EntityInterface;
use Cake\Event\Event;
use Cake\ORM\Behavior;
use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;
use Cake\Utility\Inflector;

/**
 * CompositeIncrement behavior
 */
class CompositeIncrementBehavior extends Behavior
{

    /**
     * Default configuration.
     *
     * @var array
     */
    protected $_defaultConfig = [
        'field' => 'id',
        'where_fields' => [],
        'table_name' => null
    ];

    /**
     * Auto-Increment the composite primary key.
     *
     * @param Entity $entity
     * @return bool
     */
    public function increment(Entity $entity)
    {
        if(!$entity->isNew()) {
            return true;
        }

        $config = $this->config();
        if ($config['table_name'] == null) {
            $config['table_name'] = Inflector::pluralize(Inflector::classify((new \ReflectionClass(get_class($entity)))->getShortName()));
        }

        $where = [];
        foreach ($config['where_fields'] as $field) {
            $where[$field] = $entity->$field;
        }

        $table = TableRegistry::get($config['table_name']);
        $max = $table->find()->select($config['field'])->where($where);

        if ($max->isEmpty()) {
            $entity->{$config['field']} = 1;
        } else {
            $entity->{$config['field']} = $max->max(function ($row) use ($config) {
                    return $row->{$config['field']};
                })->id + 1;
        }
        return true;
    }

    /**
     * Called before saving the entity.
     *
     * @param Event $event
     * @param EntityInterface $entity
     */
    public function beforeSave(Event $event, EntityInterface $entity)
    {
        $this->increment($entity);
    }
}
