<?php
namespace App\Mailer;

use App\Model\Entity\Announce;
use App\Model\Entity\Traineeship;
use Cake\ORM\TableRegistry;

/**
 * Traineeship mailer.
 */
class TraineeshipMailer extends Mailer
{

    /**
     * Mailer's name.
     *
     * @var string
     */
    static public $name = 'Traineeship';

    /**
     * Generate an email to notify the user that a related Traineeship has been created.
     *
     * @param $user_id
     * @param Announce $announce
     */
    public function traineeshipCreated($user_id, Announce $announce)
    {
        $user = TableRegistry::get('Users')->findById($user_id)->select(['email'])->first();
        $this
            ->to($user->email)
            ->subject('Un stage vous concernant a été initié')
            ->set(compact('announce'));
    }

    /**
     * Generate an email to notify the student that the Traineeship has been canceled.
     *
     * @param Traineeship $traineeship
     */
    public function traineeshipCanceled(Traineeship $traineeship)
    {
        $usersTable = TableRegistry::get('Users');
        $user = $usersTable->findById($traineeship->student_id)->select([
            'email',
            'firstname',
            'lastname',
        ])->firstOrFail();

        $announcesTable = TableRegistry::get('Announces');
        $announce = $announcesTable->findById($traineeship->announce_id)->select(['title'])->firstOrFail();

        $this
            ->to($user->email)
            ->subject(sprintf('%s, votre stage a été annulé', $user->name))
            ->set(compact('announce'));

    }
}
