<?php
namespace App\Mailer;

use App\Model\Entity\Announce;
use Cake\ORM\TableRegistry;

/**
 * Announce mailer.
 */
class AnnounceMailer extends Mailer
{

    /**
     * Mailer's name.
     *
     * @var string
     */
    static public $name = 'Announce';

    /**
     * Notify the validation the author of the Announce.
     *
     * @param $announce_id
     */
    public function announceValidated($announce_id)
    {
        $announce = TableRegistry::get('Announces')
            ->findById($announce_id)
            ->select([
                'id',
                'title',
                'user_id',
            ])
            ->firstOrFail();


        $user = TableRegistry::get('Users')
            ->findById($announce->user_id)
            ->select([
                'email',
                'firstname',
                'lastname',
                'title'
            ])->firstOrFail();

        $this
            ->to($user->email)
            ->subject(sprintf('Votre annonce a été publiée !'))
            ->set(compact('user', 'announce'));
    }

}
