<?php
namespace App\Mailer;

use App\Model\Entity\User;

/**
 * User mailer.
 */
class UserMailer extends Mailer
{

    /**
     * Mailer's name.
     *
     * @var string
     */
    static public $name = 'User';

    /**
     * Generate an email to notify the account deletion.
     *
     * @param User $user
     */
    public function goodbye(User $user)
    {
        $this
            ->to($user->email)
            ->subject(sprintf('Au revoir %s', $user->name));
    }
}
