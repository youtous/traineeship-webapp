<?php
namespace App\Mailer;

use App\Model\Entity\Announce;
use App\Model\Entity\User;

/**
 * Notification mailer.
 */
class NotificationMailer extends Mailer
{

    /**
     * Mailer's name.
     *
     * @var string
     */
    static public $name = 'Notification';

    /**
     * Notify the User he have a number of unread messages.
     *
     * @param User $user
     * @param int $countUnreadConversations
     */
    public function messagesUnread(User $user, $countUnreadConversations)
    {
        $this
            ->to($user->email)
            ->subject(sprintf('%s, vous avez %s messages en attente', $user->name, $countUnreadConversations))
            ->set(compact('countUnreadConversations'));
    }

    /**
     * Notify the User he have some new Candidacies.
     *
     * @param User $user
     * @param $countCandidacies
     * @param Announce $announce
     */
    public function newCandidacies(User $user, Announce $announce, $countCandidacies)
    {
        $this
            ->to($user->email)
            ->subject(sprintf('%s, vous avez %s candidatures pour votre annonce %', $user->name, $countCandidacies, $announce->title))
            ->set(compact('announce', 'countCandidacies'));
    }

}
