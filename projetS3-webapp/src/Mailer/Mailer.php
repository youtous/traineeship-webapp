<?php
namespace App\Mailer;

use Cake\Mailer\Mailer as MailerDefault;

/**
 * Default mailer of this app.
 */
class Mailer extends MailerDefault
{

    /**
     * Email format is html by default.
     *
     * Mailer constructor.
     * @param null $email
     */
    public function __construct($email = null)
    {
        parent::__construct($email);
        $this->emailFormat('html');
    }
}
