<?php
namespace App\Mailer;

use App\Model\Entity\Announce;
use Cake\ORM\TableRegistry;

/**
 * Candidacy mailer.
 */
class CandidacyMailer extends Mailer
{

    /**
     * Mailer's name.
     *
     * @var string
     */
    static public $name = 'Candidacy';

    /**
     * Notify the student that his candidacy has been accepted by the company.
     *
     * @param $student_id
     * @param $announce_id
     * @internal param $candidacy_id
     */
    public function candidacyAccepted($student_id, $announce_id)
    {
        $announce = TableRegistry::get('Announces')
            ->findById($announce_id)
            ->select([
                'title',
            ])
            ->contain([
                'Users' => function ($q) {
                    return $q->select([
                        'title'
                    ]);
                }
            ])
            ->firstOrFail();

        $user = TableRegistry::get('Users')
            ->findById($student_id)
            ->select([
                'email',
                'firstname',
                'lastname',
            ])->firstOrFail();

        $this
            ->to($user->email)
            ->subject(sprintf('Une de vos candidature a été retenue !'))
            ->set(compact('user', 'announce'));
    }

}
