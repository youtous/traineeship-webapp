<?php
namespace App\Mailer;

use App\Model\Entity\User;

/**
 * Confirmations mailer.
 */
class ValidationMailer extends Mailer
{

    /**
     * Mailer's name.
     *
     * @var string
     */
    static public $name = 'Validation';

    /**
     * Generate an email to notify the activation action.
     *
     * @param User $user
     * @param String $code
     */
    public function activation(User $user, $code)
    {
        $this
            ->to($user->email)
            ->subject(sprintf('Bienvenue %s', $user->name))
            ->set(['code' => $code, 'email' => $user->email]);
    }

    /**
     * Generate an email to notify the password change action.
     *
     * @param User $user
     * @param String $code
     */
    public function password(User $user, $code)
    {
        $this
            ->to($user->email)
            ->subject(sprintf('Requête de changement de mot de passe'))
            ->set(['code' => $code, 'email' => $user->email]);
    }

    /**
     * Generate an email to confirm the account deletion.
     *
     * @param User $user
     * @param String $code
     */
    public function deleteAccount(User $user, $code)
    {
        $this
            ->to($user->email)
            ->subject(sprintf('Demande de suppression de compte'))
            ->set(compact('code'));
    }
}
