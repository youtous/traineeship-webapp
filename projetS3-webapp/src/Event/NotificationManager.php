<?php
namespace App\Event;

use App\Model\Entity\Candidacy;
use App\Model\Entity\Message;
use App\Model\Entity\Notification;
use App\Model\Entity\User;
use App\Model\Table\CandidaciesTable;
use Cake\Event\Event;
use Cake\Event\EventListenerInterface;
use Cake\Mailer\MailerAwareTrait;
use Cake\ORM\TableRegistry;

/**
 * Manager of Notifications.
 *
 * Notify with a Notification for implemented events.
 */
class NotificationManager implements EventListenerInterface
{
    private $Notifications;
    private $Users;

    use MailerAwareTrait;

    /**
     * Notification constructor.
     */
    public function __construct()
    {
        $this->Notifications = TableRegistry::get('Notifications');
        $this->Users = TableRegistry::get('Users');
    }

    /**
     * Returns a list of events this object is implementing. When the class is registered
     * in an event manager, each individual method will be associated with the respective event.
     *
     * @return array associative array or event key names pointing to the function
     * that should be called in the object when the respective event is fired
     */
    public function implementedEvents()
    {
        return [
            'Model.Messages.afterCreate' => 'notifyMessagesUnread',
            'Model.Candidacies.afterCreate' => 'notifyNewCandidacies',
        ];
    }

    /**
     * Handle the notify message event.
     *
     * @param Event $event
     * @param Message $message
     */
    public function notifyMessagesUnread(Event $event, Message $message)
    {
        $this->_notifyLater('messagesUnread', [$message->recipient_id]);
    }

    /**
     * Handle the notify candidacy event.
     *
     * @param Event $event
     * @param Candidacy $candidacy
     * @internal param Message $message
     */
    public function notifyNewCandidacies(Event $event, Candidacy $candidacy)
    {
        $this->_notifyLater('newCandidacies', [$candidacy->announce_id]);
    }

    /**
     * Notify the company of the new candidacies.
     *
     * @param $announce_id
     * @return bool - the success of the notification
     */
    public function newCandidacies($announce_id)
    {
        $countCandidacies = TableRegistry::get('Candidacies')->findByAnnounceIdAndStatus($announce_id, CandidaciesTable::status['pending'])->count();
        $announce = TableRegistry::get('Announces')->findById($announce_id)->select(['id', 'user_id', 'title'])->firstOrFail();

        return $this->_notify($announce->user_id, compact('announce', 'countCandidacies'));
    }

    /**
     * Notify the recipient that he have unread messages.
     *
     * @param $user_id - id of the user
     * @return bool - the success of the notification
     */
    public function messagesUnread($user_id)
    {
        $countUnreadConversations = TableRegistry::get('Messages')->countNotSeenConversations($user_id);
        return $this->_notify($user_id, compact('countUnreadConversations'));
    }

    /**
     * Notify the User. This method check that the targeted User exists,
     * and notify the User if he accepts notifications and have not been
     * already notified.
     *
     * @param $user_id - User that will be notified
     * @param array $args - others arguments associated to the Notification
     * @return bool
     */
    protected function _notify($user_id, $args = [])
    {
        $user = $this->Users->findNameById($user_id)
            ->select(['accept_notifications', 'email'])->firstOrFail();

        $notification = $this->Notifications->newEntity([
            'user_id' => $user_id,
            // the type of the notification is the name of the method that call this method.
            'type' => debug_backtrace()[1]['function']
        ]);

        /*
         * Check the User accept the notifications, not alreadyNotified.
         */
        if ($user->accept_notifications &&
            !$this->Notifications->alreadyNotified($notification)
        ) {
            $notification = $this->Notifications->save($notification);

            // if Notification saved, then notify
            if ($notification) {
                $this->_notifyEmail($notification, $user, $args);
                return true;
            } else {
                return false;
            }
        }
        // required for the worker to delete the task
        return true;
    }


    /**
     * Notify a Notification by Email.
     *
     * @param Notification $notification - the Notification
     * @param User $user - the recipient of the Email
     * @param array $args - array of arguments send to the mailer method
     * @return array
     */
    protected function _notifyEmail(Notification $notification, User $user, $args = [])
    {
        return $this->getMailer('Notification')->send(
            $notification->type,
            array_merge([$user], $args)
        );
    }

    /**
     * Create a Notification job for the specified
     * method.
     *
     * @param $method - method to be called
     * @param $args - arguments of the method
     * @return int $id of the Job
     */
    protected function _notifyLater($method, array $args)
    {
        return TableRegistry::get('Queue.QueuedJobs')->createJob('Notification', [
            'method' => $method,
            'args' => $args,
        ]);
    }
}