<?php
namespace App\Controller;

use App\Model\Table\UsersTable;
use Cake\Event\Event;

/**
 * Validations Controller
 *
 * @property \App\Model\Table\ValidationsTable $Validations
 */
class ValidationsController extends AppController
{

    /**
     * Init for each method
     */
    public function initialize()
    {
        parent::initialize();
        $this->loadModel('Users');

        if (in_array($this->request->action, ['validateEmail', 'retrievePassword'])) {
            $this->loadComponent('Recaptcha.Recaptcha');
        }
    }

    /**
     * Filter the requests
     *
     * @param Event $event
     * @return \Cake\Network\Response|null
     */
    public function beforeFilter(Event $event)
    {
        if (in_array($this->request->action, [
            'validateEmail',
            'retrievePassword',
            'confirmPassword',
            'confirmEmail'
        ])) {
            if ($this->Auth->user()) {
                $this->Flash->error(__('Vous êtes déjà connecté.'));
                return $this->redirect($this->request->referer());
            }
            $this->Auth->allow();
        }
        parent::beforeFilter($event);
    }


    /**
     * Request a new validation email.
     */
    public function validateEmail()
    {
        $validation = $this->Validations->newEntity();

        if ($this->request->is('post')) {
            if ($this->Recaptcha->verify()) {
                $validation = $this->Validations->patchEntity($validation, ['email' => $this->request->data['email']], ['validate' => 'retrieve']);
                if (!$validation->errors()) {
                    $user = $this->Users->findByEmail($this->request->data('email'))->select(['id', 'role', 'email'])->firstOrFail();
                    if ($user->role === UsersTable::roles['waiting']) {
                        $validation = $this->Validations->patchEntity($validation, [
                            'user_id' => $user->id,
                            'type' => 'activation',
                        ]);
                        if ($this->Validations->save($validation)) {
                            $this->Flash->success(__('Un nouveau message de confirmation vient de vous être envoyé !'));
                        } else {
                            $this->Flash->error(__('Erreur lors de la nouvelle génération du compte.'));
                        }
                    } else {
                        $this->Flash->error(__('Votre compte est déjà validé !'));
                    }
                    return $this->redirect(['_name' => 'login']);
                } else {
                    $this->Flash->error(__('Veuillez vérifier les informations soumises.'));
                }
            } else {
                $this->Flash->error(__('Prouvez que vous n\'êtes pas un robot.'));
            }

        }

        $this->set(compact('validation'));
        $this->set('validation', $validation);
    }

    /**
     * Try to confirm the specified email.
     * Change the role of the user if valid.
     *
     * @param $email
     * @param $code
     * @return \Cake\Network\Response|null
     */
    public function confirmEmail($email, $code)
    {
        $user = $this->Users->findByEmail($email)->select(['id', 'role', 'email'])->firstOrFail();

        $valid = $this->Validations->validate($code, $user->id, 'activation');

        if (!$valid) {
            $this->Flash->error(__('Votre clef de validation a expirée ! Veuillez en générer une autre.'));
            return $this->redirect(['_name' => 'requestValidationEmail']);
        } else {
            $user->role = $this->Users->computeRole($email);
            if ($this->Users->save($user) && ($user->role !== null)) {
                $this->Flash->success(__('Votre compte est désormais validé ! Vous pouvez vous connecter'));
                return $this->redirect(['_name' => 'login']);
            }
            $this->Flash->error(__('Erreur lors de la modification du compte'));
            return $this->redirect(['_name' => 'register']);
        }
    }

    /**
     * Request a new validation change password.
     */
    public function retrievePassword()
    {
        $validation = $this->Validations->newEntity();

        if ($this->request->is('post')) {
            if ($this->Recaptcha->verify()) {
                $validation = $this->Validations->patchEntity($validation, ['email' => $this->request->data['email']], ['validate' => 'retrieve']);
                if (!$validation->errors()) {
                    $user = $this->Users->findByEmail($this->request->data('email'))->select(['id', 'email'])->firstOrFail();
                    $validation = $this->Validations->patchEntity($validation, [
                        'user_id' => $user->id,
                        'type' => 'password',
                    ]);
                    if ($this->Validations->save($validation)) {
                        $this->Flash->success(__('Un lien de reintialisation de votre mot de passe vient de vous être envoyé.'));
                    } else {
                        $this->Flash->error(__('Erreur lors de la nouvelle génération de reinitialisation.'));
                    }

                    return $this->redirect(['_name' => 'login']);
                } else {
                    $this->Flash->error(__('Veuillez vérifier les informations soumises.'));
                }
            } else {
                $this->Flash->error(__('Prouvez que vous n\'êtes pas un robot.'));
            }
        }

        $this->set(compact('validation'));
        $this->set('validation', $validation);
    }

    /**
     * Try to confirm the specified new password.
     *
     * @param $email
     * @param $code
     * @return \Cake\Network\Response|null
     */
    public function confirmPassword($email, $code)
    {
        $user = $this->Users->findByEmail($email)->select(['id', 'email'])->firstOrFail();

        if ($this->request->is(['post', 'patch', 'put'])) {
            if (!$this->Validations->validate($code, $user->id, 'password')) {
                $this->Flash->error(__('Votre clef de validation a expirée ! Veuillez en générer une autre.'));
                return $this->redirect(['_name' => 'requestNewPassword']);
            } else {
                $user = $this->Users->patchEntity($user, ['password' => $this->request->data('password')]);
                if ($this->Users->save($user)) {
                    $this->Flash->success(__('Mot de passe du compte mis à jour.'));
                    return $this->redirect(['_name' => 'login']);
                } else {
                    $this->Flash->error(__('Erreur dans le mot de passe soumis : ' . array_values($user->errors('password'))[0]));
                    return $this->redirect(['_name' => 'requestNewPassword']);
                }
            }
        }

        $this->set(compact('user'));
        $this->set('user', $user);
    }

    /**
     * Request a validation for deleting the account.
     */
    public function deleteAccount()
    {
        $this->request->allowMethod(['post', 'delete']);

        $user = $this->Users->findById($this->Auth->user('id'))->select(['id', 'email'])->firstOrFail();
        $validation = $this->Validations->newEntity();

        $validation = $this->Validations->patchEntity($validation, [
            'email' => $user->email,
            'user_id' => $user->id,
            'type' => 'deleteAccount',
        ]);
        if ($this->Validations->save($validation)) {
            $this->Flash->success(__('Veuillez confirmer via votre email, vouloir supprimer votre compte.'));
        } else {
            $this->Flash->error(__('Erreur lors de la nouvelle génération de confirmation.'));
        }

        return $this->redirect(['_name' => 'updateProfile']);
    }

    /**
     * Try to confirm the deletion of the user account.
     *
     * @param $code
     * @return \Cake\Network\Response|null
     */
    public function confirmDeleteAccount($code)
    {
        $user = $this->Users->findById($this->Auth->user('id'))->select(['id', 'email'])->firstOrFail();

        if ($this->request->is(['post', 'patch', 'put', 'delete'])) {
            if ($this->request->data('confirm') === "JE NE SOUHAITE PLUS FAIRE PARTIE DE L'AVENTURE") {
                if (!$this->Validations->validate($code, $user->id, 'deleteAccount')) {
                    $this->Flash->error(__('Votre clef de validation a expirée ! Veuillez en générer une autre.'));
                    return $this->redirect(['_name' => 'updateProfile']);
                } else {
                    if ($this->Users->delete($user)) {
                        $this->Flash->success(__('Le compte a été supprimé !'));
                        return $this->redirect($this->Auth->logout());
                    } else {
                        $this->Flash->error('Votre compte n\'a pas pu être supprimé !');
                        return $this->redirect(['_name' => 'updateProfile']);
                    }
                }
            } else {
                $this->Flash->error('Désolé, nous n\'avons pas reconnu votre confirmation !');
            }
        }
    }
}
