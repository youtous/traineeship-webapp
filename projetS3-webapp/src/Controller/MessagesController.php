<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Database\Expression\OrderByExpression;

/**
 * Messages Controller
 *
 * @property \App\Model\Table\MessagesTable $Messages
 */
class MessagesController extends AppController
{

    use FilesHandlerTrait;

    /**
     * {@inheritDoc}
     */
    public function initialize()
    {
        parent::initialize();
        $this->loadModel('Users');
    }

    /**
     * Index method
     *
     * @see \App\Model\Table\MessagesTable::countNotSeenMessages()
     * @param int $limit - how many last messages to get
     * @return \Cake\Network\Response|null
     */
    public function index($limit = 7)
    {
        /*
         * Count the number of messages not seen by the current user.
         * Used in the select as `countNotSeen` in the main Query.
         */
        $messagesNotSeen = $this->Messages->findByRecipientId($this->Auth->user('id'))
            ->where(function ($exp) {
                return $exp->isNull('seen');
            })
            ->where(['author_id = Authors__id'])
            ->select(['countNotSeen' => 'count(*)']);

        /*
         * Selecting the last messages, adding the number of not seen messages in the select.
         */
        $messages = $this->Messages->findLastMessages($this->Auth->user('id'))
            ->select(['Messages__countNotSeen' => $messagesNotSeen]);

        /*
         * Paginate the whole thing to be able to navigate.
         */
        $this->paginate($messages, ['limit' => $limit]);


        $this->set(compact('messages'));
    }

    /**
     * Index the messages of the user, more detailed.
     */
    public function indexDetailed()
    {
        $messages = $this->Messages->findLastMessages($this->Auth->user('id'));

        $search = $this->request->query('search');
        if ($search != null) {
            $search = trim($search);
            $messages = $this->Messages->search($messages, $search, 'conversations');
        }

        $messages = $this->paginate($messages, ['limit' => 15]);

        $this->set(compact('messages', 'search'));
    }

    /**
     * Print the Message in the conversation.
     *
     * @see conversation()
     * @param $id - the id of the Message
     */
    public function view($id)
    {
        $message = $this->Messages->find()
            ->select(['created', 'recipient_id', 'author_id'])
            ->where([['id' => $id],
                'OR' => [
                    ['recipient_id' => $this->Auth->user('id')],
                    ['author_id' => $this->Auth->user('id')],
                ],
            ])
            ->firstOrFail();

        // determinate the recipient
        $recipient_id = $message->recipient_id;
        if ($message->recipient_id === $this->Auth->user('id')) {
            $recipient_id = $message->author_id;
        }

        $conversation = $this->Messages->find('conversation', [$recipient_id, $this->Auth->user('id')]);
        $conversation = $this->Messages->findView($conversation);

        /*
         * Generate the conversation with next and prev messages.
         * The conversation size is fixed to 7.
         */
        $prevMessages = $conversation->cleanCopy()
            ->where(['Messages.created <' => $message->created])
            ->limit(3)
            ->order(['Messages.created' => 'DESC']);
        $nextMessages = $conversation->cleanCopy()->where(['Messages.created >' => $message->created])
            ->limit(3)
            ->order(['Messages.created' => 'ASC']);

        /**
         * limit = 3 (7 the totalSize - 1 ) /2 => 3 messages prev, 3 messages next
         *
         * if nextMessage have less than the desired limit(3), add the difference to the prevLimit
         * $prevMessages->limit = limit + (limit - $nextMessages->count()))
         */
        $maxNext = $nextMessages->count();
        $maxPrev = $prevMessages->count();
        $prevLimit = 3 + (3 - ($maxNext > 3 ? 3 : $maxNext));
        $nextLimit = 3 + (3 - ($maxPrev > 3 ? 3 : $maxPrev));
        $messageQuery = $conversation->cleanCopy()->where(['id' => $id])
            ->union($prevMessages->limit($prevLimit))
            ->union($nextMessages->limit($nextLimit));
        // be careful, ->union prevent to chain other methods to sub-queries

        $this->conversation($recipient_id, $messageQuery);
        $this->render('conversation');
    }

    /**
     * Navigate in a conversation.
     *
     * @param $recipient_id - the recipient of the messages (defining the Conversation)
     * @param $direction - next|prev : get the messages created before or after the datime.
     * @param $datetime - DateTime of the selected message.
     * @return mixed|void
     */
    public function navigateConversation($recipient_id, $direction, $datetime)
    {
        $conversation = $this->Messages->find('conversation', [$recipient_id, $this->Auth->user('id')]);

        $datetime = str_replace('_', ' ', $datetime);

        if ($direction === 'prev') {
            $conversation
                ->where(['Messages.created <' => $datetime]);
        } else {
            $conversation
                ->where(['Messages.created >' => $datetime])
                // define the order, message[next+1, next+2, next+3...
                ->order(['Messages.created' => 'ASC']);
        }

        if ($conversation->isEmpty()) {
            if ($this->request->is(['ajax', 'json', 'xml'])) {
                // terminate the action, send empty response
                return;
            } else {
                $this->Flash->default(__('Aucun message à sélectionner.'));
                return $this->redirect(['action' => 'conversation', 'recipient_id' => $recipient_id]);
            }
        }

        // format the conversation then render
        $this->conversation($recipient_id, $conversation);
        $this->render('conversation');
    }

    /**
     * Index messages exchanged between the Auth.user and the recipient.
     *
     * When this action is called, all the messages related to the Recipient
     * will be marked as read.
     *
     * @param $recipient_id
     * @param null $conversation
     * @return mixed
     */
    public function conversation($recipient_id, $conversation = null)
    {
        if ($this->Users->findById($recipient_id)->count() !== 1 || $recipient_id === $this->Auth->user('id')) {
            $this->Flash->error(__('Le destinaire du message n\'est pas joignable.'));
            return $this->redirect(['action' => 'index']);
        }

        // get the recipient of this Conversation
        $recipient = $this->Users->findNameById($recipient_id)->select(['email', 'role'])->firstOrFail();

        // prevent index if not required
        if (!$this->request->is(['ajax', 'json', 'xml'])) {
            $this->index();
        }

        // checking if the conversation defined (Collection of messages)
        if ($conversation == null) {
            $conversation = $this->Messages->find('conversation', [$recipient_id, $this->Auth->user('id')]);
        }

        $conversation = $this->Messages->findView($conversation)
            ->contain(['Files' =>
                function ($q) {
                    return $q->find('view')->select(['message_id']);
                }])
            ->limit(7)
            // define the order if not set
            ->traverse(function (&$value) {
                if ($value == null) {
                    $value = new OrderByExpression(
                        ['Messages.created' => 'DESC']
                    );
                }
            }, ['order'])
            ->sortBy('created', SORT_ASC);

        // set seen
        $this->Messages->markSeen($conversation, $recipient_id);

        $message = $this->Messages->newEntity();

        $this->set(compact('conversation', 'recipient', 'message'));
    }

    /**
     *  Send a message to an User.
     *
     * @param $recipient_id - the User to send the message
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function send($recipient_id)
    {
        $this->request->allowMethod(['post']);

        $this->request->data['recipient_id'] = $recipient_id;
        $this->request->data['author_id'] = $this->Auth->user('id');
        $this->_handleFileRequest($this->request->data['files'], 4);

        $message = $this->Messages->newEntity();

        $message = $this->Messages->patchEntity($message, $this->request->data, [
            'fieldList' => [
                'content',
                'author_id',
                'recipient_id',
                'files',
            ],
            'associated' => [
                'Files' => [
                    'fieldList' => [
                        'name',
                        'tmp_name',
                        'type',
                        'mime',
                    ]
                ]
            ],
        ]);

        if ($this->Messages->save($message)) {
            $this->Flash->success(__('Message envoyé.'));
        } else {
            $this->Flash->error(__('Erreur lors de l\'envois du message, vérifiez le type de fichier et sa taille.'));
        }

        if ($this->request->is(['ajax', 'json', 'xml'])) {
            $this->set(compact('message'));
            return;
        }
        return $this->redirect(['action' => 'conversation', 'recipient_id' => $recipient_id]);
    }

    /**
     * Edit method.
     * Associated files should not be modified.
     *
     * @param string $id Message id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id)
    {
        $message = $this->Messages->findByIdAndAuthorId($id, $this->Auth->user('id'))
            ->select(['id', 'content', 'seen', 'created'])
            ->contain(['Recipients' => function ($q) {
                return $q->find('name')->select(['email', 'role']);
            }])
            ->firstOrFail();

        if (!$message->editable) {
            $this->Flash->error(__('Ce message n\'est plus modifiable.'));
            return $this->redirect(['action' => 'view', 'id' => $id]);
        }

        if ($this->request->is(['patch', 'post', 'put'])) {
            $message = $this->Messages->patchEntity($message, $this->request->data, [
                'fieldList' => [
                    'content'
                ]
            ]);
            if ($this->Messages->save($message)) {
                $this->Flash->success(__('Votre message a été modifé.'));
                return $this->redirect(['action' => 'view', 'id' => $id]);
            } else {
                $this->Flash->error(__('Le message n\'a pas pu être modifié. Essayez encore.'));
            }
        }

        $this->set(compact('message'));
    }

    /**
     * Delete method, a Message can be deleted if he is editable.
     *
     * @param $id
     * @return \Cake\Network\Response|null Redirects to the conversation.
     */
    public function delete($id)
    {
        $this->request->allowMethod(['post', 'delete']);

        $message = $this->Messages->findByIdAndAuthorId($id, $this->Auth->user('id'))
            ->select(['id', 'created', 'recipient_id'])
            ->firstOrFail();

        if (!$message->editable) {
            $this->Flash->error(__('Ce message n\'est plus modifiable.'));
        } else if ($this->Messages->delete($message)) {
            $this->Flash->success(__('Message supprimé.'));
        } else {
            $this->Flash->error(__('Erreur lors de la suppression du message, veuillez retenter.'));
        }

        return $this->redirect(['action' => 'conversation', 'recipient_id' => $message->recipient_id]);
    }

    /**
     * Mark all the messages as seen of a Conversation.
     *
     * @param $recipient_id
     * @return \Cake\Network\Response|null
     */
    public function markSeen($recipient_id)
    {
        $this->Users->findById($recipient_id)->select('id')->firstOrFail();
        $marked = $this->Messages->markSeen(
            $this->Messages->find('conversation', [$recipient_id, $this->Auth->user('id')])->select('id'),
            $recipient_id
        );

        if ($marked > 0) {
            $this->Flash->success(sprintf(__('%s messages marqués comme lus !'), $marked));
        } else {
            $this->Flash->error(__('Aucun message à marquer comme \'vu\'.'));
        }

        return $this->redirect(['action' => 'conversation', 'recipient_id' => $recipient_id]);
    }


    /**
     * Number of conversations not seen by the Auth->user.
     * JSON action.
     */
    public function notSeenConversations()
    {
        $this->request->allowMethod(['json']);

        $count = $this->Messages->countNotSeenConversations($this->Auth->user('id'));

        $this->set(compact('count'));
    }

    /**
     * Get the updated messages for a Conversation after the $datetime.
     * That includes: new Messages, seen Messages and modified Messages.
     * Set the messages seen.
     *
     * Messages are sorted by created ASC.
     *
     * JSON action.
     * @param $recipient_id
     * @param $datetime - the last check (by the Client)
     */
    public function updated($recipient_id, $datetime)
    {
        $this->request->allowMethod(['json']);

        $this->Users->findById($recipient_id)->select('id')->firstOrFail();

        $datetime = str_replace('_', ' ', $datetime);

        $messages = $this->Messages->find('conversation', [$recipient_id, $this->Auth->user('id')])
            ->orWhere([
                'created >' => $datetime,
                'seen >' => $datetime,
                'modified >' => $datetime
            ])
            ->contain(['Files' =>
                function ($q) {
                    return $q->find('view')->select(['message_id']);
                }])
            ->order(['Messages.created' => 'ASC']);

        $this->Messages->markSeen($messages, $recipient_id);

        $this->set(compact('messages'));
    }
}
