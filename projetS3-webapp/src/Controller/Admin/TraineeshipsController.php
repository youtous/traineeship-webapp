<?php
namespace App\Controller\Admin;

use App\Controller\Admin\AppController;
use App\Model\Table\UsersTable;
use Cake\Chronos\Date;
use Cake\Core\Configure;
use Cake\Filesystem\File;
use Cake\Validation\Validator;

/**
 * Traineeships Controller
 *
 * @property \App\Model\Table\TraineeshipsTable $Traineeships
 */
class TraineeshipsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => [
                'Announces' => function ($q) {
                    return $q->select(['title', 'id']);
                },
                'Announces.Users' => function ($q) {
                    return $q->select(['firstname', 'lastname', 'title', 'id']);
                },
                'Students' => function ($q) {
                    return $q->select(['firstname', 'lastname', 'id']);
                },
                'Professors' => function ($q) {
                    return $q->select(['firstname', 'lastname', 'id']);
                }
            ],
            'limit' => 10,
            'order' => [
                'Traineeships.beginning' => 'asc'
            ]
        ];

        $traineeships = $this->Traineeships->find()->select([
            'id',
            'beginning',
            'ending'
        ]);

        $traineeships = $this->paginate($traineeships);

        Configure::load('traineeships');
        $defaultDate = Configure::readOrFail('Traineeships.beginning');

        $this->set(compact('traineeships', 'defaultDate'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Traineeship id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $traineeship = $this->Traineeships->findById($id)
            ->contain([
                'Students' => function ($q) {
                    return $q->find('table');
                },
                'Professors' => function ($q) {
                    return $q->find('table');
                },
                'Announces' => function ($q) {
                    return $q->select([
                        'description',
                        'title'
                    ]);
                },
                'Announces.Skills',
                'Announces.Users' => function ($q) {
                    return $q->find('NameFullAddress')->select(['role']);
                }
            ])
            ->select([
                'id',
                'beginning',
                'ending',
                'created'
            ])
            ->firstOrFail();

        $this->loadModel('Users');
        $professors = $this->Users->find('list', [
            'keyField' => 'id',
            'valueField' => function ($user) {
                return $user->get('name');
            }
        ])
            ->where(['role IN' => [UsersTable::roles['professor'], UsersTable::roles['admin']]])
            ->select(['firstname', 'lastname', 'id'])
            ->toArray();

        $this->set(compact('traineeship', 'appreciation', 'professors'));
    }

    /**
     * Configure the default starting date
     */
    public function configure()
    {
        $this->request->allowMethod('Post');

        $configFile = new File(CONFIG . 'traineeships.php');

        $validator = new Validator();
        $validator
            ->requirePresence('beginning')
            ->date('beginning', ['ymd'], __('Une date de début par défaut est requise.'));
        $errors = $validator->errors($this->request->data);

        if (empty($errors)) {
            $date = Date::createFromFormat('Y-m-d', $this->request->data['beginning'])->format('Y-m-d');
            $config = <<<PHP
<?php

return [
    'Traineeships' => [
        'beginning' => '$date',
        'duration' => '2 months'
  ],
];
PHP;
            if ($configFile->write($config)) {
                $this->Flash->success(__('Paramètres mis à jours.'));
            }
        } else {
            $this->Flash->error(__('Erreur lors de l\'écriture des paramètres.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    /**
     * Delete method
     *
     * @param string|null $id user id.
     * @return \Cake\Network\Response|null Redirects to index.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $traineeship = $this->Traineeships->get($id);
        if ($this->Traineeships->delete($traineeship)) {
            $this->Flash->success(__('Le stage a été supprimé.'));
        } else {
            $this->Flash->error(__('Le stage n\'a pas pû être supprimé. Veuillez retenter.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
