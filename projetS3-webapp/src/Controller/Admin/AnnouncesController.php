<?php
namespace App\Controller\Admin;

use App\Controller\Admin\AppController;
use App\Model\Table\AnnouncesTable;
use Cake\Network\Exception\NotFoundException;


/**
 * Admin AnnouncesController
 *
 * @property \App\Model\Table\UsersTable $Users
 */
class AnnouncesController extends AppController
{


    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $announces = $this->Announces->find('table')
            ->contain([
                'Users' => function ($q) {
                    return $q->find('nameAddress');
                }
            ]);

        /**
         * Search
         */
        $search = $this->request->query('search');
        if ($search != null) {
            $search = trim($search);
            $announces = $this->Announces->searchIndexAdmin($announces, $search);
        }

        /**
         * Filter by status
         */
        $status = $this->request->query('status');
        if (in_array($status, array_keys(AnnouncesTable::status))) {
            $announces = $announces->where(['status' => AnnouncesTable::status[$status]]);
        }

        $announces = $this->paginate($announces, [
            'limit' => 15,
            'order' => ['Announces.created' => 'desc']
        ]);

        $this->set(compact('announces', 'search', 'status'));
        $this->set(['statusTranslation' => $this->statusTranslation]);
    }

    /**
     * Show waiting Announces one by one.
     * Redirect to index if nothing to moderate.
     */
    public function waiting()
    {
        $announces = $this->Announces->findViewByStatus(AnnouncesTable::status['waiting'])
            ->contain([
                'Skills',
                'Users' => function ($q) {
                    return $q->find('nameFullAddress');
                }
            ])
            ->order(['Announces.created' => 'asc']);

        if ($announces->count() === 0) {
            $this->Flash->success(__('Aucune annonce à modérer.'));
            $this->redirect(['action' => 'index']);
        }

        try {
            $announces = $this->paginate($announces, [
                'limit' => 1,
                'maxLimit' => 1
            ]);
        } catch (NotFoundException $e) {
            $this->redirect(['action' => 'waiting']);
        }

        $this->set(compact('announces'));
    }

    /**
     * View any Announce.
     *
     * @param $id
     */
    public function view($id)
    {
        $announce = $this->Announces->findViewById($id)
            ->contain([
                'Skills',
                'Users' => function ($q) {
                    return $q->find('nameFullAddress');
                }
            ])->firstOrFail();

        $this->set(compact('announce'));
    }

    /**
     * Validate a waiting Announce.
     * Redirect to referer.
     *
     * @param $id
     */
    public function validate($id = null)
    {
        $this->request->allowMethod(['post']);

        $announce = $this->Announces->findByStatusAndId(AnnouncesTable::status['waiting'], $id)
            ->select(['status', 'title', 'id', 'user_id', 'modified'])
            ->firstOrFail();

        // disable modified update
        $announce->dirty('modified', true);

        $announce = $this->Announces->patchEntity($announce, [
            'status' => AnnouncesTable::status['online']
        ]);

        // disable rules -> these rules are for updating/creating...
        if ($this->Announces->save($announce, ['checkRules' => false])) {
            // send mail to the author
            $this->loadModel('Queue.QueuedJobs');
            $this->QueuedJobs->createJob('Mailer', [
                'mailer' => [
                    'name' => 'Announce',
                    'method' => 'announceValidated'
                ],
                'data' => [$announce->id]
            ]);

            $this->Flash->success(__('L\'annonce a été mise en ligne !'));
        } else {
            $this->Flash->error(__('L\'annonce n\'a pas pû être mise en ligne. Veuillez retenter.'));
        }

        $this->redirect($this->request->referer());
    }

    /**
     * Delete method
     *
     * @param string|null $id user id.
     * @return \Cake\Network\Response|null Redirects to index.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $announce = $this->Announces->get($id);
        if ($this->Announces->delete($announce)) {
            $this->Flash->success(__('L\'annonce a été supprimée.'));
        } else {
            $this->Flash->error(__('L\'annonce n\'a pas pû être supprimée. Veuillez retenter.'));
        }

        // used for validation processus
        if ($this->request->query('redirectReferer')) {
            return $this->redirect($this->request->referer());
        }

        return $this->redirect(['action' => 'index']);
    }
}
