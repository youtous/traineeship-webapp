<?php
namespace App\Controller\Admin;

use App\Controller\Admin\AppController;
use Cake\Filesystem\Folder;


/**
 * Files Controller
 *
 * @property \App\Model\Table\FilesTable $Files
 */
class FilesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->loadModel('Skills');
        $this->loadModel('Messages');
        $this->loadModel('Announces');
        $this->loadModel('Users');

        $filesCount = [
            'messages' => $this->Files->find()->where(function ($exp) {
                return $exp->isNotNull('message_id');
            })->count(),
            'skills' => $this->Skills->find()->count(),
            'candidacies' => $this->Files->find()
                ->where(function ($exp) {
                    return $exp->isNotNull('student_id');
                })
                ->andWhere(function ($exp) {
                    return $exp->isNotNull('announce_id');
                })
                ->count(),
            'newsletter' => $this->Files->find()->where(function ($exp) {
                return $exp->isNotNull('newsletter_id');
            })->count(),
        ];

        $filesCount['total'] = array_sum($filesCount);

        $tmpSize = (new Folder(ROOT . DS . 'tmp'))->dirsize();
        $logsSize = (new Folder(ROOT . DS . 'logs'))->dirsize();

        $freeSpace = disk_free_space(".");
        $used = (new Folder(ROOT))->dirsize();

        $this->set(compact('filesCount', 'logsSize', 'tmpSize', 'freeSpace', 'used', 'messagesCount', 'usersCount', 'announcesCount'));
    }

    /**
     * Delete method
     *
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete()
    {
        if($this->request->is(['post', 'delete'])) {
            $id = $this->request->data['id'];

            $this->request->allowMethod(['post', 'delete']);
            $file = $this->Files->findById($id);
            if ($file->isEmpty()) {
                $this->Flash->error(__('Fichier introuvable.'));
            } else if ($this->Files->delete($file->first())) {
                $this->Flash->success(__('Le fichier a été supprimé.'));
            } else {
                $this->Flash->error(__('Erreur lors de la suppression du fichier, veuillez retenter.'));
            }
        }
    }
}
