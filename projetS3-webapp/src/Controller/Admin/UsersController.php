<?php
namespace App\Controller\Admin;

use App\Controller\Admin\AppController;


/**
 * Admin UsersController
 *
 * @property \App\Model\Table\UsersTable $Users
 */
class UsersController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $users = $this->Users->find('table');

        $search = $this->request->query('search');
        if($search != null) {
            $search = trim($search);
            $users = $this->Users->search($users, $search, 'value');
        }

        $users = $this->paginate($users, ['limit' => 15]);

        $this->set(compact('users', 'search'));
    }

    /**
     * Edit method
     *
     * @param string|null $id user id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, $this->request->data, [
                'fieldList' => [
                    'firstname',
                    'lastname',
                    'title',
                    'street',
                    'birthday',
                    'street_number',
                    'street_number_complement',
                    'city',
                    'website',
                    'postal_code',
                    'phone',
                    'password',
                    'subscribed',
                    'accept_notifications',
                    'role',
                    'email',
                ]
            ]);
            if ($this->Users->save($user)) {
                $this->Flash->success(__('L\'utilisateur a été modifié.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('L\'utilisateur n\'a pu être modifié. Essayez encore.'));
            }
        }
        $this->set(compact('user'));
    }

    /**
     * Delete method
     *
     * @param string|null $id user id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $user = $this->Users->get($id);
        if ($this->Users->delete($user)) {
            $this->Flash->success(__('L\'utilisateur a été supprimé.'));
        } else {
            $this->Flash->error(__('L\'utilisateur n\'a pas pû être supprimé. Veuillez retenter.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
