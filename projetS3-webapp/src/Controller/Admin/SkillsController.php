<?php
namespace App\Controller\Admin;

use App\Controller\Admin\AppController;

/**
 * Skills Controller
 *
 * @property \App\Model\Table\SkillsTable $Skills
 */
class SkillsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $skills = $this->paginate($this->Skills, [
            'limit' => 7,
        ]);

        $this->set(compact('skills'));
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $skill = $this->Skills->newEntity();
        if ($this->request->is('post')) {
            $skill = $this->Skills->patchEntity($skill, $this->request->data);
            if ($this->Skills->save($skill)) {
                $this->Flash->success(__('Nouvelle compétence ajoutée.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('La compétence n\'a pas pu être ajoutée.Essayez encore.'));
            }
        }
        $this->set(compact('skill'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Skill id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $skill = $this->Skills->get($id);
        if ($this->Skills->delete($skill)) {
            $this->Flash->success(__('Compétence supprimée.'));
        } else {
            $this->Flash->error(__('Impossible de supprimer la compétence.Essayez encore.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
