<?php
namespace App\Controller;

use App\Controller\AppController;
use App\Model\Table\CandidaciesTable;
use Cake\Cache\Cache;
use Cake\Network\Exception\NotFoundException;

/**
 * Candidacies Controller
 *
 * @property \App\Model\Table\CandidaciesTable $Candidacies
 */
class CandidaciesController extends AppController
{

    use FilesHandlerTrait;

    /**
     * Postulate method.
     * Maximum one Candidacy per Announce.
     *
     * @param $id - the announce->id associated to the candidacy
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function postulate($id)
    {
        $user_id = $this->Auth->user('id');

        if ($this->Candidacies->hasPostulate($user_id, $id)) {
            $this->Flash->error(__('Vous avez déjà postulé à cette annonce !'));
            return $this->redirect(['_name' => 'announces:view', 'id' => $id]);
        }

        $candidacy = $this->Candidacies->newEntity();
        if ($this->request->is('post')) {

            $this->request->data['student_id'] = $user_id;
            $this->request->data['announce_id'] = $id;
            $this->request->data['status'] = CandidaciesTable::status['pending'];

            $this->_handleFileRequest($this->request->data['files'], 3);

            /**
             * files[0] -> lettre de motivation
             * files[1] -> c.v.
             */
            if (isset($this->request->data['files'][0])) {
                $this->request->data['files'][0]['name'] = 'Lettre de motivation';
            }
            if (isset($this->request->data['files'][1])) {
                $this->request->data['files'][1]['name'] = 'C.V.';
            }

            $candidacy = $this->Candidacies->patchEntity($candidacy,
                $this->request->data, [
                    'fieldList' => [
                        'content',
                        'status',
                        'student_id',
                        'announce_id',
                        'files'
                    ],
                    'associated' => [
                        'Files' => [
                            'fieldList' => [
                                'name',
                                'tmp_name',
                                'type',
                                'mime',
                            ]
                        ]
                    ]
                ]
            );

            if ($this->Candidacies->save($candidacy)) {
                $this->Flash->success(__('Votre candidature a été envoyée à l\'annonceur.'));
                return $this->redirect(['_name' => 'announces:view', 'id' => $id]);
            } else {
                $this->Flash->error(__('La candidature comporte des erreurs. Essayez encore, la taille des fichiers est limitée !'));
            }
        }

        $announce = $this->Candidacies->Announces->findById($id)->select(['id', 'title'])->firstOrFail();
        // set the announce id (required for view)
        $candidacy->announce_id = $announce->id;

        $this->set(compact('candidacy', 'announce'));
    }

    /**
     * Manage the Candidacies. Used by students.
     *
     * @return \Cake\Network\Response|null
     */
    public function manage()
    {
        $pending = $this->Candidacies->findTableByStudentIdAndStatus(
            $this->Auth->user('id'),
            CandidaciesTable::status['pending']
        )
            ->select(['content'])
            ->contain([
                'Announces' => function ($q) {
                    return $q
                        ->select(['id', 'title', 'status']);
                },
                'Announces.Users' => function ($q) {
                    return $q->select('title');
                },
                'Files' => function ($q) {
                    return $q->find('view')->select(['announce_id', 'student_id', 'name']);
                }
            ])
            ->order(['Candidacies.created' => 'desc']);

        $accepted = $this->Candidacies->findTableByStudentIdAndStatus(
            $this->Auth->user('id'),
            CandidaciesTable::status['accepted']
        )
            ->select(['content'])
            ->contain([
                'Announces' => function ($q) {
                    return $q
                        ->select(['id', 'title', 'status']);
                },
                'Announces.Users' => function ($q) {
                    return $q->select('title');
                },
                'Files' => function ($q) {
                    return $q->find('view')->select(['announce_id', 'student_id', 'name']);
                }
            ])
            ->order(['Candidacies.modified' => 'desc']);

        $validated = $this->Candidacies->findTableByStudentIdAndStatus(
            $this->Auth->user('id'),
            CandidaciesTable::status['validated']
        )
            ->contain([
                'Announces' => function ($q) {
                    return $q
                        ->select(['title']);
                },
                'Announces.Users' => function ($q) {
                    return $q->select('title');
                }
            ])
            ->order(['Candidacies.modified' => 'desc']);


        // redirect to post if the user don't have Candidacies
        if ($pending->isEmpty() && $accepted->isEmpty() && $validated->isEmpty()) {
            $this->Flash->set(__('Vous n\'avez pas de candidature pour le moment !'));
            return $this->redirect(['_name' => 'home']);
        }

        $this->set(compact('pending', 'accepted', 'validated'));
    }

    /**
     * Confirm the Candidacy, change the status to confirmed.
     * Create the associated Traineeship.
     *
     * The candidacy must have the accepted status to be confirmed by
     * a student.
     *
     * @param $announce_id
     * @return \Cake\Network\Response|null
     */
    public function confirm($announce_id)
    {
        $this->request->allowMethod(['post', 'patch']);

        $candadicy = $this->Candidacies->findByStudentIdAndAnnounceIdAndStatus($this->Auth->user('id'), $announce_id, CandidaciesTable::status['accepted'])
            ->select(['status', 'student_id', 'announce_id'])->firstOrFail();

        $candadicy = $this->Candidacies->patchEntity($candadicy, [
            'status' => CandidaciesTable::status['validated']
        ]);

        if ($this->Candidacies->save($candadicy)) {
            // delete the cache
            Cache::delete('candidacies_acceptedCandidacies-' . $this->Auth->user('id'), 'minutes');

            $this->Flash->success(__('La procédure de stage a été initée. Votre stage a été validé !'));
        } else {
            $this->Flash->error(__('La candidature n\'a pas pû être confirmée. Veuillez retenter.'));
        }

        return $this->redirect(['_name' => 'announces:candidacies:manage']);
    }

    /**
     * Accept the Candidacy, change the status to accepted.
     * Notify the student.
     *
     * @param $student_id
     * @param $announce_id
     * @return \Cake\Network\Response|null
     */
    public function accept($student_id, $announce_id)
    {
        $this->request->allowMethod(['post', 'patch']);

        // check the legitimate owner of the Announce
        $this->Candidacies->Announces->findTableByIdAndUserId($announce_id, $this->Auth->user('id'))->firstOrFail();

        $candadicy = $this->Candidacies->findByStudentIdAndAnnounceIdAndStatus($student_id, $announce_id, CandidaciesTable::status['pending'])
            ->select(['status', 'student_id', 'announce_id'])->firstOrFail();

        $candadicy = $this->Candidacies->patchEntity($candadicy, [
            'status' => CandidaciesTable::status['accepted']
        ]);

        if ($this->Candidacies->save($candadicy)) {

            // notify
            $this->loadModel('Queue.QueuedJobs');
            $this->QueuedJobs->createJob('Mailer', [
                'mailer' => [
                    'name' => 'Candidacy',
                    'method' => 'candidacyAccepted'
                ],
                'data' => [$candadicy->student_id, $candadicy->announce_id]
            ]);

            // delete the cache
            Cache::delete('candidacies_waitingCandidacies-' . $this->Auth->user('id'), 'minutes');

            $this->Flash->success(__('Le candidat a été informé que vous avez retenu sa candidature.'));
        } else {
            $this->Flash->error(__('La candidature n\'a pas pû être acceptée. Veuillez retenter.'));
        }

        return $this->redirect(['_name' => 'announces:candidacies:review', 'announce_id' => $announce_id]);
    }

    /**
     * Review the Candidacies for the Announce.
     *
     * @param $announce_id
     * @return \Cake\Network\Response|null
     */
    public function review($announce_id)
    {
        // check the legitimate owner of the Announce
        $announce = $this->Candidacies->Announces->findTableByIdAndUserId($announce_id, $this->Auth->user('id'))->firstOrFail();
        $candidacies = $this->Candidacies->findReviewByAnnounceIdAndStatus($announce_id, CandidaciesTable::status['pending'])
            ->contain([
                'Users' => function ($q) {
                    return $q
                        ->select([
                            'id',
                            'firstname',
                            'lastname',
                            'email',
                        ]);
                },
                'Files' => function ($q) {
                    return $q->find('view')->select(['announce_id', 'student_id', 'name']);
                }
            ])
            ->order(['Candidacies.created' => 'asc']);

        if ($candidacies->isEmpty()) {
            return $this->redirect(['_name' => 'announces:manage']);
        }

        $candidacies = $this->paginate($candidacies, ['limit' => 30]);

        $this->set(compact('announce', 'candidacies'));
    }

    /**
     * Delete method
     *
     * @param $student_id
     * @param $announce_id
     * @return \Cake\Network\Response|null Redirects to index.
     * @internal param null|string $id Candidacy id.
     */
    public function delete($student_id, $announce_id)
    {
        $this->request->allowMethod(['post', 'delete']);

        $announce_author = $this->Candidacies->Announces->findById($announce_id)->select(['user_id'])->firstOrFail();
        if (!in_array($this->Auth->user('id'), [$announce_author->user_id, $student_id])) {
            throw new NotFoundException();
        }

        $candidacy = $this->Candidacies->get([$student_id, $announce_id]);

        if ($this->Candidacies->delete($candidacy)) {
            // delete the cache
            Cache::delete('candidacies_waitingCandidacies-' . $this->Auth->user('id'), 'minutes');
            Cache::delete('candidacies_acceptedCandidacies-' . $this->Auth->user('id'), 'minutes');

            $this->Flash->success(__('La candidature a été supprimée.'));
        } else {
            $this->Flash->error(__('La candidature n\'a pas pu être supprimée. Essayez encore.'));
        }

        return $this->redirect($this->request->referer());
    }
}
