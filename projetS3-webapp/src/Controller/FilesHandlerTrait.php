<?php
/**
 * Created by PhpStorm.
 * User: youtous
 * Date: 15/12/2016
 * Time: 13:58
 */

namespace App\Controller;


trait FilesHandlerTrait
{

    /**
     * Handle the request files data sent.
     * The provided request data will be modified.
     *
     * @param array $requestDataFiles - the Request->data['files']
     * @param int $maxFiles - max files to handle
     */
    private function _handleFileRequest(array &$requestDataFiles, $maxFiles = 1) {
        // trim max files input
        if (sizeof($requestDataFiles) > $maxFiles) {
            array_splice($requestDataFiles, $maxFiles);
        }

        // manipulating array, size can change
        $arraySize = sizeof($requestDataFiles);
        //remove empty file
        for ($i = 0; $i < $arraySize; $i++) {
            if ($requestDataFiles[$i]['size'] === 0) {
                unset($requestDataFiles[$i]);
            }
        }
    }

}