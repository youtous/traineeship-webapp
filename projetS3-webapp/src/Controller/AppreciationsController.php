<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Appreciations Controller
 *
 * @property \App\Model\Table\AppreciationsTable $Appreciations
 */
class AppreciationsController extends AppController
{
    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add($traineeship_id)
    {
        $this->request->allowMethod('post');

        if (!$this->Appreciations->Traineeships->isPartOfTraineeship($this->Auth->user('id'), $this->Auth->user('role'), $traineeship_id)) {
            throw new NotFoundException();
        }

        $appreciation = $this->Appreciations->newEntity();

        $this->request->data['traineeship_id'] = $traineeship_id;
        $this->request->data['user_id'] = $this->Auth->user('id');

        $appreciation = $this->Appreciations->patchEntity($appreciation, $this->request->data, [
            'fieldList' => [
                'traineeship_id',
                'user_id',
                'content',
            ],
        ]);

        if ($this->Appreciations->save($appreciation)) {
            $this->Flash->success(__('L\'appréciation a été ajoutée.'));
        } else {
            $this->Flash->error(__('L\'appréciation n\'a pas pu être ajoutée. Veuillez retenter'));
        }

        return $this->redirect(['_name' => 'traineeships:view', 'id' => $traineeship_id]);
    }
}
