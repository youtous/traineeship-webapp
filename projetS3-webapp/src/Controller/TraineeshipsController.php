<?php
namespace App\Controller;

use App\Controller\AppController;
use App\Model\Table\UsersTable;
use Cake\Network\Exception\NotFoundException;

/**
 * Traineeships Controller
 *
 * @property \App\Model\Table\TraineeshipsTable $Traineeships
 */
class TraineeshipsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => [
                'Announces' => function ($q) {
                    return $q->select(['title']);
                },
                'Announces.Users' => function ($q) {
                    return $q->select(['firstname', 'lastname', 'title']);
                },
                'Students' => function ($q) {
                    return $q->select(['firstname', 'lastname']);
                },
                'Professors' => function ($q) {
                    return $q->select(['firstname', 'lastname']);
                }
            ],
            'limit' => 10,
            'order' => [
                'Traineeships.beginning' => 'asc'
            ]
        ];

        $traineeships = $this->Traineeships->find()->select([
            'id',
            'beginning',
            'ending'
        ]);
        switch ($this->Auth->user('role')) {
            case UsersTable::roles['student'] :
                $traineeships->where(['student_id' => $this->Auth->user('id')]);
                break;
            case UsersTable::roles['company'] :
                $announces = $this->Traineeships->Announces->findByUserId($this->Auth->user('id'))->select(['id']);
                $traineeships->where(['announce_id IN' => $announces]);
                break;
            case UsersTable::roles['professor'] :
                $traineeships->where(['professor_id' => $this->Auth->user('id')]);
                break;
        }
        if ($traineeships->isEmpty()) {
            $this->Flash->default(__('Vous n\'avez pas de stage pour le moment ! Patience...'));
            return $this->redirect(['_name' => 'home']);
        }
        $traineeships = $this->paginate($traineeships);

        $this->set(compact('traineeships'));
    }

    /**
     * View method
     *
     * @param string|null $id Traineeship id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        if (!$this->Traineeships->isPartOfTraineeship($this->Auth->user('id'), $this->Auth->user('role'), $id)) {
            throw new NotFoundException();
        }

        $traineeship = $this->Traineeships->findById($id)
            ->contain([
                'Students' => function ($q) {
                    return $q->find('table');
                },
                'Professors' => function ($q) {
                    return $q->find('table');
                },
                'Announces' => function ($q) {
                    return $q->select([
                        'description',
                        'title'
                    ]);
                },
                'Announces.Skills',
                'Announces.Users' => function ($q) {
                    return $q->find('NameFullAddress')->select(['role']);
                },
                'Appreciations' => function ($q) {
                    return $q->select(['content', 'created', 'traineeship_id'])->order(['Appreciations.created' => 'desc']);
                },
                'Appreciations.Users' => function ($q) {
                    return $q->find('table');
                }
            ])
            ->select([
                'id',
                'beginning',
                'ending',
                'created'
            ])
            ->firstOrFail();

        $appreciation = $this->Traineeships->Appreciations->newEntity();

        $this->set(compact('traineeship', 'appreciation'));
    }
}
