<?php
namespace App\Controller;

use Cake\Event\Event;
use Cake\Network\Exception\ForbiddenException;

/**
 * Files Controller
 *
 * @property \App\Model\Table\FilesTable $Files
 */
class FilesController extends AppController
{

    /**
     * Filter the requests
     *
     * @param Event $event
     * @return \Cake\Network\Response|null
     */
    public function beforeFilter(Event $event)
    {
        $this->Auth->allow('view');

        parent::beforeFilter($event);
    }

    /**
     * View method.
     * Check if the file is viewable.
     * The administrators are able to request any File.
     *
     * @param uuid $id File id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id)
    {
        if($this->Auth->user()) {
            $file = $this->Files->getRestricted($id, $this->Auth->user('id'), $this->Auth->user('role'));
            if(!$file) {
                throw new ForbiddenException('This file is not accessible.');
            }
        } else {
            $file = $this->Files->findViewById($id)->firstOrFail();
            if(!$file->public) {
                throw new ForbiddenException('This file is not public.');
            }
        }

        $this->response->file($file->path);
        return $this->response;
    }
}
