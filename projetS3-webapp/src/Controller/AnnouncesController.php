<?php
namespace App\Controller;

use App\Controller\AppController;
use App\Model\Table\AnnouncesTable;
use App\Model\Table\CandidaciesTable;
use App\Model\Table\UsersTable;
use Cake\Cache\Cache;
use Cake\Event\Event;
use Cake\ORM\TableRegistry;

/**
 * Announces Controller
 *
 * @property \App\Model\Table\AnnouncesTable $Announces
 */
class AnnouncesController extends AppController
{
    /**
     * Filter the Requests
     *
     * @param Event $event
     * @return \Cake\Network\Response|null
     */
    public function beforeFilter(Event $event)
    {
        if (in_array($this->request->action, ['index', 'view', 'search', 'indexRSS'])) {
            $this->Auth->allow();
        }
        parent::beforeFilter($event);
    }

    /**
     * Index method.
     * Announces can be searched (search query)
     * Announces can have different filters.
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $announces = $this->Announces->find('index')
            ->contain([
                'Skills',
                'Users' => function ($q) {
                    return $q->find('company');
                }
            ]);

        /**
         * Search query.
         */
        $search = $this->request->query('search');
        /**
         * use != not !==
         * $search type is string, empty string is associated to null here.
         */
        if ($search != null) {
            $search = trim($search);
            $announces = $this->Announces->searchIndex($announces, $search);
        }

        /**
         * Restrict the Announces to the specified department.
         */
        $department = $this->request->query('department');
        $regx = '/^[0-9]{2}$/';
        if (preg_match($regx, $department) === 1) {
            $announces = $this->Announces->findUserDepartment($announces, $department);
        }

        /**
         * Restrict Announces to specified skills (OR).
         */
        $restrictSkills = $this->request->query('skills');
        if ($restrictSkills != null && isset($restrictSkills['_ids']) && !empty($restrictSkills['_ids'])) {
            $restrictSkills = $restrictSkills['_ids'];
            $announces->matching(
                'Skills', function ($q) use ($restrictSkills) {
                return $q->where(['Skills.id IN' => $restrictSkills]);
            }
            );
        }

        /**
         * Remove the candidated Announces.
         */
        $candidated = $this->request->query('candidated');
        if ($candidated && $this->Auth->user('id')) {
            $candidatedAnnounces = $this->Announces->Candidacies->findByStudentId($this->Auth->user('id'))
                ->select('announce_id')->extract('announce_id')->toArray();
            // using array values for caching
            $announces = $announces->where(['Announces.id NOT IN' => $candidatedAnnounces]);
        }

        $announces->cache(function ($q) {
            return 'announces-' . md5(
                    serialize($q->clause('select')) .
                    serialize($q->clause('values')) .
                    serialize($q->clause('order')) .
                    serialize($q->clause('limit')) .
                    serialize($q->clause('offset')) .
                    serialize($q->clause('join')) .
                    serialize($q->clause('from')) .
                    serialize($q->clause('where')
                    )
                );
        }, 'dbResults');

        /**
         * Paginate the whole thing.
         */
        $announces = $this->paginate($announces, [
            'limit' => 10,
            'order' => ['Announces.created' => 'desc'],
        ]);

        $skills = $this->Announces->Skills->find('list')->cache(function ($q) {
            return 'skills-' . md5(
                    serialize($q->clause('select'))
                );
        }, 'dbResults');

        $this->set(compact('announces', 'search', 'skills', 'restrictSkills', 'department', 'candidated'));
    }

    /**
     * Index method used by RSS.
     */
    public function indexRSS()
    {
        $announces = $this->Announces->find('index')
            ->contain([
                'Users' => function ($q) {
                    return $q->select(['id', 'title']);
                }
            ])
            ->orderDesc('Announces.created')
            ->limit(25);

        $announces->cache(function ($q) {
            return 'announces-rss-' . md5(
                    serialize($q->clause('select')) .
                    serialize($q->clause('values')) .
                    serialize($q->clause('order')) .
                    serialize($q->clause('limit')) .
                    serialize($q->clause('offset')) .
                    serialize($q->clause('join')) .
                    serialize($q->clause('from')) .
                    serialize($q->clause('where')
                    )
                );
        }, 'dbResults');

        $this->set(compact('announces'));
        $this->render('index');
    }

    /**
     * Auto-matching announces with the Auth->User
     *
     * Announces/User matching based on :
     * - same department
     * - contain the skills
     * - not already candidated
     *
     * @return index
     */
    public function match()
    {
        $usersTable = TableRegistry::get('Users');
        $id = $this->Auth->user('id');
        $skills = $usersTable->Skills->find()
            ->matching('Users', function ($q) use ($id) {
                return $q->where(['Users.id' => $id]);
            })->extract('id')->toArray();

        $this->Flash->default(__('Puisse la puissance divine vous aider !'));
        $this->redirect(['action' => 'index', '?' => [
            'candidated' => true,
            'department' => substr(str_pad($this->Auth->user('postal_code'), 5, '0', STR_PAD_LEFT), 0, 2),
            'skills[_ids]' => $skills
        ]]);
    }

    /**
     * View method.
     *
     * Announces with online status can be viewed. Expired Announces
     * will redirect to index.
     *
     * @param string|null $id Announce id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id)
    {
        $announce = $this->Announces->findViewById($id)
            ->contain([
                'Skills',
                'Users' => function ($q) {
                    return $q->find('nameFullAddress');
                }
            ])
            ->where(['status IN' => [AnnouncesTable::status['online'], AnnouncesTable::status['expired']]]);

        $announce->cache('announce-view-' . $id, 'dbResults');

        $announce = $announce->firstOrFail();

        if ($announce->status === AnnouncesTable::status['expired']) {
            $this->Flash->error(__('Cette annonce a été archivée, elle n\'est donc plus disponible à la consultation.'));
            return $this->redirect(['action' => 'index']);
        }
        // create an empty candidacy
        $candidacy = $this->Announces->Candidacies->newEntity();
        $candidacy->announce_id = $id;
        // check if the user has already candidated
        $postulated = false;
        if ($this->Auth->user('role') == UsersTable::roles['student']) {
            $postulated = $this->Announces->Candidacies->hasPostulate($this->Auth->user('id'), $id);
        }

        $this->set(compact('announce', 'candidacy', 'postulated'));
    }

    /**
     * Post method
     * Users (companies) are able to post announces.
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function post()
    {
        // check maximum waiting announces the User.
        if ($this->Announces->findByUserIdAndStatus($this->Auth->user('id'), AnnouncesTable::status['waiting'])
                ->count() >= 10
        ) {
            $this->Flash->error(__('Vous avez atteint le nombre maximum d\'annonces à étudier pour le moment. Retentez après validation.'));
            $this->redirect(['action' => 'index']);
        }

        $announce = $this->Announces->newEntity();

        if ($this->request->is('post')) {
            $this->request->data['user_id'] = $this->Auth->user('id');

            // determinate the status of the Announce
            if ($this->Announces->findValidatedByUserId($this->Auth->user('id'))->count() >= 4) {
                $this->request->data['status'] = AnnouncesTable::status['online'];
            } else {
                $this->request->data['status'] = AnnouncesTable::status['waiting'];
            }


            $announce = $this->Announces->patchEntity($announce, $this->request->data, [
                'fieldList' => [
                    'title',
                    'description',
                    'status',
                    'user_id',
                    'skills'
                ]
            ]);
            if ($this->Announces->save($announce, ['associated' => ['Skills']])) {
                if ($announce->status === AnnouncesTable::status['online']) {
                    $this->Flash->success(__('La publication de votre annonce a été approuvée !'));
                } else {
                    $this->Flash->success(__('Votre annonce a été soumise à validation. Vous serez informé lors de sa mise en ligne.'));
                }

                return $this->redirect(['_name' => 'announces:manage']);
            } else {
                $this->Flash->error(__('L\'annonce n\'a pas pu être sauvegardée, essayez encore.'));
            }
        }

        $skills = $this->Announces->Skills->find('list');
        $this->set(compact('announce', 'skills'));
    }

    /**
     * Index all the Announces of Auth->user.
     */
    public function manage()
    {
        $waiting = $this->Announces->findTableByUserIdAndStatus(
            $this->Auth->user('id'),
            AnnouncesTable::status['waiting']
        )->contain(['Skills',
            'Traineeships' => function ($q) {
                return $q->select(['announce_id']);
            }
        ])
            ->order(['created' => 'desc']);

        $online = $this->Announces->findTableByUserIdAndStatus(
            $this->Auth->user('id'),
            AnnouncesTable::status['online']
        )
            ->contain(['Skills',
                'Traineeships' => function ($q) {
                    return $q->select(['announce_id']);
                },
                'Candidacies' => function ($q) {
                    return $q->where(['status' => CandidaciesTable::status['pending']])->select(['announce_id']);
                }
            ])
            ->order(['created' => 'desc']);

        $expired = $this->Announces->findTableByUserIdAndStatus(
            $this->Auth->user('id'),
            AnnouncesTable::status['expired']
        )
            ->contain(['Skills',
                'Candidacies' => function ($q) {
                    return $q->where(['status' => CandidaciesTable::status['pending']])->select(['announce_id']);
                }])
            ->order(['created' => 'desc']);

        // redirect to post if the user don't have Announces
        if ($waiting->isEmpty() && $online->isEmpty() && $expired->isEmpty()) {
            $this->Flash->set(__('Vous n\'avez pas d\'annonce pour le moment !'));
            return $this->redirect(['action' => 'post']);
        }

        $this->set(compact('expired', 'online', 'waiting'));
    }

    /**
     * Edit method
     *
     * Announces with online status can be edited.
     * Only the description can be edited.
     *
     * @param string|null $id Announce id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $announce = $this->Announces->findViewByIdAndUserIdAndStatus($id,
            $this->Auth->user('id'), AnnouncesTable::status['online'])
            ->contain([
                'Skills',
                'Traineeships' => function ($q) {
                    return $q->select(['announce_id']);
                }
            ])
            ->firstOrFail();

        if ($this->request->is(['patch', 'post', 'put'])) {
            $announce = $this->Announces->patchEntity($announce, $this->request->data, [
                'fieldList' => [
                    'description',
                ]
            ]);
            if ($this->Announces->save($announce)) {
                // delete the cache
                Cache::delete('announce-view-' . $announce->id, 'dbResults');

                $this->Flash->success(__('L’annonce a été modifiée.'));

                return $this->redirect(['_name' => 'announces:manage']);
            } else {
                $this->Flash->error(__('L’annonce n’a pas pû être modifiée. Veuillez retenter.'));
            }
        }

        $this->set(compact('announce'));
    }

    /**
     * Archive method.
     *
     * Announces with online status can be archived.
     *
     * @param string|null $id Announce id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function archive($id = null)
    {
        $this->request->allowMethod(['post']);
        $announce = $this->Announces->findByIdAndUserId($id, $this->Auth->user('id'))
            ->select(['id', 'status'])->firstOrFail();

        if ($announce->status !== AnnouncesTable::status['online']) {
            $this->Flash->error(__('Le statut de cette annonce en empêche son archivage.'));
        } else {
            $announce = $this->Announces->patchEntity($announce, [
                'status' => AnnouncesTable::status['expired']
            ]);

            if ($this->Announces->save($announce, ['checkRules' => false])) {
                $this->Flash->success(__('L’annonce a été archivée.'));
            } else {
                $this->Flash->error(__('L’annonce n’a pas pû être archivée. Veuillez retenter.'));
            }
        }

        return $this->redirect(['_name' => 'announces:manage']);
    }

    /**
     * Delete method.
     *
     * Announces status waiting,online
     * and wihtout associated Traineeships can be deleted.
     *
     * @param string|null $id Announce id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $announce = $this->Announces->findByIdAndUserId($id, $this->Auth->user('id'))
            ->select(['id', 'status'])->firstOrFail();

        if (!in_array($announce->status, [AnnouncesTable::status['waiting'], AnnouncesTable::status['online']])) {
            $this->Flash->error(__('Le statut de cette annonce en empêche sa suppression.'));
        } else {
            if ($this->Announces->delete($announce)) {
                $this->Flash->success(__('L’annonce a été supprimée.'));
            } else {
                if (!empty($announce->errors('Traineeships'))) {
                    $this->Flash->error($announce->errors('Traineeships')[0]);
                } else {
                    $this->Flash->error(__('L’annonce n’a pas pû être supprimée. Veuillez retenter.'));
                }
            }
        }

        return $this->redirect(['_name' => 'announces:manage']);
    }
}
