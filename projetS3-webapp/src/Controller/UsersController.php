<?php
namespace App\Controller;

use App\Controller\AppController;
use App\Model\Entity\User;
use App\Model\Table\UsersTable;
use Cake\Event\Event;
use Cake\I18n\Time;
use Cake\Log\Log;
use Cake\ORM\Query;
use Cake\ORM\TableRegistry;
use Cake\Routing\Router;


/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 */
class UsersController extends AppController
{
    /**
     * {@inheritDoc}
     */
    public function initialize()
    {
        parent::initialize();

        if ($this->request->action === 'register') {
            $this->loadComponent('Recaptcha.Recaptcha');
        }
    }

    /**
     * Filter the requests
     *
     * @param Event $event
     * @return \Cake\Network\Response|null
     */
    public function beforeFilter(Event $event)
    {
        if (in_array($this->request->action, ['register', 'login'])) {
            if ($this->Auth->user()) {
                $this->Flash->error(__('Vous êtes déjà connecté.'));

                // prevent redirect loop
                $routeMatched = Router::parse($this->request->referer(true));
                if (isset($routeMatched['controller']) && !empty($routeMatched['controller'])) {
                    if ($routeMatched['controller'] == 'Users' && in_array($routeMatched['action'], ['login', 'register'])) {
                        return $this->redirect($this->Auth->redirectUrl());
                    }
                }

                return $this->redirect($this->request->referer());
            } else {
                $this->Auth->allow();
            }
        }
        parent::beforeFilter($event);
    }


    /**
     * Register method used to be registered.
     *
     * @return \Cake\Network\Response|null
     */
    public function register()
    {
        $user = $this->Users->newEntity();

        if ($this->request->is(['post', 'put'])) {
            if ($this->Recaptcha->verify()) {
                $this->request->data['last_ip'] = $this->request->clientIp();
                $this->request->data['role'] = UsersTable::roles['waiting'];
                $this->request->data['last_login'] = null;

                $user = $this->Users->patchEntity($user, $this->request->data, [
                    'fieldList' => [
                        'firstname',
                        'lastname',
                        'title',
                        'street',
                        'birthday',
                        'sex',
                        'street_number',
                        'street_number_complement',
                        'city',
                        'website',
                        'postal_code',
                        'phone',
                        'email',
                        'password',
                        'subscribed',
                        'last_ip',
                        'role',
                        'last_login',
                        'cgu',
                    ]
                ]);
                if ($this->Users->save($user)) {
                    $this->Flash->success(__('Vous êtes bien enregistré ! Un mail d\'activation vous a été envoyé, veuillez confirmer le compte. <a class="btn btn-info btn-xs" href="' . Router::url(['_name' => 'requestValidationEmail']) . '">Besoin d\'une nouvelle validation ?</a>'), ['escape' => false]);
                    return $this->redirect(['_name' => 'login']);
                } else {
                    $this->Flash->error(__('Certaines informations sont erronées.'));
                }
            } else {
                $this->Flash->error(__('Prouvez que vous n\'êtes pas un robot.'));
            }
        }
        $this->set(compact('user'));
    }

    /**
     * Login method, when logged update last ip  logged and logged datetime.
     * Check if the user is validated.
     *
     * @return mixed
     */
    public function login()
    {
        if ($this->request->is('post')) {
            $user = $this->Auth->identify();
            if ($user) {
                if ($user['role'] === UsersTable::roles['waiting']) {
                    $this->Flash->error(__('Veuillez valider votre adresse email. <a class="btn btn-info btn-xs" href="' . Router::url(['_name' => 'requestValidationEmail']) . '">Besoin d\'une nouvelle validation ?</a>'), ['key' => 'auth', 'escape' => false]);
                    return;
                }
                $user = new User($user, ['guard' => false]);
                $user->last_ip = $this->request->clientIp();
                $user->last_login = Time::now();

                $this->Auth->setUser($user);
                $this->Users->save($user);
                $this->Flash->default(sprintf('Bienvenue <b>%s %s</b> !', h($user['firstname']), h($user['lastname'])), ['key' => 'auth', 'escape' => false]);
                Log::notice('[LOGIN] ip:' . $this->request->clientIp() . ', email:' . $this->request->data('email'), ['scope' => ['auth']]);

                $routeRedirect = $this->Auth->redirectUrl();
                // prevent redirect to json/xml...
                if (isset(Router::parse($routeRedirect)['_ext']) &&
                    in_array(Router::parse($routeRedirect)['_ext'], ['json', 'xml'])
                ) {
                    $routeRedirect = $this->Auth->config('loginRedirect');
                }

                return $this->redirect($routeRedirect);
            }
            Log::notice('[FAIL] ip:' . $this->request->clientIp() . ', email:' . $this->request->data('email'), ['scope' => ['auth']]);
            $this->Flash->error(__('Identifiants invalides.'), ['key' => 'auth']);
        }
    }

    /**
     * Logout method.
     *
     * @return mixed
     */
    public function logout()
    {
        $this->Flash->success('Vous êtes maintenant déconnecté.');
        return $this->redirect($this->Auth->logout());
    }

    /**
     * Update the User preferences
     *
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function update()
    {
        $user = $this->Users->findCompleteById($this->Auth->user('id'))->firstOrFail();
        if ($this->request->is(['patch', 'post', 'put'])) {
            // required to trigger title requirements check
            $this->request->data['email'] = $user->email;
            $user = $this->Users->patchEntity($user, $this->request->data, [
                'fieldList' => [
                    'firstname',
                    'lastname',
                    'title',
                    'street',
                    'birthday',
                    'street_number',
                    'street_number_complement',
                    'city',
                    'website',
                    'postal_code',
                    'phone',
                    'password',
                    'subscribed',
                    'accept_notifications',
                ]
            ]);
            if ($this->Users->save($user)) {
                $this->Auth->setUser($user);
                $this->Flash->success(__('Modifications enregistrées.'));
            } else {
                $this->Flash->error(__('Impossible de sauvegarder les modifications. Merci de retenter.'));
            }
        }
        $this->set(compact('user'));
    }

    /**
     * Update the student's skills
     */
    public function skills()
    {
        $skillsTable = TableRegistry::get('Skills');
        $user = $this->Users->findById($this->Auth->user('id'))->select(['id'])->contain(['Skills'])->firstOrFail();
        $skills = $skillsTable->find('list');
        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, $this->request->data, ['fieldList' => 'skills']);
            if ($this->Users->save($user)) {
                $this->Flash->success(__('Compétences mises à jour.'));
            } else {
                $this->Flash->error(__('Impossible de sauvegarder les compétences. Merci de retenter.'));
            }
        }
        $this->set(compact('skills', 'user'));
    }

    /**
     * Print the profile of an user
     *
     * @param $id - the id of the user
     */
    public function profile($id)
    {
        $user = $this->Users->findProfileById($id)->firstOrFail();

        $skills = null;
        $announces = null;

        switch ($user->role) {
            default:
                $skills = $this->Users->Skills->find()
                    ->matching('Users', function ($q) use ($id) {
                        return $q->where(['Users.id' => $id]);
                    });
                break;
            case UsersTable::roles['company'] :
                $announces = $this->Users->Announces->find('index')
                    ->where(['user_id' => $id])
                    ->contain([
                        'Skills',
                        'Users' => function ($q) {
                            return $q->find('company');
                        }])
                    ->limit(6)
                    ->orderDesc('Announces.created');

                $announces->cache(function ($q) {
                    return 'announce_profile-' . md5(
                            serialize($q->clause('select')) .
                            serialize($q->clause('values')) .
                            serialize($q->clause('order')) .
                            serialize($q->clause('limit')) .
                            serialize($q->clause('join')) .
                            serialize($q->clause('from')) .
                            serialize($q->clause('where')
                            )
                        );
                }, 'dbResults');
                break;
        }

        $this->set(compact('user', 'skills', 'announces'));
    }

    /**
     * Search Users matching with the query.
     */
    public function search()
    {
        $name = $this->request->query('name');

        $users = $this->Users->searchContact($this->Users->find('name'), $name);

        $users = $this->paginate($users, ['limit' => 5]);

        $this->set(compact('users', 'name'));
    }
}
