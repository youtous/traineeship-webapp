<?php use App\Model\Table\UsersTable; ?>
    <header>
        <div class="page-header">
            <h1>Utilisateurs</h1>
            <div class="row">
                <h4 class="col-md-6"><?= h($user->name) ?>
                    <small>
                        <?= $this->element('user-icon', ['role' => $user->role], [
                            'cache' => ['config' => 'short', 'key' => 'element_user-icon_' . $user->id]
                        ]) ?>
                    </small>
                </h4>
                <div class="col-md-6">
                    <div class="btn-group pull-right">
                        <?= $this->Html->link('Voir le profil ' .
                            $this->Html->tag('i', '', ['class' => 'fa fa-address-card-o', 'aria-hidden' => 'true']), [
                            '_name' => 'profile',
                            'id' => $user->id,], [
                            'escape' => false,
                            'class' => 'btn btn-info'
                        ]) ?>
                        <?= $this->Form->button('Supprimer ' .
                            $this->Html->tag('i', '', ['class' => 'fa fa-trash', 'aria-hidden' => 'true']), [
                            'data-toggle' => 'modal',
                            'data-target' => '#user-confirm-delete-' . $user->id,
                            'class' => 'btn btn-danger',
                        ]) ?>
                        <?= $this->Form->button($this->Html->faIcon('info'), [
                            'escape' => false,
                            'data-toggle' => 'collapse',
                            'data-target' => '#warningEdit',
                            'aria-expanded' => 'false',
                            'aria-controls' => 'warningEdit',
                            'type' => 'button',
                        ]) ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="meta">
            <?php
            // modal delete
            echo $this->Modal->create(['id' => 'user-confirm-delete-' . $user->id]);
            echo $this->Modal->header('Confirmation d\'action');
            echo $this->Modal->body(
                'Confimez-vous vouloir supprimer l\'utilisateur <b>#' . $user->id . ': ' . h($user->name) . '</b> ? <br>
                        La suppression entrainera la perte définitive de toutes les données associées à l\'utilisateur (messages, annonces, candidatures...).', [
                'class' => 'my-body-class'
            ]);
            echo $this->Modal->footer([
                $this->Form->postLink(
                    'Supprimer',
                    ['_name' => 'Admin:deleteUser', 'id' => $user->id],
                    [
                        'class' => 'btn btn-danger',
                        'escape' => false
                    ]
                ),
                $this->Form->button('Fermer', ['data-dismiss' => 'modal'])
            ]);
            echo $this->Modal->end();
            ?>
            <div class="row">
                <div class="col-md-6">
                    <p>
                        <i class="fa fa-history "
                           aria-hidden="true"></i> Dernière modification le <?= $user->modified->nice() ?>
                    </p>
                    <p><i class="fa fa-hourglass" aria-hidden="true"></i> Inscrit
                        le <?= $user->created->nice() ?>, vu la dernière fois
                        le <?= $user->last_login->nice() ?>.</p>
                    <p><i class="fa fa-globe" aria-hidden="true"></i> Dernière adresse ip connue :
                        <code><?= $user->last_ip ?></code></p>
                </div>
                <div class="col-md-6">
                    <p class="alert alert-warning collapse" id="warningEdit">
                        De grands pouvoirs impliquent de grandes responsabilités ! Vous manipulez les données
                        personnelles de l'utilisateur.<br>
                        Vous ne devriez avoir à vous servir de l'édition directe uniquement sur requête du propriétaire
                        du compte.
                    </p>
                </div>
            </div>
        </div>
    </header>
    <article>
        <div class="panel panel-default">
            <div class="panel-body">
                <?= $this->Form->create($user, ['id' => 'profile']) ?>
                <div class="content">
                    <div class="form-group">
                        <div class="text-center">
                            <i class="fa fa-envelope"></i>
                        </div>
                        <label for="email">Adresse email de l'utilisateur</label>
                        <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-envelope"
                                                                   aria-hidden="true"></i></span>
                            <?= $this->Form->input('email', ['label' => '', 'placeholder' => 'william@legenie.fr']); ?>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="text-center">
                            <i class="fa fa-user"></i>
                        </div>
                        <?php empty($user->title) ? $md = 6 : $md = 4; ?>
                        <div class="col-md-<?= $md ?>">
                            <?= $this->Form->input('firstname', ['label' => 'Prénom', 'placeholder' => 'John']); ?>
                        </div>
                        <div class="col-md-<?= $md ?>">
                            <?= $this->Form->input('lastname', ['label' => 'Nom', 'placeholder' => 'Doe']); ?>
                        </div>
                        <div class="col-md-4">
                            <?= $this->Form->input('title', ['label' => 'Entreprise', 'placeholder' => 'Umbrella Corporation']); ?>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="text-center">
                            <i class="fa fa-birthday-cake"></i>
                        </div>
                        <div class="col-md-12">
                            <?= $this->Form->input('birthday', ['label' => 'Date de naissance', 'minYear' => 1920,
                                'maxYear' => date('Y')]); ?>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="text-center">
                            <i class="fa fa-map-signs"></i>
                        </div>
                        <div class="col-md-4">
                            <?= $this->Form->input('postal_code', ['label' => 'Code postal', 'placeholder' => '51100']); ?>
                        </div>
                        <div class="col-md-8">
                            <?= $this->Form->input('city', ['label' => 'Ville', 'placeholder' => 'Reims']); ?>
                        </div>
                        <div class="col-md-8">
                            <?= $this->Form->input('street', ['label' => 'Rue', 'placeholder' => 'Rue des rouliers']); ?>
                        </div>
                        <div class="col-md-2">
                            <?= $this->Form->input('street_number', ['label' => 'Numéro', 'placeholder' => '1']); ?>
                        </div>
                        <div class="col-md-2">
                            <?= $this->Form->input(
                                'street_number_complement',
                                [
                                    'label' => 'Complément',
                                    'type' => 'select',
                                    'options' => ['' => 'Aucun', 'B' => 'B', 'T' => 'T', 'Q' => 'Q', 'A' => 'A']
                                ]
                            );
                            ?>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-6 col-sm-12 col-xs-12">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-phone" aria-hidden="true"></i></span>
                                <?= $this->Form->input('phone', ['label' => '', 'placeholder' => '~0808080808']); ?>
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-12">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-external-link"
                                                                   aria-hidden="true"></i></span>
                                <?= $this->Form->input('website', ['label' => '', 'placeholder' => '~http://website.com']); ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-md-12">
                        <?= $this->Form->input('subscribed', ['label' => 'Accepte de recevoir nos emails']); ?>
                        <?= $this->Form->input('accept_notifications', ['label' => 'Accepte les notifications de l\'application']); ?>
                    </div>
                </div>
                <div class="form-group">
                    <div class="text-center">
                        <i class="fa fa-users"></i>
                    </div>
                    <label for="email">Groupe de l'utilisateur</label>
                    <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-users"
                                                                   aria-hidden="true"></i></span>
                        <?php
                        $traductions = [
                            'en attente',
                            'étudiant',
                            'entreprise',
                            'professeur',
                            'administrateur',
                        ];
                        $roles = array_combine(array_values(UsersTable::roles), $traductions);
                        ?>
                        <?= $this->Form->select('role', $roles, ['size' => sizeof($roles), 'id' => 'roles']); ?>
                        <?php if ($user->role == UsersTable::roles['company']) : ?>
                            <p class="text-danger">Changer le rôle d'une entreprise lui retirera les droits sur ses annonces.</p>
                        <?php endif; ?>
                    </div>
                </div>
                <div class="footer text-center">
                    <?= $this->Form->button(__('Modifier l\'utilisateur'), ['class' => 'btn btn-success btn-large']) ?>
                </div>
                <?= $this->Form->end() ?>
                <hr>
                <?= $this->Form->create(null, ['id' => 'password-update']) ?>
                <div class="content">
                    <h4 class="text-center">Modifier le mot de passe</h4>
                    <div class="text-center">
                        <i class="fa fa-lock"></i>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-6 col-xs-12">
                            <?= $this->Form->input('password', ['label' => 'Mot de passe', 'placeholder' => '6 à 100 caractères', 'value' => '']); ?>
                        </div>
                        <div class="col-md-6 col-xs-12">
                            <?= $this->Form->input('password', ['label' => 'Confirmation', 'placeholder' => 'confirmez le mot de passe', 'value' => '', 'id' => 'passwordConfirmation']); ?>
                        </div>
                    </div>
                </div>
                <div class="footer text-center">
                    <?= $this->Form->button(__('Changer le mot de passe'), ['class' => 'btn btn-success btn-large']) ?>
                </div>
                <?= $this->Form->end() ?>
            </div>
        </div>
    </article>
<?= $this->Html->script('material.min', ['block' => 'scriptBottom']); ?>
<?= $this->Html->script('material-kit', ['block' => 'scriptBottom']); ?>
<?= $this->Html->script('forms.utils', ['block' => 'scriptBottom']); ?>
<?= $this->Html->css('https://fonts.googleapis.com/icon?family=Material+Icons') ?>
<?= $this->Html->script('adminEditUser', ['block' => 'scriptBottom']); ?>
<?= $this->element('js-activate-url', ['url' => ['_name' => 'Admin:indexUsers']]) ?>