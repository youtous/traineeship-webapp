<?php use App\Model\Table\UsersTable; ?>
<header>
    <div class="page-header">
        <h1>Utilisateurs</h1>
        <?= $this->Form->create(null, ['type' => 'get', 'class' => 'form-inline']) ?>
        <?= $this->Form->input('search', ['label' => '', 'placeholder' => 'nom,prénom,année,email...', 'required' => true, 'value' => $search]) ?>
        <?= $this->Form->button($this->Html->faIcon('search'), ['escape' => false]) ?>
        <?= $this->Form->button($this->Html->faIcon('info'), [
            'escape' => false,
            'data-toggle' => 'collapse',
            'data-target' => '#helpSearch',
            'aria-expanded' => 'false',
            'aria-controls' => 'helpSearch',
            'type' => 'button',
        ]) ?>
        <?= $this->Form->end() ?>
        <div class="panel-body">
            <?php if ($search != null) : ?>
                <p class="text-info"><?= sizeof($users) ?> résultats à votre recherche.</p>
                <?= $this->Html->link('Annuler ma recherche', '', ['class' => 'btn btn-primary btn-xs']) ?>
            <?php endif; ?>
            <p class="help-block collapse" id="helpSearch">
                La recherche vous permet de retrouver un utilisateur grâce à son : adresse ip, prénom, nom, titre,
                email, année d'inscription, rôle, ville. <br>
                Vous pouvez combiner vos recherches en espaçant les termes ou délimiter une chaîne précise avec " " ou '
                '.<br>
                <em>Exemple : "Sylvain COMBRAQUE" recherchera précisement les deux termes combinés dans cet ordre.</em>
            </p>
        </div>
    </div>
</header>
<div class="table-responsive">
    <table class="table table-striped table-hover ">
        <thead>
        <tr>
            <th><?= $this->Paginator->sort('firstname', 'Prénom ' . $this->Html->tag('i', '', ['class' => 'fa fa-sort-alpha-asc', 'aria-hidden' => 'true']), ['escape' => false]); ?></th>
            <th><?= $this->Paginator->sort('lastname', 'Nom ' . $this->Html->tag('i', '', ['class' => 'fa fa-sort-alpha-asc', 'aria-hidden' => 'true']), ['escape' => false]); ?></th>
            <th><?= $this->Paginator->sort('title', 'Titre ' . $this->Html->tag('i', '', ['class' => 'fa fa-sort-alpha-asc', 'aria-hidden' => 'true']), ['escape' => false]); ?></th>
            <th><?= $this->Paginator->sort('email', 'Email ' . $this->Html->tag('i', '', ['class' => 'fa fa-sort-alpha-asc', 'aria-hidden' => 'true']), ['escape' => false]); ?></th>
            <th><?= $this->Paginator->sort('role', 'Rôle ' . $this->Html->tag('i', '', ['class' => 'fa fa-sort-alpha-asc', 'aria-hidden' => 'true']), ['escape' => false]); ?></th>
            <th class="action-3">Action</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($users as $user): ?>
            <tr>
                <td><?= h($user->firstname) ?></td>
                <td><?= h($user->lastname) ?></td>
                <td><?= h($user->title) ?></td>
                <td><?= $this->Text->autoLinkEmails(h($user->email)) ?></td>
                <td>
                    <?php switch ($user->role):
                        case UsersTable::roles['company']: ?>
                            <a href="<?= $this->Url->build(['_name' => 'Admin:indexUsers', '?' => ['search' => 'entreprise']]) ?>">
                                <i class="fa fa-building" aria-hidden="true"></i>
                                <small> entreprise</small>
                            </a>
                            <?php break; ?>
                        <?php case UsersTable::roles['student']: ?>
                            <a href="<?= $this->Url->build(['_name' => 'Admin:indexUsers', '?' => ['search' => 'étudiant']]) ?>">
                                <i class="fa fa-graduation-cap" aria-hidden="true"></i>
                                <small> étudiant</small>
                            </a>
                            <?php break; ?>
                        <?php case UsersTable::roles['professor']: ?>
                            <a href="<?= $this->Url->build(['_name' => 'Admin:indexUsers', '?' => ['search' => 'professeur']]) ?>">
                                <i class="fa fa-book" aria-hidden="true"></i>
                                <small> professeur</small>
                            </a>
                            <?php break; ?>
                        <?php case UsersTable::roles['admin']: ?>
                            <a href="<?= $this->Url->build(['_name' => 'Admin:indexUsers', '?' => ['search' => 'administrateur']]) ?>">
                                <i class="fa fa-diamond" aria-hidden="true"></i>
                                <small> administrateur</small>
                            </a>
                            <?php break; ?>
                        <?php case UsersTable::roles['waiting']: ?>
                            <a href="<?= $this->Url->build(['_name' => 'Admin:indexUsers', '?' => ['search' => 'attente']]) ?>">
                                <i class="fa fa-flag" aria-hidden="true"></i>
                                <small> en attente</small>
                            </a>
                            <?php break; ?>
                        <?php endswitch; ?>
                </td>
                <td>
                    <div class="btn-group" role="group" aria-label="Actions">
                        <?= $this->Html->link(
                            $this->Html->tag('i', '', ['class' => 'fa fa-address-card-o', 'aria-hidden' => 'true']), [
                            '_name' => 'profile',
                            'id' => $user->id,], [
                            'escape' => false,
                            'class' => 'btn btn-info'
                        ]) ?>
                        <?= $this->Html->link($this->Html->tag('i', '', ['class' => 'fa fa-pencil', 'aria-hidden' => 'true']), [
                            '_name' => 'Admin:editUser',
                            'id' => $user->id,
                        ], [
                            'escape' => false,
                            'class' => 'btn btn-primary'
                        ]) ?>
                        <?= $this->Form->button(
                            $this->Html->tag('i', '', ['class' => 'fa fa-trash', 'aria-hidden' => 'true']), [
                            'data-toggle' => 'modal',
                            'data-target' => '#user-confirm-delete-' . $user->id,
                            'class' => 'btn btn-danger',
                        ]) ?>
                    </div>
                    <?php
                    // modal delete
                    echo $this->Modal->create(['id' => 'user-confirm-delete-' . $user->id]);
                    echo $this->Modal->header('Confirmation d\'action');
                    echo $this->Modal->body(
                        'Confirmez-vous vouloir supprimer l\'utilisateur <b>#' . $user->id . ': ' . h($user->name) . '</b> ? <br>
                        La suppression entrainera la perte définitive de toutes les données associées à l\'utilisateur (messages, annonces, candidatures...).', [
                        'class' => 'my-body-class'
                    ]);
                    echo $this->Modal->footer([
                        $this->Form->postLink(
                            'Supprimer',
                            ['_name' => 'Admin:deleteUser', 'id' => $user->id],
                            [
                                'class' => 'btn btn-danger',
                                'escape' => false
                            ]
                        ),
                        $this->Form->button('Fermer', ['data-dismiss' => 'modal'])
                    ]);
                    echo $this->Modal->end();
                    ?>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
</div>
<nav class="text-center">
    <div>
        <div class="pagination pagination-lg">
            <?= $this->Paginator->numbers([
                'first' => 'début',
                'last' => 'fin',
            ]); ?>
        </div>
    </div>
</nav>