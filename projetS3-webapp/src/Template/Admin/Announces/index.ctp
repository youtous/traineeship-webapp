<?php use App\Model\Table\AnnouncesTable; ?>
<header>
    <div class="page-header">
        <h1>Annonces</h1>
        <?= $this->Form->create(null, ['type' => 'get', 'class' => 'form-inline']) ?>
        <?= $this->Form->input('search', ['label' => '', 'placeholder' => 'titre,date,code postal,nom entreprise,id...', 'required' => true, 'value' => $search]) ?>
        <?= $this->Form->hidden('status', ['default' => $status]); ?>
        <?= $this->Form->button($this->Html->faIcon('search'), ['escape' => false]) ?>
        <?= $this->Form->button($this->Html->faIcon('info'), [
            'escape' => false,
            'data-toggle' => 'collapse',
            'data-target' => '#helpSearch',
            'aria-expanded' => 'false',
            'aria-controls' => 'helpSearch',
            'type' => 'button',
        ]) ?>
        <?= $this->Form->end() ?>
        <div class="panel-body">
            <?php if ($search != null) : ?>
                <p class="text-info"><?= sizeof($announces) ?> résultats à votre recherche.</p>
                <?= $this->Html->link('Annuler ma recherche', '', ['class' => 'btn btn-primary btn-xs']) ?>
            <?php endif; ?>
            <p class="help-block collapse" id="helpSearch">
                La recherche vous permet de retrouver les annonces grâce à leur : titre, date, code postal, nom
                d'entreprise, identifiant<br>
            </p>
        </div>
    </div>
</header>
<div class="table-responsive">
    <h6>Filtrer par status</h6>
    <?= $this->Form->create(null, ['type' => 'get', 'class' => 'form-inline']) ?>
    <?= $this->Form->hidden('search', ['default' => $search]); ?>
    <?= $this->Form->hidden('status', ['default' => 'waiting']); ?>
    <?= $this->Form->button('en attente', ['class' => 'btn btn-xs btn-warning']) ?>
    <?= $this->Form->end() ?>
    <?= $this->Form->create(null, ['type' => 'get', 'class' => 'form-inline']) ?>
    <?= $this->Form->hidden('search', ['default' => $search]); ?>
    <?= $this->Form->hidden('status', ['default' => 'online']); ?>
    <?= $this->Form->button('en ligne', ['class' => 'btn btn-xs btn-default']) ?>
    <?= $this->Form->end() ?>
    <?= $this->Form->create(null, ['type' => 'get', 'class' => 'form-inline']) ?>
    <?= $this->Form->hidden('search', ['default' => $search]); ?>
    <?= $this->Form->hidden('status', ['default' => 'expired']); ?>
    <?= $this->Form->button('archivée', ['class' => 'btn btn-xs btn-info']) ?>
    <?= $this->Form->end() ?>
    <table class="table table-striped table-hover ">
        <thead>
        <tr>
            <?php
            $this->Paginator->templates([
                'sort' => '<a href="{{url}}">{{text}} ' . $this->Html->faIcon('sort-alpha-asc') . '</a>',
                'sortAsc' => '<a class="asc" href="{{url}}">{{text}} ' . $this->Html->faIcon('sort-alpha-asc') . '</a>',
                'sortDesc' => '<a class="desc" href="{{url}}">{{text}} ' . $this->Html->faIcon('sort-alpha-desc') . '</a>',
            ]);
            ?>
            <th><?= $this->Paginator->sort('Announces.title', 'Titre'); ?></th>
            <th><?= $this->Paginator->sort('Users.title', 'Entreprise') ?></th>
            <th><?= $this->Paginator->sort('Users.city', 'Ville') ?></th>
            <?php
            $this->Paginator->templates([
                'sort' => '<a href="{{url}}">{{text}} ' . $this->Html->faIcon('calendar-o') . '</a>',
                'sortAsc' => '<a class="asc" href="{{url}}">{{text}} ' . $this->Html->faIcon('calendar-plus-o') . '</a>',
                'sortDesc' => '<a class="desc" href="{{url}}">{{text}} ' . $this->Html->faIcon('calendar-minus-o') . '</a>',
            ]);
            ?>
            <th><?= $this->Paginator->sort('Announces.created', 'Soumise'); ?></th>
            <th><?= $this->Paginator->sort('Announces.modified', 'Modifiée'); ?></th>
            <th class="action-2">Action</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($announces as $announce): ?>
            <tr class="<?php
            switch ($announce->status) {
                case AnnouncesTable::status['waiting']:
                    echo 'warning';
                    break;
                case AnnouncesTable::status['expired']:
                    echo 'info';
                    break;
            }
            ?>">
                <td><?= h($announce->title) ?></td>
                <td><?= $this->Html->link($announce->user->title, ['?' => ['search' => $announce->user->title]]) ?></td>
                <td><?= $this->Html->link($announce->user->city . ' (' . $announce->user->postal_code_formatted . ')', ['?' => ['search' => $announce->user->city]]) ?></td>
                <td><?= $this->Html->link($announce->created->format('d/m/y'), ['?' => ['search' => $announce->created->format('d/m/Y')]]) ?></td>
                <td><?= $announce->modified->format('d/m/y') ?></td>
                <td>
                    <div class="btn-group" role="group" aria-label="Actions">
                        <?= $this->Html->link(
                            $this->Html->faIcon('search fa-flip-horizontal'),
                            [
                                '_name' => 'Admin:announces:view',
                                'id' => $announce->id],
                            [
                                'escape' => false,
                                'class' => 'btn btn-info',
                                'title' => 'voir'
                            ]
                        ) ?>
                        <?php if ($announce->status !== AnnouncesTable::status['expired']) : ?>
                            <?= $this->Form->button(
                                $this->Html->faIcon('trash'), [
                                'data-toggle' => 'modal',
                                'data-target' => '#announce-confirm-delete-' . $announce->id,
                                'class' => 'btn btn-danger',
                                'title' => 'supprimer'
                            ]) ?>
                        <?php endif; ?>
                    </div>
                    <?= $this->element('Announces' . DS . 'modal-delete-announce',
                        ['announce' => $announce,],
                        ['cache' => ['config' => 'elementsDays', 'key' => 'modal-delete-announce_' . $announce->id]]) ?>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
</div>
<nav class="text-center">
    <div>
        <div class="pagination pagination-lg">
            <?= $this->Paginator->numbers([
                'first' => 'début',
                'last' => 'fin',
            ]); ?>
        </div>
    </div>
</nav>