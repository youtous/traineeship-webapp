<header>
    <div class="page-header">
        <h1>Annonces</h1>
    </div>
</header>
<section>
    <h5 class="text-center text-info"><?= $this->Paginator->counter('{{count}}') ?> annonces en attente de
        validation</h5>
    <div class="row">
        <?php foreach ($announces as $announce) : ?>
            <div class="col-md-10 col-md-push-1 col-md-pull-1 col-sm-12 col-xs-12">
                <section class="announce">
                    <?= $this->element('Announces' . DS . 'announce', ['announce' => $announce]) ?>
                </section>
                <div class="text-center">
                    <nav class="btn-group navigation-announces">
                        <?php
                        $this->Paginator->templates([
                            'nextActive' => '<a rel="next" class="btn btn-nav btn-lg" href="{{url}}">{{text}}</a>',
                            'nextDisabled' => '<a href="" class="btn btn-nav btn-lg disabled" onclick="return false;">{{text}}</a>',
                            'prevActive' => '<a rel="prev" class="btn btn-nav btn-lg" href="{{url}}">{{text}}</a>',
                            'prevDisabled' => '<a href="" class="btn btn-nav btn-lg disabled" onclick="return false;">{{text}}</a>',
                        ]);
                        ?>
                        <?= $this->Paginator->prev('«') ?>
                        <?= $this->Form->postLink('Supprimer' . ' ' . $this->Html->faIcon('remove'),
                            ['_name' => 'Admin:announces:delete', 'id' => $announce->id, 'redirectReferer' => 'true'], [
                                'class' => 'btn btn-danger btn-lg',
                                'escape' => false

                            ]) ?>
                        <?= $this->Form->postLink('Accepter' . ' ' . $this->Html->faIcon('check'),
                            ['_name' => 'Admin:announces:validate', 'id' => $announce->id], [
                                'class' => 'btn btn-success btn-lg',
                                'escape' => false
                            ]) ?>
                        <?= $this->Paginator->next('»') ?>
                    </nav>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
</section>