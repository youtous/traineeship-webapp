<?php use App\Model\Table\AnnouncesTable; ?>
    <header>
        <div class="page-header">
            <h1>Annonces</h1>
        </div>
    </header>
    <section class="well">
        <?php if ($announce->status === AnnouncesTable::status['waiting']) : ?>
            <?= $this->Form->create(null, ['url' => ['_name' => 'Admin:announces:validate', 'id' => $announce->id]]) ?>
        <?php endif; ?>

        <div class="btn-group pull-right">
            <?php if ($announce->status === AnnouncesTable::status['waiting']) : ?>
                <?= $this->Form->button('Valider ' . $this->Html->faIcon('check'), [
                    'class' => 'btn btn-success'
                ]) ?>
            <?php endif; ?>
            <?php if ($announce->status === AnnouncesTable::status['online']) : ?>
                <?= $this->Html->link('Voir l’annonce', [
                    '_name' => 'announces:view',
                    'id' => $announce->id,], [
                    'escape' => false,
                    'class' => 'btn btn-info'
                ]) ?>
            <?php endif; ?>
            <?php if ($announce->status !== AnnouncesTable::status['expired']) : ?>
                <?= $this->Form->button('Supprimer' . ' ' .
                    $this->Html->faIcon('trash'), [
                    'type' => 'button',
                    'data-toggle' => 'modal',
                    'data-target' => '#announce-confirm-delete-' . $announce->id,
                    'class' => 'btn btn-danger',
                ]) ?>
            <?php endif; ?>
        </div>

        <?php if ($announce->status === AnnouncesTable::status['waiting']) : ?>
            <?= $this->Form->end(); ?>
        <?php endif; ?>

        <?= $this->element('Announces' . DS . 'modal-delete-announce', [
            'announce' => $announce],
            ['cache' => ['config' => 'elementsDays', 'key' => 'modal-delete-announce_' . $announce->id]])
        ?>
        <?= $this->element('Announces' . DS . 'announce', ['announce' => $announce]) ?>
    </section>
<?= $this->element('js-activate-url', ['url' => ['_name' => 'Admin:announces:index']]) ?>