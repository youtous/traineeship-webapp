<header>
    <div class="page-header">
        <h1>Stages</h1>
        <div class="row">
            <h4 class="col-md-6"><?= h($traineeship->announce->title) ?>
                <small>
                    <?= $this->element('user-icon', ['role' => $traineeship->announce->user->role], [
                        'cache' => ['config' => 'short', 'key' => 'element_user-icon_' . $traineeship->announce->user->id]
                    ]) ?>
                </small>
            </h4>
            <div class="col-md-6">
                <div class="btn-group pull-right">
                    <?= $this->Form->button('Supprimer ' .
                        $this->Html->tag('i', '', ['class' => 'fa fa-trash', 'aria-hidden' => 'true']), [
                        'data-toggle' => 'modal',
                        'data-target' => '#traineeship-confirm-delete-' . $traineeship->id,
                        'class' => 'btn btn-danger',
                    ]) ?>
                </div>
            </div>
        </div>
    </div>
    <div class="meta">
        <?php
        // modal delete
        echo $this->Modal->create(['id' => 'traineeship-confirm-delete-' . $traineeship->id]);
        echo $this->Modal->header('Confirmation d\'action');
        echo $this->Modal->body(
            'Confimez-vous vouloir supprimer le stage <b>#' . $traineeship->id . '</b> ? <br>
                        La suppression entrainera la perte définitive de toutes les données associées au stage.', [
            'class' => 'my-body-class'
        ]);
        echo $this->Modal->footer([
            $this->Form->postLink(
                'Supprimer',
                ['_name' => 'Admin:traineeships:delete', 'id' => $traineeship->id],
                [
                    'class' => 'btn btn-danger',
                    'escape' => false
                ]
            ),
            $this->Form->button('Fermer', ['data-dismiss' => 'modal'])
        ]);
        echo $this->Modal->end();
        ?>
        <div class="row">
            <div class="col-md-6">
                <?php if ($traineeship->announce->user->phone != null) : ?>
                    <span><?= $this->Html->faIcon('phone') ?> <b>Téléphone</b></span>
                    <address class="text-left">
                        <?= $traineeship->announce->user->phone ?>
                    </address>
                <?php endif; ?>
                <?= $this->element('Users' . DS . 'address', ['user' => $traineeship->announce->user]) ?>
                </li>
                <li>
                    <?= $this->element('user-icon', ['role' => $traineeship->student->role]) ?>
                    <b><?= h($traineeship->student->name) ?></b>
                    <div>
                        <?= $this->element('Users' . DS . 'image-profile', ['user' => $traineeship->student]) ?>
                    </div>
                </li>
                <li>
                    <?= $this->element('user-icon', ['role' => $traineeship->professor->role]) ?>
                    <b><?= h($traineeship->professor->name) ?></b>
                    <div>
                        <?= $this->element('Users' . DS . 'image-profile', ['user' => $traineeship->professor]) ?>
                    </div>
            </div>
            <div class="col-md-6">
                <p class="alert alert-warning collapse" id="warningEdit">
                    De grands pouvoirs impliquent de grandes responsabilités ! Vous manipulez les données
                    personnelles de l'utilisateur.<br>
                    Vous ne devriez avoir à vous servir de l'édition directe uniquement sur requête du propriétaire
                    du compte.
                </p>
            </div>
        </div>
    </div>
</header>
<article>
    <div class="panel panel-default">
        <div class="panel-body">
            <div class="panel panel-default">
                <div class="panel-heading" id="headingOne">
                    <h4 class="panel-title">
                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne"
                           aria-expanded="true" aria-controls="collapseOne">
                            Intitulé de la mission
                        </a>
                    </h4>
                </div>
                <div id="collapseOne" class="panel-collapse collapse" role="tabpanel"
                     aria-labelledby="headingOne">
                    <div class="panel-body">
                        <?= $this->Text->autoParagraph(h($traineeship->announce->description)) ?>
                    </div>
                </div>
            </div>
            <?= $this->Form->label('professor', 'Professeur en charge') ?>
            <?= $this->Form->select('professor', $professors, ['size' => 5, 'default' => $traineeship->professor->id]); ?>
            <h6>Du <?= $traineeship->beginning->format('d-m-Y') ?>
                au <?= $traineeship->ending->format('d-m-Y') ?>
                <small>
                    <?php if ($traineeship->beginning->isPast()) : ?>
                        <?php if ($traineeship->ending->isPast()) : ?>
                            achevé
                        <?php else: ?>
                            <?= $daysBetween ?> jours restants
                        <?php endif; ?>
                    <?php else: ?>
                        dans <?= $traineeship->beginning->diffInDays(\Cake\I18n\Time::now()) ?> jours
                    <?php endif; ?>
                </small>
            </h6>
        </div>
    </div>
</article>
<?= $this->Html->script('material.min', ['block' => 'scriptBottom']); ?>
<?= $this->Html->script('material-kit', ['block' => 'scriptBottom']); ?>
<?= $this->Html->script('forms.utils', ['block' => 'scriptBottom']); ?>
<?= $this->element('js-activate-url', ['url' => ['_name' => 'Admin:traineeships:index']]) ?>