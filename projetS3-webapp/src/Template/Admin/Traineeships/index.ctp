<header>
    <div class="page-header">
        <h1>Stages</h1>
        <div class="panel-body ">
            <?= $this->Form->create(null, ['type' => 'post', 'url' => ['_name' => 'Admin:traineeships:configure']]) ?>
            <div class="row">
                <div class="col-xs-4">
                    <?= $this->Form->input('beginning', ['label' => 'Date par défaut de début des stages', 'value' => $defaultDate, 'required' => true]) ?>
                    <?= $this->Form->button($this->Html->faIcon('gears') . ' modifier', ['escape' => false]) ?>
                </div>
            </div>
            <?= $this->Form->end() ?>
            <p class="help-block">
                La date par défaut du début des stages doit être redéfinie chaque année.
                <br>La date doit-être au format AAAA-MM-JJ.
            </p>
        </div>
    </div>
</header>
<div class="table-responsive">
    <table class="table table-striped table-hover ">
        <thead>
        <th>Annonce relative</th>
        <th>Entreprise</th>
        <th>Professeur</th>
        <th>Stagiaire</th>
        <?php
        $this->Paginator->templates([
            'sort' => '<a href="{{url}}">{{text}} ' . $this->Html->faIcon('calendar-o') . '</a>',
            'sortAsc' => '<a class="asc" href="{{url}}">{{text}} ' . $this->Html->faIcon('calendar-plus-o') . '</a>',
            'sortDesc' => '<a class="desc" href="{{url}}">{{text}} ' . $this->Html->faIcon('calendar-minus-o') . '</a>',
        ]);
        ?>
        <th><?= $this->Paginator->sort('Traineeships.beginning', 'Période'); ?></th>
        <th>Action</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($traineeships as $traineeship): ?>
            <tr>
                <td><?= $this->Html->link($traineeship->announce->title, ['_name' => 'Admin:announces:view', 'id' => $traineeship->announce->id]) ?></td>
                <td><?= $this->Html->link($traineeship->announce->user->name, ['_name' => 'Admin:editUser', 'id' => $traineeship->announce->user->id]) ?></td>
                <td><?= $this->Html->link($traineeship->professor->name, ['_name' => 'Admin:editUser', 'id' => $traineeship->professor->id]) ?></td>
                <td><?= $this->Html->link($traineeship->student->name, ['_name' => 'Admin:editUser', 'id' => $traineeship->student->id]) ?></td>
                <td>du <?= $traineeship->beginning->format('d-m-Y') ?>
                    au <?= $traineeship->ending->format('d-m-Y') ?></td>
                <td>
                    <?= $this->Html->link(
                        $this->Html->faIcon('pencil'),
                        [
                            '_name' => 'Admin:traineeships:edit',
                            'id' => $traineeship->id
                        ],
                        [
                            'escape' => false,
                            'class' => 'btn btn-info',
                            'title' => 'éditer'
                        ]
                    ) ?>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
</div>
<nav class="text-center">
    <div>
        <div class="pagination pagination-lg">
            <?= $this->Paginator->numbers([
                'first' => 'début',
                'last' => 'fin',
            ]); ?>
        </div>
    </div>
</nav>