<header>
<div class="page-header">
    <h1>Bienvenue dans l'interface d'administration</h1>
    <p class="text-success">
        Selons les droits qui vous sont accordés, vous pourrez accèder aux fonctions associées au menu de navigation.
    </p>
</div>
</header>
<?= $this->cell('Stats', [], ['cache' => ['config' => 'minutes']]); ?>
<article>
    <h3>Logs d'authentification</h3>
    <pre class="pre-scrollable">
        <?= $logContent ?>
    </pre>
</article>
