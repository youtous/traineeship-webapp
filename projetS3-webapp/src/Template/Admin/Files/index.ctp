<header>
    <div class="page-header">
        <h1>Données du site</h1>
        <p>Statistiques détaillées relatives à l'application.</p>
    </div>
</header>
<?= $this->cell('Stats', [], ['cache' => true]); ?>
<div class="x_panel tile">
    <div class="x_title">
        <h2>Fichiers</h2>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
        <?php
        $filesPercents = [
            'messages' => (int)(($filesCount['messages'] / $filesCount['total']) * 100),
            'newsletter' => (int)(($filesCount['newsletter'] / $filesCount['total']) * 100),
            'candidacies' => (int)(($filesCount['candidacies'] / $filesCount['total']) * 100),
            'skills' => (int)(($filesCount['skills'] / $filesCount['total']) * 100),
        ];
        ?>
        <h4>Module</h4>
        <div class="widget_summary">
            <div class="w_left w_25">
                <span>Messages</span>
            </div>
            <div class="w_center w_55">
                <div class="progress">
                    <div class="progress-bar bg-green" role="progressbar" aria-valuenow="60" aria-valuemin="0"
                         aria-valuemax="100"
                         style="width: <?= $filesPercents['messages'] ?>%;">
                        <span class="sr-only"><?= $filesPercents['messages'] ?>
                            % occupés</span>
                    </div>
                </div>
            </div>
            <div class="w_right w_20">
                <span><?= $filesPercents['messages'] ?>%</span>
            </div>
            <div class="clearfix"></div>
        </div>

        <div class="widget_summary">
            <div class="w_left w_25">
                <span>Candidatures</span>
            </div>
            <div class="w_center w_55">
                <div class="progress">
                    <div class="progress-bar bg-green" role="progressbar" aria-valuenow="60" aria-valuemin="0"
                         aria-valuemax="100"
                         style="width: <?= $filesPercents['candidacies'] ?>%;">
                        <span class="sr-only"><?= $filesPercents['candidacies'] ?>
                            % occupés</span>
                    </div>
                </div>
            </div>
            <div class="w_right w_20">
                <span><?= $filesPercents['candidacies'] ?>%</span>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="widget_summary">
            <div class="w_left w_25">
                <span>Newsletter</span>
            </div>
            <div class="w_center w_55">
                <div class="progress">
                    <div class="progress-bar bg-green" role="progressbar" aria-valuenow="60" aria-valuemin="0"
                         aria-valuemax="100"
                         style="width: <?= $filesPercents['newsletter'] ?>%;">
                        <span class="sr-only"><?= $filesPercents['newsletter'] ?>
                            % occupés</span>
                    </div>
                </div>
            </div>
            <div class="w_right w_20">
                <span><?= $filesPercents['newsletter'] ?>%</span>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="widget_summary">
            <div class="w_left w_25">
                <span>Compétences</span>
            </div>
            <div class="w_center w_55">
                <div class="progress">
                    <div class="progress-bar bg-green" role="progressbar" aria-valuenow="60" aria-valuemin="0"
                         aria-valuemax="100" style="width: <?= $filesPercents['skills'] ?>%;">
                        <span class="sr-only"><?= $filesPercents['skills'] ?>% occupés</span>
                    </div>
                </div>
            </div>
            <div class="w_right w_20">
                <span><?= $filesPercents['skills'] ?>%</span>
            </div>
            <div class="clearfix"></div>
        </div>
        <h4>Application</h4>
        <div class="widget_summary">
            <div class="w_left w_25">
                <span>Fichiers temporaires</span>
            </div>
            <div class="w_center w_55">
                <div class="progress">
                    <?php $tmpPercent = (int)(($tmpSize / $used * 100)); ?>
                    <div class="progress-bar bg-green" role="progressbar" aria-valuenow="60" aria-valuemin="0"
                         aria-valuemax="100"
                         style="width: <?= $tmpPercent ?>%;">
                        <span class="sr-only"><?= $tmpPercent  ?>
                            % occupés</span>
                    </div>
                </div>
            </div>
            <div class="w_right w_20">
                <span><?= $tmpPercent ?>%</span>
            </div>
            <div class="clearfix"></div>
        </div>

        <div class="widget_summary">
            <div class="w_left w_25">
                <span>Fichiers de journalisation (logs)</span>
            </div>
            <div class="w_center w_55">
                <div class="progress">
                    <?php $logsPercent = (int)(($logsSize / $used * 100)); ?>
                    <div class="progress-bar bg-green" role="progressbar" aria-valuenow="60" aria-valuemin="0"
                         aria-valuemax="100"
                         style="width: <?= $logsPercent ?>%;">
                        <span class="sr-only"><?= $logsPercent ?>
                            % occupés</span>
                    </div>
                </div>
            </div>
            <div class="w_right w_20">
                <span><?= $logsPercent ?>%</span>
            </div>
            <div class="clearfix"></div>
        </div>
        <h4>Total</h4>
        <div class="widget_summary">
            <div class="w_left w_25">
                <span>Espace de stockage sur le disque dur</span>
            </div>
            <div class="w_center w_55">
                <div class="progress">
                    <?php $totalPercent = (int)(($used / $freeSpace * 100)); ?>
                    <div class="progress-bar bg-purple" role="progressbar" aria-valuenow="60" aria-valuemin="0"
                         aria-valuemax="100" style="width: <?= $totalPercent ?>%;">
                        <span class="sr-only"><?= $totalPercent ?>% occupés</span>
                    </div>
                </div>
            </div>
            <div class="w_right w_20">
                <span><?= $this->Number->toReadableSize($used) ?>
                    /<?= $this->Number->toReadableSize($freeSpace) ?></span>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>

