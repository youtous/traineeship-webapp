<header>
    <div class="page-header">
        <h1>Fichiers utilisateurs</h1>
        <p>Fichiers de l'application soumis par les utilisateurs.</p>
    </div>
</header>
<div class="panel panel-default">
    <div class="panel-body">
        <?= $this->Form->create(null, ['url' => ['_name' => 'Admin:deleteFile']]) ?>
        <fieldset>
            <legend>
                <span class="fa-stack">
                  <i class="fa fa-file-o fa-stack-2x"></i>
                  <i class="fa fa-trash fa-stack-1x"></i>
                </span> Supprimer un fichier
            </legend>
            <p class="warning">
                Vous pouvez supprimer directement le fichier à partir de son identifiant unique. Ceci est utile si des
                droits d'auteurs ont été violés par exemple.<br>
                Cet identifiant alphanumérique de 36 caractères (<a
                        href="https://fr.wikipedia.org/wiki/Universal_Unique_Identifier">UUID</a>)
                est disponible dans l'adresse du fichier, il correspond à la dernière partie de l'URL.
            </p>
            <pre class="well well-sm">http://example.com/files/view/<span class="green">699bdc36-9b92-4d55-a083-1c9213168458</span></pre>
            <?= $this->Form->input('id', [
                'label' => 'UUID',
                'placeholder' => '699bdc36-9b92-4d55-a083-1c9213168458',
                'required' => true,
            ]); ?>
        </fieldset>
        <?= $this->Form->button(__('Supprimer le fichier'), ['class' => 'btn-block btn-danger btn-margin-top']) ?>
        <?= $this->Form->end() ?>
    </div>
</div>