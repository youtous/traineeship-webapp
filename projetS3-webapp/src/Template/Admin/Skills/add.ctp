<header>
    <div class="page-header">
        <h1>Ajouter une compétence</h1>
    </div>
    <p class="well">
        Les compétences sont un ensemble d'acquis qui permettent de répondre à une solution technique.
    </p>
</header>
<?= $this->Form->create($skill, ['type' => 'file']) ?>
<div class="panel panel-default">
    <div class="panel-body">
        <fieldset>
            <legend>Nouvelle compétence</legend>
            <?= $this->Form->input('icon', [
                    'label' => 'Image d\'illustration',
                    'type' => 'file'
            ]); ?>
            <?= $this->Form->input('name', [
                'label' => 'Nom de la compétence',
            ]); ?>
        </fieldset>
    </div>
</div>

<?= $this->Form->button(__('Ajouter la compétence'), ['class' => 'btn-lg btn-block btn-success btn-margin-top']) ?>
<?= $this->Form->end() ?>

