<header>
    <div class="page-header">
        <h1>Compétences informatiques</h1>
    </div>
</header>
<div class="table-responsive">
    <table class="table table-striped table-hover ">
        <thead>
        <tr>
            <th>Miniature</th>
            <th><?= $this->Paginator->sort('name', 'Nom de compétence ' . $this->Html->tag('i', '', ['class' => 'fa fa-sort-alpha-asc', 'aria-hidden' => 'true']), ['escape' => false]); ?></th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($skills as $skill): ?>
            <tr>
                <td>
                    <?= $this->Html->image($skill->webpath, [
                        "alt" => h($skill->name),
                        'class' => 'img-responsive skill-img',
                    ]); ?>
                </td>
                <td><?= h($skill->name) ?></td>
                <td>
                    <?php
                    // modal delete
                    echo $this->Modal->create(['id' => 'skill-confirm-delete-' . $skill->id]);
                    echo $this->Modal->header('Confirmation d\'action');
                    echo $this->Modal->body(
                        'Confimez-vous vouloir supprimer la compétence #' . $skill->id . ': ' . h($skill->name).'<br>
                         La suppression de la compétence affectera les annonces possédant cette compétence.', [
                        'class' => 'my-body-class'
                    ]);
                    echo $this->Modal->footer([
                        $this->Form->postLink(
                            'Supprimer',
                            ['action' => 'delete', $skill->id],
                            [
                                'class' => 'btn btn-danger',
                                'escape' => false
                            ]
                        ),
                        $this->Form->button('Fermer', ['data-dismiss' => 'modal'])
                    ]);
                    echo $this->Modal->end();
                    ?>
                    <?= $this->Form->button(
                        $this->Html->tag('i', '', ['class' => 'fa fa-trash', 'aria-hidden' => 'true']), [
                        'data-toggle' => 'modal',
                        'data-target' => '#skill-confirm-delete-' . $skill->id,
                        'class' => 'btn btn-danger',
                    ]) ?>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
</div>
<nav class="text-center">
    <div>
        <div class="pagination pagination-lg">
            <?= $this->Paginator->numbers([
                'first' => 'début',
                'last' => 'fin',
            ]); ?>
        </div>
    </div>
</nav>
