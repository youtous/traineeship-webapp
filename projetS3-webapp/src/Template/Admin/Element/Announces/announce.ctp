<h3><?= h($announce->title) ?>
    <small><?= $this->element('Announces' . DS . 'status-button', ['status' => $announce->status]) ?></small>
</h3>
<div title="Compétences liées">
    <?= $this->Html->faIcon('tags') ?>
    <?= $this->element('Skills' . DS . 'list-inline', ['skills' => $announce->skills]) ?>
</div>
<div class="row">
    <div class="col-xs-9">
        <div class="pull-left">
            <?= $this->element('Users' . DS . 'address', ['user' => $announce->user]) ?>
            <?php if ($announce->user->phone != null) : ?>
                <p><?= $this->Html->faIcon('phone') ?> <?= h($announce->user->phone) ?></p>
            <?php endif; ?>
        </div>
    </div>
    <div class="col-xs-3">
        <h6 class="text-center">Le <?= $announce->created->nice() ?> <?= $announce->created != $announce->modified ? '<br><small>' . $this->Html->faIcon('pencil') . ' (' . $announce->modified->format('d/m/Y H:i') . ')</small>' : '' ?></h6>
        <?= $this->element('Users' . DS . 'vertical_card', ['user' => $announce->user]) ?>
    </div>
</div>
<article>
    <?= $this->Text->autoParagraph($announce->description) ?>
</article>