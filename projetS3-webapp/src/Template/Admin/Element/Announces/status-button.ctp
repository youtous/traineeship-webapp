<?php use App\Model\Table\AnnouncesTable; ?>
<?php switch ($status) {
    case AnnouncesTable::status['waiting']:
        echo $this->Html->link('en attente', ['_name' => 'Admin:announces:index', '?' => ['status' => 'waiting']], ['class' => 'btn btn-xs btn-warning']);
        break;
    case AnnouncesTable::status['online']:
        echo $this->Html->link('en ligne', ['_name' => 'Admin:announces:index', '?' => ['status' => 'online']], ['class' => 'btn btn-xs btn-default']);
        break;
    case AnnouncesTable::status['expired']:
        echo $this->Html->link('archivée', ['_name' => 'Admin:announces:index', '?' => ['status' => 'expired']], ['class' => 'btn btn-xs btn-info']);
        break;
}