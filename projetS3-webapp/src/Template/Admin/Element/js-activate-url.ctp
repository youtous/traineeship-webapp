<?php $this->Html->scriptStart(['block' => 'scriptBottom']); ?>
<?= <<<JS
$(document).ready(function() {
    setActive('{$this->Url->build($url, ['fullBase' => true])}')
    });
JS;
?>
<?php $this->Html->scriptEnd(); ?>