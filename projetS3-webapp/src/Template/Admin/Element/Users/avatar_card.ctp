<div class="text-center">
    <a href="<?= $this->Url->build(['_name' => 'Admin:editUser', 'id' => $user->id]) ?>" title="" data-toggle="tooltip" data-original-title="éditer l'utilisateur"
       data-placement="right">
        <?= $this->Gravatar->image($user->email, ['class' => 'img-circle', 'default' => 'monsterid']); ?>
    </a>
</div>