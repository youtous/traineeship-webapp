<div class="main main-raised">
    <div class="section section-basic">
        <div class="container">
            <div class="title">
                <h2 class="text-center">Stages <?= $this->Html->faIcon('briefcase') ?></h2>
            </div>
            <div>
                <table class="table table-striped table-hover ">
                    <thead>
                    <tr>
                        <th>Stage</th>
                        <th>Entreprise</th>
                        <th>Professeur</th>
                        <th>Stagiaire</th>
                        <th>Période</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($traineeships as $traineeship): ?>
                        <tr>
                            <td><?= $this->Html->link($traineeship->announce->title, ['_name' => 'traineeships:view', 'id' => $traineeship->id]) ?></td>
                            <td><?= h($traineeship->announce->user->name) ?></td>
                            <td><?= h($traineeship->professor->name) ?></td>
                            <td><?= h($traineeship->student->name) ?></td>
                            <td>du <?= $traineeship->beginning->format('d-m-Y') ?>
                                au <?= $traineeship->ending->format('d-m-Y') ?></td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
                <?php if ($this->Paginator->hasPage(2)) : ?>
                    <div class="section-pagination">
                        <div class="text-center">
                            <ul class="pagination">
                                <?= $this->Paginator->prev('« ') ?>
                            </ul>
                            <?= $this->Paginator->numbers(['first' => 2, 'last' => 2, 'modulus' => 6]) ?>
                            <ul class="pagination">
                                <?= $this->Paginator->next(' »') ?>
                            </ul>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
