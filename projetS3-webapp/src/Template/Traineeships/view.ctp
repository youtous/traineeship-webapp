<div class="main main-raised">
    <div class="section section-basic">
        <div class="container">
            <div class="title row">
                <div class="col-xs-4 col-xs-offset-4">
                    <h2 class="text-center">Stage
                        chez <?= h($traineeship->announce->user->name) ?> <?= $this->Html->faIcon('briefcase') ?></h2>
                </div>
                <div class="col-xs-4">
                    <?= $this->Html->link($this->Html->faIcon('list'),
                        ['_name' => 'traineeships:index'], ['escape' => false, 'data-original-title' => 'Retour aux stages', 'data-placement' => 'left', 'class' => 'btn btn-xs btn-primary btn-simple pull-right']) ?>
                </div>
            </div>
            <article class="col-md-8 col-md-push-2">
                <div class="row">
                    <div class="col-sm-12 text-center">
                        <p class="media-heading">Page de suivi du stage, favorise la communication entre l'entreprise,
                            l'école et le stagiaire.
                        </p>
                        <p class="small help-block">
                            Ce stage a été initié par le système le <?= $traineeship->created->nice() ?>
                        </p>
                        <div class="col-sm-8 col-sm-push-2">
                            <hr>
                        </div>
                    </div>
                </div>
                <div class="row announce-description equal">
                    <section class="col-sm-9">
                        <h4><?= h($traineeship->announce->title) ?></h4>

                        <div class="panel panel-default">
                            <div class="panel-heading" id="headingOne">
                                <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne"
                                       aria-expanded="true" aria-controls="collapseOne">
                                        Intitulé de la mission
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseOne" class="panel-collapse collapse" role="tabpanel"
                                 aria-labelledby="headingOne">
                                <div class="panel-body">
                                    <?= $this->Text->autoParagraph(h($traineeship->announce->description)) ?>
                                </div>
                            </div>
                        </div>

                        <h6>Du <?= $traineeship->beginning->format('d-m-Y') ?>
                            au <?= $traineeship->ending->format('d-m-Y') ?>
                            <small>
                                <?php if ($traineeship->beginning->isPast()) : ?>
                                    <?php if ($traineeship->ending->isPast()) : ?>
                                        achevé
                                    <?php else: ?>
                                        <?= $daysBetween ?> jours restants
                                    <?php endif; ?>
                                <?php else: ?>
                                    dans <?= $traineeship->beginning->diffInDays(\Cake\I18n\Time::now()) ?> jours
                                <?php endif; ?>
                            </small>
                        </h6>
                        <?php
                        $daysBetween = $traineeship->ending->diffInDays($traineeship->beginning);
                        if (!$traineeship->beginning->gte(\Cake\I18n\Time::now()) && $traineeship->ending()->gte(\Cake\I18n\Time::now())) {
                            $progress = \Cake\I18n\Time::now()->diffInDays($traineeship->ending) / $daysBetween;
                        } else {
                            $progress = 0;
                        }
                        ?>
                        <div class="progress-original">
                            <div class="progress-bar" role="progressbar" aria-valuenow="<?= $progress ?>"
                                 aria-valuemin="0"
                                 aria-valuemax="100" style="min-width: 2em;">
                                <?= $progress ?>%
                            </div>
                        </div>
                        <hr>
                        <div class="comments">
                            <h5>Commentaires de stage</h5>
                            <?= $this->Form->create($appreciation, ['url' => ['_name' => 'traineeships:appreciations:add', 'traineeship_id' => $traineeship->id]]); ?>
                            <fieldset>
                                <?php
                                echo $this->Form->textarea('content', [
                                    'placeHolder' => 'Message'
                                ]);
                                ?>
                            </fieldset>
                            <?= $this->Form->button(__('Commenter') . ' ' . $this->Html->faIcon('weixin'), ['class' => 'btn-block btn-primary btn-simple']) ?>
                            <?= $this->Form->end() ?>

                            <?php foreach ($traineeship->appreciations as $appreciation) : ?>
                                <div class="media">
                                    <div class="media-left media-middle">
                                        <?= $this->element('Users' . DS . 'image-profile', ['user' => $appreciation->user]) ?>
                                    </div>
                                    <div class="media-body">
                                        <h5 class="media-heading small">
                                            <?= h($appreciation->user->name) ?>,
                                            <small>le <?= $appreciation->created->nice() ?></small>
                                        </h5>
                                        <?= $this->Text->autoParagraph(h($appreciation->content)) ?>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    </section>
                    <hr class="hidden-md hidden-lg">
                    <div class="col-sm-3">
                        <ul class="list-unstyled text-center" id="nav-announce">
                            <li>
                                <?= $this->element('user-icon', ['role' => $traineeship->announce->user->role]) ?>
                                <b><?= h($traineeship->announce->user->name) ?></b>
                                <div>
                                    <?= $this->element('Users' . DS . 'image-profile', ['user' => $traineeship->announce->user]) ?>
                                </div>
                            </li>
                            <li class="small">
                                <?php if ($traineeship->announce->user->phone != null) : ?>
                                    <span><?= $this->Html->faIcon('phone') ?> <b>Téléphone</b></span>
                                    <address class="text-left">
                                        <?= $traineeship->announce->user->phone ?>
                                    </address>
                                <?php endif; ?>
                                <?= $this->element('Users' . DS . 'address', ['user' => $traineeship->announce->user]) ?>
                            </li>
                            <li>
                                <?= $this->element('user-icon', ['role' => $traineeship->student->role]) ?>
                                <b><?= h($traineeship->student->name) ?></b>
                                <div>
                                    <?= $this->element('Users' . DS . 'image-profile', ['user' => $traineeship->student]) ?>
                                </div>
                            </li>
                            <li>
                                <?= $this->element('user-icon', ['role' => $traineeship->professor->role]) ?>
                                <b><?= h($traineeship->professor->name) ?></b>
                                <div>
                                    <?= $this->element('Users' . DS . 'image-profile', ['user' => $traineeship->professor]) ?>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </article>
        </div>
    </div>
</div>
<?= $this->Html->script('jquery.sticky-kit.min', ['block' => 'scriptBottom']); ?>
<?php
$this->Html->scriptStart(['block' => 'scriptBottom']);
echo <<<JS
$('#nav-announce').stick_in_parent({
    offset_top: 70
});
JS;
$this->Html->scriptEnd(); ?>