<?php use App\Model\Table\UsersTable; ?>
    <div class="main main-raised">
        <div class="section section-basic">
            <div class="container">
                <div class="title row">
                    <div class="col-xs-4 col-xs-offset-4">
                        <h2 class="text-center">Les annonces <?= $this->Html->faIcon('bullhorn') ?></h2>
                    </div>
                    <div class="col-xs-4">
                        <?= $this->Html->link($this->Html->faIcon('list'),
                            ['_name' => 'home'], ['escape' => false, 'data-original-title' => 'Retour aux annonces', 'data-placement' => 'left', 'class' => 'btn btn-xs btn-primary btn-simple pull-right']) ?>
                    </div>
                </div>
                <article class="col-md-8 col-md-push-2">
                    <div class="row">
                        <div class="col-sm-7">
                            <h3><?= h($announce->title) ?></h3>
                            <label>Postée
                                le <?= $announce->created->nice() ?> <?= $announce->created != $announce->modified ? '<small>' . $this->Html->faIcon('pencil') . ' (' . $announce->modified->format('d/m/Y H:i') . ')</small>' : '' ?></label>
                        </div>
                        <div class="col-sm-5 text-center">
                            <div class="announce-skills">
                                <?= $this->element('Skills' . DS . 'images', ['skills' => $announce->skills],
                                    ['cache' => ['config' => 'minutes', 'key' => 'skills-images-announce_' . $announce->id]]) ?>
                            </div>
                        </div>
                    </div>
                    <div class="row announce-description equal">
                        <section class="col-sm-9">
                            <?= $this->Text->autoParagraph(h($announce->description)) ?>
                        </section>
                        <hr class="hidden-md hidden-lg">
                        <div class="col-sm-3">
                            <ul class="list-unstyled text-center" id="nav-announce">
                                <li>
                                    <?= $this->Html->faIcon('building') ?> <b><?= h($announce->user->name) ?></b>
                                    <div>
                                        <?= $this->element('Users' . DS . 'image-profile', ['user' => $announce->user]) ?>
                                    </div>
                                </li>
                                <li>
                                    <?= $this->element('Users' . DS . 'address', ['user' => $announce->user]) ?>
                                </li>
                                <?php if (!$this->request->session()->check('Auth.User.email')) : ?>
                                    <li>
                                        <?= $this->Form->button($this->Html->faIcon('share') . ' Postuler', [
                                            'class' => 'btn btn-success btn-block',
                                            'data-toggle' => 'modal',
                                            'data-target' => '#not-logged',
                                        ]) ?>
                                    </li>
                                <?php else: ?>
                                    <?php if ($this->request->session()->read('Auth.User.role') === UsersTable::roles['student']): ?>
                                        <li>
                                            <?= $this->Form->button(
                                                $this->Html->faIcon('share') . ' Postuler', [
                                                'data-toggle' => 'modal',
                                                'escape' => false,
                                                'data-target' => '#candidacy-postulate-announce-' . $candidacy->announce_id,
                                                'class' => 'btn btn-success btn-block',
                                                'disabled' => $postulated
                                            ]) ?>
                                            <?= $this->element('Candidacy' . DS . 'modal-postulate-announce',
                                                ['candidacy' => $candidacy,],
                                                ['cache' => ['config' => 'elementsDays', 'key' => 'modal-postulate-announce_' . $candidacy->announce_id]]
                                            ) ?>
                                        </li>
                                    <?php endif; ?>
                                    <?php if ($this->request->session()->read('Auth.User.id') === $announce->user->id): ?>
                                        <li>
                                            <?= $this->Html->link('éditer' . ' ' . $this->Html->faIcon('pencil'), [
                                                '_name' => 'announces:edit', 'id' => $announce->id
                                            ], [
                                                'class' => 'btn btn-primary btn-block',
                                                'escape' => false
                                            ]) ?>
                                        </li>
                                    <?php endif; ?>
                                <?php endif; ?>
                            </ul>
                        </div>
                    </div>
                    <nav class="row sharing-area text-center">
                        <hr>
                        <h5 class="text-center">Partager cette annonce</h5>

                        <a id="twitter" class="btn btn-raised btn-twitter sharrre" target="_blank"
                           href="https://www.twitter.com/share?url=<?= $this->Url->build(null, true); ?>">
                            <i class="fa fa-twitter"></i>
                            twitter
                        </a>
                        <a id="facebook" class="btn btn-raised btn-facebook sharrre" target="_blank"
                           href="https://www.facebook.com/sharer/sharer.php?u=<?= $this->Url->build(null, true); ?>"><i
                                    class="fa fa-facebook-square"></i>
                            facebook
                        </a>

                        <a id="mail"
                           class="btn btn-raised btn-github"
                           href='mailto:?Subject=Annonce%20de%20stage&body=<?= rawurlencode($announce->title . ' : ' . $this->Url->build(null, true)); ?>'
                           target="_top">
                            <i class="fa fa-send"></i>
                            mail
                        </a>

                        <button id="link" data-clipboard-text="<?= $this->Url->build(null, true) ?>"
                                class="btn btn-raised btn-primary" data-toggle="popover" data-placement="top"
                                data-content="Le lien a été copié dans votre presse-papier !">
                            <i class="fa fa-link"></i>
                            lien
                        </button>
                    </nav>
                </article>
            </div>
        </div>
    </div>
<?= $this->Html->script('jquery.sticky-kit.min', ['block' => 'scriptBottom']); ?>
<?= $this->Html->script('clipboard.min', ['block' => 'scriptBottom']); ?>
<?php
$this->Html->scriptStart(['block' => 'scriptBottom']);
echo <<<JS
new Clipboard('#link');
$('#nav-announce').stick_in_parent({
    offset_top: 70
});
JS;
$this->Html->scriptEnd(); ?>