<div class="main main-raised">
    <div class="section section-basic">
        <div class="container">
            <div class="title">
                <h2 class="text-center">Déposer une annonce <?= $this->Html->faIcon('bullhorn') ?></h2>
            </div>
            <div class="col-md-8 col-md-push-2">
                <?= $this->Form->create($announce, ['id' => 'postAnnouceForm']) ?>
                <?= $this->Form->input('title', ['label' => 'Titre', 'placeholder' => 'Développeur CakePHP 3', 'minLength' => 6]) ?>

                <div class="form-group row <?= $this->Form->isFieldError('skills') ? 'has-error' : '' ?>">
                    <label class="control-label"><?= $this->Html->faIcon('tags') ?> Compétences du poste</label>
                    <p class="help-block block-relative">Veuillez sélectionner 1 à 4 compétences associées à
                        l'annonce. <a data-toggle="collapse" class="label" href="#collapseSkills"
                                      title="affiche/masque les compétences" aria-expanded="false"
                                      aria-controls="collapseSkills"><?= $this->Html->faIcon('arrows-v') ?></a>
                    </p>
                    <div class="collapse in" id="collapseSkills">
                        <?php
                        $this->Form->templates([
                            'checkboxWrapper' => '<div class="checkbox checkbox-inline-block">{{label}}</div>',
                        ]);
                        ?>
                        <?= $this->Form->select('skills._ids', $skills, [
                            'multiple' => 'checkbox',
                        ]); ?>
                    </div>
                </div>

                <?= $this->Form->input('description', ['placeholder' => 'Proposez une description attrayante de l\'offre...']) ?>

                <div class="text-center">
                    <p id="help-message" class="text-danger"></p>
                    <?= $this->Form->button(__('Soumettre l\'annonce') . ' ' . $this->Html->faIcon('check'), ['class' => 'btn btn-success btn-block']) ?>
                    <p class="help-block">Les annonces sont soumises à une validation, il est normal que votre annonce
                        n’apparaisse pas immédiatement.</p>
                    <?= $this->Html->link(__('Annuler'), ['_name' => 'home']) ?>
                </div>
                <?= $this->Form->end() ?>
            </div>
        </div>
    </div>
</div>
<?= $this->Html->script('autogrow.min', ['block' => 'scriptBottom']) ?>
<?php
$this->Html->scriptStart(['block' => 'scriptBottom']);
echo <<<JS
$(function() {
    $('textarea').autogrow();
});
JS;
$this->Html->scriptEnd(); ?>
<?= $this->Html->script('forms.utils', ['block' => 'scriptBottom']); ?>
<?= $this->Html->script('postAnnounce', ['block' => 'scriptBottom']); ?>
