<?php
$this->set('channel', [
    'title' => __('Dernières annonces'),
    'link' => $this->Url->build(['_name' => 'home'], true),
    'description' => __('Dernières annonces de stages postées.'),
    'language' => 'fr-FR'
]);

foreach ($announces as $announce) {
    $created = strtotime($announce->created);

    $link = [
        '_name' => 'announces:view', 'id' => $announce->id
    ];

    // Retire & échappe tout HTML pour être sûr que le contenu va être validé.
    $body = h(strip_tags($announce->description));
    $body = $this->Text->truncate($body, 400, [
        'ending' => '...',
        'exact' => true,
        'html' => true,
    ]);

    echo $this->Rss->item([], [
        'title' => h(strip_tags($announce->title . ' (par ' . $announce->user->name . ')')),
        'link' => $link,
        'guid' => ['url' => $link, 'isPermaLink' => 'true'],
        'description' => $body,
        'pubDate' => $announce->created
    ]);
}