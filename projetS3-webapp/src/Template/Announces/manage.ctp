<div class="main main-raised">
    <div class="section section-basic">
        <div class="container">
            <div class="title">
                <h2 class="text-center">Vos annonces <?= $this->Html->faIcon('bullhorn') ?></h2>
            </div>
            <div class="card card-nav-tabs">
                <?php $activeOnline = 'active'; ?>
                <?php $activeExpired = 'active'; ?>
                <div class="header header-basic">
                    <div class="nav-tabs-navigation">
                        <div class="nav-tabs-wrapper">
                            <ul class="nav nav-tabs" data-tabs="tabs">
                                <?php if (!$waiting->isEmpty()) : ?>
                                    <li class="active">
                                        <a href="#waiting" data-toggle="tab" aria-expanded="true" class="text-warning">
                                            <?= $this->Html->faIcon('warning') ?>
                                            En attente
                                        </a>
                                    </li>
                                    <?php
                                    $activeOnline = null;
                                    $activeExpired = null;
                                    ?>
                                <?php endif; ?>
                                <?php if (!$online->isEmpty()) : ?>
                                    <li class="<?= $activeOnline ?>">
                                        <a href="#online" data-toggle="tab" aria-expanded="false" class="text-success">
                                            <?= $this->Html->faIcon('check-circle-o fa-2x') ?>
                                            En ligne
                                        </a>
                                    </li>
                                    <?php $activeExpired = null; ?>
                                <?php endif; ?>
                                <?php if (!$expired->isEmpty()) : ?>
                                    <li class="<?= $activeExpired ?>">
                                        <a href="#expired" data-toggle="tab" aria-expanded="false" class="text-info">
                                            <?= $this->Html->faIcon('archive') ?>
                                            Archivées
                                        </a>
                                    </li>
                                <?php endif; ?>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="content">
                    <div class="tab-content">
                        <?php if (!$waiting->isEmpty()) : ?>
                            <div class="tab-pane active" id="waiting">
                                <p class="text-center text-info">
                                    Les annonces en attente de validation sont soumisent à vérification.<br>
                                    Vous serez notifié lors de la publication de l'annonce.
                                </p>
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th>Titre</th>
                                            <th>Compétences</th>
                                            <th>Soumise le</th>
                                            <th>Actions</th>
                                        </tr>
                                        </thead>
                                        <tbody class="announces-table">
                                        <?php foreach ($waiting as $announce) : ?>
                                            <tr>
                                                <?= $this->element('Announce' . DS . 'item-list', ['announce' => $announce]) ?>
                                                <td>
                                                    <?= $this->Form->button(
                                                        $this->Html->faIcon('trash'), [
                                                        'data-toggle' => 'modal',
                                                        'data-target' => '#announce-confirm-delete-' . $announce->id,
                                                        'class' => 'btn btn-simple btn-xs btn-danger btn-fab btn-fab-mini btn-round',
                                                        'title' => 'supprimer'
                                                    ]) ?>
                                                    <?= $this->element('Announce' . DS . 'modal-delete-announce',
                                                        ['announce' => $announce,],
                                                        ['cache' => ['config' => 'elementsDays', 'key' => 'modal-delete-announce-user_' . $announce->id]]) ?>
                                                </td>
                                            </tr>
                                        <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        <?php endif; ?>
                        <?php if (!$online->isEmpty()) : ?>
                            <div class="tab-pane <?= $activeOnline ?>" id="online">
                                <p class="text-center text-info">
                                    Les annonces en ligne liées à des stages ne peuvent plus être supprimées.
                                    Néanmoins, vous pouvez les archiver.
                                </p>
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th>Titre</th>
                                            <th>Compétences</th>
                                            <th>Création</th>
                                            <th>Actions</th>
                                            <th></th>
                                        </tr>
                                        </thead>
                                        <tbody class="announces-table">
                                        <?php foreach ($online as $announce) : ?>
                                            <tr>
                                                <?= $this->element('Announce' . DS . 'item-list', ['announce' => $announce]) ?>
                                                <td>
                                                    <div class="btn-group" role="group" aria-label="Actions">
                                                        <?= $this->Html->link(
                                                            $this->Html->faIcon('search fa-flip-horizontal'),
                                                            [
                                                                '_name' => 'announces:view',
                                                                'id' => $announce->id],
                                                            [
                                                                'escape' => false,
                                                                'class' => 'btn btn-xs btn-simple btn-info btn-fab btn-fab-mini btn-round',
                                                                'title' => 'voir'
                                                            ]
                                                        ) ?>
                                                        <?= $this->Html->link(
                                                            $this->Html->faIcon('pencil'), [
                                                            '_name' => 'announces:edit',
                                                            'id' => $announce->id,
                                                        ], [
                                                            'escape' => false,
                                                            'class' => 'btn btn-xs btn-simple btn-primary btn-fab btn-fab-mini btn-round',
                                                            'title' => 'éditer'
                                                        ]) ?>
                                                        <?php if ($announce->traineeships != null): ?>
                                                            <?= $this->Form->button(
                                                                $this->Html->faIcon('archive'), [
                                                                'data-toggle' => 'modal',
                                                                'data-target' => '#announce-confirm-archive-' . $announce->id,
                                                                'class' => 'btn btn-xs btn-simple btn-danger btn-fab btn-fab-mini btn-round',
                                                                'title' => 'archiver'
                                                            ]) ?>
                                                            <?= $this->element('Announce' . DS . 'modal-archive-announce',
                                                                ['announce' => $announce,],
                                                                ['cache' => ['config' => 'elementsDays', 'key' => 'modal-delete-archive-user_' . $announce->id]]) ?>
                                                        <?php else: ?>
                                                            <?= $this->Form->button(
                                                                $this->Html->faIcon('trash'), [
                                                                'data-toggle' => 'modal',
                                                                'data-target' => '#announce-confirm-delete-' . $announce->id,
                                                                'class' => 'btn btn-xs btn-simple btn-danger btn-fab btn-fab-mini btn-round',
                                                                'title' => 'supprimer'
                                                            ]) ?>
                                                            <?= $this->element('Announce' . DS . 'modal-delete-announce',
                                                                ['announce' => $announce,],
                                                                ['cache' => ['config' => 'elementsDays', 'key' => 'modal-delete-announce-user_' . $announce->id]]) ?>
                                                        <?php endif; ?>
                                                    </div>
                                                </td>
                                                <td>
                                                    <?php if (sizeof($announce->candidacies) > 0) : ?>
                                                        <?= $this->Html->link(
                                                            sizeof($announce->candidacies) . ' candidatures',
                                                            [
                                                                '_name' => 'announces:candidacies:review',
                                                                'announce_id' => $announce->id],
                                                            [
                                                                'escape' => false,
                                                                'class' => 'btn btn-xs btn-warning',
                                                                'title' => 'voir les candidatures'
                                                            ]
                                                        ) ?>
                                                    <?php endif; ?>
                                                </td>
                                            </tr>
                                        <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        <?php endif; ?>
                        <?php if (!$expired->isEmpty()) : ?>
                            <div class="tab-pane <?= $activeExpired ?>" id="expired">
                                <p class="text-center text-info">
                                    Les archives contiennent l'historique de vos annonces soumises.
                                </p>
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th>Titre</th>
                                            <th>Compétences</th>
                                            <th>Création</th>
                                            <th>Archivage</th>
                                            <th></th>
                                        </tr>
                                        </thead>
                                        <tbody class="announces-table">
                                        <?php foreach ($expired as $announce) : ?>
                                            <tr>
                                                <?= $this->element('Announce' . DS . 'item-list', ['announce' => $announce]) ?>
                                                <td><?= $announce->modified->nice() ?></td>
                                                <td><?php if (sizeof($announce->candidacies) > 0) : ?>
                                                        <?= $this->Html->link(
                                                            sizeof($announce->candidacies) . ' candidatures',
                                                            [
                                                                '_name' => 'announces:candidacies:review',
                                                                'announce_id' => $announce->id],
                                                            [
                                                                'escape' => false,
                                                                'class' => 'btn btn-xs btn-warning',
                                                                'title' => 'voir les candidatures'
                                                            ]
                                                        ) ?>
                                                    <?php endif; ?></td>
                                            </tr>
                                        <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>

            </div>
            <p class="help-block text-center">
                <?= $this->cell('Messages::report', ['class' => 'btn-xs btn-no-margin btn-margin-top']) ?>
                <br>
                si vous souhaitez obtenir une assistance concernant une annonce supprimée ou refusée.
            </p>
        </div>
    </div>
</div>
