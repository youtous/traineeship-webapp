<div class="main main-raised">
    <div class="section section-basic">
        <div class="container">
            <div class="title">
                <h2 class="text-center">Vos annonces <?= $this->Html->faIcon('bullhorn') ?>
                    <small>éditer une annonce <?= $this->Html->faIcon('edit') ?></small>
                </h2>
            </div>
            <div class="col-md-8 col-md-push-2">
                <?= $this->Form->create($announce) ?>
                <div class="row">
                    <div class="col-sm-7">
                        <h3><?= h($announce->title) ?></h3>
                        <label><?= $announce->created->nice() ?> <?= $announce->created != $announce->modified ? '<small>' . $this->Html->faIcon('pencil') . ' (' . $announce->modified->format('d/m/Y H:i') . ')</small>' : '' ?></label>
                    </div>
                    <div class="col-sm-5 text-center">
                        <div class="announce-skills">
                            <?= $this->element('Skills' . DS . 'images', ['skills' => $announce->skills],
                                ['cache' => ['config' => 'minutes', 'key' => 'skills-images-announce_' . $announce->id]]) ?>
                        </div>
                    </div>
                </div>

                <?= $this->Form->input('description', ['placeholder' => 'Proposez une description attrayante de l\'offre...']) ?>

                <?php if ($announce->traineeships != null): ?>
                    <?= $this->Form->button(
                        $this->Html->faIcon('archive'), [
                        'data-toggle' => 'modal',
                        'data-target' => '#announce-confirm-archive-' . $announce->id,
                        'class' => 'btn btn-default btn-alert',
                        'title' => 'archiver',
                        'type' => 'button'
                    ]) ?>
                    <?= $this->element('Announce' . DS . 'modal-archive-announce',
                        ['announce' => $announce,],
                        ['cache' => ['config' => 'elementsDays', 'key' => 'modal-delete-archive-user_' . $announce->id]]) ?>
                <?php else: ?>
                    <?= $this->Form->button(
                        $this->Html->faIcon('trash'), [
                        'data-toggle' => 'modal',
                        'data-target' => '#announce-confirm-delete-' . $announce->id,
                        'class' => 'btn btn-default btn-alert',
                        'title' => 'supprimer',
                        'type' => 'button'
                    ]) ?>
                    <?= $this->element('Announce' . DS . 'modal-delete-announce',
                        ['announce' => $announce,],
                        ['cache' => ['config' => 'elementsDays', 'key' => 'modal-delete-announce-user_' . $announce->id]]) ?>
                <?php endif; ?>


                <?= $this->Form->button($this->Html->faIcon('check') . ' ' . __('Modifier'), ['class' => 'btn btn-success pull-right']) ?>
                <?= $this->Html->link($this->Html->faIcon('close') . ' ' . __('Annuler'), ['_name' => 'announces:manage'], ['class' => 'btn btn-default pull-right', 'escape' => false]) ?>
                <?= $this->Form->end() ?>
            </div>
        </div>
    </div>
</div>
<?= $this->Html->script('autogrow.min', ['block' => 'scriptBottom']) ?>
<?php
$this->Html->scriptStart(['block' => 'scriptBottom']);
echo <<<JS
$(function() {
    $('textarea').autogrow();
});
JS;
$this->Html->scriptEnd(); ?>