<?php use App\Model\Table\UsersTable; ?>
<div class="main main-raised">
    <div class="section section-basic">
        <div class="container">
            <div class="row">
                <?= $this->Html->link($this->Html->faIcon('rss'),
                    ['_name' => 'rss:announces:last', '_ext' => 'rss'],
                    ['escape' => false, 'data-original-title' => 'S\'abonner au flux RSS', 'data-placement' => 'left', 'class' => 'btn btn-xs btn-primary btn-simple pull-right']) ?>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <?= $this->Form->create(null, ['type' => 'get']) ?>
                    <h5 class="text-center">Filtrer les annonces</h5>
                    <div class="form-group is-empty form-group-no-margin">
                        <h6><?= $this->Html->faIcon('tags') ?> Compétences du poste</h6>
                        <?php
                        $this->Form->templates([
                            'checkboxWrapper' => '<div class="checkbox checkbox-inline-block">{{label}}</div>',
                        ]);
                        ?>
                        <?= $this->Form->select('skills._ids', $skills, [
                            'multiple' => 'checkbox',
                            'default' => $restrictSkills
                        ]); ?>
                    </div>
                    <div class="form-group is-empty form-group-no-margin">
                        <h6><?= $this->Html->faIcon('globe') ?> Géographie</h6>
                        <?php $departments = [
                            "Toute la France",
                            '01' => "01 - Ain",
                            '02' => "02 - Aisne",
                            '03' => "03 - Allier",
                            '04' => "04 - Alpes de Haute Provence",
                            '05' => "05 - Hautes Alpes",
                            '06' => "06 - Alpes Maritimes",
                            '07' => "07 - Ardèche",
                            '08' => "08 - Ardennes",
                            '09' => "09 - Ariège",
                            "10" => "10 - Aube",
                            "11" => "11 - Aude",
                            "12" => "12 - Aveyron",
                            "13" => "13 - Bouches du Rhône",
                            "14" => "14 - Calvados",
                            "15" => "15 - Cantal",
                            "16" => "16 - Charente",
                            "18" => "18 - Charente Maritime",
                            "18" => "18 - Cher",
                            "19" => "19 - Corrèze",
                            "20" => "2A|B - Corse",
                            "21" => "21 - Côte d'Or",
                            "22" => "22 - Côtes d'Armor",
                            "23" => "23 - Creuse",
                            "24" => "24 - Dordogne",
                            "25" => "25 - Doubs",
                            "26" => "26 - Drôme",
                            "27" => "27 - Eure",
                            "28" => "28 - Eure et Loir",
                            "29" => "29 - Finistère",
                            "30" => "30 - Gard",
                            "31" => "31 - Haute Garonne",
                            "32" => "32 - Gers",
                            "33" => "33 - Gironde",
                            "34" => "34 - Hérault",
                            "35" => "35 - Ille et Vilaine",
                            "36" => "36 - Indre",
                            "37" => "37 - Indre et Loire",
                            "38" => "38 - Isère",
                            "39" => "39 - Jura",
                            "40" => "40 - Landes",
                            "41" => "41 - Loir et Cher",
                            "42" => "42 - Loire",
                            "43" => "43 - Haute Loire",
                            "44" => "44 - Loire Atlantique",
                            "45" => "45 - Loiret",
                            "46" => "46 - Lot",
                            "47" => "47 - Lot et Garonne",
                            "48" => "48 - Lozère",
                            "49" => "49 - Maine et Loire",
                            "50" => "50 - Manche",
                            "51" => "51 - Marne",
                            "52" => "52 - Haute Marne",
                            "53" => "53 - Mayenne",
                            "54" => "54 - Meurthe et Moselle",
                            "55" => "55 - Meuse",
                            "56" => "56 - Morbihan",
                            "57" => "57 - Moselle",
                            "58" => "58 - Nièvre",
                            "59" => "59 - Nord",
                            "60" => "60 - Oise",
                            "61" => "61 - Orne",
                            "62" => "62 - Pas de Calais",
                            "63" => "63 - Puy de Dôme",
                            "64" => "64 - Pyrénées Atlantiques",
                            "65" => "65 - Hautes Pyrénées",
                            "66" => "66 - Pyrénées Orientales",
                            "67" => "67 - Bas Rhin",
                            "68" => "68 - Haut Rhin",
                            "69" => "69 - Rhône",
                            "70" => "70 - Haute Saône",
                            "71" => "71 - Saône et Loire",
                            "72" => "72 - Sarthe",
                            "73" => "73 - Savoie",
                            "74" => "74 - Haute Savoie",
                            "75" => "75 - Paris",
                            "76" => "76 - Seine Maritime",
                            "77" => "77 - Seine et Marne",
                            "78" => "78 - Yvelines",
                            "79" => "79 - Deux Sèvres",
                            "80" => "80 - Somme",
                            "81" => "81 - Tarn",
                            "82" => "82 - Tarn et Garonne",
                            "83" => "83 - Var",
                            "84" => "84 - Vaucluse",
                            "85" => "85 - Vendée",
                            "86" => "86 - Vienne",
                            "87" => "87 - Haute Vienne",
                            "88" => "88 - Vosges",
                            "89" => "89 - Yonne",
                            "90" => "90 - Territoire de Belfort",
                            "91" => "91 - Essonne",
                            "92" => "92 - Hauts de Seine",
                            "93" => "93 - Seine St Denis",
                            "94" => "94 - Val de Marne",
                            "95" => "95 - Val d'Oise",
                            "97" => "97 - DOM"
                        ];
                        ?>
                        <?= $this->Form->select('department', $departments, ['value' => $department]); ?>
                    </div>
                    <?php if ($this->request->session()->check('Auth.User.role')) : ?>
                        <?php if ($this->request->session()->read('Auth.User.role') === UsersTable::roles['student']) : ?>
                            <div class="form-group is-empty form-group-no-margin">
                                <h6><?= $this->Html->faIcon('plus') ?> Autres</h6>
                                <?= $this->Form->input('candidated', ['label' => 'Afficher uniquement les annonces non candidaté', 'type' => 'checkbox', 'default' => $candidated]); ?>
                            </div>
                        <?php endif; ?>
                    <?php endif; ?>
                    <?=
                    // keeping already existing research
                    $this->Form->hidden('search', ['default' => $search]); ?>
                    <?= $this->Form->button(__('Appliquer les filtres') . ' ' . $this->Html->faIcon('filter'), ['class' => 'btn btn-primary btn-simple btn-block']) ?>
                    <?= $this->Form->end() ?>
                </div>
                <div class="col-md-8">
                    <?php if ($search != null || sizeof($announces) === 0) : ?>
                        <div class="panel-body">
                            <?= $this->Form->create(null, ['type' => 'get']) ?>
                            <div class="form-group row">
                                <div class="input-group">
                                    <?= $this->Form->input('search', ['label' => '', 'placeholder' => 'Rechercher une annonce', 'required' => true, 'value' => $search]) ?>
                                    <span class="input-group-addon">
                                        <?= $this->Form->button($this->Html->faIcon('search'), ['escape' => false, 'class' => 'btn btn-simple btn-primary btn-fab-small btn-fab btn-fab-mini']) ?>
                                        <?= $this->Form->button($this->Html->faIcon('info'), [
                                            'escape' => false,
                                            'data-toggle' => 'collapse',
                                            'data-target' => '#helpSearch',
                                            'aria-expanded' => 'false',
                                            'aria-controls' => 'helpSearch',
                                            'type' => 'button',
                                            'class' => 'btn btn-simple btn-info btn-fab-small btn-fab btn-fab-mini'
                                        ]) ?>
                                    </span>
                                </div>
                            </div>
                            <?= $this->Form->end() ?>
                            <?php if ($search != null) : ?>
                                <p class="text-info"><?= sizeof($announces) ?> résultats à votre recherche.</p>
                                <?= $this->Html->link('Annuler ma recherche', ['_name' => 'home'], ['class' => 'btn btn-primary btn-xs']) ?>
                            <?php endif; ?>
                            <p class="help-block collapse" id="helpSearch">
                                La recherche vous permet de retrouver des annonces grâce à son titre ou le titre de
                                l'entreprise ou sa date de publication.<br>
                                Vous pouvez combiner vos recherches en espaçant les termes ou délimiter une chaîne
                                précise avec " "
                                ou ' '.
                            </p>
                        </div>
                    <?php endif; ?>
                    <?php if (sizeof($announces) === 0) : ?>
                        <div class="text-center">
                            <h5>Aucune annonce trouvée. Essayez d'autres termes.</h5>
                        </div>
                    <?php else: ?>
                        <span class="text-filter text-left"><?= $this->Paginator->counter('{{count}}') ?>
                            annonces</span>
                        <span class="text-filter pull-right">
                            <?php
                            $this->Paginator->templates([
                                'sort' => '<a href="{{url}}">{{text}} ' . $this->Html->faIcon('sort-alpha-asc') . '</a>',
                                'sortAsc' => '<a class="asc" href="{{url}}">{{text}} ' . $this->Html->faIcon('sort-alpha-asc') . '</a>',
                                'sortDesc' => '<a class="desc" href="{{url}}">{{text}} ' . $this->Html->faIcon('sort-alpha-desc') . '</a>',
                            ]);
                            ?>
                            <?= $this->Paginator->sort('Announces.title', 'Titre'); ?>
                            <?php
                            $this->Paginator->templates([
                                'sort' => '<a href="{{url}}">{{text}} ' . $this->Html->faIcon('calendar-o') . '</a>',
                                'sortAsc' => '<a class="asc" href="{{url}}">{{text}} ' . $this->Html->faIcon('calendar-plus-o') . '</a>',
                                'sortDesc' => '<a class="desc" href="{{url}}">{{text}} ' . $this->Html->faIcon('calendar-minus-o') . '</a>',
                            ]);
                            ?>
                            ● <?= $this->Paginator->sort('Announces.created', 'Date de publication'); ?>
                        </span>
                        <ul class="list-group">
                            <?php foreach ($announces as $announce) : ?>
                                <li class="list-group-item list-announces">
                                    <div class="row">
                                        <div class="col-xs-2 table-cell company-avatar">
                                            <div class="text-center">
                                                <?= $this->element('Users' . DS . 'image-profile', ['user' => $announce->user]) ?>
                                                <h6 class="title-company"
                                                    title="<?= h($announce->user->name) ?>"><?= h($this->Text->truncate($announce->user->name, 30)) ?></h6>
                                            </div>
                                        </div>
                                        <div class="col-xs-10 table-cell">
                                            <?= $this->element('Announce' . DS . 'table-cell', ['announce' => $announce],
                                                ['cache' => ['config' => 'minutes', 'key' => 'announces-table-cell_' . $announce->id]]) ?>
                                        </div>
                                    </div>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                        <?php if ($this->Paginator->hasPage(2)) : ?>
                            <div class="section-pagination">
                                <div class="text-center">
                                    <ul class="pagination">
                                        <?= $this->Paginator->prev('« ') ?>
                                    </ul>
                                    <?= $this->Paginator->numbers(['first' => 2, 'last' => 2, 'modulus' => 6]) ?>
                                    <ul class="pagination">
                                        <?= $this->Paginator->next(' »') ?>
                                    </ul>
                                </div>
                            </div>
                        <?php endif; ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>
