<span><?= $this->Html->faIcon('map-marker') ?> <b>Adresse</b></span>
<address class="text-left">
    <?= $user->street_number . ' ' . $user->street_number_complement ?> <?= h($user->street) ?> <br/>
    <?= $user->postal_code_formatted ?> <?= h($user->city) ?><br/>
    FRANCE<br/>
</address>
