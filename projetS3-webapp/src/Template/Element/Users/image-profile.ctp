<?php if ($this->request->session()->check('Auth.User.email')) : ?>
    <a class="gravatar-link"
       href="<?= $this->Url->build(['_name' => 'profile', 'id' => $user->id]) ?>"
       title="" data-original-title="Voir le profil"
       data-placement="bottom">
        <?= $this->Gravatar->image($user->email, ['class' => 'img-circle gravatar-link', 'default' => 'monsterid']); ?>
    </a>
<?php else: ?>
    <a class="gravatar-link"
       href="#not-connected"
       data-target="#not-logged"
       data-toggle="modal"
       title="" data-original-title="Voir le profil"
       data-placement="bottom">
        <?= $this->Gravatar->image($user->email, ['class' => 'img-circle gravatar-link', 'default' => 'monsterid']); ?>
    </a>
<?php endif; ?>

