<h4 class="text-center"><?= h($user->firstname) ?> <?= h($user->lastname) ?>
    <small><?= $this->element('user-icon', ['role' => $user->role], [
            'cache' => ['config' => 'short', 'key' => 'element_user-icon_' . $user->id]
        ]) ?></small>
    <?php if ($user->title != null): ?>
        <br>
        <small><?= h($user->title) ?></small>
    <?php endif; ?>
</h4>