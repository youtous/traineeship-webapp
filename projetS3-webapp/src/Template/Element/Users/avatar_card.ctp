<div class="text-center">
    <a href="<?= $this->Url->build(['_name' => 'profile', 'id' => $user->id]) ?>" title="" data-original-title="Voir le profil"
       data-placement="left">
        <?= $this->Gravatar->image($user->email, ['class' => 'img-circle', 'default' => 'monsterid']); ?>
    </a>
</div>