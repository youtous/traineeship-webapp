<?= $this->Form->create($message, ['url' => $url, 'type' => 'file', 'class' => 'send form-inline form-message row']) ?>
<div class="col-xs-1">
    <?= $this->Form->label('files.0', $this->Html->faIcon('upload'), ['id' => 'buttonAddFile','class' => 'btn btn-xs btn-simple btn-message', 'escape' => false, 'data-original-title' => 'Joindre un fichier (pdf, txt, image)',
        'data-placement' => 'bottom']) ?>
    <?= $this->Form->input('files.0', [
        'type' => 'file',
        'label' => '',
        'escape' => false,
    ]); ?>
    <?= $this->Form->input('files.1', [
        'type' => 'file',
        'label' => '',
        'escape' => false,
    ]); ?>
    <?= $this->Form->input('files.2', [
        'type' => 'file',
        'label' => '',
        'escape' => false,
    ]); ?>
    <?= $this->Form->input('files.3', [
        'type' => 'file',
        'label' => '',
        'escape' => false,
    ]); ?>
</div>
<div class="col-xs-10">
    <?= $this->Form->textarea('content', ['placeholder' => 'Il était une fois..', 'rows' => '1', 'class' => 'textarea-messages']); ?>
</div>
<div class="col-xs-1">
    <?= $this->Form->button($this->Html->faIcon('send'), ['class' => 'btn-message btn btn-xs btn-simple btn-primary', 'escape' => false, 'data-original-title' => 'Envoyer',
        'data-placement' => 'bottom']) ?>
</div>
<div id="infoAddFile"></div>
<?= $this->Form->end() ?>
<?= $this->Html->script('autogrow.min', ['block' => 'scriptBottom']) ?>
<?= $this->Html->script('uploadFileConversation', ['block' => 'scriptBottom']) ?>
<?php
$this->Html->scriptStart(['block' => 'scriptBottom']);
echo <<<JS
$(function() {
    $('textarea').autogrow();
});
JS;
$this->Html->scriptEnd(); ?>
