<?= $this->Form->create($candidacy, ['enctype' => 'multipart/form-data', 'url' => ['_name' => 'announces:candidacies:postulate', 'id' => $candidacy->announce_id]]) ?>
<?= $this->Form->input('content', ['class' => 'textarea-messages', 'label' => 'Message de candidature', 'placeholder' => 'Il n\'est pas conseillé d\'y incorporer la lettre de candidature.']) ?>
    <hr>
    <p class="help-block">La lettre de motivation et le C.V. sont optionnels.</p>
    <div class="row">
        <div class="col-xs-3">
            <?= $this->Form->label('files.0', $this->Html->faIcon('upload') . ' Lettre de motivation', ['class' => 'btn btn-xs btn-simple btn-message', 'escape' => false, 'data-original-title' => 'Joindre une lettre de motivation (pdf, txt, image)',
                'data-placement' => 'right']) ?>
            <?= $this->Form->input('files.0', [
                'type' => 'file',
                'label' => '',
                'escape' => false,
            ]); ?>
        </div>
        <div class="col-xs-3">
            <?= $this->Form->label('files.1', $this->Html->faIcon('upload'). ' C.V.', ['class' => 'btn btn-xs btn-simple btn-message', 'escape' => false, 'data-original-title' => 'Joindre un CV (pdf, txt, image)',
                'data-placement' => 'right']) ?>
            <?= $this->Form->input('files.1', [
                'type' => 'file',
                'label' => '',
                'escape' => false,
            ]); ?>
        </div>
    </div>

<?= $this->Form->button($this->Html->faIcon('certificate') . ' ' . __('Postuler'), ['class' => 'btn btn-success pull-right']) ?>
<?= $this->Html->link($this->Html->faIcon('close') . ' ' . __('Annuler'), ['_name' => 'announces:view', 'id' => $candidacy->announce_id], ['class' => 'btn btn-default pull-right', 'escape' => false]) ?>
<?= $this->Form->end() ?>

<?= $this->Html->script('autogrow.min', ['block' => 'scriptBottom']) ?>
<?php
$this->Html->scriptStart(['block' => 'scriptBottom']);
echo <<<JS
$(function() {
    $('textarea').autogrow();
});
JS;
$this->Html->scriptEnd(); ?>