<?php
$this->start('modal');
echo $this->fetch('modal');
echo $this->Modal->create(['id' => 'candidacy-confirm-confirm-' . $candidacy->announce_id]);
echo $this->Modal->header('Confirmation d\'action');
echo $this->Modal->body(
    'Confirmez-vous votre engagement en tant que stagiaire pour l\'annonce <br><b>' . h($candidacy->announce->title) . '</b> ?', [
    'class' => 'my-body-class'
]);
echo $this->Modal->footer([
    $this->Form->postLink(
        'Je m\'engage ' . $this->Html->faIcon('rocket'),
        ['_name' => 'announces:candidacies:confirm', 'announce_id' => $candidacy->announce_id],
        [
            'class' => 'btn btn-info',
            'escape' => false
        ]
    ),
    $this->Form->button('Fermer', ['data-dismiss' => 'modal'])
]);
echo $this->Modal->end();
$this->end();