<?php
$this->start('modal');
echo $this->fetch('modal');
echo $this->Modal->create(['id' => 'candidacy-view-' . $candidacy->student_id . $candidacy->announce_id]);
echo $this->Modal->header('
    Candidature pour l\'annonce
    <br>
    <small>' . h($candidacy->announce->title) . '</small>
    ');
echo $this->Modal->body(
    $this->element('Candidacy' . DS . 'candidacy', ['candidacy' => $candidacy]), [
    'class' => 'my-body-class'
]);
echo $this->Modal->footer([
    $this->Form->button('Fermer', ['data-dismiss' => 'modal'])
]);
echo $this->Modal->end();
$this->end();