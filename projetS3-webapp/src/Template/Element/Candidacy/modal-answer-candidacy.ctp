<?php
$this->start('modal');
echo $this->fetch('modal');
echo $this->Modal->create(['id' => 'candidacy-answer-' . $candidacy->student_id . $candidacy->announce_id]);
echo $this->Modal->header('Affichage de la candidature ');
echo $this->Modal->body(
    '<p>Reçue le ' . $candidacy->created->nice() . '</p>' .
    $this->element('Candidacy' . DS . 'candidacy', ['candidacy' => $candidacy]), [
    'class' => 'my-body-class'
]);
echo $this->Modal->footer([
    '<hr>
    <p class="help-block text-center">En acceptant la candidature, vous vous engagez à signer la convention de stage associée.</p>',
    $this->Html->link('<i class="material-icons">chat</i> Contacter', ['_name' => 'conversation', 'recipient_id' => $candidacy->student_id], ['escape' => false, 'class' => 'btn btn-info pull-left']),
    $this->Form->postLink(
        $this->Html->faIcon('check') . ' Accepter',
        ['_name' => 'announces:candidacies:accept', 'student_id' => $candidacy->student_id, 'announce_id' => $candidacy->announce_id],
        [
            'class' => 'btn btn-success',
            'escape' => false
        ]
    ),
    $this->Form->postLink(
        $this->Html->faIcon('close') . ' Refuser',
        ['_name' => 'announces:candidacies:cancel', 'student_id' => $candidacy->student_id, 'announce_id' => $candidacy->announce_id],
        [
            'class' => 'btn btn-danger',
            'escape' => false
        ]
    ),
]);
echo $this->Modal->end();
$this->end();