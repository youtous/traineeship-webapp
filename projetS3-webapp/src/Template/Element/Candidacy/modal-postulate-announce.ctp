<?php
$this->start('modal');
echo $this->fetch('modal');
echo $this->Modal->create(['id' => 'candidacy-postulate-announce-' . $candidacy->announce_id]);
echo $this->Modal->header('Postuler à l\'annonce');
echo $this->Modal->body(
    $this->element('Candidacy' . DS . 'postulate', ['candidacy' => $candidacy]), [
    'class' => 'my-body-class'
]);
echo $this->Modal->footer();
echo $this->Modal->end();
$this->end();