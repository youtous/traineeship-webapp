<article>
    <div class="well">
        <?= $this->Text->autoParagraph(h($candidacy->content)) ?>
    </div>
    <?php foreach ($candidacy->files as $file) : ?>
        <?= $this->File->thumbnail($file, ['showName' => true]) ?>
    <?php endforeach; ?>
</article>