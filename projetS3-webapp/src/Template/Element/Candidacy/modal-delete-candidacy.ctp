<?php
$this->start('modal');
echo $this->fetch('modal');
echo $this->Modal->create(['id' => 'candidacy-confirm-delete-' . $candidacy->student_id . $candidacy->announce_id]);
echo $this->Modal->header('Confirmation d\'action');
echo $this->Modal->body(
    'Confirmez-vous vouloir supprimer la candidature pour l\'annonce <br><b>' . h($candidacy->announce->title) . '</b> ?', [
    'class' => 'my-body-class'
]);
echo $this->Modal->footer([
    $this->Form->postLink(
        'Supprimer',
        ['_name' => 'announces:candidacies:cancel', 'student_id' => $candidacy->student_id, 'announce_id' => $candidacy->announce_id],
        [
            'class' => 'btn btn-danger',
            'escape' => false
        ]
    ),
    $this->Form->button('Fermer', ['data-dismiss' => 'modal'])
]);
echo $this->Modal->end();
$this->end();