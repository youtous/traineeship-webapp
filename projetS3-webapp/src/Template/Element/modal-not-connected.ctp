<?php
$this->start('modal');
echo $this->fetch('modal');
echo $this->Modal->create(['id' => 'not-logged']);
echo $this->Modal->header('Vous n\'êtes pas connecté ! ');
echo $this->Modal->body(
    'Rejoignez la plateforme, l\'inscription se fait en quelques secondes !
    <div class="pull-right">'.$this->Html->faIcon(' fa-blind fa-2x').'</div>', [
    'class' => 'my-body-class'
]);
echo $this->Modal->footer([
    $this->Html->link($this->Html->faIcon(' fa-user-plus').
        ' Nous rejoindre',
        ['_name' => 'register'],
        [
            'class' => 'btn btn-success',
            'escape' => false,
        ]
    ),
    $this->Html->link($this->Html->faIcon(' fa-key').
        ' Déjà membre ?',
        ['_name' => 'login'],
        [
            'class' => 'btn btn-basic',
            'escape' => false,
            'rel' => 'nofollow',
        ]
    ),
]);
echo $this->Modal->end();
$this->end();