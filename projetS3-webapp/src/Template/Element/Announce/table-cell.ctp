<?= $this->Html->link($announce->title, ['_name' => 'announces:view', 'id' => $announce->id], ['class' => 'text-center title-announce h4']) ?>
<?= $this->Html->link('#' . $announce->id, ['_name' => 'announces:view', 'id' => $announce->id], ['class' => 'announce-id']) ?>
<div class="announce-info">
    <span title="Localisation"><?= $this->Html->faIcon('map-marker') . ' ' . h($announce->user->city) . ' (' . h($announce->user->postal_code_formatted) . ')' ?></span>
    <div class="row">
        <div class="col-xs-8">
            <div title="Compétences liées">
                <?= $this->Html->faIcon('tags') ?>
                <?= $this->element('Skills' . DS . 'list-inline', ['skills' => $announce->skills]) ?>
            </div>
        </div>
        <div class="col-xs-4">
            <div title="Date de création de l'annonce" class="pull-right">
                <?= $this->Html->faIcon('clock-o') . ' ' . $announce->created->nice() ?>
            </div>
        </div>
    </div>
</div>