<?php
$this->start('modal');
echo $this->fetch('modal');
echo $this->Modal->create(['id' => 'announce-confirm-archive-' . $announce->id]);
echo $this->Modal->header('Confirmation d\'action');
echo $this->Modal->body(
    'Confirmez-vous vouloir archiver l\'annonce <b>#' . $announce->id . '</b> du ' . $announce->created->nice() . ' ? <br>
     Il est impossible de changer le statut d\'une annonce archivée, cette action est <b>IRRÉVERSIBLE</b>.', [
    'class' => 'my-body-class'
]);
echo $this->Modal->footer([
    $this->Form->postLink(
        'Archiver',
        ['_name' => 'announces:archive', 'id' => $announce->id],
        [
            'class' => 'btn btn-danger',
            'escape' => false
        ]
    ),
    $this->Form->button('Fermer', ['data-dismiss' => 'modal'])
]);
echo $this->Modal->end();
$this->end();