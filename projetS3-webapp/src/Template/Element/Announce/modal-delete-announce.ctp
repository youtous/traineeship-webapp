<?php
$this->start('modal');
echo $this->fetch('modal');
echo $this->Modal->create(['id' => 'announce-confirm-delete-' . $announce->id]);
echo $this->Modal->header('Confirmation d\'action');
echo $this->Modal->body(
    'Confirmez-vous vouloir supprimer l\'annonce <b>#' . $announce->id . '</b> du ' . $announce->created->nice() . ' ? <br>
                        La suppression entrainera la perte définitive de toutes les données associées : candidatures.', [
    'class' => 'my-body-class'
]);
echo $this->Modal->footer([
    $this->Form->postLink(
        'Supprimer',
        ['_name' => 'announces:delete', 'id' => $announce->id],
        [
            'class' => 'btn btn-danger',
            'escape' => false
        ]
    ),
    $this->Form->button('Fermer', ['data-dismiss' => 'modal'])
]);
echo $this->Modal->end();
$this->end();