<td><?= h($announce->title) ?></td>
<td><?php foreach ($announce->skills as $skill): ?>
        <?=
        $this->Html->image($skill->webpath, [
            'class' => 'skill-img',
            'data-placement' => 'bottom',
            'data-container' => 'body',
            'data-original-title' => h($skill->name),
            'title' => ''
        ]) ?>
    <?php endforeach; ?></td>
<td><?= $announce->created->nice() ?> <?= $announce->created != $announce->modified ? '<br><small>' . $this->Html->faIcon('pencil') . ' (' . $announce->modified->format('d/m/Y H:i') . ')</small>' : '' ?></td>