<?php
use App\Model\Table\UsersTable;

?>
<?php switch ($role):
    case UsersTable::roles['company']: ?>
        <i class="fa fa-building" aria-hidden="true" title="" data-placement="bottom"
           data-original-title="entreprise"></i>
        <?php break; ?>
    <?php case UsersTable::roles['student']: ?>
        <i class="fa fa-graduation-cap" aria-hidden="true" title=""
           data-placement="bottom"
           data-original-title="étudiant"></i>
        <?php break; ?>
    <?php case UsersTable::roles['professor']: ?>
        <i class="fa fa-book" aria-hidden="true" title="" data-placement="bottom"
           data-original-title="professeur"></i>
        <?php break; ?>
    <?php case UsersTable::roles['admin']: ?>
        <i class="fa fa-diamond" aria-hidden="true" title="" data-placement="bottom"
           data-original-title="administrateur"></i>
        <?php break; ?>
    <?php case UsersTable::roles['waiting']: ?>
        <i class="fa fa-flag" aria-hidden="true" title="" data-placement="bottom"
           data-original-title="en attente"></i>
        <?php break; ?>
    <?php endswitch; ?>
