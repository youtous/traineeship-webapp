<div class="alert alert-success fixed-alert">
    <div class="container-fluid">
        <div class="alert-icon">
            <i class="fa fa-check-circle-o  "></i>
        </div>
        <button type="button" class="close" data-dismiss="alert" aria-label="Fermer">
            <span aria-hidden="true"><i class="fa fa-close"></i></span>
        </button>
        <p class="text"><b>Succès :</b> <?= $message ?></p>
    </div>
</div>