<div class="alert alert-danger fixed-alert">
    <div class="container-fluid">
        <div class="alert-icon">
            <i class="fa fa-times-circle-o"></i>
        </div>
        <button type="button" class="close" data-dismiss="alert" aria-label="Fermer">
            <span aria-hidden="true"><i class="fa fa-close"></i></span>
        </button>
        <p class="text"><b>Erreur :</b> <?= $message ?></p>
    </div>
</div>