<div class="alert alert-info fixed-alert">
    <div class="container-fluid">
        <div class="alert-icon">
            <i class="fa fa-bell-o"></i>
        </div>
        <button type="button" class="close" data-dismiss="alert" aria-label="Fermer">
            <span aria-hidden="true"><i class="fa fa-close"></i></span>
        </button>
        <p class="text"><b>Information :</b> <?= $message ?></p>
    </div>
</div>