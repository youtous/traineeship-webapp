<?php
use App\Model\Table\UsersTable;

?>
<div class="navbar navbar-fixed-top" id="navigation-container">
    <div class="container">

        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-index">
                <span class="sr-only">Navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand hidden-lg hidden-md hidden-sm" href="#">
                <span>
                <?
                $this->Html->image('logo-blanc.png', [
                    'alt' => 'Acceuil du site',
                    'id' => 'logo',
                ]);
                ?></span> Liste d'Enrôlements Obtenables</a>
        </div>
        <div class="collapse navbar-collapse" id="navigation-index">
            <ul class="nav navbar-nav navbar-left">
                <li id="logo-iut">
                    <a href="http://iut-info.univ-reims.fr/"
                       title="" data-placement="bottom"
                       data-original-title="Se rendre sur le site du département informatique de l'I.U.T.">

                        <div class="logo-container">
                            <?= $this->Html->image('logo-iut-info.png', [
                                'alt' => 'I.U.T. Informatique de Reims',
                            ]); ?>
                        </div>
                    </a>
                </li>
                <li>
                    <?= $this->Html->link(
                        '<i class="material-icons">explore</i>  Annonces',
                        ['_name' => 'home'], ['escape' => false]) ?>
                </li>
                <li>
                    <?php if ($this->request->session()->read('Auth.User.role') === UsersTable::roles['student']): ?>
                        <?= $this->Html->script('shake', ['block' => 'scriptBottom']) ?>
                        <?= $this->Html->link(
                            $this->Html->tag('i', '', ['id' => 'shake-button-top', 'class' => 'fa fa-bolt bolt shake-rotate', 'aria-hidden' => 'true']) . ' Recherche divine',
                            ['_name' => 'announces:match'], ['escape' => false, 'data-original-title' => 'Recherche par affinités', 'data-placement' => 'bottom']) ?>
                    <?php elseif (in_array($this->request->session()->read('Auth.User.role'), [UsersTable::roles['professor'], UsersTable::roles['admin']])): ?>
                        <?= $this->Html->link(
                            $this->Html->tag('i', '', ['class' => 'fa fa-binoculars', 'aria-hidden' => 'true']) . ' Mes supervisions',
                            ['_name' => 'traineeships:index'], ['escape' => false])
                        ?>
                    <?php else: ?>
                        <?php if ($this->request->session()->check('Auth.User.email')) : ?>
                            <?= $this->Html->link(
                                $this->Html->faIcon('bullhorn') . ' Déposer une annonce',
                                ['_name' => 'announces:post'], ['escape' => false,]) ?>
                        <?php else: ?>
                            <?= $this->Element('modal-not-connected') ?>
                            <?= $this->Html->link(
                                $this->Html->faIcon('bullhorn') . ' Déposer une annonce', ['_name' => 'announces:post'], [
                                'data-toggle' => 'modal',
                                'data-target' => '#not-logged',
                                'escape' => false,
                            ]) ?>
                        <?php endif; ?>
                    <?php endif; ?>
                </li>
                <li>
                    <div class="form-inline" id="search-button">
                        <?= $this->Form->create(null, ['class' => 'form-inline form-navbar form-as-inline', 'id' => 'search-form', 'url' => ['_name' => 'home'], 'type' => 'get']) ?>
                        <div class="input-group">
                            <?= $this->Form->button($this->Html->tag('i', '', ['class' => 'fa fa-search sb-icon-search', 'aria-hidden' => 'true']), ['class' => 'btn btn-simple btn-search btn-fab btn-fab-mini btn-round', 'type' => 'submit']) ?>
                            <?= $this->Form->input('search', ['label' => '', 'placeholder' => 'Rechercher…', 'type' => 'search', 'required' => 'true', 'id' => 'search-text']); ?>
                        </div>
                        <?= $this->Form->end(); ?>
                    </div>
                </li>
            </ul>
            <?php if ($this->request->session()->check('Auth.User.email')) : ?>
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a href="<?= $this->Url->build([
                            '_name' => 'profile',
                            'id' => $this->request->session()->read('Auth.User.id')
                        ]) ?>">
                            <?php if ($this->request->session()->read('Auth.User.role') === UsersTable::roles['company']) : ?>
                                <i class="fa fa-building" aria-hidden="true"></i>
                                <?= $this->request->session()->read('Auth.User.title') ?>
                            <?php else: ?>
                                <i class="material-icons">face</i>
                                <?= $this->request->session()->read('Auth.User.firstname') ?>
                                <?= $this->request->session()->read('Auth.User.lastname') ?>
                            <?php endif; ?>
                        </a>
                    </li>
                    <li>
                        <a href="<?= $this->Url->build(['_name' => 'messagesOfUser']) ?>">
                            <?= $this->Html->script('notify', ['block' => 'scriptBottom']) ?>
                            <div class="icon-wrapper" id="messagesIconNavbar">
                                <i class="material-icons">chat</i>
                                <?= $this->cell('Messages::notSeenConversations', [$this->request->session()->read('Auth.User.id')]) ?>
                            </div>
                            Messages
                            <div class="ripple-container"></div>
                        </a>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                            <?php switch ($this->request->session()->read('Auth.User.role')) {
                                case UsersTable::roles['student']:
                                    $notificationBadge = $this->cell('Candidacies::acceptedCandidacies',
                                        [$this->request->session()->read('Auth.User.id')], ['cache' => ['config' => 'minutes', 'key' => 'candidacies_acceptedCandidacies-' . $this->request->session()->read('Auth.User.id')]]);
                                    break;
                                case UsersTable::roles['company']:
                                    $notificationBadge = $this->cell('Candidacies::waitingCandidacies',
                                        [$this->request->session()->read('Auth.User.id')], ['cache' => ['config' => 'minutes', 'key' => 'candidacies_waitingCandidacies-' . $this->request->session()->read('Auth.User.id')]]);
                                    break;
                                default:
                                    $notificationBadge = null;
                                    break;
                            } ?>
                            <div class="icon-wrapper">
                                <i class="material-icons">settings</i>
                                <?= $notificationBadge ?>
                            </div>
                            <b class="caret"></b>
                            <div class="ripple-container"></div>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-right">
                            <li class="dropdown-header">Actions</li>
                            <li><?= $this->Html->link('Editer mon profil', [
                                    '_name' => 'updateProfile',
                                ]) ?></li>
                            <li class="divider"></li>
                            <?php switch ($this->request->session()->read('Auth.User.role')):
                                case UsersTable::roles['student']: ?>
                                    <li><?= $this->Html->link('Mes compétences', ['_name' => 'updateSkills']) ?></li>
                                    <li>
                                        <?= $this->Html->link('Mes candidatures', ['_name' => 'announces:candidacies:manage']) ?><?= $notificationBadge ?>
                                    </li>
                                    <li><?= $this->Html->link('Mes stages', ['_name' => 'traineeships:index']) ?></li>
                                    <?php break; ?>
                                <?php case UsersTable::roles['professor']: ?>
                                    <li><?= $this->Html->link('Mes compétences', ['_name' => 'updateSkills']) ?></li>
                                    <?php break; ?>
                                <?php case UsersTable::roles['company']: ?>
                                    <li>
                                        <?= $this->Html->link('Mes annonces', ['_name' => 'announces:manage']) ?><?= $notificationBadge ?>
                                    </li>
                                    <li><?= $this->Html->link('Conventions de stage', ['_name' => 'traineeships:index']) ?></li>
                                    <?php break; ?>
                                <?php case UsersTable::roles['admin']: ?>
                                    <li><?= $this->Html->link('Mes compétences', ['_name' => 'updateSkills']) ?></li>
                                    <li class="divider"></li>
                                    <li><?= $this->Html->link('Administrer', ['_name' => 'Admin:index'], ['class' => 'btn btn-xs btn-info']) ?></li>
                                    <?php break; ?>
                                <?php endswitch; ?>
                            <li class="divider"></li>
                            <li><?= $this->Html->link($this->Html->tag('i', '', ['class' => 'fa fa-sign-out', 'aria-hidden' => 'true']) . ' Déconnexion', ['_name' => 'logout'], ['class' => 'btn btn-simple btn-danger', 'escape' => false]) ?></li>
                        </ul>
                    </li>
                </ul>
            <?php else: ?>
                <?= $this->Form->create(null, ['class' => 'form-inline form-navbar nav navbar-nav navbar-right', 'url' => ['_name' => 'login']]) ?>
                <div class="input-group">
                    <span class="input-group-addon"><i class="material-icons">email</i></span>
                    <?= $this->Form->input('email', ['label' => '', 'placeholder' => 'will@aie.iam', 'id' => 'email-navbar', 'required' => true]); ?>

                    <span class="input-group-addon"><i class="material-icons">lock_outline</i></span>
                    <?= $this->Form->input('password', ['label' => '', 'placeholder' => '*******', 'id' => 'password-navbar', 'required' => true]); ?>
                </div>

                <button class="btn btn-simple btn-neutral btn-fab btn-fab-mini btn-round" type="submit">
                    <i class="fa fa-check"></i>
                </button>
                <a title="" data-placement="bottom"
                   class="btn btn-simple btn-neutral btn-fab btn-fab-mini btn-round"
                   href="<?= $this->Url->build(['_name' => 'register']); ?>"
                   data-original-title="Proposez ou découvrez les offres du moment en vous inscrivant !">
                    <i class="fa fa-user-plus" aria-hidden="true"></i>
                </a>
                <?= $this->Form->end(); ?>
            <?php endif; ?>
        </div>
    </div>
</div>