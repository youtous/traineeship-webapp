<?php foreach ($skills as $skill): ?>
    <?=
    $this->Html->image($skill->webpath, [
        'class' => 'skill-img',
        'data-placement' => 'bottom',
        'data-container' => 'body',
        'data-original-title' => h($skill->name),
        'title' => ''
    ]) ?>
<?php endforeach; ?>