<ul class="list list-inline list-inline-block">
    <?php foreach ($skills as $skill) : ?>
        <li class="label label-primary"><?= $skill->name ?></li>
    <?php endforeach; ?>
</ul>