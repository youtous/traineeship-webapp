<div class="main main-raised">
    <div class="section section-basic">
        <div class="container">
            <div class="title row">
                <div class="col-xs-4 col-xs-offset-4">
                    <h2 class="text-center">Vos annonces <?= $this->Html->faIcon('bullhorn') ?>
                        <br>
                        <small><?= $this->Html->link($announce->title, ['_name' => 'announces:view', 'id' => $announce->id]) ?></small>
                    </h2>
                </div>
                <div class="col-xs-4">
                    <?= $this->Html->link($this->Html->faIcon('list'),
                        ['_name' => 'announces:manage'], ['escape' => false, 'data-original-title' => 'Retour à vos annonces', 'data-placement' => 'left', 'class' => 'btn btn-xs btn-primary btn-simple pull-right']) ?>
                </div>
            </div>
            <div class="col-md-6 col-md-offset-3">
                <h3>Candidatures reçues
                    <small>(<?= sizeof($candidacies->toArray()) ?>)</small>
                </h3>
                <ul class="list-group">
                    <?php foreach ($candidacies as $candidacy): ?>
                        <li class="list-group-item">
                            <?= $this->Html->link($candidacy->user->name, ['_name' => 'profile', 'id' => $candidacy->student_id], ['title' => 'voir le profil']) ?>
                            <div class="pull-right">
                                <?= $this->Form->button(
                                    $this->Html->faIcon('search fa-flip-horizontal') . ' afficher', [
                                    'data-toggle' => 'modal',
                                    'data-target' => '#candidacy-answer-' . $candidacy->student_id . $candidacy->announce_id,
                                    'class' => 'btn btn-xs btn-primary btn-no-margin',
                                ]) ?>
                                <?= $this->element('Candidacy' . DS . 'modal-answer-candidacy',
                                    ['candidacy' => $candidacy,],
                                    ['cache' => ['config' => 'elementsDays', 'key' => 'modal-answer-candidacy-user_' . $candidacy->student_id . $candidacy->announce_id]]) ?>
                            </div>
                        </li>
                    <?php endforeach; ?>
                </ul>
                <?php $this->Paginator->options(['url' => ['_name' => 'announces:candidacies:review', 'announce_id' => $announce->id]]) ?>
                <?php if ($this->Paginator->hasPage(2)) : ?>
                    <div class="section-pagination">
                        <div class="text-center">
                            <ul class="pagination">
                                <?= $this->Paginator->prev('« ') ?>
                            </ul>
                            <?= $this->Paginator->numbers(['first' => 2, 'last' => 2, 'modulus' => 6]) ?>
                            <ul class="pagination">
                                <?= $this->Paginator->next(' »') ?>
                            </ul>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
