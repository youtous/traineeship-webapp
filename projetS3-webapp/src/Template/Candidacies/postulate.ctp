<div class="main main-raised">
    <div class="section section-basic">
        <div class="container">
            <div class="title">
                <div class="col-xs-4 col-xs-offset-4">
                    <h2 class="text-center">Les annonces <?= $this->Html->faIcon('bullhorn') ?>
                        <small>postuler à une annonce <?= $this->Html->faIcon('certificate') ?></small>
                    </h2>
                </div>
                <div class="col-xs-4">
                    <?= $this->Html->link($this->Html->faIcon('list'),
                        ['_name' => 'home'], ['escape' => false, 'data-original-title' => 'Retour aux annonces', 'data-placement' => 'left', 'class' => 'btn btn-xs btn-primary btn-simple pull-right']) ?>
                </div>
            </div>
            <div class="col-md-8 col-md-push-2">
                <h3>Postuler à : <?= h($announce->title) ?></h3>
                <?= $this->element('Candidacy' . DS . 'postulate', ['candidacy' => $candidacy]) ?>
            </div>
        </div>
    </div>
</div>