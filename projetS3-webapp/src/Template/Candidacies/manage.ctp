<?php use App\Model\Table\AnnouncesTable; ?>
<div class="main main-raised">
    <div class="section section-basic">
        <div class="container">
            <div class="title">
                <h2 class="text-center">Vos candidatures <?= $this->Html->faIcon('certificate') ?></h2>
            </div>
            <div class="card card-nav-tabs">
                <?php $activeAccepted = 'active'; ?>
                <?php $activeValidated = 'active'; ?>
                <div class="header header-basic">
                    <div class="nav-tabs-navigation">
                        <div class="nav-tabs-wrapper">
                            <ul class="nav nav-tabs" data-tabs="tabs">
                                <?php if (!$pending->isEmpty()) : ?>
                                    <li class="active">
                                        <a href="#waiting" data-toggle="tab" aria-expanded="true" class="text-warning">
                                            <?= $this->Html->faIcon('hourglass-start') ?>
                                            En attente
                                        </a>
                                    </li>
                                    <?php
                                    $activeAccepted = null;
                                    $activeValidated = null;
                                    ?>
                                <?php endif; ?>
                                <?php if (!$accepted->isEmpty()) : ?>
                                    <li class="<?= $activeAccepted ?>">
                                        <a href="#accepted" data-toggle="tab" aria-expanded="false"
                                           class="text-success">
                                            <?= $this->Html->faIcon('hourglass-half') ?>
                                            A confirmer
                                        </a>
                                        <?= $this->cell('Candidacies::acceptedCandidacies',
                                            [$this->request->session()->read('Auth.User.id')], [
                                                'cache' => [
                                                    'config' => 'minutes',
                                                    'key' => 'candidacies_acceptedCandidacies-' . $this->request->session()->read('Auth.User.id')
                                                ]
                                            ]); ?>
                                    </li>
                                    <?php $activeValidated = null; ?>
                                <?php endif; ?>
                                <?php if (!$validated->isEmpty()) : ?>
                                    <li class="<?= $activeValidated ?>">
                                        <a href="#validated" data-toggle="tab" aria-expanded="false" class="text-info">
                                            <?= $this->Html->faIcon('archive') ?>
                                            Confirmées
                                        </a>
                                    </li>
                                <?php endif; ?>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="content">
                    <div class="tab-content">
                        <?php if (!$pending->isEmpty()) : ?>
                            <div class="tab-pane active" id="waiting">
                                <p class="text-center text-info">
                                    Vos candidatures sont listées ici. Si l'entreprise vous répond favorablement, vous
                                    devrez confirmer la candidature pour engager le stage.<br>
                                    Les candidatures refusées seront automatiquement supprimées, vous ne serez pas
                                    notifié.
                                </p>
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th>Annonce</th>
                                            <th>Entreprise</th>
                                            <th>Envoyée le</th>
                                            <th>Actions</th>
                                        </tr>
                                        </thead>
                                        <tbody class="announces-table">
                                        <?php foreach ($pending as $candidacy) : ?>
                                            <tr>
                                                <td>
                                                    <?php if ($candidacy->announce->status == AnnouncesTable::status['online']) : ?>
                                                        <?= $this->Html->link($candidacy->announce->title, ['_name' => 'announces:view', 'id' => $candidacy->announce->id]) ?>
                                                    <?php else: ?>
                                                        <?= h($candidacy->announce->title) ?>
                                                    <?php endif; ?>
                                                </td>
                                                <td>
                                                    <?= h($candidacy->announce->user->name) ?>
                                                </td>
                                                <td>
                                                    <?= $candidacy->created->nice() ?>
                                                </td>
                                                <td>
                                                    <?= $this->Form->button(
                                                        $this->Html->faIcon('search fa-flip-horizontal'), [
                                                        'data-toggle' => 'modal',
                                                        'data-target' => '#candidacy-view-' . $candidacy->student_id . $candidacy->announce_id,
                                                        'class' => 'btn btn-xs btn-simple btn-primary btn-fab btn-fab-mini btn-round',
                                                        'title' => 'voir'
                                                    ]) ?>
                                                    <?= $this->element('Candidacy' . DS . 'modal-view-candidacy',
                                                        ['candidacy' => $candidacy,],
                                                        ['cache' => ['config' => 'elementsDays', 'key' => 'modal-view-candidacy-user_' . $candidacy->student_id . $candidacy->announce_id]]) ?>

                                                    <?= $this->Form->button(
                                                        $this->Html->faIcon('close'), [
                                                        'data-toggle' => 'modal',
                                                        'data-target' => '#candidacy-confirm-delete-' . $candidacy->student_id . $candidacy->announce_id,
                                                        'class' => 'btn btn-xs btn-simple btn-danger btn-fab btn-fab-mini btn-round',
                                                        'title' => 'annuler'
                                                    ]) ?>
                                                    <?= $this->element('Candidacy' . DS . 'modal-delete-candidacy',
                                                        ['candidacy' => $candidacy,],
                                                        ['cache' => ['config' => 'elementsDays', 'key' => 'modal-delete-candidacy-user_' . $candidacy->student_id . $candidacy->announce_id]]) ?>
                                                </td>
                                            </tr>
                                        <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        <?php endif; ?>
                        <?php if (!$accepted->isEmpty()) : ?>
                            <div class="tab-pane <?= $activeAccepted ?>" id="accepted">
                                <p class="text-center text-info">
                                    Si vous souhaitez toujours pourvoir le poste de la candidature, validez la
                                    candidature, la procédure de stage sera alors engagée.<br>
                                    Sinon veuillez annuler votre candidature.
                                </p>
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th>Annonce</th>
                                            <th>Entreprise</th>
                                            <th>Acceptée le</th>
                                            <th>Actions</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php foreach ($accepted as $candidacy) : ?>
                                            <tr>
                                                <td>
                                                    <?php if ($candidacy->announce->status == AnnouncesTable::status['online']) : ?>
                                                        <?= $this->Html->link($candidacy->announce->title, ['_name' => 'announces:view', 'id' => $candidacy->announce->id]) ?>
                                                    <?php else: ?>
                                                        <?= h($candidacy->announce->title) ?>
                                                    <?php endif; ?>
                                                </td>
                                                <td>
                                                    <?= h($candidacy->announce->user->name) ?>
                                                </td>
                                                <td>
                                                    <?= $candidacy->modified->nice() ?>
                                                </td>
                                                <td>
                                                    <?= $this->Form->button(
                                                        $this->Html->faIcon('check'), [
                                                        'data-toggle' => 'modal',
                                                        'data-target' => '#candidacy-confirm-confirm-' . $candidacy->announce_id,
                                                        'class' => 'btn btn-xs btn-simple btn-success btn-fab btn-fab-mini btn-round',
                                                        'title' => 'confirmer'
                                                    ]) ?>
                                                    <?= $this->element('Candidacy' . DS . 'modal-confirm-candidacy',
                                                        ['candidacy' => $candidacy,],
                                                        ['cache' => ['config' => 'elementsDays', 'key' => 'modal-confirm-candidacy-user_' . $candidacy->student_id . $candidacy->announce_id]]) ?>

                                                    <?= $this->Form->button(
                                                        $this->Html->faIcon('close'), [
                                                        'data-toggle' => 'modal',
                                                        'data-target' => '#candidacy-confirm-delete-' . $candidacy->student_id . $candidacy->announce_id,
                                                        'class' => 'btn btn-xs btn-simple btn-danger btn-fab btn-fab-mini btn-round',
                                                        'title' => 'annuler'
                                                    ]) ?>
                                                    <?= $this->element('Candidacy' . DS . 'modal-delete-candidacy',
                                                        ['candidacy' => $candidacy,],
                                                        ['cache' => ['config' => 'elementsDays', 'key' => 'modal-delete-candidacy-user_' . $candidacy->student_id . $candidacy->announce_id]]) ?>
                                                    <?= $this->Form->button(
                                                        $this->Html->faIcon('search fa-flip-horizontal'), [
                                                        'data-toggle' => 'modal',
                                                        'data-target' => '#candidacy-view-' . $candidacy->student_id . $candidacy->announce_id,
                                                        'class' => 'btn btn-xs btn-simple btn-primary btn-fab btn-fab-mini btn-round',
                                                        'title' => 'voir'
                                                    ]) ?>
                                                    <?= $this->element('Candidacy' . DS . 'modal-view-candidacy',
                                                        ['candidacy' => $candidacy,],
                                                        ['cache' => ['config' => 'elementsDays', 'key' => 'modal-view-candidacy-user_' . $candidacy->student_id . $candidacy->announce_id]]) ?>

                                                </td>
                                            </tr>
                                        <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        <?php endif; ?>
                        <?php if (!$validated->isEmpty()) : ?>
                            <div class="tab-pane <?= $activeValidated ?>" id="validated">
                                <p class="text-center text-info">
                                    Ces candidatures sont liées à des stages.
                                </p>
                                <div class="table-responsive">
                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th>Annonce</th>
                                            <th>Entreprise</th>
                                            <th>Validée le</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php foreach ($validated as $candidacy) : ?>
                                            <tr>
                                                <td>
                                                    <?= h($candidacy->announce->title) ?>
                                                </td>
                                                <td>
                                                    <?= h($candidacy->announce->user->name) ?>
                                                </td>
                                                <td>
                                                    <?= $candidacy->modified->nice() ?>
                                                </td>
                                                <td>

                                                </td>
                                            </tr>
                                        <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
