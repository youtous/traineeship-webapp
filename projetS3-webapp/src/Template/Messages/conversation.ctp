<?php $this->extend('index') ?>
<?php $this->start('conversation') ?>
<?= $this->element('Users' . DS . 'vertical_card', ['user' => $recipient]) ?>
<?php if ($conversation->isEmpty()) : ?>
    <hr>
    <p class="text-center text-info">Aucun message !<br>Que diriez-vous d'engager la conversation ?</p>
    <hr>
<?php else : ?>
    <div class="section-pagination">
        <div class="row">
            <div class="col-xs-4 col-xs-offset-4 text-center">
                <?= $this->Html->link($this->Html->faIcon('angle-double-up'), [
                    '_name' => 'navigateConversation',
                    'direction' => 'prev',
                    'datetime' => $conversation->first()->created->format('Y-m-d_H:i:s'),
                    'recipient_id' => $recipient->id
                ], [
                    'rel' => 'prev',
                    'data-original-title' => 'Messages précédents',
                    'data-placement' => 'bottom',
                    'class' => 'btn btn-simple',
                    'escape' => false,
                ]) ?>
            </div>
            <div class="col-xs-4">
                <?= $this->Html->link($this->Html->faIcon('eye'), [
                    '_name' => 'markSeenConversation',
                    'recipient_id' => $recipient->id
                ], [
                    'rel' => 'prev',
                    'data-original-title' => 'Marquer comme lue la conversation',
                    'data-placement' => 'bottom',
                    'class' => 'btn btn-simple btn-primary pull-right',
                    'escape' => false,
                ]) ?>
            </div>
        </div>
    </div>
    <div class="row chat">
        <?php foreach ($conversation as $messageExchanged): ?>
            <?php ($messageExchanged->author_id == $this->request->session()->read('Auth.User.id')) ? $target = 'me' : $target = 'you' ?>
            <div class="bubble <?= $target ?>" id="<?= $messageExchanged->id ?>">
                <?php foreach ($messageExchanged->files as $file) : ?>
                    <?= $this->File->thumbnail($file) ?>
                <?php endforeach; ?>
                <?= $this->Text->autoParagraph($this->Text->autoLink(h($messageExchanged->content), ['escape' => false, 'rel' => 'nofollow', 'target' => '_blank'])) ?>
                <div class="view">envoyé
                    le <?= $this->Html->link($messageExchanged->created->nice(), ['_name' => 'viewMessage', 'id' => $messageExchanged->id, '#' => $messageExchanged->id]) ?>
                    <?php if ($target === 'me') : ?>
                        <span class="tools">
                        <?php if ($messageExchanged->seen !== null) : ?>
                            <i class="fa fa-check" aria-hidden="true"
                               data-original-title="vu le <?= $messageExchanged->seen->nice() ?>"
                               data-placement="bottom"></i>
                        <?php endif; ?>
                            <?php if ($messageExchanged->editable) : ?>
                                ● <?= $this->Html->link($this->Html->faIcon('pencil'), ['_name' => 'editMessage', 'id' => $messageExchanged->id], ['escape' => false, 'title' => 'éditer']) ?>
                            <?php endif; ?>
                    </span>
                    <?php else: ?>
                        <?php if ($messageExchanged->created != $messageExchanged->modified) : ?>
                            <span class="tools">
                        ● <i class="fa fa-pencil" aria-hidden="true"
                             data-original-title="modifié le <?= $messageExchanged->modified->nice() ?>"
                             data-placement="bottom"></i>
                        </span>
                        <?php endif; ?>
                    <?php endif; ?>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
    <div class="section-pagination">
        <div class="text-center">
            <?= $this->Html->link($this->Html->faIcon('angle-double-down'), [
                '_name' => 'navigateConversation',
                'direction' => 'next',
                'datetime' => $conversation->last()->created->format('Y-m-d_H:i:s'),
                'recipient_id' => $recipient->id
            ], [
                'rel' => 'prev',
                'data-original-title' => 'Messages suivants',
                'data-placement' => 'bottom',
                'class' => 'btn btn-simple',
                'escape' => false,
            ]) ?>
        </div>
    </div>
<?php endif; ?>
<?= $this->element('Form' . DS . 'message-files', ['message' => $message, 'url' => ['_name' => 'sendMessage', 'recipient_id' => $recipient->id]]) ?>
<?php $this->end() ?>
