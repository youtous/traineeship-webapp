<div class="main main-raised">
    <div class="section section-basic">
        <div class="container">
            <div class="title row">
                <div class="col-xs-4 col-xs-offset-4">
                    <h2 class="text-center">Vos conversations <?= $this->Html->faIcon('weixin') ?></h2>
                </div>
                <div class="col-xs-4">
                    <?= $this->Html->link($this->Html->faIcon('exchange'),
                        ['_name' => 'messagesOfUser'], ['escape' => false, 'data-original-title' => 'Affichage moderne', 'data-placement' => 'left', 'class' => 'btn btn-xs btn-primary btn-simple pull-right']) ?>
                </div>
            </div>
            <div class="panel-body">
                <?= $this->Form->create(null, ['type' => 'get']) ?>
                <div class="form-group row">
                    <div class="input-group">
                        <?= $this->Form->input('search', ['label' => '', 'placeholder' => 'Rechercher une conversation', 'required' => true, 'value' => $search]) ?>
                        <span class="input-group-addon">
                        <?= $this->Form->button($this->Html->faIcon('search'), ['escape' => false, 'class' => 'btn btn-simple btn-primary btn-fab-small btn-fab btn-fab-mini']) ?>
                        <?= $this->Form->button($this->Html->faIcon('info'), [
                            'escape' => false,
                            'data-toggle' => 'collapse',
                            'data-target' => '#helpSearch',
                            'aria-expanded' => 'false',
                            'aria-controls' => 'helpSearch',
                            'type' => 'button',
                            'class' => 'btn btn-simple btn-info btn-fab-small btn-fab btn-fab-mini'
                        ]) ?></span>
                    </div>
                </div>
                <?= $this->Form->end() ?>
                <?php if ($search != null) : ?>
                    <p class="text-info"><?= sizeof($messages) ?> résultats à votre recherche.</p>
                    <?= $this->Html->link('Annuler ma recherche', '', ['class' => 'btn btn-primary btn-xs']) ?>
                <?php endif; ?>
                <p class="help-block collapse pull-right" id="helpSearch">
                    La recherche vous permet de retrouver des conversations grâce au nom du destinataire ou à la date du
                    message.<br>
                    Vous pouvez combiner vos recherches en espaçant les termes ou délimiter une chaîne précise avec " "
                    ou ' '.<br>
                    Pour rechercher les messages d'une date particulière, entrez la date : jj/mm/aaaa<br>
                    <em>Exemple : "Sylvain COMBRAQUE" recherchera précisement les deux termes combinés dans cet
                        ordre.</em>
                </p>
            </div>
            <?php if (sizeof($messages) === 0) : ?>
                <div class="text-center">
                    <h5>Aucune conversation, commencer une conversation ?</h5>
                    <?= $this->Html->link($this->Html->faIcon('envelope').' Nouveau message', ['_name' => 'messagesOfUser'], ['class' => 'btn btn-success', 'escape' => false]) ?>
                </div>
            <?php else: ?>
                <div class="table-responsive">
                    <table class="table table-striped table-hover">
                        <thead>
                        <tr>
                            <th>Correspondant</th>
                            <th>Message</th>
                            <th>Dernier message</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($messages as $message): ?>
                            <tr>
                                <td>
                                    <?= $this->Html->link($message->author->name, ['_name' => 'conversation', 'recipient_id' => $message->author->id], ['class' => 'info']) ?>
                                </td>
                                <td><?=
                                    $this->Text->truncate(
                                        h($message->content),
                                        100,
                                        [
                                            'ellipsis' => '...',
                                            'exact' => false
                                        ]
                                    ) ?></td>
                                <td><?= $message->created->nice() ?></td>

                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
                <?php if ($this->Paginator->hasPage(2)) : ?>
                    <div class="section-pagination">
                        <div class="text-center">
                            <ul class="pagination">
                                <?= $this->Paginator->prev('« ') ?>
                            </ul>
                            <?= $this->Paginator->numbers(['first' => 2, 'last' => 2, 'modulus' => 6]) ?>
                            <ul class="pagination">
                                <?= $this->Paginator->next(' »') ?>
                            </ul>
                        </div>
                    </div>
                <?php endif; ?>
            <?php endif; ?>
        </div>
    </div>
</div>
