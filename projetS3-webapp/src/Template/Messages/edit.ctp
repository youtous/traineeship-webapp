<div class="main main-raised">
    <div class="section section-basic">
        <div class="container">
            <div class="title">
                <h2 class="text-center">Messagerie
                    <small>éditer un message <?= $this->Html->faIcon('edit') ?></small>
                </h2>
                <?= $this->element('Users' . DS . 'vertical_card_inversed', ['user' => $message->recipient]) ?>
            </div>
            <div class="col-md-8 col-md-push-2">
                <p>
                    <b>Envoyé à : </b><?= $message->created->format('H:m:s') ?>
                    <br>
                    <b>Heure limite d'édition : </b><?= $message->created->modify('+10 minutes')->format('H:m:s') ?>,
                    <em>
                        passé ce délai, vous ne pourrez plus modifier le message.
                    </em>
                </p>
                <?= $this->Form->create($message) ?>
                <?= $this->Form->input('content', ['class' => 'textarea-messages', 'label' => 'Message']) ?>
                <p class="help-block">Les fichiers associés aux messages ne peuvent être édités.</p>
                <?php
                $this->start('modal');
                echo $this->fetch('modal');
                echo $this->Modal->create(['id' => 'delete-message']);
                echo $this->Modal->header('Confirmation d\'action');
                echo $this->Modal->body(
                    'Confirmez-vous vouloir supprimer ce message ? <br>
                     La suppression entraînera la suppression des fichiers associés au message.', [
                    'class' => 'my-body-class'
                ]);
                echo $this->Modal->footer([
                    $this->Form->postLink(
                        'Supprimer',
                        ['_name' => 'deleteMessage', 'id' => $message->id],
                        [
                            'class' => 'btn btn-danger',
                        ]
                    ),
                    $this->Form->button('Fermer', ['data-dismiss' => 'modal'])
                ]);
                echo $this->Modal->end();
                $this->end();
                ?>
                <?= $this->Html->link(
                    $this->Html->faIcon('trash'), '#delete', [
                    'data-toggle' => 'modal',
                    'data-target' => '#delete-message',
                    'class' => 'btn btn-default btn-alert',
                    'escape' => false,
                ]) ?>
                <?= $this->Form->button($this->Html->faIcon('check') . ' ' . __('Modifier'), ['class' => 'btn btn-success pull-right']) ?>
                <?= $this->Html->link($this->Html->faIcon('close') . ' ' . __('Annuler'), ['_name' => 'viewMessage', 'id' => $message->id], ['class' => 'btn btn-default pull-right', 'escape' => false]) ?>
                <?= $this->Form->end() ?>
            </div>
        </div>
    </div>
</div>
<?= $this->Html->script('autogrow.min', ['block' => 'scriptBottom']) ?>
<?php
$this->Html->scriptStart(['block' => 'scriptBottom']);
echo <<<JS
$(function() {
    $('textarea').autogrow();
});
JS;
$this->Html->scriptEnd(); ?>