<div class="main main-raised">
    <div class="section section-basic">
        <div class="container">
            <div class="title row">
                <div class="col-xs-4 col-xs-offset-4">
                    <h2 class="text-center">Messagerie</h2>
                </div>
                <div class="col-xs-4">
                    <?= $this->Html->link($this->Html->faIcon('list'),
                        ['_name' => 'messagesOfUserDetailed'], ['escape' => false, 'data-original-title' => 'Affichage classique', 'data-placement' => 'left', 'class' => 'btn btn-xs btn-primary btn-simple pull-right']) ?>
                </div>
            </div>
            <div class="col-md-4">
                <?php if ($messages->isEmpty()) : ?>
                    <h4 class="text-center">Recherchez un contact puis commencez une conversation !</h4>
                <?php else : ?>
                    <h4 class="text-center">Dernières conversations</h4>
                <?php endif; ?>

                <?= $this->Form->create(null, ['type' => 'get', 'class' => 'form-inline', 'id' => 'search-message-form', 'url' => ['_name' => 'searchUser']]) ?>
                <div class="input-group">
                    <?= $this->Form->button($this->Html->tag('i', 'groups', ['class' => 'material-icons', 'aria-hidden' => 'true']), ['class' => 'btn btn-simple btn-lg input-group-addon btn-search-contacts', 'type' => 'submit']) ?>
                    <?= $this->Form->input('name', ['label' => '', 'placeholder' => 'Contacter...', 'type' => 'search', 'required' => 'true', 'autocomplete' => 'off']); ?>
                    <div id='loadDiv'></div>
                    <div class="list-group" id="listConversation">
                    </div>
                </div>
                <div class="input-group">
                </div>
                <?= $this->Form->end(); ?>
                <div class="list-group img-raised">
                    <?php foreach ($messages as $message): ?>
                        <div class="media list-group-item <?= $message->author->id == $this->request->param('recipient_id') ? 'active' : '' ?>">
                            <a href="<?= $this->Url->build(['_name' => 'conversation', 'recipient_id' => $message->author->id]) ?>"
                               class="txt-white">
                                <div class="media-left media-middle">
                                    <?= $this->Gravatar->image($message->author->email, ['class' => 'img-rounded media-img', 'default' => 'monsterid']); ?>
                                </div>
                                <div class="media-body">
                                    <h5 class="media-heading"><?= h($message->author->name) ?>
                                        <?= $this->element('Message' . DS . 'not_seen_messages', ['count' => $message->countNotSeen]) ?>
                                        <br>
                                        <small><?= $message->created->timeAgoInWords(['end' => '1 year', 'accuracy' => 'minute']) ?></small>
                                    </h5>
                                    <p><?=
                                        $this->Text->truncate(
                                            h($message->content),
                                            20,
                                            [
                                                'ellipsis' => '...',
                                                'exact' => false
                                            ]
                                        ) ?></p>
                                </div>
                            </a>
                        </div>
                    <?php endforeach; ?>
                </div>
                <div class="section-pagination">
                    <div class="text-center">
                        <?php if ($this->Paginator->hasPrev()) : ?>
                            <ul class="pagination">
                                <?= $this->Paginator->prev('« ') ?>
                            </ul>
                        <?php endif; ?>
                        <?= $this->Paginator->numbers(['first' => 2, 'last' => 2, 'modulus' => 6]) ?>
                        <?php if ($this->Paginator->hasNext()) : ?>
                            <ul class="pagination">
                                <?= $this->Paginator->next(' »') ?>
                            </ul>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
            <div class="col-md-8 conversation">
                <?php if ($this->fetch('conversation')): ?>
                    <?= $this->fetch('conversation') ?>
                <?php else: ?>
                    <h4 class="text-center">Aucune conversation sélectionnée !</h4>
                    <div class="conversation-messages">
                        <h4>Que souhaitez-vous faire ?</h4>
                        <p>Vous pouvez engager la conversation avec un nouveau destinataire ! Vous pouvez aussi nous
                            signaler un message.</p>
                        <?= $this->Html->link($this->Html->faIcon('envelope') . ' Nouveau message', ['_name' => 'searchUser'], ['escape' => false, 'class' => 'btn btn-primary']) ?>
                        <?= $this->cell('Messages::report') ?>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>

<?= $this->Html->script('searchConversations', ['block' => 'scriptBottom']); ?>