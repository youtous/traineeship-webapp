<div class="row tile_count">
    <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
        <span class="count_top"><i class="fa fa-users"></i> Utilisateurs</span>
        <div class="count blue"><?= $usersCount ?></div>
    </div>
    <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
        <span class="count_top"><i class="fa  fa-share-alt-square"></i> Annonces</span>
        <div class="count"><?= $announcesCount ?></div>
    </div>
    <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
        <span class="count_top"><i class="fa  fa-certificate"></i> Candidatures</span>
        <div class="count"><?= $candidaciesCount ?></div>
    </div>
    <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
        <span class="count_top"><i class="fa fa-briefcase"></i> Stages</span>
        <div class="count"><?= $traineeshipsCount ?></div>
    </div>
    <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
        <span class="count_top"><i class="fa fa-file"></i> Fichiers</span>
        <div class="count green"><?= $filesCount ?></div>
    </div>
    <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
        <span class="count_top"><i class="fa fa-weixin"></i> Messages</span>
        <div class="count"><?= $messagesCount ?></div>
    </div>
</div>