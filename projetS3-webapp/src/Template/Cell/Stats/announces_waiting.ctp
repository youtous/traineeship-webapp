<?php if ($count > 0) : ?>
    <?= $this->Html->link($this->Html->faIcon('share-alt') . '<span class="badge bg-warning">' . $count . '</span>',
        ['_name' => 'Admin:announces:waiting'],
        [
            'escape' => false,
            'class' => 'dropdown-toggle info-number',
            'data-toggle' => 'tooltip',
            'data-placement' => 'bottom',
            'title' => 'Annonces à modérer'
        ]) ?>
<?php endif; ?>
