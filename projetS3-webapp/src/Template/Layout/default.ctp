<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

$webSiteTitle = 'Centralisons les stages ! - IUT REIMS - Informatique';
?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description"
          content="Site internet de dépôt d'annonces de stages à l'IUT Reims, département informatique.">
    <meta name="keywords" content="reims, iut, informatique, stage, entreprises, formation">
    <meta name="robots" content="index, follow, archive">
    <?= $this->Html->meta('Annonces', ['_name' => 'rss:announces:last', '_ext' => 'rss'], ['type' => 'rss']) ?>

    <title>
        <?= isset($titleAdmin) ? 'Administration :: ' : '' ?>
        <?= isset($title) ? htmlspecialchars($title, ENT_NOQUOTES) : ''; ?>
        <?= isset($action) ? ' - ' . htmlspecialchars($action, ENT_NOQUOTES) : ''; ?>
        <?= isset($title) ? ' | ' : '' ?>
        <?= $webSiteTitle ?>
    </title>
    <?= $this->Html->meta('icon') ?>

    <!--     Fonts and icons     -->
    <?= $this->Html->css('https://fonts.googleapis.com/icon?family=Material+Icons') ?>
    <?= $this->Html->css('https://fonts.googleapis.com/css?family=Roboto:300,400,500,700') ?>
    <?= $this->Html->css('https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css') ?>


    <?= $this->Html->css('bootstrap.min.css') ?>
    <?= $this->Html->css('material-kit.css') ?>
    <?= $this->Html->css('csshake-rotate.min') ?>
    <?= $this->Html->css('theme.css') ?>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
</head>
<body class="signup-page">
<?= $this->element('navbar') ?>
<?= $this->Flash->render() ?>
<?= $this->Flash->render('auth') ?>
<div class="wrapper">
    <div class="header header-filter">
        <header>
            <nav class="navbar navbar-transparent">
                <div class="container">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <a class="navbar-brand hidden-sm hidden-xs" href="#">
                <span>
                <?=
                $this->Html->image('logo-blanc.png', [
                    'alt' => 'Acceuil du site',
                    'class' => 'logo',
                ]);
                ?></span> Liste d'Enrôlements Obtenables</a>
                    </div>

                    <div class="collapse navbar-collapse">
                        <ul class="nav navbar-nav navbar-right">
                            <li><a title="" data-placement="bottom"
                                   href="https://www.facebook.com/dut.info.reims"
                                   data-original-title="Rejoignez nous Facebook"><i class="fa fa-lg fa-facebook"
                                                                                    aria-hidden="true"></i></a></li>
                            <li><a title="" data-placement="bottom"
                                   href="https://twitter.com/dutinforeims"
                                   data-original-title="Rejoignez nous sur Twitter"><i class="fa fa-lg fa-twitter"
                                                                                       aria-hidden="true"></i></a></li>
                            <li><a title="" data-placement="bottom"
                                   href="https://fr.linkedin.com/in/iut-reims-ch%C3%A2lons-charleville-216b14119"
                                   data-original-title="Venez suivre des cours avec Jésus"><i
                                            class="fa fa-lg fa-linkedin" aria-hidden="true"></i></a></li>
                            <li><a title="" data-placement="bottom"
                                   href="https://www.youtube.com/user/dutinforeims"
                                   data-original-title="Rejoignez nous sur youtube"><i class="fa fa-lg fa-youtube"
                                                                                       aria-hidden="true"></i></a></li>
                        </ul>
                    </div>
                </div>
            </nav>
        </header>
        <div class="container">
            <div class="row">
                <?= $this->fetch('content') ?>
            </div>
        </div>
        <footer class="footer navbar-static-bottom">
            <div class="container">
                <nav class="pull-left">
                    <ul>
                        <li><a href="http://www.iut-rcc.fr">IUT RCC</a></li>
                        <li><a href="http://www.univ-reims.fr">URCA</a></li>
                        <li><a href="#cgu-modal" data-toggle="modal" data-target="#cgu-modal">
                                C.G.U.
                            </a></li>
                    </ul>
                </nav>
                <div class="copyright pull-right">
                    &copy; <?php $now = new \Cake\I18n\Date();
                    echo $now->format('Y'); ?> - <i class="fa fa-graduation-cap" aria-hidden="true"></i> Rejoignez l'<a
                            href="http://iut-info.univ-reims.fr/"
                    >I.U.T. Informatique de Reims</a>
                </div>
            </div>
        </footer>
        <?= $this->element('cgu') ?>
        <?= $this->Html->script('jquery.min') ?>
        <?= $this->Html->script('bootstrap.min') ?>
        <?= $this->Html->script('material.min') ?>
        <?= $this->Html->script('material-kit') ?>
        <?= $this->Html->script('search') ?>
        <?= $this->Html->script('promise.min') ?>
        <?= $this->Html->script('url') ?>
        <?= $this->Html->script('fetch') ?>
        <?= $this->fetch('scriptBottom') ?>
    </div>
</div>
<?= $this->fetch('modal') ?>
</body>
</html>

