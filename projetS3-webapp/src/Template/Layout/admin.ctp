<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

$webSiteTitle = 'Plateforme Des Stages - IUT Reims';
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description"
          content="Application d'aministration du site.">
    <title>
        <?= isset($titleAdmin) ? 'Administration :: ' : '' ?>
        <?= isset($title) ? htmlspecialchars($title, ENT_NOQUOTES) : ''; ?>
        <?= isset($action) ? ' - ' . htmlspecialchars($action, ENT_NOQUOTES) : ''; ?>
        <?= isset($title) ? ' | ' : '' ?>
        <?= $webSiteTitle ?>
    </title>
    <?= $this->Html->meta('icon') ?>
    <?= $this->Html->css('https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css') ?>

    <?= $this->Html->css('bootstrap.min.css') ?>
    <?= $this->Html->css('fonts.css') ?>

    <?= $this->Html->css('bootstrap.min.css') ?>

    <?= $this->Html->css('admin.css') ?>
    <?= $this->Html->css('theme-admin.css') ?>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
</head>
<body class="nav-md">
<div class="container body">
    <div class="main_container">
        <div class="col-md-3 left_col">
            <div class="left_col scroll-view">

                <div class="navbar nav_title" style="border: 0;">
                    <a href="<?= $this->Url->build(['_name' => 'home']) ?>" class="site_title"><i
                                class="fa fa-home"></i> <span>Retour au site</span></a>
                </div>

                <div class="clearfix"></div>

                <!-- menu profile quick info -->
                <div class="profile">
                    <div class="profile_pic">
                        <?= $this->Gravatar->image($this->request->session()->read('Auth.User.email'), ['class' => 'img-circle img-square profile_img ', 'default' => 'monsterid']); ?>
                    </div>
                    <div class="profile_info">
                        <span>Bienvenue,</span>
                        <h2><?= $this->request->session()->read('Auth.User.firstname') ?> <?= $this->request->session()->read('Auth.User.lastname') ?></h2>
                    </div>
                </div>
                <!-- /menu profile quick info -->


                <!-- sidebar menu -->
                <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                    <div class="menu_section">
                        <ul class="nav side-menu">
                            <li><a><i class="fa fa-users"></i> Utilisateurs <span
                                            class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <li><?= $this->Html->link('Gérer', ['_name' => 'Admin:indexUsers']) ?></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <div class="menu_section">
                        <ul class="nav side-menu">
                            <li><a><i class="fa fa-magic"></i> Compétences <span
                                            class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <li><?= $this->Html->link('Gérer', ['_name' => 'Admin:indexSkills']) ?></li>
                                    <li><?= $this->Html->link('Ajouter', ['_name' => 'Admin:addSkill']) ?></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <div class="menu_section">
                        <ul class="nav side-menu">
                            <li><a><i class="fa fa-share-alt"></i> Annonces <span
                                            class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <li><?= $this->Html->link('Gérer', ['_name' => 'Admin:announces:index']) ?></li>
                                    <li><?= $this->Html->link('Modérer (valider)', ['_name' => 'Admin:announces:waiting']) ?></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <div class="menu_section">
                        <ul class="nav side-menu">
                            <li><a><i class="fa fa-briefcase"></i> Stages <span
                                            class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <li><?= $this->Html->link('Gérer', ['_name' => 'Admin:traineeships:index']) ?></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <div class="menu_section">
                        <ul class="nav side-menu">
                            <li><a><i class="fa fa-database"></i> Données <span
                                            class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <li><?= $this->Html->link('Statistiques', ['_name' => 'Admin:indexFiles']) ?></li>
                                    <li><?= $this->Html->link('Supprimer un fichier', ['_name' => 'Admin:deleteFile']) ?></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <div class="menu_section">
                        <ul class="nav side-menu">
                            <li><a><i class="fa fa-envelope-open"></i> Missives <span
                                            class="fa fa-chevron-down"></span></a>
                                <ul class="nav child_menu">
                                    <li><?= $this->Html->link('Historique', ['_name' => 'Admin:newsletters:index']) ?></li>
                                    <li><?= $this->Html->link('Rédiger', ['_name' => 'Admin:newsletters:add']) ?></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
                <!-- /sidebar menu -->

                <!-- /menu footer buttons -->
                <div class="sidebar-footer hidden-small">
                    <a data-toggle="tooltip" data-placement="top" title="Déconnexion"
                       href="<?= $this->Url->build(['_name' => 'logout']) ?>">
                        <i class="fa fa-times-circle" aria-hidden="true"></i>
                    </a>
                    <a data-toggle="tooltip" data-placement="top" title="Administration"
                       href="<?= $this->Url->build(['_name' => 'Admin:index']) ?>">
                        <i class="fa fa-cogs" aria-hidden="true"></i>
                    </a>
                </div>
                <!-- /menu footer buttons -->
            </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
            <div class="nav_menu">
                <nav>
                    <div class="nav toggle">
                        <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                    </div>
                    <div class="alert-top">
                        <?= $this->Flash->render() ?>
                    </div>
                    <ul class="nav navbar-nav navbar-right">
                        <li class="dropdown">
                            <?= $this->Cell('Stats::announcesWaiting', [], ['cache' => ['config' => 'minutes']]) ?>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
            <div id="page">
                <?= $this->fetch('content'); ?>
                <div class="push"></div>
            </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer class="footer">
            <div class="pull-right">
                Portail d'administration (Gentelella)
            </div>
            <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
    </div>
</div>
<?= $this->Html->script('jquery.min') ?>
<?= $this->Html->script('bootstrap.min') ?>

<?= $this->Html->script('admin') ?>
<?= $this->fetch('scriptBottom') ?>
</body>
</html>
