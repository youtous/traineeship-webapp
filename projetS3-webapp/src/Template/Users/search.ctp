<div class="main main-raised">
    <div class="section section-basic">
        <div class="container">
            <div class="title">
                <?= $this->Html->link($this->Html->faIcon('exchange'),
                    ['_name' => 'messagesOfUser'], ['escape' => false, 'data-original-title' => 'Conversations', 'data-placement' => 'left', 'class' => 'btn btn-xs btn-primary btn-simple pull-right']) ?>
                <h2 class="text-center">Utilisateurs
                    <span class="fa-stack">
                        <i class="fa fa-users fa-stack-1x"></i>
                        <i class="fa fa-search fa-stack-1x txt-brightgrey"></i>
                    </span>
                </h2>
            </div>
            <div class="col-md-6 col-md-offset-3">
                <div class="panel-body">
                    <?= $this->Form->create(null, ['type' => 'get']) ?>
                    <div class="form-group row">
                        <div class="input-group">
                            <?= $this->Form->input('name', ['label' => '', 'placeholder' => 'Rechercher un utilisateur', 'required' => true, 'value' => $name]) ?>
                            <span class="input-group-addon">
                        <?= $this->Form->button($this->Html->faIcon('search'), ['escape' => false, 'class' => 'btn btn-simple btn-primary btn-fab-small btn-fab btn-fab-mini']) ?></span>
                        </div>
                    </div>
                    <?= $this->Form->end() ?>
                    <?php if ($name != null) : ?>
                        <p class="text-info"><?= sizeof($users) ?> résultats à votre recherche.</p>
                        <?= $this->Html->link('Annuler ma recherche', ['_name' => 'messagesOfUser'], ['class' => 'btn btn-primary btn-xs']) ?>
                    <?php endif; ?>
                </div>
                <?php if (sizeof($users) === 0) : ?>
                    <div class="text-center">
                        <h5>Aucun utilisateur trouvé !</h5>
                        <p>Essayez d'autres mots clefs dans votre recherche...<p>
                    </div>
                <?php else: ?>
                    <ul class="list-group">
                        <?php foreach ($users as $user): ?>
                            <li class="list-group-item">
                                <?= h($user->name) ?>
                                <div class="pull-right">
                                    <?= $this->Html->link($this->Html->faIcon('address-card-o') . ' Profil', ['_name' => 'profile', 'id' => $user->id], ['escape' => false, 'class' => 'btn btn-xs btn-success btn-no-margin']) ?>
                                    <?= $this->Html->link('<i class="material-icons">chat</i>' . ' Converser', ['_name' => 'conversation', 'recipient_id' => $user->id], ['escape' => false, 'class' => 'btn btn-xs btn-primary btn-no-margin']) ?>
                                </div>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                    <?php if ($this->Paginator->hasPage(2)) : ?>
                        <div class="section-pagination">
                            <div class="text-center">
                                <ul class="pagination">
                                    <?= $this->Paginator->prev('« ') ?>
                                </ul>
                                <?= $this->Paginator->numbers(['first' => 2, 'last' => 2, 'modulus' => 6]) ?>
                                <ul class="pagination">
                                    <?= $this->Paginator->next(' »') ?>
                                </ul>
                            </div>
                        </div>
                    <?php endif; ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
