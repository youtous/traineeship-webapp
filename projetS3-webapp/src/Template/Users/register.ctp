<section class="col-md-6 col-md-offset-3 col-sm-6 col-sm-offset-3">
    <div class="card card-signup">
        <?= $this->Form->create($user, ['id' => 'register']) ?>
        <div class="header header-primary text-center">
            <h4>Création d'un compte</h4>
        </div>
        <p class="text-divider text-info">Les champs facultatifs sont indiqués par ~.</p>
        <div class="content">
            <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-envelope-open"></i></span>
                <?= $this->Form->input('email', ['label' => '', 'placeholder' => 'john.doe@reims.fr']); ?>
            </div>
            <div id="divMailSuccess" class="text-center hidden">
                <span id="spanMailSuccess" data-toggle="tooltip" data-placement="bottom" title=""
                      data-original-title="Nous l'avons deviné avec votre adresse email !" data-container="body"
                      class="label btn-tooltip label-success"></span>
            </div>
            <div class="form-group row">
                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-user-circle-o"></i></span>
                </div>
                <div class="col-md-12">
                    <p class="text-center">Vous êtes ?</p>
                    <?php
                    $this->Form->templates([
                        'radioWrapper' => '<div class="radio radio-inline-material">{{label}}</div>'
                    ]); ?>
                    <?= $this->Form->radio('sex', [
                        ['value' => 'H', 'text' => 'Homme', 'class' => 'checkbox-inline'],
                        ['value' => 'F', 'text' => 'Femme', 'class' => 'checkbox-inline'],
                        ['value' => 'I', 'text' => 'Indéterminé', 'class' => 'checkbox-inline'],
                    ]); ?>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-4">
                    <?= $this->Form->input('firstname', ['label' => 'Prénom', 'placeholder' => 'John']); ?>
                </div>
                <div class="col-md-4">
                    <?= $this->Form->input('lastname', ['label' => 'Nom', 'placeholder' => 'Doe']); ?>
                </div>
                <div class="col-md-4">
                    <?= $this->Form->input('title', ['label' => 'Entreprise', 'placeholder' => 'Umbrella Corporation', 'required' => false]); ?>
                </div>
            </div>
            <div class="form-group row">
                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-birthday-cake"></i></span>
                </div>
                <div class="col-md-12">
                    <?= $this->Form->input('birthday', ['label' => 'Date de naissance', 'minYear' => 1920,
                        'maxYear' => date('Y')]); ?>
                </div>
            </div>
            <div class="form-group row">
                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-map-signs"></i></span>
                </div>
                <div class="col-md-4">
                    <?= $this->Form->input('postal_code', ['label' => 'Code postal', 'placeholder' => '51100']); ?>
                </div>
                <div class="col-md-8">
                    <?= $this->Form->input('city', ['label' => 'Ville', 'placeholder' => 'Reims']); ?>
                </div>
                <div class="col-md-8">
                    <?= $this->Form->input('street', ['label' => 'Rue', 'placeholder' => 'Rue des rouliers']); ?>
                </div>
                <div class="col-md-2">
                    <?= $this->Form->input('street_number', ['label' => 'Numéro', 'placeholder' => '1']); ?>
                </div>
                <div class="col-md-2">
                    <?= $this->Form->input(
                        'street_number_complement',
                        [
                            'label' => 'Complément',
                            'type' => 'select',
                            'options' => ['' => 'Aucun', 'B' => 'B', 'T' => 'T', 'Q' => 'Q', 'A' => 'A']
                        ]
                    );
                    ?>
                </div>
            </div>
        </div>
        <div class="form-group row">
            <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-phone" aria-hidden="true"></i></span>
                <div class="col-md-12">
                    <?= $this->Form->input('phone', ['label' => '', 'placeholder' => '~0808080808']); ?>
                </div>
                <span class="input-group-addon"><i class="fa fa-external-link" aria-hidden="true"></i></span>
                <div class="col-md-12">
                    <?= $this->Form->input('website', ['label' => '', 'placeholder' => '~http://website.com']); ?>
                </div>
            </div>
        </div>
        <div class="form-group row">
            <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-lock"></i></span>
            </div>
            <div class="col-md-6">
                <?= $this->Form->input('password', ['label' => 'Mot de passe', 'placeholder' => '6 à 100 caractères']); ?>
            </div>
            <div class="col-md-6">
                <?= $this->Form->input('password', ['label' => 'Confirmation', 'placeholder' => 'confirmez le mot de passe', 'id' => 'passwordConfirmation']); ?>
            </div>
        </div>
        <div class="checkbox">
            <div class="form-group row">
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-12">
                            <?= $this->Form->input('subscribed', ['label' => 'Recevoir missives']); ?>
                        </div>
                        <div class="col-md-12">
                            <?= $this->Form->input('cgu', ['type' => 'checkbox', 'label' => 'Accepter les C.G.U.']); ?>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <?= $this->Recaptcha->display() ?>
                </div>
            </div>
        </div>
        <div class="footer text-center">
            <?= $this->Form->button(__('Je confirme l\'exactitude des informations'), ['class' => 'btn btn-success btn-large']) ?>
        </div>
        <?= $this->Form->end() ?>
    </div>
</section>
<?= $this->Html->script('forms.utils', ['block' => 'scriptBottom']); ?>
<?= $this->Html->script('register', ['block' => 'scriptBottom']); ?>
