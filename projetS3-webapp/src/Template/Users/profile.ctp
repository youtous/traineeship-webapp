<?php use App\Model\Table\UsersTable; ?>
<div class="main main-raised">
    <div class="section section-basic section-spacetop">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <?= $this->Html->link('<i class="material-icons">chat</i> Contacter', ['_name' => 'conversation', 'recipient_id' => $user->id], ['escape' => false, 'class' => 'btn btn-success pull-right']) ?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="text-center">
                    <?= $this->Gravatar->image(h($user->email), ['class' => 'img-circle', 'default' => 'monsterid']); ?>
                    <h3>
                        <?= h($user->firstname) ?> <?= h($user->lastname) ?>
                        <small>
                            <?= $this->element('user-icon', ['role' => $user->role], [
                                'cache' => ['config' => 'short', 'key' => 'element_user-icon_' . $user->id]
                            ]) ?>
                            <?php if ($user->title != null): ?>
                                <br><?= h($user->title) ?>
                            <?php endif; ?>
                        </small>
                    </h3>
                </div>
                <div class="text-center">
                    <!-- ville -->
                    <p><i class="fa fa-home" aria-hidden="true"></i> <?= h($user->city) ?></p>
                    <!-- telephone -->
                    <?php if ($user->phone != null): ?>
                        <?php if ($user->role == UsersTable::roles['company']) : ?>
                            <p><i class="fa fa-phone" aria-hidden="true"></i> <?= $user->phone ?></p>
                        <?php endif ?>
                    <?php endif ?>

                    <!-- site -->
                    <?php if ($user->website != null): ?>
                        <?php
                        $this->start('modal');
                        echo $this->fetch('modal');
                        echo $this->Modal->create(['id' => 'website-redirect']);
                        echo $this->Modal->header('Confirmation de redirection vers <em>' . h($user->website) . '</em>');
                        echo $this->Modal->body(
                            'Ce lien n\'a pas été approuvé par notre site, voulez-vous continuer ? :', [
                            'class' => 'my-body-class'
                        ]);
                        echo $this->Modal->footer([
                            $this->Html->link(
                                'Oui',
                                $user->website,
                                [
                                    'class' => 'btn btn-info',
                                    'escape' => false,
                                    'rel' => 'nofollow',
                                ]
                            ),
                            $this->Form->button('Non', ['data-dismiss' => 'modal'])
                        ]);
                        echo $this->Modal->end();
                        $this->end();
                        ?>
                        <?= $this->Form->button(
                            $this->Html->tag('i', '', ['class' => 'fa fa-external-link', 'aria-hidden' => 'true']) . ' site internet', [
                            'data-toggle' => 'modal',
                            'data-target' => '#website-redirect',
                            'class' => 'btn btn-simple',
                        ]) ?>
                    <?php endif ?>
                    <p>
                        Inscrit le <?= $user->created->nice() ?>
                        <br>
                        Connecté le <?= $user->last_login->nice() ?>
                    </p>
                </div>

            </div>
            <div class="col-md-8">
                <div class="row">
                    <div class="col-xs-push-1 col-xs-10 text-center">
                        <?php if ($user->role == UsersTable::roles['company']) : ?>
                            <h3>Dernières annonces</h3>
                            <?php if (!$announces->isEmpty()) : ?>
                                <ul class="list-group text-left profile-announces">
                                    <?php foreach ($announces as $announce) : ?>
                                        <li class="list-group-item list-announces">
                                            <?= $this->element('Announce' . DS . 'table-cell', ['announce' => $announce],
                                                ['cache' => ['config' => 'minutes', 'key' => 'announces-table-cell_' . $announce->id]]) ?>
                                        </li>
                                    <?php endforeach; ?>
                                </ul>
                            <?php else: ?>
                                <p>Aucune annonce pour le moment !</p>
                            <?php endif; ?>
                        <?php else: ?>
                            <h3>Compétences</h3>
                            <?php if (!$skills->isEmpty()) : ?>
                                <?= $this->element('Skills' . DS . 'images', ['skills' => $skills],
                                    ['cache' => ['config' => 'minutes', 'key' => 'skills-images-user_' . $user->id]]) ?>
                            <?php else: ?>
                                <p>Aucune compétence renseignée !</p>
                            <?php endif; ?>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
