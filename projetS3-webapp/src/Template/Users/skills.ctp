<?php use App\Model\Table\SkillsTable; ?>
<div class="main main-raised">
    <div class="section section-basic">
        <div class="container">
            <div class="title text-center">
                <h2>Vos Compétences</h2>
                <p class="help-block">
                    Renseignez ce que vous maîtrisez, les compétences seront visibles sur votre profil.
                </p>
            </div>
            <div class="col-md-12">
                <?= $this->Form->create($user) ?>
                <?php
                $this->Form->templates([
                    'checkbox' => '<img class="skill-img" src="' . $this->Url->build('/') . 'img' . DS . SkillsTable::SKILLS_IMAGES_WEBPATH . '{{value}}.png"><input type="checkbox" name="{{name}}" value="{{value}}"{{attrs}}>',
                    'checkboxFormGroup' => '{{label}}',
                    'nestingLabel' => '{{hidden}}<label{{attrs}}>{{input}}<legend class="hidden">{{text}}</legend></label>',
                    'checkboxWrapper' => '<div class="togglebutton togglebutton-inline">{{label}}</div>',
                    'checkboxContainer' => '{{h_checkboxContainer_start}}<div class="togglebutton {{required}}">{{content}}</div>{{h_checkboxContainer_end}}',
                ]);
                ?>
                <div class="input-group">
                    <?= $this->Form->select('skills._ids', $skills, [
                        'multiple' => 'checkbox',
                    ]); ?>
                </div>
                <div class="text-center">
                    <?= $this->Form->button($this->Html->faIcon('check') . ' ' . __('Mettre à jours mes compétences'), ['class' => 'btn btn-success btn-large']) ?>
                </div>
                <?= $this->Form->end() ?>
            </div>
        </div>
    </div>
</div>