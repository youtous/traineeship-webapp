<?php use App\Model\Table\UsersTable; ?>
    <div class="main main-raised">
        <div class="section section-basic">
            <div class="container">
                <div class="title">
                    <h2 class="text-center">Vos informations personnelles</h2>
                </div>
                <div class="col-md-8 col-md-push-4">
                    <?= $this->Form->create($user, ['id' => 'profile']) ?>
                    <h3 class="text-center">Modifier mes données</h3>
                    <div class="content">
                        <div class="form-group row">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                            </div>
                            <?php ($this->request->session()->read('Auth.User.role') !== UsersTable::roles['company']) ? $md = 6 : $md = 4; ?>
                            <div class="col-md-<?= $md ?>">
                                <?= $this->Form->input('firstname', ['label' => 'Prénom', 'placeholder' => 'John']); ?>
                            </div>
                            <div class="col-md-<?= $md ?>">
                                <?= $this->Form->input('lastname', ['label' => 'Nom', 'placeholder' => 'Doe']); ?>
                            </div>
                            <?php if ($this->request->session()->read('Auth.User.role') === UsersTable::roles['company']) : ?>
                                <div class="col-md-4">
                                    <?= $this->Form->input('title', ['label' => 'Entreprise', 'placeholder' => 'Umbrella Corporation']); ?>
                                </div>
                            <?php endif; ?>
                        </div>
                        <div class="form-group row">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-birthday-cake"></i></span>
                            </div>
                            <div class="col-md-12">
                                <?= $this->Form->input('birthday', ['label' => 'Date de naissance', 'minYear' => 1920,
                                    'maxYear' => date('Y')]); ?>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-map-signs"></i></span>
                            </div>
                            <div class="col-md-4">
                                <?= $this->Form->input('postal_code', ['label' => 'Code postal', 'placeholder' => '51100']); ?>
                            </div>
                            <div class="col-md-8">
                                <?= $this->Form->input('city', ['label' => 'Ville', 'placeholder' => 'Reims']); ?>
                            </div>
                            <div class="col-md-8">
                                <?= $this->Form->input('street', ['label' => 'Rue', 'placeholder' => 'Rue des rouliers']); ?>
                            </div>
                            <div class="col-md-2">
                                <?= $this->Form->input('street_number', ['label' => 'Numéro', 'placeholder' => '1']); ?>
                            </div>
                            <div class="col-md-2">
                                <?= $this->Form->input(
                                    'street_number_complement',
                                    [
                                        'label' => 'Complément',
                                        'type' => 'select',
                                        'options' => ['' => 'Aucun', 'B' => 'B', 'T' => 'T', 'Q' => 'Q', 'A' => 'A']
                                    ]
                                );
                                ?>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="input-group">

                                <span class="input-group-addon"><i class="fa fa-phone" aria-hidden="true"></i></span>
                                <div class="col-md-12">
                                    <?= $this->Form->input('phone', ['label' => '', 'placeholder' => '~0808080808']); ?>
                                </div>
                                <span class="input-group-addon"><i class="fa fa-external-link"
                                                                   aria-hidden="true"></i></span>
                                <div class="col-md-12">
                                    <?= $this->Form->input('website', ['label' => '', 'placeholder' => '~http://website.com']); ?>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <h6>Préférences email</h6>
                            <div class="col-xs-6">
                                <?= $this->Form->input('subscribed', ['label' => 'Missives']); ?>
                                <?= $this->Form->input('accept_notifications', ['label' => 'Notifications']); ?>
                            </div>
                            <div class="col-xs-6">
                                <p class="help-block block-relative">Les missives sont des messages informatifs.<br>
                                    Les notifications sont utilisées pour vous informer de nouveaux messages privés,
                                    nouvelles annonces ciblées etc.</p>
                            </div>
                        </div>
                    </div>
                    <div class="footer text-center">
                        <?= $this->Form->button(__('Mettre à jour mes informations'), ['class' => 'btn btn-success btn-large']) ?>
                    </div>
                    <?= $this->Form->end() ?>
                    <?= $this->Form->create(null, ['id' => 'password-update']) ?>
                    <div class="form-group row">
                        <h4 class="text-center">Choisir un nouveau mot de passe</h4>
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                        </div>
                        <div class="col-md-6">
                            <?= $this->Form->input('password', ['label' => 'Mot de passe', 'placeholder' => '6 à 100 caractères', 'value' => '']); ?>
                        </div>
                        <div class="col-md-6">
                            <?= $this->Form->input('password', ['label' => 'Confirmation', 'placeholder' => 'confirmez le mot de passe', 'value' => '', 'id' => 'passwordConfirmation']); ?>
                        </div>
                    </div>
                    <div class="footer text-center">
                        <?= $this->Form->button(__('Changer mon mot de passe'), ['class' => 'btn btn-success btn-large']) ?>
                    </div>
                    <?= $this->Form->end() ?>
                </div>
                <div class="row">
                    <div class="col-md-4 col-md-pull-8">
                        <h3 class="text-center">Informations complémentaires</h3>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title text-center">Votre compte</h3>
                            </div>
                            <div class="panel-body">
                                <figure class="center-block text-center">
                                    <?= $this->Gravatar->image($user->email, ['class' => 'img-rounded img-raised ', 'default' => 'monsterid']); ?>
                                    <figcaption>
                                        <a data-toggle="tooltip" data-placement="bottom" title="" data-container="body"
                                           data-original-title="Nous utilisons un service externe pour les avatars."
                                           href="https://fr.gravatar.com/" class="btn btn-sm btn-primary"
                                           target="_blank"><i
                                                    class="fa fa-cloud-upload" aria-hidden="true"></i> Changer d'avatar</a>
                                    </figcaption>
                                </figure>
                                <hr>
                                <address>
                                    <strong>Adresse email principale</strong><br>
                                    <a href="mailto:<?= $user->email ?>"><?= $user->email ?></a>
                                </address>
                                <p>
                                    <strong>Création du compte : </strong> <?= $user->created->nice() ?>
                                </p>
                                <p>
                                    <strong>Dernière modification : </strong> <?= $user->modified->nice() ?>
                                </p>
                                <p>
                                    <strong>Dernière connexion : </strong> <?= $user->last_login->nice() ?>
                                    <br>
                                    Sur l'adresse ip <code><?= $user->last_ip ?></code>
                                </p>
                            </div>
                        </div>
                        <blockquote class="text-justify text-inherit">
                            "Avertissement : ce site Web comporte des informations nominatives concernant notamment les
                            personnels du département informatique de l'I.U.T. de Reims, de ses élèves et de ses
                            partenaires. Conformément à la loi n° 78-17 du 6 janvier 1978 modifiée, relative à
                            l’informatique, aux fichiers et aux libertés, vous disposez d’un droit d’accès, de
                            modification,
                            de rectification et de suppression des données vous concernant en ligne sur ce site. Pour
                            exercer ce droit, adressez-vous à l’administrateur du site".
                            <footer>Plus d'informations sur <cite title="Commission Nationale de l'Informatique et des Libertés
"><a href="https://cnil.fr">CNIL</a></cite></footer>
                        </blockquote>
                        <p>
                            Les données collectées sont utilisées dans le but de réaliser une fiche personnelle
                            disponible
                            pour les membres de ce site et constituent des impératifs quant au bon fonctionnement de
                            notre
                            réseau de stage.
                        </p>
                        <p>
                            Les données qui y figurent ne peuvent être collectées ou utilisées à d’autres fins.
                        </p>
                        <hr>
                        <?php
                        $this->start('modal');
                        // modal delete
                        echo $this->Modal->create(['id' => 'user-confirm-delete-' . $user->id]);
                        echo $this->Modal->header('Confirmation d\'action');
                        echo $this->Modal->body(
                            'Confimez-vous vouloir supprimer votre compte ? Toutes les données associées seront perdues définitivement.<br>
Pour procéder à la suppression de votre compte, vous pouvez aussi contacter un administrateur par l\'adresse email de contact disponible dans les C.G.U.', [
                            'class' => 'my-body-class'
                        ]);
                        echo $this->Modal->footer([
                            $this->Form->postLink(
                                'Supprimer ' . $this->Html->tag('i', '', ['class' => 'fa fa-trash', 'aria-hidden' => 'true']),
                                ['_name' => 'requestDeleteAccount'],
                                [
                                    'class' => 'btn btn-danger',
                                    'escape' => false
                                ]
                            ),
                            $this->Form->button('Fermer', ['data-dismiss' => 'modal'])
                        ]);
                        echo $this->Modal->end();
                        $this->end();
                        ?>
                        <?= $this->Html->link('Supprimer mon compte ', '#', [
                            'data-toggle' => 'modal',
                            'data-target' => '#user-confirm-delete-' . $user->id,
                            'class' => 'btn btn-simple btn-danger center-block',
                        ]) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?= $this->Html->script('forms.utils', ['block' => 'scriptBottom']); ?>
<?= $this->Html->script('updateProfile', ['block' => 'scriptBottom']); ?>