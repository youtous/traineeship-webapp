<section class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3">
<div class="card card-signup">
    <div class="header header-primary text-center">
        <h4>S'authentifier</h4>
    </div>
    <p class="text-divider">
        Informations de connexion
    </p>
    <?= $this->Form->create() ?>
    <div class="content">
        <div class="input-group">
            <span class="input-group-addon"><i class="material-icons">email</i></span>
            <?= $this->Form->input('email', ['label' => '', 'placeholder' => 'will@aie.iam', 'required' => true]); ?>
        </div>

        <div class="input-group">
            <span class="input-group-addon"><i class="material-icons">lock_outline</i></span>
            <?= $this->Form->input('password', ['label' => '', 'placeholder' => '*******', 'required' => true]); ?>
        </div>
    </div>
    <div class="footer text-center">
        <div class="btn-group" role="group">
            <?= $this->Form->button(__('Se connecter'), ['class' => 'btn btn-simple btn-primary']) ?>
            <?= $this->Html->link('Un oubli ?', ['_name' => 'requestNewPassword'], ['class' => 'btn btn-simple btn-default']) ?>
        </div>
    </div>
    <?= $this->Form->end() ?>
</div>
<div class="card card-nav-tabs">
    <div class="content text-center">
        Besoin d'un compte ? <?= $this->Html->link('S\'inscrire', ['_name' => 'register']) ?>
    </div>
</div>
</section>



