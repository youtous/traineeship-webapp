<section class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3">
    <div class="card card-signup">
        <div class="header header-primary text-center">
            <h4>Obtenir un nouveau mot de passe</h4>
        </div>
        <p class="text-divider">
            Veuillez entrer l'addresse email associée au compte.
        </p>
        <?= $this->Form->create($validation, ['context' => ['validator' => 'retrieve']]) ?>
        <div class="content">
            <div class="input-group">
                <span class="input-group-addon"><i class="material-icons">email</i></span>
                <?= $this->Form->input('email', ['label' => '', 'placeholder' => 'will@aie.iam']); ?>
            </div>
        </div>
        <div class="footer text-center">
            <?= $this->Recaptcha->display() ?>
            <div class="btn-group" role="group">
                <?= $this->Form->button(__('Soumettre'), ['class' => 'btn btn-simple btn-primary']) ?>
                <?= $this->Html->link('Se connecter', ['_name' => 'login'], ['class' => 'btn btn-simple btn-default']) ?>
            </div>
        </div>
        <?= $this->Form->end() ?>
    </div>
    <div class="card card-nav-tabs">
        <div class="content text-center">
            Besoin d'un compte ? <?= $this->Html->link('S\'inscrire', ['_name' => 'register']) ?>
        </div>
    </div>
</section>