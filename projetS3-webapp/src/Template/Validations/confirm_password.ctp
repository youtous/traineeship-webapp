<section class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3">
    <div class="card card-signup">
        <div class="header header-primary text-center">
            <h4>Changement de mot de passe</h4>
        </div>
        <p class="text-divider">
            Veuillez confirmer le nouveau mot de passe du compte.
        </p>
        <div class="content">
            <?= $this->Form->create($user, ['id' => 'change-password']) ?>
            <div class="input-group">
                <div class="form-group row">
                    <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                    <div class="col-md-12">
                        <?= $this->Form->input('password', ['label' => 'Mot de passe', 'placeholder' => '6 à 100 caractères']); ?>
                    </div>
                    <div class="col-md-12">
                        <?= $this->Form->input('password', ['label' => 'Confirmation', 'placeholder' => 'confirmez le mot de passe', 'id' => 'passwordConfirmation']); ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer text-center">
            <div class="btn-group" role="group">
                <?= $this->Form->button(__('Soumettre'), ['class' => 'btn btn-simple btn-primary']) ?>
                <?= $this->Html->link('Se connecter', ['_name' => 'login'], ['class' => 'btn btn-simple btn-default']) ?>
            </div>
        </div>
        <?= $this->Form->end() ?>
    </div>
</section>
<?= $this->Html->script('forms.utils', ['block' => 'scriptBottom']); ?>
<?= $this->Html->script('changePassword', ['block' => 'scriptBottom']); ?>