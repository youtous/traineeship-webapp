<section class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3">
    <div class="card card-signup">
        <div class="header header-primary text-center">
            <h4>Supprimer votre compte</h4>
        </div>
        <p class="text-danger text-divider">
            Vous êtes sur le point de nous quitter
            Veuillez entrer en toutes lettres la phrase suivante indiquée.
        </p>
        <div class="content">
            <?= $this->Form->create(null) ?>
            <div class="input-group">
                <div class="form-group row">
                    <span class="input-group-addon"><i class="fa fa-handshake-o"></i></span>
                    <div class="col-md-12">
                        <?= $this->Form->input('confirm', ['label' => 'JE NE SOUHAITE PLUS FAIRE PARTIE DE L\'AVENTURE', 'placeholder' => 'à vous de jouer !']); ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer text-center">
            <div class="btn-group" role="group">
                <?= $this->Form->button(__('Supprimer mon compte'), ['class' => 'btn btn-danger btn-simple btn-primary']) ?>
                <?= $this->Html->link('Annuler', ['_name' => 'updateProfile'], ['class' => 'btn btn-simple btn-default']) ?>
            </div>
        </div>
        <?= $this->Form->end() ?>
    </div>
</section>