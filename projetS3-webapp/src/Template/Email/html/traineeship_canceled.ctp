<?php
$this->extend('default');
$this->assign('title', 'Annulation de votre stage.');
$this->assign('paragraph', 'Votre stage relatif à l\'annonce "' . h($announce->title) . '" a été annulé par un administrateur.<br>Vous pouvez en savoir plus en contactant l\'administrateur par la fonction "Signaler par message".'); ?>

<tr>
    <td align="center" valign="top" style="border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0; padding-left: 6.25%; padding-right: 6.25%; width: 87.5%;
			padding-top: 25px;
			padding-bottom: 5px;" class="button"><a
                href="<?= $this->Url->build([
                    '_name' => 'messagesOfUser'
                ], true) ?>" target="_blank"
                style="text-decoration: underline;">
            <table border="0" cellpadding="0" cellspacing="0" align="center"
                   style="max-width: 240px; min-width: 120px; border-collapse: collapse; border-spacing: 0; padding: 0;">
                <tr>
                    <td align="center" valign="middle"
                        style="padding: 12px 24px; margin: 0; text-decoration: underline; border-collapse: collapse; border-spacing: 0; border-radius: 4px; -webkit-border-radius: 4px; -moz-border-radius: 4px; -khtml-border-radius: 4px;"
                        bgcolor="#f55145"><a target="_blank" style="text-decoration: underline;
					color: #FFFFFF; font-family: sans-serif; font-size: 17px; font-weight: 400; line-height: 120%;"
                                             href="
                                <?= $this->Url->build([
                                                 '_name' => 'messagesOfUser'
                                             ], true) ?>
                                ">
                            <?= __('Demander des renseignements') ?>
                        </a>
                    </td>
                </tr>
            </table>
        </a>
    </td>
</tr>

