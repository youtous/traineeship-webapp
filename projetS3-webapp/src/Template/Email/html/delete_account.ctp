<?php
$this->extend('default');
$this->assign('title', 'Vous voulez nous quitter ?');
$this->assign('paragraph', 'Une demande de suppression de compte a été émise pour le compte rattaché à cette adresse email. Veuillez confirmer votre choix.'); ?>

<!-- BUTTON -->
<!-- Set button background color at TD, link/text color at A and TD, font family ("sans-serif" or "Georgia, serif") at TD. For verification codes add "letter-spacing: 5px;". Link format: http://domain.com/?utm_source={{Campaign-Source}}&utm_medium=email&utm_content={{Button-Name}}&utm_campaign={{Campaign-Name}} -->
<tr>
    <td align="center" valign="top" style="border-collapse: collapse; border-spacing: 0; margin: 0; padding: 0; padding-left: 6.25%; padding-right: 6.25%; width: 87.5%;
			padding-top: 25px;
			padding-bottom: 5px;" class="button"><a
            href="<?= $this->Url->build([
                '_name' => 'validateDeleteAccount',
                'code' => $code
            ], true) ?>" target="_blank"
            style="text-decoration: underline;">
            <table border="0" cellpadding="0" cellspacing="0" align="center"
                   style="max-width: 240px; min-width: 120px; border-collapse: collapse; border-spacing: 0; padding: 0;">
                <tr>
                    <td align="center" valign="middle"
                        style="padding: 12px 24px; margin: 0; text-decoration: underline; border-collapse: collapse; border-spacing: 0; border-radius: 4px; -webkit-border-radius: 4px; -moz-border-radius: 4px; -khtml-border-radius: 4px;"
                        bgcolor="#f55145"><a target="_blank" style="text-decoration: underline;
					color: #FFFFFF; font-family: sans-serif; font-size: 17px; font-weight: 400; line-height: 120%;"
                                             href="
                                <?= $this->Url->build([
                                                 '_name' => 'validateDeleteAccount',
                                                 'code' => $code
                                             ], true) ?>
                                ">
                            Supprimer mon compte
                        </a>
                    </td>
                </tr>
            </table>
        </a>
    </td>
</tr>

