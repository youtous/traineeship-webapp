<?php
/**
 * Created by PhpStorm.
 * User: youtous
 * Date: 11/01/2017
 * Time: 23:14
 */

namespace App\Shell\Task;

use App\Model\Table\CandidaciesTable;
use App\Model\Table\UsersTable;
use Cake\Collection\Collection;
use Cake\Core\Configure;
use Cake\I18n\Time;
use Cake\Mailer\MailerAwareTrait;
use Cake\ORM\TableRegistry;
use DateInterval;
use Http\Adapter\Guzzle6\Client;
use Http\Message\MessageFactory\GuzzleMessageFactory;
use Ivory\GoogleMap\Service\Base\Location\AddressLocation;
use Ivory\GoogleMap\Service\Base\TrafficModel;
use Ivory\GoogleMap\Service\Base\TravelMode;
use Ivory\GoogleMap\Service\Base\UnitSystem;
use Ivory\GoogleMap\Service\DistanceMatrix\DistanceMatrixService;
use Ivory\GoogleMap\Service\DistanceMatrix\Request\DistanceMatrixRequest;
use Ivory\GoogleMap\Service\DistanceMatrix\Response\DistanceMatrixElementStatus;
use Ivory\GoogleMap\Service\DistanceMatrix\Response\DistanceMatrixStatus;
use Queue\Shell\Task\QueueTask;

/**
 * Task used for creating Traineeships of not yet assigned Candidacies.
 *
 * Class QueueTraineeshipTask
 * @package App\Shell\Task
 */
class QueueTraineeshipTask extends QueueTask
{

    use MailerAwareTrait;

    /**
     * List of default variables for Mailer
     *
     * @var array
     */
    public $defaults = [
        /**
         * Frequency e.g. : every days the Traineeships will be created.
         */
        'frequency' => '1 day',
    ];

    /**
     * Create the Traineeship Task.
     *
     * @return void
     */
    public function add()
    {
        $this->out('Traineeship assignment task.');
        /*
         * Adding a task of type 'example' with no additionally passed data
         */
        if ($this->QueuedJobs->createJob('Traineeship', null)) {
            $this->out('OK, job created, now run the worker');
        } else {
            $this->err('Could not create Job');
        }
    }

    /**
     * Run method called by the worker.
     * Find all the Candidacies validated with no associated Traineeship.
     *
     * Create the Traineeship :
     *  - starting date : config['default starting date']
     *  - ending date : starting + config['default duration traineeship']
     *  - assign a professor based on :
     *      - 0 not finished Traineeship
     *      - matching skills
     *      - distance between the professor and the company
     *
     * @param array $data
     * @param int $id
     * @return bool Success
     */
    public function run(array $data, $id)
    {
        Configure::load('traineeships');
        $config = Configure::readOrFail('Traineeships');

        $announcesTable = TableRegistry::get('Announces');
        $usersTable = TableRegistry::get('Users');
        $candidaciesTable = TableRegistry::get('Candidacies');


        $traineeships = $candidaciesTable->Traineeships->find()
            ->where([
                'Traineeships.student_id = Candidacies.student_id',
                'Traineeships.announce_id = Candidacies.announce_id',
            ])
            ->select(['announce_id', 'student_id']);

        $candidaciesNotMatched = $candidaciesTable->find()
            ->where([
                'Candidacies.status' => CandidaciesTable::status['validated'],
                function ($q) use ($traineeships) {
                    return $q->notExists($traineeships);
                }
            ])
            ->select(['announce_id', 'student_id']);

        if($candidaciesNotMatched->isEmpty()) {
            $this->out('No candidacy to handle.');
            return true;
        }

        /*
         * Get all the Announces with Candidacies Validated with no associating Traineeship
         */
        $announces = $announcesTable->find()
            ->where([
                'Announces.id IN' => $candidaciesNotMatched->extract('announce_id')->toArray()
            ])
            ->contain([
                'Skills' => function ($q) {
                    return $q->select(['id']);
                },
                'Users' => function ($q) {
                    return $q->select([
                        'city',
                        'postal_code',
                        'street',
                        'street_number',
                        'street_number_complement'
                    ]);
                },
                // candidacies not matched only
                'Candidacies' => function ($q) use ($traineeships) {
                    return $q->where([
                        'Candidacies.status' => CandidaciesTable::status['validated'],
                        function ($q) use ($traineeships) {
                            return $q->notExists($traineeships);
                        }
                    ])
                        ->select(['Candidacies.announce_id', 'Candidacies.student_id']);
                },

            ])
            ->select(['id', 'user_id', 'title']);

        $data = [];
        $index = 0;
        $totalRecords = $announces->count();

        foreach ($announces as $i => $announce) {
            // select the professors that match with the announce
            $professorsWithSkillsMatching = $usersTable->find()
                ->where([
                    'role IN' => [UsersTable::roles['admin'], UsersTable::roles['professor']],
                ])
                ->matching('Skills', function ($q) use ($announce) {
                    return $q->where(['Skills.id IN' => (new Collection($announce->skills))->extract('id')->toArray()]);
                })
                // with no traineeships
                ->notMatching('TraineeshipsProfessor', function ($q) {
                    return $q->where([
                        'TraineeshipsProfessor.ending >' => (new Time())->format('Y-m-d'),
                    ]);
                })
                ->select([
                    'id',
                ]);

            $professors = $usersTable->find()
                ->where(['role IN' => [UsersTable::roles['admin'], UsersTable::roles['professor']]])
                ->select([
                    'id',
                    'city',
                    'postal_code',
                    'street',
                    'street_number',
                    'street_number_complement'
                ]);

            if (!$professorsWithSkillsMatching->isEmpty()) {
                $professors = $professors->where(['id IN' => $professorsWithSkillsMatching->extract('id')->toArray()]);
            }

            if ($professors->isEmpty()) {
                $this->err('No available professors.');
                return false;
            }

            // store the destinations
            $origins = [];
            $destinations = [new AddressLocation(
                $announce->user->street_number . ' ' .
                $announce->user->street . ', ' .
                $announce->user->postal_code . ' ' .
                $announce->user->city
            )];
            // House Number, Street Direction, Street Name, Street Suffix, City, State, Zip, Country
            foreach ($professors as $professor) {
                array_push($origins, new AddressLocation(
                    $professor->street_number . ' ' .
                    $professor->street . ', ' .
                    $professor->postal_code . ' ' .
                    $professor->city
                ));
            }

            $distanceMatrix = new DistanceMatrixService(new Client(), new GuzzleMessageFactory());

            $request = new DistanceMatrixRequest($origins, $destinations);
            // configure the options
            $request->setTravelMode(TravelMode::DRIVING);
            $request->setUnitSystem(UnitSystem::METRIC);
            $request->setRegion('fr');

            $response = $distanceMatrix->process($request);

            // handle the response
            // compute the matching professors based on the distance
            $indexR = 0;
            if ($response->getStatus() != DistanceMatrixStatus::OK) {
                $this->err('Error when requesting the Google API ' . $response->getStatus());
                return false;
            }

            $closestProfessor = [
                'id' => null,
                'distance' => 99999999999999999999999999,
            ];
            foreach ($response->getRows() as $row) {
                // each row match with a professor
                // first element of the row
                $element = $row->getElements()[0];

                if ($element->getStatus() == DistanceMatrixElementStatus::OK &&
                    $element->getDistance()->getValue() < $closestProfessor['distance']
                ) {
                    $closestProfessor = [
                        'id' => $professors->toArray()[$indexR]['id'],
                        'distance' => $element->getDistance()->getValue()
                    ];
                }
                $indexR += 1;
            }

            // if no match, first professor is assigned
            if ($closestProfessor['id'] == null) {
                $closestProfessor['id'] = $professors->first()->id;
            }

            // notify the professor
            $this->getMailer('Traineeship')->send('traineeshipCreated', [$closestProfessor['id'], $announce]);

            // assign the closest professor to the Candidacies related to the Announce
            foreach ($announce->candidacies as $candidacy) {
                $data[$index] = [
                    'beginning' => $config['beginning'],
                    'ending' => (new Time($config['beginning'] . ' 00:00:00'))->add(DateInterval::createFromDateString($config['duration']))->format('Y-m-d'),
                    'announce_id' => $announce->id,
                    'student_id' => $candidacy->student_id,
                    'professor_id' => $closestProfessor['id'],
                ];

                // notify the student
                $this->getMailer('Traineeship')->send('traineeshipCreated', [$candidacy->student_id, $announce]);

                $index += 1;
            }
            $this->QueuedJobs->updateProgress($id, ($i + 1) / $totalRecords);
        }

        $traineeshipsTable = TableRegistry::get('Traineeships');
        $traineeships = $traineeshipsTable->newEntities($data);

        return $traineeshipsTable->saveMany($traineeships);
    }
}