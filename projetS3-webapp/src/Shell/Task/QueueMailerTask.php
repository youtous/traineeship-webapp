<?php
/**
 * Created by PhpStorm.
 * User: youtous
 * Date: 11/01/2017
 * Time: 23:14
 */

namespace App\Shell\Task;


use Cake\Mailer\MailerAwareTrait;
use Queue\Shell\Task\QueueTask;

class QueueMailerTask extends QueueTask
{

    use MailerAwareTrait;

    /**
     * List of default variables for Mailer
     *
     * @var array
     */
    public $defaults = [
        'mailer' => null,
        'data' => null,
    ];

    /**
     * "Add" the task, not possible for QueueEmailTask
     *
     * @return void
     */
    public function add() {
        $this->err('Queue Mailer Task cannot be added via Console.');
        $this->out('This method is not yet implemented.');
    }

    /**
     * Run method called by the worker.
     *
     * @param mixed $data Job data
     * @param int|null $id The id of the QueuedTask
     * @return bool Success
     */
    public function run(array $data, $id) {
        if (!isset($data['mailer'])) {
            $this->err('Queue Mailer task called without mailer specified.');
            return false;
        }

        $mailer = $data['mailer'];

        return (bool) $this->getMailer($mailer['name'])->send($mailer['method'], $data['data']);
    }
}