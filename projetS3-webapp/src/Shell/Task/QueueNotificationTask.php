<?php

namespace App\Shell\Task;

use App\Event\NotificationManager;
use Queue\Shell\Task\QueueTask;

/**
 * Notification task
 * @author youtous
 */
class QueueNotificationTask extends QueueTask {

	/**
	 * List of default variables for Notification
	 *
	 * @var array
	 */
	public $defaults = [
		'method' => null,
		'args' => null,
	];

	/**
	 * "Add" the task, not possible for QueueEmailTask
	 *
	 * @return void
	 */
	public function add() {
		$this->err('Queue Mailer Task cannot be added via Console.');
		$this->out('This method is not yet implemented.');
	}

	/**
     * Run method called by the worker.
     *
	 * @param mixed $data Job data
	 * @param int|null $id The id of the QueuedTask
	 * @return bool Success
	 */
	public function run(array $data, $id) {
		if (!isset($data['method']) && is_null($data['method'])) {
			$this->err('Queue Notification task called without method specified.');
			return false;
		}

		$return = call_user_func_array([new NotificationManager(),  $data['method']], $data['args']);
        return (bool) $return;
	}
}
