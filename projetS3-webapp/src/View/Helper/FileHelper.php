<?php
namespace App\View\Helper;

use App\Model\Entity\File;
use Cake\View\Helper;
use Cake\View\StringTemplateTrait;
use Cake\View\View;

/**
 * File helper
 */
class FileHelper extends Helper
{
    use StringTemplateTrait;

    public $helpers = ['Html', 'Url'];

    /**
     * Default config for this class
     *
     * @var array
     */
    protected $_defaultConfig = [
        'download' => 'télécharger',
        'previewTypes' => ['image', 'video', 'audio'],
        'templates' => [
            'thumbnail' => '<div class="thumbnail">{{preview}}{{caption}}</div>',
            'caption' => '<div class="caption">{{description}}</div>',
            'defaultPreview' => '<p class="text-center txt-grey">{{description}}<br><i class="fa {{icon}} fa-4x" aria-hidden="true"></i></p>',
            'downloadIcon' => '<i class="fa fa-cloud-download" aria-hidden="true"></i>',
            'downloadButton' => '<a href="{{url}}" class="btn btn-success btn-xs btn-block btn-no-margin btn-no-padding-top btn-simple" download="{{fileName}}">{{downloadIcon}} {{content}}</a>',
        ]
    ];

    public function thumbnail(File $file, $options = [])
    {
        $fullBase = false;
        $showName = false;
        if (isset($options['fullBase'])) {
            $fullBase = (boolean)$options['fullBase'];
        }
        if (isset($options['showName'])) {
            $showName = (boolean)$options['showName'];
        }

        $templater = $this->templater();
        $fileURL = $this->Url->build(['_name' => 'viewFile', 'id' => $file->id]);

        if (in_array($file->type, $this->config('previewTypes'))) {
            switch ($file->type) {
                case 'image' :
                    $preview = $this->Html->image($fileURL, ['alt' => $file->id, 'url' => $fileURL, 'fullBase' => $fullBase]);
                    break;
                default:
                    $preview = $this->Html->media($fileURL, [
                        'fullBase' => $fullBase,
                        'text' => $file->id
                    ]);
                    break;
            }
        } else {
            switch ($file->extension) {
                case 'pdf' :
                    $preview = $templater->format('defaultPreview', ['icon' => 'fa-file-pdf-o', 'description' => $showName ? h($file->name) : 'Fichier PDF']);
                    break;
                case 'txt':
                    $preview = $templater->format('defaultPreview', ['icon' => 'fa-file-text-o', 'description' => $showName ? h($file->name) : 'Fichier texte']);
                    break;
                default:
                    $preview = $templater->format('defaultPreview', ['icon' => 'fa-file-o', 'description' => $showName ? h($file->name) : 'Fichier']);
                    break;
            }
        }

        $button = $templater->format('downloadButton', [
            'url' => $fileURL,
            'downloadIcon' => $templater->format('downloadIcon', []),
            'content' => $this->config('download'),
            'fileName' => $file->id . '.' . $file->extension,
        ]);

        $caption = $templater->format('caption', ['description' => $button]);

        return $templater->format('thumbnail', ['caption' => $caption, 'preview' => $preview]);
    }

}
