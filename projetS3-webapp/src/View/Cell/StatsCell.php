<?php
namespace App\View\Cell;

use App\Model\Table\AnnouncesTable;
use App\Model\Table\CandidaciesTable;
use Cake\I18n\Time;
use Cake\View\Cell;

/**
 * Admin/Stats cell
 */
class StatsCell extends Cell
{

    /**
     * List of valid options that can be passed into this
     * cell's constructor.
     *
     * @var array
     */
    protected $_validCellOptions = [];

    /**
     * Default display method.
     *
     * @return void
     */
    public function display()
    {
        $this->loadModel('Messages');
        $this->loadModel('Announces');
        $this->loadModel('Users');
        $this->loadModel('Files');
        $this->loadModel('Traineeships');
        $this->loadModel('Candidacies');

        $messagesCount = $this->Messages->find()->count();
        $usersCount = $this->Users->find()->count();
        $announcesCount = $this->Announces->find()->count();
        $filesCount = $this->Files->find()->count();
        $candidaciesCount = $this->Candidacies->find()->where(['status IN' => [CandidaciesTable::status['pending'], CandidaciesTable::status['accepted']]])->count();
        $traineeshipsCount = $this->Traineeships->find()->where(['ending >=' =>  Time::now()->format('Y-m-d')])->count();

        $this->set(compact('filesCount', 'messagesCount', 'usersCount', 'announcesCount', 'candidaciesCount', 'traineeshipsCount'));
    }

    /**
     * How many Announces are not yet validated ?
     */
    public function announcesWaiting()
    {
        $this->loadModel('Announces');

        $count = $this->Announces->findByStatus(AnnouncesTable::status['waiting'])
            ->count();

        $this->set(compact('count'));
    }
}
