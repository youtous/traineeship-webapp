<?php
namespace App\View\Cell;

use App\Model\Table\UsersTable;
use Cake\View\Cell;

/**
 * Messages cell
 */
class MessagesCell extends Cell
{

    /**
     * List of valid options that can be passed into this
     * cell's constructor.
     *
     * @var array
     */
    protected $_validCellOptions = [];

    /**
     * Number of not seen conversations.
     *
     * @param $user_id - the related user
     * @return void
     */
    public function notSeenConversations($user_id)
    {
        $this->loadModel('Messages');
        $count = $this->Messages->countNotSeenConversations($user_id);

        $this->set(compact('count'));
    }

    /**
     * Generate a report link to the last connect administrator.
     *
     * @param null $class - class added to the button
     * @return void
     */
    public function report($class = null)
    {
        $this->loadModel('Users');
        // select the last connected admin
        $admin = $this->Users->findByRole(UsersTable::roles['admin'])
            ->select(['id'])
            ->order(['last_login' => 'DESC'])
            ->firstOrFail();

        $this->set(compact('admin', 'class'));
    }
}
