<?php
namespace App\View\Cell;

use App\Model\Table\CandidaciesTable;
use Cake\View\Cell;

/**
 * Candidacies cell
 */
class CandidaciesCell extends Cell
{

    /**
     * List of valid options that can be passed into this
     * cell's constructor.
     *
     * @var array
     */
    protected $_validCellOptions = [];

    /**
     * Number of accepted candidacies for an user.
     * These Candidacies are waiting for an answer.
     *
     * @param $user_id - the related user
     * @return void
     */
    public function acceptedCandidacies($user_id)
    {
        $this->loadModel('Candidacies');
        $count = $this->Candidacies->findByStudentIdAndStatus(
            $user_id,
            CandidaciesTable::status['accepted']
        )->count();

        $this->set(compact('count'));
    }

    /**
     * Number of submitted and waiting candidacies for an user.
     *
     * @param $user_id - the author of the announces
     * @return void
     */
    public function waitingCandidacies($user_id)
    {
        $this->loadModel('Announces');
        $announces = $this->Announces->findByUserId($user_id)->select(['id']);

        $this->loadModel('Candidacies');
        $count = $this->Candidacies->findByStatus(CandidaciesTable::status['pending'])
            ->where(['announce_id IN' => $announces])
            ->count();

        $this->set(compact('count'));
    }
}
