document.addEventListener("DOMContentLoaded", function () {
    /**
     * Email check
     */
        // functions used for checking
    var divEmail = document.getElementById('divMailSuccess')
    var divLastName = document.getElementById('lastname').parentNode.parentNode
    var divFirstName = document.getElementById('firstname').parentNode.parentNode

    var title = document.getElementById('title')


    if (title.value.length === 0) {
        hideCompanyInput( divLastName, divFirstName, title)
    }

    var notifyAccountType = function (message) {
        var spanEmail = document.getElementById('spanMailSuccess')
        if (divEmail.classList.contains("hidden")) {
            divEmail.classList.remove("hidden")
        }
        spanEmail.innerHTML = message
    }

    // begin checking input
    var type = 'N'
    var checkEmail = function (element) {
        var regexTeacher = /^[a-z0-9._-]+@univ-reims.fr$/
        var regexStudent = /^[a-z0-9._-]+@etudiant.univ-reims.fr$/
        var regexMail = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/
        if (regexMail.test(element.value.toLowerCase())) {
            setCrossCheck(element, true)
            if (regexTeacher.test(element.value.toLowerCase())) {
                type = 'T'
            } else if (regexStudent.test(element.value.toLowerCase())) {
                type = 'S'
            } else {
                type = 'C'
            }
            return true;
        } else {
            type = 'N'
            setCrossCheck(element, false)
            return false;
        }
    }
    var emailInput = document.getElementById('email')
    emailInput.checkValidity = function () {
        return checkEmail(emailInput)
    }
    emailInput.addEventListener("blur", function () {
        switch (type) {
            case 'T' :
                hideCompanyInput( divLastName, divFirstName, title)
                notifyAccountType("Vous êtes un professeur !")
                break;
            case 'C' :
                showCompanyInput( divLastName, divFirstName, title)
                notifyAccountType("Vous êtes une entreprise !")
                break;
            case 'S' :
                hideCompanyInput( divLastName, divFirstName, title)
                notifyAccountType("Vous êtes un étudiant !")
                break;
            default :
                hideCompanyInput( divLastName, divFirstName, title)
                if (!divEmail.classList.contains("hidden")) {
                    divEmail.classList.add("hidden")
                }
                break;
        }
    })
    /**
     * end of Email check
     */


    /**
     * Password check
     */
    var password1 = document.getElementById('password')
    var password2 = document.getElementById('passwordConfirmation')
    password1.checkValidity = function () {
        return checkPasswordsEquals(password1, password2)
    }
    password2.checkValidity = function () {
        return checkPasswordsEquals(password2, password1)
    }
    /**
     * end of Password check
     */

    /**
     * Firstname check
     */
    var firstName = document.getElementById('firstname')
    firstName.checkValidity = function () {
        return checkLength(firstName)
    }
    /**
     * end of Firstname check
     */

    /**
     * Lastname check
     */
    var lastName = document.getElementById('lastname')
    lastName.checkValidity = function () {
        return checkLength(lastName)
    }
    /**
     * end of Lastname check
     */

    /**
     * Title check
     */
    title.checkValidity = function () {
        return checkLength(title)
    }
    /**
     * end of Title check
     */

    /**
     * Postalcode check
     */
    var postalCode = document.getElementById('postal-code')
    postalCode.checkValidity = function () {
        return checkPostalCode(postalCode)
    }
    /**
     * end of Postalcode check
     */

    /**
     * City check
     */
    var city = document.getElementById('city')
    city.checkValidity = function () {
        return checkLength(city)
    }
    /**
     * end of City check
     */

    /**
     * StreetNumber check
     */
    var streetNumber = document.getElementById('street-number')
    streetNumber.checkValidity = function () {
        return checkNaturalNumber(streetNumber)
    }
    /**
     * end of StreetNumber check
     */

    /**
     * Street check
     */
    var street = document.getElementById('street')
    street.checkValidity = function () {
        return checkLength(street)
    }
    /**
     * end of Street check
     */

    /**
     * Phone check
     */
    var phone = document.getElementById('phone')
    phone.checkValidity = function () {
        return checkPhone(phone)
    }
    /**
     * end of Phone check
     */

    /**
     * Website check
     */
    var website = document.getElementById('website')
    website.checkValidity = function () {
        return checkWebsite(website)
    }
    /**
     * end of Website check
     */

        // perform a checking if the form is already filled
    var form = document.getElementById('register')
    checkForm(form)
    form.addEventListener('submit', function (event) {
        if(!password1.checkValidity()) {
            event.preventDefault()
            setHelpMessage(password1, "Les mots de passe doivent concorder et avoir une taille de 6 à 100 caractères.")
            return false;
        }
        return true;
    })
})
