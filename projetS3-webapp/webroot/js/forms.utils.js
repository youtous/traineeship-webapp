/**
 * Created by youtous on 02/12/2016.
 */
var setSuccess = function (element, success) {
    // see https://github.com/FezVrasta/bootstrap-material-design/blob/master/scripts/material.js#L210
    if (success) {
        if (!element.parentNode.classList.contains("has-success")) {
            element.parentNode.classList.add("has-success")
        }
    } else {
        element.parentNode.classList.remove("has-success")
    }
}

var updateSuccess = function (element, success) {
    if (success) {
        element.parentNode.classList.remove("has-error")
        if (!element.parentNode.classList.contains("has-success")) {
            element.parentNode.classList.add("has-success")
        }
    } else {
        element.parentNode.classList.remove("has-success")
        if (!element.parentNode.classList.contains("has-error")) {
            element.parentNode.classList.add("has-error")
        }
    }
}

var resetSuccess = function (element) {
    element.parentNode.classList.remove("has-error")
    element.parentNode.classList.remove("has-success")
}

var setCrossCheck = function (element, valid) {
    var exists = false
    var checkCross
    if (element.nextSibling.classList.contains('form-control-feedback')) {
        exists = true
        checkCross = element.nextSibling
    } else {
        checkCross = document.createElement('span')
        checkCross.classList.add('form-control-feedback')
    }
    setSuccess(element, valid)
    if (valid) {
        checkCross.innerHTML = '<i class="material-icons">done</i>'
    } else {
        checkCross.innerHTML = '<i class="material-icons">clear</i>'
    }
    if (!exists) {
        element.parentNode.insertBefore(checkCross, element.nextSibling)
    }
}
var showCompanyInput = function (lastnameElement, firstnameElement, title) {
    //LastName
    lastnameElement.classList.remove("col-md-6")
    lastnameElement.classList.add("col-md-4")
    //FirstName
    firstnameElement.classList.remove("col-md-6")
    firstnameElement.classList.add("col-md-4")
    //Company
    title.parentNode.classList.remove("hidden")
    title.required = true
}

var hideCompanyInput = function (lastnameElement, firstnameElement, title) {
    //LastName
    lastnameElement.classList.add("col-md-6")
    lastnameElement.classList.remove("col-md-4")
    //FirstName
    firstnameElement.classList.remove("col-md-4")
    firstnameElement.classList.add("col-md-6")
    //Company
    title.parentNode.classList.add("hidden")
    resetSuccess(title)

    // reset value for form submit
    title.value = ''
    title.required = false
}

/**
 * Utils
 */

/**
 * Count number of check checkboxes
 * @param container - the container of the checkboxes
 * @returns {number}
 */
var countChecked = function (container) {
    var checkboxes = container.querySelectorAll('input[type="checkbox"]')
    var counter = 0
    for (var i = 0; i < checkboxes.length; ++i) {
        if (checkboxes[i].checked) {
            counter += 1
        }
    }
    return counter
}

/**
 * Révèle les éléments avec erreur du formulaire
 * @param form
 */
var checkForm = function (form) {
    var inputs = form.querySelectorAll('input.has-error')
    for (var i = 0; i < inputs.length; ++i) {
        setCrossCheck(inputs[i], false)
    }
}

var setHelpMessage = function (element, message) {
    var parent = element.parentNode
    var helpBlock = parent.querySelector('span.help-block.error-message')
    if (helpBlock === null) {
        helpBlock = document.createElement('span')
        helpBlock.classList.add('help-block')
        helpBlock.classList.add('error-message')
        parent.appendChild(helpBlock)
    }
    helpBlock.innerHTML = message
}

/**
 * Check functions
 */

var checkPasswordsEquals = function (password1, password2) {
    if (password1.value.length === 0 && !password1.required && password2.value.length === 0 && !password2.required) {
        resetSuccess(password2)
        return true;
    }
    var equals = (password1.value === password2.value &&
    password1.value.length >= 6 && password1.value.length <= 100 &&
    password2.value.length >= 6 && password2.value.length <= 100)

    setCrossCheck(password1, equals)
    setCrossCheck(password2, equals)
    updateSuccess(password2, equals)
    return equals
}

var _checkLength = function (element, minLength, maxLength) {
    if (element.value.length === 0 && !element.required) {
        return true;
    }
    var eval = (element.value.length <= maxLength && element.value.length > minLength)
    setCrossCheck(element, eval)
    return eval
}
var checkLength = function (element) {
    var maxLength = 255
    var minLength = 0
    if (element.hasAttribute('maxLength')) {
        maxLength = element.maxLength
    }
    if (element.hasAttribute('minLength')) {
        minLength = element.minLength
    }
    return _checkLength(element, minLength, maxLength)
}

var checkPhone = function (element) {
    if (element.value.length === 0 && !element.required) {
        return true;
    }
    var regex = /^0[1-9]{1}(([0-9]{8})|((\s[0-9]{2}){4})|((-[0-9]{2}){4})|((\.[0-9]{2}){4}))$/
    var eval = regex.test(element.value)
    setCrossCheck(element, eval)
    return eval
}

var checkPostalCode = function (element) {
    if (element.value.length === 0 && !element.required) {
        return true;
    }
    var regex = /^\d{5}$/
    var eval = regex.test(element.value) && element.value >= 1001 && element.value <= 99138
    setCrossCheck(element, eval)
    return eval
}

var checkWebsite = function (element) {
    if (element.value.length === 0 && !element.required) {
        return true;
    }
    var regex = /^(http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?$/
    var eval = regex.test(element.value)
    setCrossCheck(element, eval)
    return eval
}

var checkNaturalNumber = function (element) {
    if (element.value.length === 0 && !element.required) {
        return true;
    }
    var eval = element.value > 0 && element.value <= 99999
    setCrossCheck(element, eval)
    return eval
}

var checkEmailRegex = function (element) {
    if (element.value.length === 0 && !element.required) {
        return true;
    }
    var regexMail = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/
    var eval = regexMail.test(element.value.toLowerCase())
    setCrossCheck(element, eval)
    return eval
}