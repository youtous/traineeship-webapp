/**
 * Created by youtous on 25/12/2016.
 */

/**
 * Update the badge associated to an element (parent).
 *
 * @param parent - the parent of the badge
 * @param int value - the new value of the badge
 */
var updateBadge = function (parent, value) {
    var badge = parent.getElementsByClassName('badge')
    if (badge.length > 0) {
        badge = badge[0]
    } else {
        badge = false;
    }

    if (value === 0) {
        if (badge !== false) {
            badge.parentNode.removeChild(badge)
        }
    } else {
        if (badge === false) {
            parent.innerHTML += '<span class="badge badge-red badge-top-counter">' + value + '</span >'
        } else {
            badge.innerHTML = value
        }
    }
}

/**
 * Update the counter of the title of the page
 *
 * @param int value the new value of the counter
 */
var updateTitleCount = function (value) {
    var title = document.getElementsByTagName("title")[0]

    const regex = /^(?:\(\d*\)\s)(.*)$/g;
    var titleString = title.innerHTML.trim().replace(regex, '$1')

    if (value === 0) {
        title.innerHTML = titleString
    } else {
        title.innerHTML = '(' + value + ') ' + titleString
    }
}


/**
 * AJAX loop that check if there is unread conversations.
 * Update the title and the badge.
 */
var checkNewConversations = function () {
    var badge = document.getElementById('messagesIconNavbar')

    var reqParams = {
        method: "GET",
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Cache': 'no-cache'
        },
        credentials: 'include',
        redirect: 'error'
    }

    fetch('/api/messages/stats/notSeenConversations.json', reqParams)
        .then(function (response) {
            if (response.ok) {
                response.json().then(function (json) {
                    updateBadge(badge, json.count)
                    updateTitleCount(json.count)
                })
                setTimeout(checkNewConversations, 10 * 1000)
            }
        })
}

checkNewConversations()

