/**
 * Created by youtous on 30/11/2016.
 */
document.addEventListener("DOMContentLoaded", function (event) {
    var searchInput = document.getElementById('search-text')
    searchInput.setAttribute('style', 'width: 0 !important');
    var searchButton = document.getElementById('search-button')
    var update = function() {
        if (searchInput.value.length === 0 && searchInput !== document.activeElement) {
            searchInput.setAttribute('style', 'width: 0 !important')
        }
    }
    searchButton.addEventListener('mouseenter', function () {
        searchInput.setAttribute('style', 'width: 100% !important')
    })
    searchButton.addEventListener('mouseleave', function () {
        update()
    })
    searchInput.addEventListener('blur', function () {
        update()
    })
});
