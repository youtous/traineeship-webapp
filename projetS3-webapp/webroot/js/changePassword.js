/**
 * Created by youtous on 03/12/2016.
 */
document.addEventListener("DOMContentLoaded", function () {

    var password1 = document.getElementById('password')
    var password2 = document.getElementById('passwordConfirmation')
    password1.checkValidity = function () {
        return checkPasswordsEquals(password1, password2)
    }
    password2.checkValidity = function () {
        return checkPasswordsEquals(password2, password1)
    }

    // perform a checking if the form is already filled
    var form = document.getElementById('change-password')
    form.addEventListener('submit', function (event) {
        if(!password1.checkValidity()) {
            event.preventDefault()
            setHelpMessage(password1, "Les mots de passe doivent concorder et avoir une taille de 6 à 100 caractères.")
            return false;
        }
        return true;
    })
})