/**
 * Created by youtous on 26/12/2016.
 */
document.addEventListener("DOMContentLoaded", function () {

    /**
     * Title check
     */
    var title = document.getElementById('title')
    title.checkValidity = function () {
        return checkLength(title)
    }

    /**
     * Skills check
     * Between 1 and 4 selected
     */
    var containerCheckboxes = document.getElementById('collapseSkills')
    containerCheckboxes.checkValidity = function () {
        var countCheckedF = countChecked(containerCheckboxes)
        var eval = countCheckedF <= 4 && countCheckedF >= 1

        updateSuccess(containerCheckboxes, eval)
        return eval
    }
    containerCheckboxes.addEventListener('click', function (event) {
        containerCheckboxes.checkValidity()
    })

    var form = document.getElementById('postAnnouceForm')
    checkForm(form)
    form.addEventListener('submit', function (event) {
            if (!containerCheckboxes.checkValidity()) {
                document.getElementById('help-message').innerHTML = 'Veuillez vérifier Les compétences associées à l’annonce.'
                event.preventDefault()
                return false;
            }
            return true;
        }
    )

})