var parent = document.getElementById('buttonAddFile').parentNode
var nodeListInputs = parent.querySelectorAll('input[type="file"]')
var buttonAdd = document.getElementById('buttonAddFile')

/**
 * si on click sur rajout d'une pièce joint alors on appele la méthode addFileInput
 */
document.getElementById("files-0").addEventListener("change", function () {
    handleFileInput(this)
});
document.getElementById("files-1").addEventListener("change", function () {
    handleFileInput(this)
});
document.getElementById("files-2").addEventListener("change", function () {
    handleFileInput(this)
});
document.getElementById("files-3").addEventListener("change", function () {
    handleFileInput(this)
});

var preventClick = function (event) {
    event.preventDefault()
}

/**
 * For the specified element, handle the file
 */
var handleFileInput = function (element) {
    displayNameFile(element)

    var inputFile = firstAvailableInputFile(nodeListInputs)
    if (inputFile && nodeListInputs.length <= 4) {
        updateLabelTarget(inputFile)
    } else {
        buttonAdd.className += ' disabled'
        buttonAdd.addEventListener('click', preventClick)
    }
};

/**
 * Change the target of the label for the next available input file
 */
var updateLabelTarget = function (fileElement) {
    buttonAdd.setAttribute("for", fileElement.id)
}
/**
 * Affichage du nom du document joint
 * @param fileElement
 */
var displayNameFile = function (fileElement) {

    var button = document.createElement('button')
    button.classList.add('btn')
    button.classList.add('btn-default')
    button.classList.add('btn-simple')
    button.type = 'button'
    button.id = 'reset-' + fileElement.id
    button.value = 'reset'
    button.innerHTML = '<i class="fa fa-trash-o" aria-hidden="true"></i> ' + fileElement.files[0].name

    document.getElementById('infoAddFile').appendChild(button)

    button.addEventListener("click", function () {
        // reset the fileElement and remove the associated button
        fileElement.value = ""
        this.parentNode.removeChild(this)

        // update the target label (free file)
        updateLabelTarget(fileElement)

        // release the button
        if(buttonAdd.classList.contains('disabled')) {
            buttonAdd.classList.remove('disabled')

            buttonAdd.removeEventListener('click',
                preventClick,
                false
            );
        }
    });

};

/**
 * Get the first available input file.
 *
 * @param nodeList (parent.querySelector("file")
 * @return the available element ou false if there is no available element
 */
var firstAvailableInputFile = function (nodeList) {
    for (var i = 0; i < nodeList.length; i++) {
        if (nodeList[i].files.length == 0) {
            return nodeList[i];
        }
    }
    return false;
}

