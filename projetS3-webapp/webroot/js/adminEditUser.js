/**
 * Created by youtous on 03/12/2016.
 */
document.addEventListener("DOMContentLoaded", function () {
    /**
     * Email check
     */
    var divLastName = document.getElementById('lastname').parentNode.parentNode
    var divFirstName = document.getElementById('firstname').parentNode.parentNode
    var title = document.getElementById('title')
    var emailInput = document.getElementById('email')

    if (title.value.length === 0) {
        hideCompanyInput( divLastName, divFirstName, title)
    }
    emailInput.checkValidity = function () {
        return checkEmailRegex(emailInput)
    }
    /**
     * end of Email check
     */

    /**
     * Role check
     */
    var roles = document.getElementById('roles')
    roles.addEventListener("change", function () {
        var type = document.getElementById('roles').options[roles.selectedIndex].value
        switch (type) {
            case 'T' :
                hideCompanyInput( divLastName, divFirstName, title)
                break;
            case 'C' :
                showCompanyInput( divLastName, divFirstName, title)
                break;
            case 'S' :
                hideCompanyInput( divLastName, divFirstName, title)
                break;
            default :
                hideCompanyInput( divLastName, divFirstName, title)
                break;
        }
    })
    /**
     * end of Role check
     */


    /**
     * Firstname check
     */
    var firstName = document.getElementById('firstname')
    firstName.checkValidity = function () {
        return checkLength(firstName)
    }
    /**
     * end of Firstname check
     */

    /**
     * Lastname check
     */
    var lastName = document.getElementById('lastname')
    lastName.checkValidity = function () {
        return checkLength(lastName)
    }
    /**
     * end of Lastname check
     */

    /**
     * Title check
     */
    if(title !== null) {
        title.checkValidity = function () {
            return checkLength(title)
        }
    }
    /**
     * end of Title check
     */

    /**
     * Postalcode check
     */
    var postalCode = document.getElementById('postal-code')
    postalCode.checkValidity = function () {
        return checkPostalCode(postalCode)
    }
    /**
     * end of Postalcode check
     */

    /**
     * City check
     */
    var city = document.getElementById('city')
    city.checkValidity = function () {
        return checkLength(city)
    }
    /**
     * end of City check
     */

    /**
     * StreetNumber check
     */
    var streetNumber = document.getElementById('street-number')
    streetNumber.checkValidity = function () {
        return checkNaturalNumber(streetNumber)
    }
    /**
     * end of StreetNumber check
     */

    /**
     * Street check
     */
    var street = document.getElementById('street')
    street.checkValidity = function () {
        return checkLength(street)
    }
    /**
     * end of Street check
     */

    /**
     * Phone check
     */
    var phone = document.getElementById('phone')
    phone.checkValidity = function () {
        return checkPhone(phone)
    }
    /**
     * end of Phone check
     */

    /**
     * Website check
     */
    var website = document.getElementById('website')
    website.checkValidity = function () {
        return checkWebsite(website)
    }
    /**
     * end of Website check
     */

    var form = document.getElementById('profile')
    checkForm(form)


    /**
     * Password check
     */
    var password1 = document.getElementById('password')
    var password2 = document.getElementById('passwordConfirmation')
    password1.checkValidity = function () {
        return checkPasswordsEquals(password1, password2)
    }
    password2.checkValidity = function () {
        return checkPasswordsEquals(password2, password1)
    }
    /**
     * end of Password check
     */

    var formPassword = document.getElementById('password-update')
    checkForm(formPassword)
    formPassword.addEventListener('submit', function (event) {
        if(!password1.checkValidity()) {
            event.preventDefault()
            setHelpMessage(password1, "Les mots de passe doivent concorder et avoir une taille de 6 à 100 caractères.")
            return false;
        }
        return true;
    })
})