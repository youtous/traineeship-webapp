/*
 * Code fonctionnant mais pas aussi efficace qu'un cancel() --> car latence dû au setTimeout.
 * Trouver une technique sans aucune latence ?
 */

var inputName = document.getElementById('name');
var dateGlobal = new Date().getTime();
var nameGlobal = inputName.value;
var listConversation = document.getElementById('listConversation');
const MAX_RES = 5;
var loadDiv = document.getElementById('loadDiv');

/**
 * AJAX loop that search users from an input string.
 * Show or update the clicked list.
 *
 * Check if this request is the latest, if not, cancel.
 */
var searchConversations = function () {
    var time = new Date().getTime();
    var name = inputName.value;
    if (time >= dateGlobal && name != nameGlobal) {
        nameGlobal = inputName.value;
        var reqParams = {
            method: "GET",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Cache': 'no-cache'
            },
            credentials: 'include',
            redirect: 'error'
        }

        var url = new URL('/api/users/search.json', location.origin),
            params = {name: name}
            Object.keys(params).forEach(function (key) {
                url.searchParams.append(key, params[key])
            })

        fetch(url, reqParams)
            .then(function (response) {
                if (response.ok) {
                    response.json().then(function (json) {
                        modifyConversations(json);
                    })
                }
            })
    }
}

/**
 * Show or remove the loading icon
 *
 * @param bool
 */
var loading = function (bool) {
    if (bool == true) {
        loadDiv.innerHTML = "<i class='fa fa-spinner fa-spin' aria-hidden='true'></i>";
    } else {
        loadDiv.innerHTML = "";
    }
}

/**
 * Populate the Users list based on the json object.
 *
 * @param json
 */
var modifyConversations = function (json) {
    //Modify listConversation
    listConversation.innerHTML = "";
    if (json.users.length != 0) {
        var taille = json.users.length;
        if (taille > MAX_RES) {
            taille = MAX_RES;
        }
        for (var i = 0; i < taille; i++) {
            listConversation.innerHTML += "<a href='/messages/conversations/" + json.users[i].id + "' class='list-group-item'>" + json.users[i].name + "</a>";
        }

        if (taille + 1 == MAX_RES) {
            listConversation.innerHTML += "<a href='/users/search?name=" + json.name + "' class='list-group-item'><i class='fa fa-search' aria-hidden='true'></i> Plus de résultats..</a>";
        }
    } else {
        listConversation.innerHTML = "<a href='#' class='list-group-item'>" + "Aucun résultat" + "</a>";
    }
    loading(false);
}


/**
 * Function load on keyup
 * Call the function searchConversations after 0.7 seconds
 */
inputName.addEventListener('input', function () {
    dateGlobal = new Date().getTime();
    setTimeout(function () {
        searchConversations()
    }, 700);
    loading(true);
});

/**
 * Reset the list
 */
inputName.addEventListener('blur', function () {
    listConversation.innerHTML = "";
});