/**
 * Created by youtous on 13/12/2016.
 */
document.addEventListener('DOMContentLoaded', function () {
    var topButtonShake = document.getElementById('shake-button-top')
    topButtonShake.parentNode.addEventListener('mouseover', function () {
        topButtonShake.classList.add('shake-constant')
    })
    topButtonShake.parentNode.addEventListener('mouseleave', function () {
        topButtonShake.classList.remove('shake-constant')
    })
})