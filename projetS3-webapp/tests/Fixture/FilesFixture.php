<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * FilesFixture
 *
 */
class FilesFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'uuid', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        'mime' => ['type' => 'string', 'fixed' => true, 'length' => 5, 'null' => false, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null],
        'name' => ['type' => 'string', 'length' => 255, 'null' => false, 'default' => null, 'collate' => 'utf8_general_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'newsletter_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'announce_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'student_id' => ['type' => 'uuid', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        'message_id' => ['type' => 'uuid', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        '_indexes' => [
            'newsletter_id' => ['type' => 'index', 'columns' => ['newsletter_id'], 'length' => []],
            'announce_id' => ['type' => 'index', 'columns' => ['announce_id'], 'length' => []],
            'student_id' => ['type' => 'index', 'columns' => ['student_id'], 'length' => []],
            'message_id' => ['type' => 'index', 'columns' => ['message_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'files_ibfk_1' => ['type' => 'foreign', 'columns' => ['newsletter_id'], 'references' => ['newsletters', 'id'], 'update' => 'noAction', 'delete' => 'restrict', 'length' => []],
            'files_ibfk_2' => ['type' => 'foreign', 'columns' => ['announce_id'], 'references' => ['announces', 'id'], 'update' => 'noAction', 'delete' => 'restrict', 'length' => []],
            'files_ibfk_3' => ['type' => 'foreign', 'columns' => ['student_id'], 'references' => ['users', 'id'], 'update' => 'noAction', 'delete' => 'restrict', 'length' => []],
            'files_ibfk_4' => ['type' => 'foreign', 'columns' => ['message_id'], 'references' => ['messages', 'id'], 'update' => 'noAction', 'delete' => 'restrict', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_general_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => '737b0b35-8864-4f4c-833e-daec50b99f02',
            'mime' => 'Lor',
            'name' => 'Lorem ipsum dolor sit amet',
            'newsletter_id' => 1,
            'announce_id' => 1,
            'student_id' => 'a1d4d7ab-ccb0-4ef6-aa1d-2870eea05310',
            'message_id' => '7c941e9c-6686-43fc-95fb-a902753172ae'
        ],
    ];
}
