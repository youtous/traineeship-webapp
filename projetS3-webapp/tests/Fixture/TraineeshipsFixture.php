<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * TraineeshipsFixture
 *
 */
class TraineeshipsFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'beginning' => ['type' => 'date', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        'ending' => ['type' => 'date', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        'created' => ['type' => 'datetime', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        'announce_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'student_id' => ['type' => 'uuid', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        'professor_id' => ['type' => 'uuid', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        '_indexes' => [
            'announce_id' => ['type' => 'index', 'columns' => ['announce_id'], 'length' => []],
            'student_id' => ['type' => 'index', 'columns' => ['student_id'], 'length' => []],
            'professor_id' => ['type' => 'index', 'columns' => ['professor_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'traineeships_ibfk_1' => ['type' => 'foreign', 'columns' => ['announce_id'], 'references' => ['announces', 'id'], 'update' => 'cascade', 'delete' => 'setNull', 'length' => []],
            'traineeships_ibfk_2' => ['type' => 'foreign', 'columns' => ['student_id'], 'references' => ['users', 'id'], 'update' => 'cascade', 'delete' => 'setNull', 'length' => []],
            'traineeships_ibfk_3' => ['type' => 'foreign', 'columns' => ['professor_id'], 'references' => ['users', 'id'], 'update' => 'cascade', 'delete' => 'setNull', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_general_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'beginning' => '2016-12-31',
            'ending' => '2016-12-31',
            'created' => '2016-12-31 02:10:21',
            'announce_id' => 1,
            'student_id' => '4d8fecbc-c0c3-43af-b840-a113a0dc0be9',
            'professor_id' => '5a8491a9-19d2-414c-b81c-dad612c4109a'
        ],
    ];
}
