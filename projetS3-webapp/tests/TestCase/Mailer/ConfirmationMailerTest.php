<?php
namespace App\Test\TestCase\Mailer;

use App\Mailer\ConfirmationMailer;
use Cake\TestSuite\TestCase;

/**
 * App\Mailer\ConfirmationMailer Test Case
 */
class ConfirmationMailerTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Mailer\ConfirmationMailer
     */
    public $Confirmation;

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
