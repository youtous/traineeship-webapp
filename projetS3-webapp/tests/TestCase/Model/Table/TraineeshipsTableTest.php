<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\TraineeshipsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\TraineeshipsTable Test Case
 */
class TraineeshipsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\TraineeshipsTable
     */
    public $Traineeships;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.traineeships',
        'app.announces',
        'app.users',
        'app.traineeships_student',
        'app.appreciations',
        'app.traineeships_professor',
        'app.messages_received',
        'app.recipients',
        'app.messages_sent',
        'app.authors',
        'app.validations',
        'app.files',
        'app.newsletters',
        'app.candidacies',
        'app.messages',
        'app.skills',
        'app.students',
        'app.users_skills',
        'app.announces_skills'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Traineeships') ? [] : ['className' => 'App\Model\Table\TraineeshipsTable'];
        $this->Traineeships = TableRegistry::get('Traineeships', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Traineeships);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
