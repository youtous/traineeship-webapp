<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\AnnouncesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\AnnouncesTable Test Case
 */
class AnnouncesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\AnnouncesTable
     */
    public $Announces;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.announces',
        'app.users',
        'app.traineeships_student',
        'app.traineeships_professor',
        'app.messages_received',
        'app.recipients',
        'app.messages_sent',
        'app.authors',
        'app.appreciations',
        'app.validations',
        'app.files',
        'app.newsletters',
        'app.candidacies',
        'app.messages',
        'app.skills',
        'app.students',
        'app.users_skills',
        'app.traineeships',
        'app.tags',
        'app.announces_tags'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Announces') ? [] : ['className' => 'App\Model\Table\AnnouncesTable'];
        $this->Announces = TableRegistry::get('Announces', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Announces);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
