<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\CandidaciesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\CandidaciesTable Test Case
 */
class CandidaciesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\CandidaciesTable
     */
    public $Candidacies;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.candidacies',
        'app.users',
        'app.announces',
        'app.traineeships_student',
        'app.traineeships_professor',
        'app.messages_received',
        'app.recipients',
        'app.messages_sent',
        'app.authors',
        'app.appreciations',
        'app.validations',
        'app.files',
        'app.newsletters',
        'app.messages',
        'app.skills',
        'app.students',
        'app.users_skills'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Candidacies') ? [] : ['className' => 'App\Model\Table\CandidaciesTable'];
        $this->Candidacies = TableRegistry::get('Candidacies', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Candidacies);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
