<?php
namespace App\Test\TestCase\Controller;

use App\Controller\NewslettersController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\NewslettersController Test Case
 */
class NewslettersControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.newsletters',
        'app.files',
        'app.candidacies',
        'app.users',
        'app.announces',
        'app.traineeships',
        'app.appreciations',
        'app.skills',
        'app.students',
        'app.traineeships_student',
        'app.traineeships_professor',
        'app.messages_received',
        'app.recipients',
        'app.messages_sent',
        'app.authors',
        'app.validations',
        'app.users_skills',
        'app.professors',
        'app.announces_skills',
        'app.messages'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
