<?php
namespace App\Test\TestCase\Controller;

use App\Controller\TraineeshipsController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\TraineeshipsController Test Case
 */
class TraineeshipsControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.traineeships',
        'app.announces',
        'app.users',
        'app.traineeships_student',
        'app.students',
        'app.traineeships_professor',
        'app.professors',
        'app.messages_received',
        'app.recipients',
        'app.messages_sent',
        'app.authors',
        'app.appreciations',
        'app.validations',
        'app.files',
        'app.newsletters',
        'app.candidacies',
        'app.messages',
        'app.skills',
        'app.users_skills',
        'app.announces_skills'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
