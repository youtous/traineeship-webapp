<?php
namespace App\Test\TestCase\View\Helper;

use App\View\Helper\FileHelper;
use Cake\TestSuite\TestCase;
use Cake\View\View;

/**
 * App\View\Helper\FileHelper Test Case
 */
class FileHelperTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\View\Helper\FileHelper
     */
    public $File;

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $view = new View();
        $this->File = new FileHelper($view);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->File);

        parent::tearDown();
    }

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
