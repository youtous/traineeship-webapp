# CakePHP Web APP Stage
Application de gestions des stages de l'IUT de Reims.

## Installation

1. Download [Composer](http://getcomposer.org/doc/00-intro.md) or update `composer self-update`.
2. Run `composer install `
3. Run `bin/cake migrations migrate && bin/cake migrations migrate -p Queue`
4. Run `bin/cake migrations seed --seed SkillsSeed`

Configure the crontab : 

 - For the workers :
 
`*/10 * * * * www-data /usr/local/bin/php /absolute/path/to/the/webapp/bin/cake.php queue runworker -q >> /absolute/path/to/the/webapp/logs/cron.log 2>&1`
 
 - For the daily Traineeship assignment:
 
  `0 0 * * * www-data /usr/local/bin/php /absolute/path/to/the/webapp/bin/cake.php queue add Traineeship >> /absolute/path/to/the/webapp/logs/cron.log 2>&1`

You should check your Cache configuration too and use Redis if possible.


## Configuration

Read and edit `config/app.php` and setup the 'Datasources'.
Read and edit `config/traineeships.php` and setup your configuration of the Traineeships.